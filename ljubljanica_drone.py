import numpy as np
from gps_utils import gps_get_image
from matplotlib import pyplot as plt
import math

data = [('most čez Ljubljanico', (46.023477440581495, 14.508078187366467)), ('meja 3d posnetka', (46.01661076310837, 14.50600150013978)), ('nekaj na vodi', (46.01535344058629, 14.503919432720679)), ('nekaj na vodi', (46.01004380600482, 14.491850631313588)), ('nekaj na vodi', (46.003217899031384, 14.472789157686886)), ('začetek res slabih satelitskih slik', (46.000026136916496, 14.468065402297867)), ('pritok ob pikniku', (45.993414638736596, 14.446650618173676)), ('konec res slabih satelitskih slik', (45.990311033811864, 14.439385681978374)), ('nekaj na vodi', (45.98857170932168, 14.435193466995823)), ('Podpeč', (45.97495960309461, 14.419287817069103)), ('kovinski most', (45.9717205115143, 14.398227710479237)), ('občina Vrhnika', (45.9654249682727, 14.371521082765465)), ('izliv Borovniščice', (45.965347362369116, 14.367718212964261)), ('izliv Male Ljubljanice', (45.96324577557197, 14.34866565474778)), ('neimenovan izliv', (45.96209487352426, 14.34363024896863)), ('nekaj na vodi', (45.96218428363782, 14.32971184535796)), ('izliv Podlipščice', (45.96218428363782, 14.32971184535796)), ('zlata postrv', (45.971134780360366, 14.314871653421047)), ('most čez Ljubljanico (Vrhnika)', (45.970873788315195, 14.305593335172514))]

def distance(origin, destination):

	# https://stackoverflow.com/a/38187562

    """
    Calculate the Haversine distance.

    Parameters
    ----------
    origin : tuple of float
        (lat, long)
    destination : tuple of float
        (lat, long)

    Returns
    -------
    distance_in_km : float

    Examples
    --------
    >>> origin = (48.1372, 11.5756)  # Munich
    >>> destination = (52.5186, 13.4083)  # Berlin
    >>> round(distance(origin, destination), 1)
    504.2
    """
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return d

def draw_point(lat, lon, lon_min, lon_max, lat_min, lat_max, w, h, text=''):
	x = (lon-lon_min)/(lon_max-lon_min)
	y = (lat-lat_min)/(lat_max-lat_min)

	res_x = x*w
	res_y = h-(y*h)
	plt.plot(res_x, res_y,'r.')
	plt.text(res_x, res_y-20, text)

	# print(res_x, res_y)

def main():

	delta_lat = 0.004
	delta_lon = 0.01

	names = [x for x,y in data]
	points = [y for (x,y) in data]
	points = np.array(points)
	# print(points)

	min_lat = np.min(points[:,0])
	max_lat = np.max(points[:,0])
	min_lon = np.min(points[:,1])
	max_lon = np.max(points[:,1])

	# print(min_lat, max_lat, min_lon, max_lon)

	zoom = 15

	full_distance = 0

	for i, pt in enumerate(points[:-1]):
		print(pt, points[i+1])
		full_distance+= distance(pt, points[i+1])

	# d = distance(points[0,:], points[1,:])
	print(f"distance: {full_distance:.2f}km")

	gps_image, gps_bbox = gps_get_image(min_lat, min_lon, max_lat-min_lat, max_lon-min_lon, zoom)

	h, w, _ = gps_image.shape
	# print(w,h)
	(lon_min, lat_min, lon_max, lat_max) = gps_bbox

	print(gps_bbox)
	print(lon_min, lon_max, lat_min, lat_max)

	plt.figure()
	plt.clf()
	plt.imshow(gps_image)
	

	for name, (lat, lon) in zip(names, points):
		draw_point(lat, lon, lon_min, lon_max, lat_min, lat_max, w, h, name)

	plt.show()

if __name__=='__main__':
	main()