import numpy as np
import cv2, os
from numpy.linalg import inv

def rt_to_projection_matrix(R,t):
	P = np.hstack((R,t.T))
	P = np.vstack((P,np.array((0,0,0,1))))

	return P

def eulerAnglesToRotationMatrix(theta):
	theta = [np.radians(x) for x in theta]
	
	R_x = np.array([[1, 0, 0],
					[0, np.cos(theta[0]), -np.sin(theta[0]) ],
					[0, np.sin(theta[0]), np.cos(theta[0]) ]
					])
		
		
					
	R_y = np.array([[np.cos(theta[1]), 0,np.sin(theta[1]) ],
					[0, 1,0],
					[-np.sin(theta[1]),0,np.cos(theta[1]) ]
					])
				
	R_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
					[np.sin(theta[2]), np.cos(theta[2]), 0],
					[0, 0,1]
					])
					
	R = np.dot(R_x, np.dot( R_y, R_z ))

	return R

def load_camera_calibration(filename):

	if os.path.isfile(filename):

		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		sz = fs.getNode("imageSize")
		M = fs.getNode("cameraMatrix").mat()
		D = fs.getNode("distCoeffs").mat()
		if sz.isSeq(): # za Rokov format, ki lahko vsebuje sezname
			width = sz.at(0).real()
			height = sz.at(1).real()
		else:
			sz = sz.mat()
			height = sz[0][0]
			width = sz[1][0]
		return (width, height, M,D)
	else:
		print("calibration file not found!")

def load_calibration_data(filename):
	if os.path.isfile(filename):
		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		M1 = fs.getNode("M1").mat()
		D1 = fs.getNode("D1").mat()
		M2 = fs.getNode("M2").mat()
		D2 = fs.getNode("D2").mat()
		R1 = fs.getNode("R1").mat()
		T1 = fs.getNode("T1").mat()
		R2 = fs.getNode("R2").mat()
		T2 = fs.getNode("T2").mat()
		return (M1, M2, D1, D2, R1, T1, R2, T2)
	else:
		print("calibration file not found!")


def project_image_final(im1, im2, P, M1, M2, t = 1):
	# P should be the R|t matrix between the cameras

	i,j = np.meshgrid(np.arange(im1.shape[1]),np.arange(im1.shape[0]))
	pixels = np.vstack((i.flatten(),j.flatten())).T

	Z = np.ones_like(i) # ones for Z, because of normalized coordinates
	p = np.vstack((i.flatten(),j.flatten(),Z.flatten()))
	p = np.dot(inv(M1),p).T # unproject pixels to normalized coordinates
	# print(P.shape)

	# z = 2.7
	p = p*t # include distance to calibration pattern
	
	# project points

	# extend with 4th dimension
	p = np.hstack((p,np.ones((p.shape[0],1))))
	# PM = np.dot(P2, inv(P1)) # precalculate transformation matrices
	P_ = np.dot(P,p.T) # one line precalculated
	P_/=P_[-1]
	P_ = P_[0:3]

	pts, _ = cv2.projectPoints(P_, np.eye(3), np.zeros(3), M2, distCoeffs=None)
	pts = pts[:,0,:]
	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<im2.shape[1]-1) & (pts[:,1]<im2.shape[0]-1) # create mask for valid pixels
	pixels = pixels[mask,:]
	pts = pts[mask,:].astype(np.float32)

	# set map
	mp = np.zeros((i.shape[0],j.shape[1],2),dtype=np.float32)-1
	
	i = pts[:,1]
	j = pts[:,0]
	x = pixels[:,1]
	y = pixels[:,0]

	mp[x,y,0]=j
	mp[x,y,1]=i

	return mp