
from multiprocessing import Process, Lock
from shared_memory import *
import numpy as np
import signal, sys

from read_utils import LidarParser


lock1 = Lock()

def lidar_reader(shr_name, N = 60000):

	parser = LidarParser()
	parser.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=1, elevation_filter=1)
	parser.start_all()

	existing_shm = shared_memory.SharedMemory(name=shr_name)
	np_array = np.ndarray((N, 4), dtype=np.float32, buffer=existing_shm.buf)

	def term(a,b):
		parser.stop_all()
		sys.exit()

	signal.signal(signal.SIGTERM, term)

	while True:
		try:
			data, addr = parser.socket_in.recvfrom(1000)
			parser.parse_packet(data)
			if parser.pc is not None and parser.data_ok:
			# if parser.pc is not None:
				lock1.acquire()
				# print("lock gotted")
				# pc = parser.pc.copy()
				# print("shm reader", pc.shape)
				
				# print("lock released")
				# print("pc shape", parser.pc.shape)
				np_array[:parser.pc.shape[0],:] = parser.pc.copy()
				np_array[parser.pc.shape[0]:, :] = 0
				lock1.release()

				parser.data_ok = False
				

		except (KeyboardInterrupt, SystemExit):
			print("Exiting...")
			break
		

def create_shm_lidar(N):
	a = np.zeros(shape=(N, 4), dtype=np.float32)

	shm = shared_memory.SharedMemory(create=True, size=a.nbytes)
	np_array = np.ndarray(a.shape, dtype=np.float32, buffer=shm.buf)
	np_array[:] = a[:]
	return shm, np_array