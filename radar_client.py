import socket, time, datetime, math
import numpy as np
import struct, zlib, binascii
# import pptk
from read_utils import RadarParser

def main():
	R = RadarParser()
	# R.IP = ""

	# R.set_parameters(chunk_size=500, reboot=None, shutdown=None, sending=1)

	# return
	# R.start_sending()
	R.start_all()

	while True:
		# try:
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		#print()
		# except:
			# print("timeout")
		if R.pc is not None:
			print(R.pc.shape)


if __name__=='__main__':
	main()
