import base64
from datetime import datetime, timedelta
import subprocess, os, glob, sys, socket
import threading
import numpy as np

# info about sensors, like IP, file directory and filename format
sensor_info = {
	'zed': {'IP': '192.168.90.70', 'IP_VPN': '10.8.0.9', 'user': 'jetson', 'dir': '/mnt/SSD/'},
	'stereo_front': {'IP': '192.168.90.74', 'IP_VPN': '10.8.0.5', 'user': 'pi', 'dir': '/mnt/SSD/', 'format': '%Y-%m-%d'},
	'stereo_side': {'IP': '192.168.90.71', 'IP_VPN': '10.8.0.4', 'user': 'pi', 'dir': '/mnt/SSD/', 'format': '%Y-%m-%d'},
	'thermal_camera': {'IP': '192.168.90.72', 'IP_VPN': '10.8.0.6', 'user': 'pi', 'dir': '/mnt/SSD/', 'format': '%Y-%m-%d'},
	'polarization_camera': {'IP': '192.168.90.73', 'IP_VPN': '10.8.0.7', 'user': 'pi', 'dir': '/mnt/SSD/', 'format': '%Y-%m-%d'},	
	'lidar': {'IP': '192.168.90.75', 'IP_VPN': '10.8.0.3', 'user': 'pi', 'dir': '/mnt/SSD/', 'format': '%Y-%m-%d'},
	'radar': {'IP': '192.168.90.76', 'IP_VPN': '10.8.0.8', 'user': 'pi', 'dir': '/mnt/SSD/', 'format': '%Y-%m-%d'},
	'GPSIMU': {'IP': '192.168.90.77', 'IP_VPN': '10.8.0.2', 'user': 'pi', 'dir': '/mnt/SSD/', 'format': '%Y-%m-%d'},
}

out_dir = '/media/jon/disk1/davimar/'
name_format = '%Y-%m-%d'

def worker(k, v, date):
	sz = np.random.randint(10,100)

	print(f"thread started: {k} with size {sz}")
	
	# print(sz)

	# command = f"head -c {sz}M </dev/urandom >test.bin"
	# subprocess.call(['ssh', '-t', v['user']+"@"+v['IP'], command], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

	d = date.strftime(name_format)
	# format_out = '%Y-%m-%d'
	d_out = date.strftime(name_format)
	print(d_out)

	zip_fn = '{}/{}.zip'.format(out_dir+d_out, k)
	print(zip_fn)

	if not os.path.exists(zip_fn):
		command_zip = "zip -0 -j {}{}.zip {}*{}*".format(v['dir'], d_out,v['dir'],d)
		command_scp = "scp {}@{}:{}{}.zip {}/{}.zip".format(v['user'], v['IP'], v['dir'], d_out, out_dir+d_out, k)
		command_rm = "rm {}{}.zip".format(v['dir'],d_out)
		print(command_zip)
		print(command_scp)
		print(command_rm)

		subprocess.call(['ssh', '-t', v['user']+"@"+v['IP'], command_zip])		
		os.system(command_scp)
		subprocess.call(['ssh', '-t', v['user']+"@"+v['IP'], command_rm])

	print(f"thread finished: {k}")

def download_drive_files_parallel(date=None):

	if date is None:
		date = datetime.now()

	for k,v in sensor_info.items():
		# print(k,v)

		t = threading.Thread(target=worker, args=[k, v, date])
		t.start()

		# worker(v)

		# command = "fallocate -l 2G test.bin"
		# command = "rm test.bin"
		# command = "ls"
		# command = "head -c 100M </dev/urandom >test.bin"
		# subprocess.call(['ssh', '-t', v['user']+"@"+v['IP'], command])

	pass

if __name__ == '__main__':

	format_out = '%Y-%m-%d'
	
	# date = datetime.now() - timedelta(days=1)
	date = datetime.now()
	# print(date)

	# date_time_str = '21/5/21'	
	# date = datetime.strptime(date_time_str, '%d/%m/%y')


	d_out = date.strftime(format_out)

	# print(d_out)
	# return

	try:
		os.mkdir(out_dir)
	except:
		print("fail")
		pass

	try:
		# print(out_dir+d)
		os.mkdir(out_dir+d_out)
	except:
		print("fail")
		pass

	
	download_drive_files_parallel(date)
	# testing()