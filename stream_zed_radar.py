"""
	Read a stream and display the left images using OpenCV also, get radar points and project them
"""
import sys
import pyzed.sl as sl
import cv2, threading
import subprocess
from read_utils import RadarParser
from projection_utils import *

gray_values = np.arange(256, dtype=np.uint8)[::-1]
# color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_HOT).reshape(256, 3)))
# color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_VIRIDIS).reshape(256, 3)))
color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_TURBO).reshape(256, 3)))

def load_radar_calibration(filename):
	if os.path.isfile(filename):
		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		R = fs.getNode("R").mat()
		t = fs.getNode("t").mat()
		
		return (R, t)
	else:
		print("calibration file not found!")

def get_colors(points, rnge):
	global gray_values, color_values	

	colors = []
	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)
	# rnge = np.max(dist)
	idx = (dist/rnge)*255
	for i in idx:
		# print(idx)
		colors.append(color_values[int(i)])

	return np.array(colors,dtype=np.uint8)

def project_radar_points(pc, im, R, t, M, D, skip=1, rn=1e6):
	# l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
	l2im = np.array([[-1, 0, 0], [0, 0, -1], [0, 1, 0]])

	# pc = pc[::skip, 0:3].T

	pc = pc[::skip, :].T
	speed = pc[3,:]
	rcs = pc[4,:]
	pc = pc[:3, :]

	# pc = pc.astype(float)*1e-3
	idx = np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)<rn
	# pc = pc[:,np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)<rn]

	pc = pc[:, idx]
	speed = speed[idx]
	rcs = rcs[idx]

	# pc = pc[:,pc[1,:]>0] # remove points behind the camera
	pc = l2im.dot(pc)


	dst = np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)

	if pc.shape[1]==0:
		return pc, None, np.array([])

	# print(pc.shape)

	colors = get_colors(pc, rn)

	pts, _ = cv2.projectPoints(pc, R, t, M, distCoeffs=D)
	pts = pts[:,0,:]
	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<im.shape[1]-1) & (pts[:,1]<im.shape[0]-1) # create mask for valid pixels

	pts = pts[mask,:]
	colors = colors[mask,:]
	dst = dst[mask]
	speed = speed[mask]
	rcs = rcs[mask]

	# colors = [(x[2]/255,x[1]/255,x[0]/255) for x in colors]
	colors = [(x[0]/255,x[1]/255,x[2]/255) for x in colors]

	return pts, colors, dst, speed, rcs

thread_stop = False
# im_thermal = None
# thermal = None
radar = None
pc = None

# R_radar, T_radar = load_radar_calibration("calibration_radar.yaml")
(width1, height1, M1, D1) = load_camera_calibration('calibration_zed.yaml')

yaw = 2.0
pitch = -2.0
roll = 0.9
offx = -0.21
offy = -0.625
offz = 0.44

# radar_calib = {'x': 0.07, 'y': -0.625, 'z': 0.1, 'yaw': 0.0, 'pitch': -2.0, 'roll': 0.9}
radar_calib = {'x': offx, 'y': offy, 'z': offz, 'yaw': yaw, 'pitch': pitch, 'roll': roll}
R_radar = eulerAnglesToRotationMatrix([radar_calib['pitch'], radar_calib['yaw'], radar_calib['roll']]) # TODO load this from yaml file
T_radar = np.array([radar_calib['x'], radar_calib['y'], radar_calib['z']])

# print(width1, height1)
# print(width2, height2)

im1 = np.ones((int(height1), int(width1)))
# im2 = np.zeros((int(height2), int(width2)))

# mp = project_image_final(im1, im2, P_zed_thermal, M1, M2, t = t2[0,-1])

# print(mp.shape)

# exit()

def worker():
	global thread_stop, pc, radar

	while True:
		# print("radar thread")
		data, addr = radar.socket_in.recvfrom(1000)
		# print(addr)
		radar.parse_packet(data)
		if radar.pc is not None:
			# print(len(R.pc))
			pc = radar.pc
			# pc = pc[:,0:3]

		# print(thread_stop)

		if thread_stop:
			break

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

def get_grid(): # for calibration, not needed really
	# create grid

	objectPoints= []
	# grid_size = 0.06 # 6cm
	grid_size = 0.3
	# grid_size = 0.05
	# grid_size = 300
	rows, cols = 3, 6


	z = 0
	off = grid_size/2

	# objectPoints.append([0,0.0,0])

	for i in range(rows):
		row = []
		for j in range(cols):
			
			# objectPoints.append( ((2*j + i%2)*grid_size, i*grid_size, z) )
			if j%2==0:
				# p = (j*off,i*grid_size,z)
				p = (-j*off,i*(-grid_size),z)
				row.append(p)
				# row.insert(1, p)
			else:
				if i<rows-1:
					# p = (j*(off),i*(off*2)+off,z)
					p = (-j*(off),-(i*(off*2)+off),z)
					row.append(p)

		# print(i,j,row)
		# print(row[::2], row[1::2])
		if i<rows-1:
			row = row[::2]+row[1::2]
		# else:
		# 	pass

		# print(i,j,row,'\n')

		objectPoints.extend(row)

	objectPoints= np.array(objectPoints).astype('float32')


	return objectPoints

def main():

	global thread_stop, pc, radar

	detect_corners = True
	# detect_corners = False

	# yaw = 2.0;pitch = -2.0;roll = 0.9;offx = -0.21;offy = -0.625;offz = 0.44 # initial	

	# offx= -0.06; offy= -0.83; offz= 0.41; roll = 0.80; pitch = -1.90; yaw = -0.30  # optimized by hand

	# x: 0.07, y: -0.73, z: 0.21, roll: 0.80, pitch: -9.10, yaw: 0.30 # optimized by hand, but for 9 degree pitch

	offx= -0.03; offy= -0.3; offz= 0.04; roll = 0.0; pitch = 0.00; yaw = 0.0

	dt = 0.025
	da = 0.6

	dx = dy = dz = dt
	dr = dp = dw = da

	azi_filter = 4
	elev_filter = 1

	display_radar_points = False
	display_radar_points = True


	radar = RadarParser()
	# radar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
	radar.start_all()
	# radar.start_sending()

	t = threading.Thread(target=worker)
	t.start()

	IP = '192.168.90.70'
	command_start = f"python3 stream_zed.py"
	command_kill = "sudo pkill -f stream_zed.py"

	subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_start], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

	init = sl.InitParameters()
	init.camera_resolution = sl.RESOLUTION.HD720
	# init.camera_resolution = sl.RESOLUTION.HD2K
	init.depth_mode = sl.DEPTH_MODE.PERFORMANCE

	ip = "192.168.90.70"
	init.set_from_stream(ip)

	cam = sl.Camera()
	status = cam.open(init)
	if status != sl.ERROR_CODE.SUCCESS:
		print("fail")
		print(repr(status))
		exit(1)

	runtime = sl.RuntimeParameters()
	mat = sl.Mat()
	point_cloud = sl.Mat()

	alpha = 0.5
	rnge = 30
	# rnge = 5

	grid = get_grid()
	# print(grid)

	key = -1
	print("  Quit : CTRL+C\n")
	while key & 0xFF != ord("q"):
		err = cam.grab(runtime)
		if (err == sl.ERROR_CODE.SUCCESS) :
			cam.retrieve_image(mat, sl.VIEW.LEFT)
			im = mat.get_data()
			im = im[...,0:3].astype(np.uint8)
			im = cv2.resize(im, (int(width1), int(height1)))
			# print(im.shape)			

			if pc is not None and display_radar_points:
				ldr = pc.copy()

				radar_calib = {'x': offx, 'y': offy, 'z': offz, 'yaw': yaw, 'pitch': pitch, 'roll': roll}
				R_radar = eulerAnglesToRotationMatrix([radar_calib['pitch'], radar_calib['yaw'], radar_calib['roll']]) # TODO load this from yaml file
				T_radar = np.array([radar_calib['x'], radar_calib['y'], radar_calib['z']])

				# print(ldr.shape)

				# print(ldr[:,-2])

				# print(ldr)

				# if not 'tvec' in locals():
				# 	pts, colors, _ = project_radar_points(ldr, im, R_radar, T_radar, M1, np.array([]), rn=rnge)
				# else:
				# 	print(tvec.T)
				# 	print(tvec[2])

				# 	pts, colors, _ = project_radar_points(ldr, im, R_radar, T_radar, M1, np.array([]), rn=tvec[2])
				pts, colors, _, speed, rcs = project_radar_points(ldr, im, R_radar, T_radar, M1, np.array([]), rn=rnge)

				# print(pts.shape)
				# print(len(colors))


				if pts.shape[0]!=0 and pts is not None:
					# print(pts.shape)
					for point, clr, sp, rc in zip(pts, colors, speed, rcs):
						# for point in pts:
						point = (int(point[0]), int(point[1]))
						# clr = (0,0,255)
						# print(clr)
						clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
						# print(clr, type(clr[0]), type(clr))
						# im = cv2.circle(im, point, 4, clr, -1)
						# print(sp, rc)
						# if sp!=0:
						im = cv2.circle(im, point, max(1,int(rc*2)), clr, -1)

			im = cv2.resize(im, None, fx=0.5, fy=0.5)

			cv2.imshow("ZED", im)

			# np.save('streamed_radar.npy', ldr)

			# break
			

			key = cv2.waitKey(1)
			# print(key)
			
			if key & 0xFF == ord("e"):
				offx-=dx
			elif key & 0xFF == ord("r"):
				offx+=dx
			elif key & 0xFF == ord("s"):
				offy-=dy
			elif key & 0xFF == ord("d"):
				offy+=dy
			elif key & 0xFF == ord("x"):
				offz-=dz
			elif key & 0xFF == ord("c"):
				offz+=dz

			elif key & 0xFF == ord("u"):
				roll-=dr
			elif key & 0xFF == ord("i"):
				roll+=dr
			elif key & 0xFF == ord("j"):
				pitch-=dp
			elif key & 0xFF == ord("k"):
				pitch+=dp
			elif key & 0xFF == ord("n"):
				yaw-=dw
			elif key & 0xFF == ord("m"):
				yaw+=dw
			elif key & 0xFF == ord("+"):
				# print("+")

				azi_filter = max(1,azi_filter-1)
				print(azi_filter)
				radar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("-"):
				# print("-")
				azi_filter = min(16,azi_filter+1)
				print(azi_filter)
				radar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("z"):
				display_radar_points = not display_radar_points
			elif key & 0xFF == ord("p"):
				# save both datas
				cam.retrieve_measure(point_cloud, sl.MEASURE.XYZRGBA,sl.MEM.CPU)
				point_cloud_zed = point_cloud.get_data()[...,0:3]

				im = mat.get_data()
				im = im[...,0:3].astype(np.uint8)

				np.save("zed_depth", point_cloud_zed)
				np.save("radar_scan", ldr)
				cv2.imwrite("zed_left.png", im)
				break
			elif key & 0xFF == 32: # reset calibration
				offx = 0; offy = 0; offz = 0.0; roll = pitch = yaw = 0.0

			# print(offx,offz)
			# print(f"x: {offx}, z: {offz}", end='\r')
			print(f"x: {offx:.2f}, y: {offy:.2f}, z: {offz:.2f}, roll: {roll:.2f}, pitch: {pitch:.2f}, yaw: {yaw:.2f}", end='\r')


		else :
			key = cv2.waitKey(1)


	thread_stop=True
	t.join()
	# R.stop_all()
	radar.stop_all()
	# radar.stop_sending()
	subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_kill], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	cam.close()

if __name__ == "__main__":
	main()