import socket, math
import numpy as np
import struct, zlib, binascii, time, datetime, signal, sys
from read_utils import LidarParser
from socket import *

R = LidarParser()

def sigint_handler(sig, frame):

	R.stop_all()
	sys.exit()

def main():

	signal.signal(signal.SIGINT, sigint_handler)


	# R.set_parameters(fov_start=0, fov_stop=360, sending=1, acquisition=1)
	# R.set_parameters(fov_start=0, fov_stop=360, sending=1, acquisition=1, write=1)
	# R.set_parameters(sending=1, acquisition=1)
	# R.set_parameters(fov_start=0, fov_stop=360, acquisition=1)
	theta = 40
	# R.set_parameters(azimuth_filter=1, elevation_filter=1, fov_start=360-theta, fov_stop=theta, acquisition=1)


	# R.set_parameters(azimuth_filter=4, elevation_filter=4, fov_start=30, fov_stop=40)
	# R.set_parameters(azimuth_filter=1, elevation_filter=1, fov_start=0, fov_stop=360)
	# return

	R.start_all()
	# R.start_acquisition()
	# R.stop_all()
	# R.start_sending()
	# R.stop_sending()
	# R.get_parameters(status=True)
	# R.get_parameters(diagnostics=True)

	
	# return

	while True:
		# try:
		data, addr = R.socket_in.recvfrom(63000)
		# print(addr)
		R.parse_packet(data)
		# print("waiting")
		# except:
		# 	print("timeout")
		if R.data_ok:
			print(R.pc.shape)
			R.data_ok = False
		# if R.pc is not None:
			# print(len(R.pc))


if __name__=='__main__':
	main()
