"""
	read the stream from left camera and implement manual parameter adjustment
"""
import sys
import pyzed.sl as sl
import cv2, threading
import subprocess
from read_utils import RGBParser, LidarParser
from projection_utils import *

gray_values = np.arange(256, dtype=np.uint8)[::-1]
# color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_HOT).reshape(256, 3)))
# color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_VIRIDIS).reshape(256, 3)))
color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_TURBO).reshape(256, 3)))

def load_lidar_calibration(filename):
	if os.path.isfile(filename):
		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		R = fs.getNode("R").mat()
		t = fs.getNode("t").mat()
		
		return (R, t)
	else:
		print("calibration file not found!")

def get_colors(points, rnge):
	global gray_values, color_values	

	colors = []
	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)
	rnge = np.max(dist)
	idx = (dist/rnge)*255
	for i in idx:
		colors.append(color_values[int(i)])

	return np.array(colors,dtype=np.uint8)

def project_lidar_points(pc, im, R, T, M, D, skip=1, rn=1e6):
	# l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
	l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])


	# l2im = l2im @ t
	# l2im = t @ l2im
	# l2im = t.T @ l2im

	pc = pc[::skip, 0:3].T
	# pc = pc.astype(float)*1e-3
	pc = pc[:,np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)<rn]
	pc = pc[:,pc[1,:]>0] # remove points behind the camera
	pc = l2im.dot(pc)


	dst = np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)

	if pc.shape[1]==0:
		return pc, None, np.array([])

	# print(pc.shape)

	colors = get_colors(pc, rn)

	pts, _ = cv2.projectPoints(pc, R, T, M, distCoeffs=D)
	pts = pts[:,0,:]
	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<im.shape[1]-1) & (pts[:,1]<im.shape[0]-1) # create mask for valid pixels

	pts = pts[mask,:]
	colors = colors[mask,:]
	dst = dst[mask]

	# colors = [(x[2]/255,x[1]/255,x[0]/255) for x in colors]
	colors = [(x[0]/255,x[1]/255,x[2]/255) for x in colors]

	return pts, colors, dst

thread_stop = False
# im_thermal = None
# thermal = None
lidar = None
pc = None

camera = 'left'
camera = 'right'

# R_lidar, T_lidar = load_lidar_calibration("calibration_lidar.yaml")
(width1, height1, M1, D1) = load_camera_calibration('calibration_zed.yaml')

# yaw = 2.0
# pitch = -2.0
# roll = 0.9
# offx = -0.21
# offy = -0.625
# offz = 0.44

yaw = 0.0
pitch = 0.0
roll = 0.0
offx = 0.0
offy = 0.0
offz = 0.0

# lidar_calib = {'x': 0.07, 'y': -0.625, 'z': 0.1, 'yaw': 0.0, 'pitch': -2.0, 'roll': 0.9}
lidar_calib = {'x': offx, 'y': offy, 'z': offz, 'yaw': yaw, 'pitch': pitch, 'roll': roll}
R_lidar = eulerAnglesToRotationMatrix([lidar_calib['pitch'], lidar_calib['yaw'], lidar_calib['roll']]) # TODO load this from yaml file
T_lidar = np.array([lidar_calib['x'], lidar_calib['y'], lidar_calib['z']])

# print(width1, height1)
# print(width2, height2)

im1 = np.ones((int(height1), int(width1)))
# im2 = np.zeros((int(height2), int(width2)))

# mp = project_image_final(im1, im2, P_zed_thermal, M1, M2, t = t2[0,-1])

# print(mp.shape)

# exit()

def worker():
	global thread_stop, pc, lidar

	while True:
		# print("lidar thread")
		data, addr = lidar.socket_in.recvfrom(1000)
		# print(len(data))
		lidar.parse_packet(data)
		if lidar.pc is not None and len(lidar.pc)!=0 and lidar.data_ok:
		# if lidar.pc is not None and lidar.data_ok:
			pc = np.array(lidar.pc).copy()
			lidar.data_ok = False
		# if lidar.pc is not None:
			# print(len(R.pc))
		
			# pc = pc[:,0:3]

			# print(pc.shape)

		# print(thread_stop)

		if thread_stop:
			break

def signal_handler(self, sig, frame):
	# self.stop_all()
	sys.exit()

def main():

	global thread_stop, pc, lidar

	(width1, height1, M1, D1) = load_camera_calibration('calibration_stereo_side_left.yaml')


	

	azi_filter = 4
	elev_filter = 4

	# dt = 0.025
	# da = 0.6

	dt = 0.05
	da = 1

	dx = dy = dz = dt
	dr = dp = dw = da

	# offx = -0.5; offy = 0.0; offz = 0.0; roll = 0.0; pitch = 0.0; yaw = 0.0
	# offx = -0.0; offy = 0.0; offz = 0.0; roll = 0.0; pitch = 0.0; yaw = 0.0
	if camera=='left':
		offx= 0.15; offy= -0.55; offz= -0.15; roll= 11.00; pitch= 0.00; yaw= 0.00
	else:
		offx= 0.15; offy= -0.55; offz= -0.15; roll= -11.00; pitch= 0.00; yaw= -7.00

	lidar = LidarParser()
	lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
	lidar.start_all()

	t = threading.Thread(target=worker)
	t.start()

	# while True:
	# 	# print("loop")
	# 	if pc is not None:
	# 		print(pc.shape)
	# 		# print(len(pc))

	# return

	parser = RGBParser('side')
	parser.signal_handler = signal_handler
	lidar.signal_handler = signal_handler

	key = -1
	# print("  Quit : CTRL+C\n")
	while key & 0xFF != ord("q"):
	# while True:

		# continue
		data, addr = parser.socket_in.recvfrom(1000)
		# print(addr)
		parser.parse_packet(data)
		if parser.image is not None and parser.image_ok:
			im = parser.image.copy()
			parser.image_ok = False

			if camera == 'left':
				im = im[:,0:im.shape[1]//2].copy()
			else:
				im = im[:,im.shape[1]//2:].copy()
			im = cv2.cvtColor(im,cv2.COLOR_GRAY2RGB)

			# im = cv2.undistort(im, M1, D1)

			im = cv2.resize(im, None, fx=4, fy=4)
			# print(im.shape)

			

			if pc is not None and len(pc)!=0:
				ldr = pc.copy()

				# t = np.array([[0, 0, -1], [0, 1, 0], [1, 0, 0]])
				# t = eulerAnglesToRotationMatrix((90,0,0))
				if camera=='left':
					tm = eulerAnglesToRotationMatrix((0,0,90))
				else:
					tm = eulerAnglesToRotationMatrix((0,0,-90))
				# print(rx)


				# ldr = (t.T @ ldr.T).T
				ldr = ldr @ tm


				# ldr[:, [1, 0]] = ldr[:, [0, 1]]
				# ldr[:, 0] = -ldr[:,0]

				lidar_calib = {'x': offx, 'y': offy, 'z': offz, 'yaw': yaw, 'pitch': pitch, 'roll': roll}
				R_lidar = eulerAnglesToRotationMatrix([lidar_calib['pitch'], lidar_calib['yaw'], lidar_calib['roll']]) # TODO load this from yaml file
				T_lidar = np.array([lidar_calib['x'], lidar_calib['y'], lidar_calib['z']])

				pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M1, np.array([]))

				# print(ldr.shape)

				if pts.shape[0]!=0 and pts is not None:
					# print(pts.shape)
					for point, clr in zip(pts, colors):
						# for point in pts:
						point = (int(point[0]), int(point[1]))
						# clr = (0,0,255)
						# print(clr)
						clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
						# print(clr, type(clr[0]), type(clr))
						im = cv2.circle(im, point,4, clr, -1)

			cv2.imshow("side", im)

			# np.save('streamed_lidar.npy', ldr)

			# break
		

		key = cv2.waitKey(1)

		if key & 0xFF == ord("e"):
			offx-=dx
		elif key & 0xFF == ord("r"):
			offx+=dx
		elif key & 0xFF == ord("s"):
			offy-=dy
		elif key & 0xFF == ord("d"):
			offy+=dy
		elif key & 0xFF == ord("x"):
			offz-=dz
		elif key & 0xFF == ord("c"):
			offz+=dz

		elif key & 0xFF == ord("u"):
			roll-=dr
		elif key & 0xFF == ord("i"):
			roll+=dr
		elif key & 0xFF == ord("j"):
			pitch-=dp
		elif key & 0xFF == ord("k"):
			pitch+=dp
		elif key & 0xFF == ord("n"):
			yaw-=dw
		elif key & 0xFF == ord("m"):
			yaw+=dw
		elif key & 0xFF == ord("+"):
			# print("+")

			azi_filter = max(1,azi_filter-1)
			print(azi_filter)
			lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
		elif key & 0xFF == ord("-"):
			# print("-")
			azi_filter = min(16,azi_filter+1)
			print(azi_filter)
			lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
		elif key & 0xFF == ord("z"):
			display_lidar_points = not display_lidar_points
		# elif key & 0xFF == ord("p"):
		# 	# save both datas
		# 	cam.retrieve_measure(point_cloud, sl.MEASURE.XYZRGBA,sl.MEM.CPU)
		# 	point_cloud_zed = point_cloud.get_data()[...,0:3]

		# 	im = mat.get_data()
		# 	im = im[...,0:3].astype(np.uint8)

		# 	np.save("zed_depth", point_cloud_zed)
		# 	np.save("lidar_scan", ldr)
		# 	cv2.imwrite("zed_left.png", im)
		# 	break

		print(f"x: {offx:.2f}, y: {offy:.2f}, z: {offz:.2f}, roll: {roll:.2f}, pitch: {pitch:.2f}, yaw: {yaw:.2f}", end='\r\n')


	thread_stop=True
	t.join()
	# R.stop_all()
	# lidar.stop_all()
	# subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_kill], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	# cam.close()

	return

	yaw = 2.0;pitch = -2.0;roll = 0.9;offx = -0.21;offy = -0.625;offz = 0.44 # initial	

	offx = -0.06; offy = -0.83; offz = 0.41; roll = 0.80; pitch = -1.90; yaw = -0.30  # optimized by hand

	# x: 0.07, y: -0.73, z: 0.21, roll: 0.80, pitch: -9.10, yaw: 0.30 # optimized by hand, but for 9 degree pitch

	offx= 0.07; offy= -0.73; offz= 0.21; roll = 0.80; pitch = -9.10; yaw = 0.30

	dt = 0.025
	da = 0.6

	dx = dy = dz = dt
	dr = dp = dw = da

	
	# azi_filter = 16
	# elev_filter = 16

	# azi_filter = 1
	# elev_filter = 1

	display_lidar_points = False
	display_lidar_points = True



	key = -1
	print("  Quit : CTRL+C\n")
	while key & 0xFF != ord("q"):
		err = cam.grab(runtime)
		if (err == sl.ERROR_CODE.SUCCESS) :
			cam.retrieve_image(mat, sl.VIEW.LEFT)
			im = mat.get_data()
			im = im[...,0:3].astype(np.uint8)
			im = cv2.resize(im, (int(width1), int(height1)))
			# print(im.shape)

			
			

			if pc is not None and display_lidar_points:
				ldr = pc.copy()

				lidar_calib = {'x': offx, 'y': offy, 'z': offz, 'yaw': yaw, 'pitch': pitch, 'roll': roll}
				R_lidar = eulerAnglesToRotationMatrix([lidar_calib['pitch'], lidar_calib['yaw'], lidar_calib['roll']]) # TODO load this from yaml file
				T_lidar = np.array([lidar_calib['x'], lidar_calib['y'], lidar_calib['z']])

				# print(ldr.shape)

				# print(ldr[:,-2])

				# print(ldr)

				# if not 'tvec' in locals():
				# 	pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M1, np.array([]), rn=rnge)
				# else:
				# 	print(tvec.T)
				# 	print(tvec[2])

				# 	pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M1, np.array([]), rn=tvec[2])
				pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M1, np.array([]))

				# print(pts.shape)
				# print(len(colors))


				if pts.shape[0]!=0 and pts is not None:
					# print(pts.shape)
					for point, clr in zip(pts, colors):
						# for point in pts:
						point = (int(point[0]), int(point[1]))
						# clr = (0,0,255)
						# print(clr)
						clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
						# print(clr, type(clr[0]), type(clr))
						im = cv2.circle(im, point,4, clr, -1)

			im = cv2.resize(im, None, fx=0.5, fy=0.5)

			cv2.imshow("ZED", im)

			# np.save('streamed_lidar.npy', ldr)

			# break
			

			key = cv2.waitKey(1)
			
			if key & 0xFF == ord("e"):
				offx-=dx
			elif key & 0xFF == ord("r"):
				offx+=dx
			elif key & 0xFF == ord("s"):
				offy-=dy
			elif key & 0xFF == ord("d"):
				offy+=dy
			elif key & 0xFF == ord("x"):
				offz-=dz
			elif key & 0xFF == ord("c"):
				offz+=dz

			elif key & 0xFF == ord("u"):
				roll-=dr
			elif key & 0xFF == ord("i"):
				roll+=dr
			elif key & 0xFF == ord("j"):
				pitch-=dp
			elif key & 0xFF == ord("k"):
				pitch+=dp
			elif key & 0xFF == ord("n"):
				yaw-=dw
			elif key & 0xFF == ord("m"):
				yaw+=dw
			elif key & 0xFF == ord("+"):
				# print("+")

				azi_filter = max(1,azi_filter-1)
				print(azi_filter)
				lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("-"):
				# print("-")
				azi_filter = min(16,azi_filter+1)
				print(azi_filter)
				lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("z"):
				display_lidar_points = not display_lidar_points
			elif key & 0xFF == ord("p"):
				# save both datas
				cam.retrieve_measure(point_cloud, sl.MEASURE.XYZRGBA,sl.MEM.CPU)
				point_cloud_zed = point_cloud.get_data()[...,0:3]

				im = mat.get_data()
				im = im[...,0:3].astype(np.uint8)

				np.save("zed_depth", point_cloud_zed)
				np.save("lidar_scan", ldr)
				cv2.imwrite("zed_left.png", im)
				break

			# print(offx,offz)
			# print(f"x: {offx}, z: {offz}", end='\r')
			print(f"x: {offx:.2f}, y: {offy:.2f}, z: {offz:.2f}, roll: {roll:.2f}, pitch: {pitch:.2f}, yaw: {yaw:.2f}", end='\r')


		else :
			key = cv2.waitKey(1)


	thread_stop=True
	t.join()
	# R.stop_all()
	lidar.stop_all()
	subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_kill], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	cam.close()

if __name__ == "__main__":
	main()