"""
	Read a stream and display the left images using OpenCV also, get lidar points and project them
"""
import sys
import pyzed.sl as sl
import cv2, threading
import subprocess
from read_utils import LidarParser
from projection_utils import *

gray_values = np.arange(256, dtype=np.uint8)[::-1]
# color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_HOT).reshape(256, 3)))
# color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_VIRIDIS).reshape(256, 3)))
color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_TURBO).reshape(256, 3)))

def load_lidar_calibration(filename):
	if os.path.isfile(filename):
		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		R = fs.getNode("R").mat()
		T = fs.getNode("T").mat()
		
		return (R, T)
	else:
		print("calibration file not found!")

def get_colors(points, rnge):
	global gray_values, color_values	

	colors = []
	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)
	rnge = np.max(dist)
	idx = (dist/rnge)*255
	for i in idx:
		colors.append(color_values[int(i)])

	return np.array(colors,dtype=np.uint8)

def project_lidar_points(pc, im, R, t, M, D, skip=1, rn=1e6):
	# l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
	l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])

	pc = pc[::skip, 0:3].T
	# pc = pc.astype(float)*1e-3
	pc = pc[:,np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)<rn]
	pc = pc[:,pc[1,:]>0] # remove points behind the camera
	pc = l2im.dot(pc)


	dst = np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)

	if pc.shape[1]==0:
		return pc, None, np.array([])

	# print(pc.shape)

	colors = get_colors(pc, rn)

	pts, _ = cv2.projectPoints(pc, R, t, M, distCoeffs=D)
	pts = pts[:,0,:]
	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<im.shape[1]-1) & (pts[:,1]<im.shape[0]-1) # create mask for valid pixels

	pts = pts[mask,:]
	colors = colors[mask,:]
	dst = dst[mask]

	# colors = [(x[2]/255,x[1]/255,x[0]/255) for x in colors]
	colors = [(x[0]/255,x[1]/255,x[2]/255) for x in colors]

	return pts, colors, dst

thread_stop = False
# im_thermal = None
# thermal = None
lidar = None
pc = None

R_lidar, T_lidar = load_lidar_calibration(f"helper_calibrations/calibration_lidar_zed.yaml")

def worker():
	global thread_stop, pc, lidar

	while True:
		data, addr = lidar.socket_in.recvfrom(1000)
		lidar.parse_packet(data)
		if lidar.pc is not None:
			pc = lidar.pc

		if thread_stop:
			break

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

def get_grid(): # for calibration, not needed really
	# create grid

	objectPoints= []
	# grid_size = 0.06 # 6cm
	grid_size = 0.3
	# grid_size = 0.05
	# grid_size = 300
	rows, cols = 3, 6


	z = 0
	off = grid_size/2

	# objectPoints.append([0,0.0,0])

	for i in range(rows):
		row = []
		for j in range(cols):
			
			# objectPoints.append( ((2*j + i%2)*grid_size, i*grid_size, z) )
			if j%2==0:
				# p = (j*off,i*grid_size,z)
				p = (-j*off,i*(-grid_size),z)
				row.append(p)
				# row.insert(1, p)
			else:
				if i<rows-1:
					# p = (j*(off),i*(off*2)+off,z)
					p = (-j*(off),-(i*(off*2)+off),z)
					row.append(p)

		# print(i,j,row)
		# print(row[::2], row[1::2])
		if i<rows-1:
			row = row[::2]+row[1::2]
		# else:
		# 	pass

		# print(i,j,row,'\n')

		objectPoints.extend(row)

	objectPoints= np.array(objectPoints).astype('float32')


	return objectPoints

def main():

	global thread_stop, pc, lidar

	(width1, height1, M1, D1) = load_camera_calibration('helper_calibrations/calibration_zed_helper.yaml')
	(width1, height1, M1, D1) = load_camera_calibration('helper_calibrations/calibration_zed_helper_720p.yaml')
	im1 = np.ones((int(height1), int(width1)))

	detect_corners = True
	# detect_corners = False

	window_name = 'zed'
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)


	# yaw = 2.0;pitch = -2.0;roll = 0.9;offx = -0.21;offy = -0.625;offz = 0.44 # initial	

	# offx= -0.06; offy= -0.83; offz= 0.41; roll = 0.80; pitch = -1.90; yaw = -0.30  # optimized by hand

	# x: 0.07, y: -0.73, z: 0.21, roll: 0.80, pitch: -9.10, yaw: 0.30 # optimized by hand, but for 9 degree pitch

	offx= 0.07; offy= -0.73; offz= 0.21; roll = 0.80; pitch = -9.10; yaw = 0.30

	dt = 0.025
	da = 0.6

	dx = dy = dz = dt
	dr = dp = dw = da

	azi_filter = 4
	elev_filter = 1

	display_lidar_points = False
	# display_lidar_points = True


	lidar = LidarParser()
	lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
	lidar.start_all()

	t = threading.Thread(target=worker)
	t.start()

	IP = '192.168.90.70'
	command_start = f"python3 /home/jetson/robocoln_2/ZED/stream_zed.py"
	command_kill = "sudo pkill -f stream_zed.py"

	subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_start], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

	init = sl.InitParameters()
	init.camera_resolution = sl.RESOLUTION.HD720
	# init.camera_resolution = sl.RESOLUTION.HD2K
	init.depth_mode = sl.DEPTH_MODE.PERFORMANCE

	# ip = "192.168.90.70"
	init.set_from_stream(IP)

	cam = sl.Camera()
	status = cam.open(init)
	if status != sl.ERROR_CODE.SUCCESS:
		print("fail")
		print(repr(status))
		exit(1)

	runtime = sl.RuntimeParameters()
	mat = sl.Mat()
	point_cloud = sl.Mat()

	alpha = 0.5
	rnge = 30

	grid = get_grid()
	# print(grid)

	key = -1
	print("  Quit : CTRL+C\n")
	while key & 0xFF != ord("q"):
		err = cam.grab(runtime)
		if (err == sl.ERROR_CODE.SUCCESS) :
			cam.retrieve_image(mat, sl.VIEW.LEFT)
			im = mat.get_data()
			im = im[...,0:3].astype(np.uint8)
			# print(im.shape)

			# im = cv2.resize(im, (int(width1), int(height1)))
			# print(im.shape)

			if detect_corners:
				corners = find_corners(im)
				# print(corners)

				if corners is not None:
					# im = cv2.drawChessboardCorners(im, (3,5), corners, True)
					# print(grid.shape, grid.dtype)
					# print(corners.shape, corners.dtype)

					retval, rvec, tvec, inliers = cv2.solvePnPRansac(grid, corners, M1, None, iterationsCount=1000, reprojectionError = 1.0)
					# print(rvec, tvec)

					Rz, _ = cv2.Rodrigues(rvec)

					

					# TODO display board corners
					pts, _ = cv2.projectPoints(grid, rvec, tvec, M1, distCoeffs=None)
					# pts, _ = cv2.projectPoints(border, rvec, tvec, M1, distCoeffs=None)
					pts = pts[:,0,:]			
					# plt.plot(pts[:,0],pts[:,1],'r.', alpha=1)
					c = (255,0,255)
					for i,pt in enumerate(pts):
						im = cv2.circle(im, (int(pt[0]),int(pt[1])), radius=5, color=c, thickness=-1)
						im = cv2.putText(im, str(i), (int(pt[0]),int(pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, c, 1 ,cv2.LINE_AA)

					plane_w = -1.48
					plane_h = -1.05
					points = np.array([[0,0,0],[0,plane_h,0],[plane_w,plane_h,0],[plane_w,0,0]])
					points[:,0]+=0.365
					points[:,1]+=0.215

					corner_pts, _ = cv2.projectPoints(points, rvec, tvec, M1, distCoeffs=None)
					corner_pts = corner_pts[:,0,:]
					c = (0,0,255)

					for i,pt in enumerate(corner_pts):
						im = cv2.circle(im, (int(pt[0]),int(pt[1])), radius=5, color=c, thickness=-1)
						p = corner_pts[i-1]
						im = cv2.line(im,(int(p[0]),int(p[1])),(int(pt[0]),int(pt[1])),(255,0,0),5)
			

			if pc is not None and display_lidar_points:
				ldr = pc.copy()
				# print(ldr.shape)

				lidar_calib = {'x': offx, 'y': offy, 'z': offz, 'yaw': yaw, 'pitch': pitch, 'roll': roll}
				R_lidar = eulerAnglesToRotationMatrix([lidar_calib['pitch'], lidar_calib['yaw'], lidar_calib['roll']]) # TODO load this from yaml file
				T_lidar = np.array([lidar_calib['x'], lidar_calib['y'], lidar_calib['z']])

				# print(ldr.shape)

				# print(ldr[:,-2])

				# print(ldr)

				# if not 'tvec' in locals():
				# 	pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M1, np.array([]), rn=rnge)
				# else:
				# 	print(tvec.T)
				# 	print(tvec[2])

				# 	pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M1, np.array([]), rn=tvec[2])
				pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M1, np.array([]))

				# print(pts.shape)
				# print(len(colors))


				if pts.shape[0]!=0 and pts is not None:
					# print(pts.shape)
					for point, clr in zip(pts, colors):
						# for point in pts:
						point = (int(point[0]), int(point[1]))
						# clr = (0,0,255)
						# print(clr)
						clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
						# print(clr, type(clr[0]), type(clr))
						im = cv2.circle(im, point,4, clr, -1)

			im = cv2.resize(im, None, fx=0.5, fy=0.5)

			cv2.imshow(window_name, im)

			# np.save('streamed_lidar.npy', ldr)

			# break
			

			key = cv2.waitKey(1)
			
			if key & 0xFF == ord("e"):
				offx-=dx
			elif key & 0xFF == ord("r"):
				offx+=dx
			elif key & 0xFF == ord("s"):
				offy-=dy
			elif key & 0xFF == ord("d"):
				offy+=dy
			elif key & 0xFF == ord("x"):
				offz-=dz
			elif key & 0xFF == ord("c"):
				offz+=dz

			elif key & 0xFF == ord("u"):
				roll-=dr
			elif key & 0xFF == ord("i"):
				roll+=dr
			elif key & 0xFF == ord("j"):
				pitch-=dp
			elif key & 0xFF == ord("k"):
				pitch+=dp
			elif key & 0xFF == ord("n"):
				yaw-=dw
			elif key & 0xFF == ord("m"):
				yaw+=dw
			elif key & 0xFF == ord("+"):
				# print("+")

				azi_filter = max(1,azi_filter-1)
				print(azi_filter)
				lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("-"):
				# print("-")
				azi_filter = min(16,azi_filter+1)
				print(azi_filter)
				lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("z"):
				display_lidar_points = not display_lidar_points
			# elif key & 0xFF == ord("p"):
			# 	# save both datas
			# 	cam.retrieve_measure(point_cloud, sl.MEASURE.XYZRGBA,sl.MEM.CPU)
			# 	point_cloud_zed = point_cloud.get_data()[...,0:3]

			# 	im = mat.get_data()
			# 	im = im[...,0:3].astype(np.uint8)

			# 	np.save("zed_depth", point_cloud_zed)
			# 	np.save("lidar_scan", ldr)
			# 	cv2.imwrite("zed_left.png", im)
			# 	break

			# print(offx,offz)
			# print(f"x: {offx}, z: {offz}", end='\r')
			print(f"x: {offx:.2f}, y: {offy:.2f}, z: {offz:.2f}, roll: {roll:.2f}, pitch: {pitch:.2f}, yaw: {yaw:.2f}", end='\r')


		else :
			key = cv2.waitKey(1)


	thread_stop=True
	t.join()
	# R.stop_all()
	lidar.stop_all()
	subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_kill], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	cam.close()

if __name__ == "__main__":
	main()