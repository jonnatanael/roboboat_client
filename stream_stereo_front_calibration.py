from socket import *
# import time, datetime, math
import numpy as np
# import datetime
import time, threading
import cv2, os
import datetime


from read_utils import RGBParser, LidarParser
# from live_client import load_camera_calibration

from calibration_utils import *

# thread_stop = False
# lidar = None
# pc = None

side = 'left'
# side = 'right'

calib_fn = f'helper_calibrations/calibration_stereo_front_{side}_helper.yaml'

(width, height, M, D) = load_camera_calibration(calib_fn)
R_lidar, T_lidar, _ = load_lidar_calibration(f"helper_calibrations/calibration_lidar_stereo_front_{side}.yaml")

patternsize = (3,5)

n_iter = 1000
repr_err = 0.001
err_thr = 0.5
err_thr = 1.0

def main():

	params = cv2.SimpleBlobDetector_Params()

	# Change thresholds
	params.minThreshold = 10
	params.maxThreshold = 1000

	# Filter by Area.
	params.filterByArea = True
	params.minArea = 50

	# Filter by Circularity
	params.filterByCircularity = False
	params.minCircularity = 0.1

	# Filter by Convexity
	params.filterByConvexity = False
	params.minConvexity = 0.87

	# Filter by Inertia
	params.filterByInertia = False
	params.minInertiaRatio = 0.01

	# Create a detector with the parameters
	detector = cv2.SimpleBlobDetector_create(params)

	window_name = f"stereo front {side}"
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

	R = RGBParser('front')
	# R.start_sending()
	# R.start_all()

	grid = get_grid()
	cnt = 0
	thr = 11
	thr_step = 1
	k = 3

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		# print(data)

		if R.image is not None and R.image_ok:
			# print(R.image.shape)
			ts = datetime.datetime.now()


			R.image = cv2.undistort(R.image, M, D)
			im = R.image.copy()
			
			im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			R.image_ok = False

			if side=='left':
				im = im[:, :im.shape[1]//2]				
			else:				
				im = im[:, im.shape[1]//2:]

			print(im.shape)

			im2 = im.copy()
			
			# idx = im2<thr
			# im2 = np.where(im2>thr, 255, 0).astype(np.uint8)
			# im2[idx]=255
			# im2[~idx]=0

			# im2 = cv2.adaptiveThreshold(im2[...,0], 255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV,15,2)

			# kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(thr,thr))
			# im2 = cv2.morphologyEx(im2, cv2.MORPH_OPEN, kernel)

			keypoints = detector.detect(~im2)
			# print(keypoints)

			# im3 = cv2.applyColorMap(im3, cv2.COLORMAP_PLASMA)

			# print(keypoints)
			# im2 = cv2.cvtColor(im2, cv2.COLOR_GRAY2RGB)
			# # Draw detected blobs as red circles.
			# # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
			# im3 = cv2.drawKeypoints(im3, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
			m = 5
			for p in keypoints:
				# print(p.pt, p.size)
				x = int(p.pt[0])
				y = int(p.pt[1])
				s = int(p.size//2)
				cv2.circle(im, (x, y), radius=s, color=(0, 255, 0), thickness=1)


			cv2.imshow(window_name, im)		
			cv2.imshow("raw", im2)		

			key = cv2.waitKey(1) & 0xFF

			if key == ord('q'):
				# thread_stop=True
				# R.stop_all()
				# R.stop_sending()
				# t.join()
				break
			elif key == ord("+"):

				print("+")

				thr = min(255,thr+thr_step)
			# 	print(azi_filter)
			# 	lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key == ord("-"):
				print('-')
				thr = max(0,thr-thr_step)

	# R.stop_all()

if __name__=='__main__':
	main()