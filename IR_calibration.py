import numpy as np
import socket
import cv2, struct, io, os, sys
from PIL import Image
import subprocess
from datetime import datetime
from termcolor import colored


from calibration_utils import *
from utils import get_local_IP
# from read_utils import LidarParser
from camera_lidar_calibration_helper import get_edges

def draw_border(im, clr=(0,255,0), thickness=5):

	# draw stability indicator
	frame_width = 5
	frame_margin = thickness//2
	# frame_clr = (0,255,0) if stable else (0,0,255)
	cv2.rectangle(im, (frame_margin, frame_margin), (im.shape[1]-frame_margin, im.shape[0]-frame_margin), clr, thickness)

	return im

# read existing calibration, if available
# display error
# save data on button press

def main():

	patternsize = (3,5)

	if len(sys.argv)>1:
		camera_side = sys.argv[1]
	else:
		camera_side = 'left'

	if camera_side=='left':
		camera = 0
		shutter = 6000000
		# shutter = 15000
		framerate = 10
		sensor_name = 'stereo_front_left'
		position_threshold = 0.05



	elif camera_side=='right':
		camera = 1
		shutter = 5000
		shutter = 3000
		# shutter = 2000
		framerate = 10
		sensor_name = 'stereo_front_right'
		# position_threshold = 0.2
		position_threshold = 20
		# position_threshold = 0.15
		# position_threshold = 0.2
		# position_threshold = 0.05

	try:
		(width, height, M, D) = load_camera_calibration(f'/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_{sensor_name}_helper.yaml')	
	except:
		f = 100.0
		width, height, _ = im.shape
		M = np.array([[f, 0, width/2],[0, f, height/2],[0,0,1]])
		D = np.array([])


	start_ts = datetime.now()
	start_timestamp = int(round(start_ts.timestamp()))
	out_dir = f'calibration_data/{sensor_name}_calibration_set_{start_timestamp}/'

	grid = get_grid()
	grid[:,0]*=-1
	grid[:,1]*=-1

	local_IP = get_local_IP()
	remote_IP = '192.168.90.74'

	# stolen from: https://stackoverflow.com/questions/45924220/netcat-h-264-video-from-raspivid-into-opencv
	server_socket = socket.socket()
	server_socket.bind(('0.0.0.0', 1200))
	server_socket.listen(0)	

	# start raspivid on RPi
	command = f"python camera_stream.py {camera} {local_IP} {shutter} {framerate}"
	subprocess.call(['ssh', '-t', '-f', "pi@"+remote_IP, "nohup", command], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	connection = server_socket.accept()[0].makefile('rb')

	contrast = 1.0
	brightness = 0

	contrast_dx = 0.25
	brightness_dx = 10

	thickness = 4	

	cv2.namedWindow(camera_side, cv2.WINDOW_NORMAL)

	corners_data = []

	# stability
	pos_prev = None
	c_prev = None
	stable = False
	stability_threshold = 0.5 # difference between poses when camera and target are stationary
	stability_timeout = 10 # number of frames to determine stability
	stability_count = 0

	err_thr = 0.5


	positions = []
	position_threshold = 5

	# calib_exists = False

	try:
		while True:
			
			x = connection.read(struct.calcsize('<L'))
			image_len = struct.unpack('<L', x)[0]
			if not image_len:
				break
			
			image_stream = io.BytesIO()
			image_stream.write(connection.read(image_len))
			image_stream.seek(0)
			image = Image.open(image_stream)

			data = np.fromstring(image_stream.getvalue(), dtype=np.uint8)
			im = cv2.imdecode(data, 1)

			ts = datetime.now()
			im_raw = im.copy()

			im = adjust_contrast_brightness(im, contrast = contrast, brightness=brightness)

			# im = cv2.undistort(im, M, D)

			edges = None
			corners = find_corners(im)
			if corners is None:
				corners = find_corners(255-im)

			if corners is not None:
				# cv2.drawChessboardCorners(im, patternsize, corners, True)

				# print(M)

				_, rvec, tvec = cv2.solvePnP(grid, corners, M, D, flags=cv2.SOLVEPNP_IPPE) # coplanar points

				# check stability
				if c_prev is not None:
					diff = np.mean(np.abs(corners-c_prev))
					# print(diff)
					if diff>stability_threshold:
						stable=False
						stability_count = 0
					else:
						stability_count+=1
						if stability_count>stability_timeout:
							stable=True
				c_prev = corners.copy()

				# check if new position
				new_position = True
				for c in positions:
					diff = np.mean(np.abs(c-corners))
					if diff < position_threshold:
						new_position = False

				e = -1
				# calculate position error if previous calibration exists
				# if calib_exists:
					
				pts, _ = cv2.projectPoints(grid, rvec, tvec, M, distCoeffs=D)
				for p in pts:
					x = int(p[0,0]); y = int(p[0,1])
					# cv2.circle(im, (x,y), radius=2, color=(0,0,255), thickness=-1)

				e = np.mean(np.abs(pts-corners))

				clr = (0,255,0) if e<err_thr else (0,0,255)
				cv2.putText(im, f"{e:.2f}", (50,70), cv2.FONT_HERSHEY_SIMPLEX, 2, clr, 3)

				edges = get_edges(im, M, corners, c=11)
				edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2RGB)
				edges = edges.astype(np.float32)
			
				# add new data if position is new
				if stable:
					# add = True
					# for c in corners_data:
					# 	# print(pts)
					# 	# print(c)
					# 	# diff = np.mean(np.abs(pts-c))
					# 	diff = np.mean(np.abs(corners-c))
					# 	# diff = np.mean(np.abs(pos-p)) # calculate difference between saved positions and current position
					# 	# print(diff)

					# 	if diff < position_threshold:
					# 		add = False
					# 		break

					if edges is not None:
						if not new_position:
							edges[...,0]=0
							edges[...,2]=0
						else:
							edges[...,0]=0
							edges[...,1]=0

						im = im.astype(np.float32)/255
						im = im*(1-edges)+edges
				

			draw_border(im, clr=(0,255,0) if stable else (0,0,255), thickness=5)

			cv2.imshow(camera_side, im)

			key = cv2.waitKey(1) & 0xFF

			if key == ord('q'):
				break
			elif key==ord('e'):
				contrast=max(0, contrast-contrast_dx)
			elif key==ord('r'):
				contrast=contrast+contrast_dx
			elif key==ord('u'):
				brightness=max(-255, brightness-brightness_dx)
			elif key==ord('i'):
				brightness=min(255, brightness+brightness_dx)
			elif key == ord('s'):
				if corners is not None and stable:
					if new_position:

						# create output dir							
						try:
							os.mkdir(out_dir)
						except:
							pass

						positions.append(corners)

						timestamp = int(round(ts.timestamp()))
						cv2.imwrite(f"{out_dir}{timestamp}.jpg", im_raw)
						np.save(f"{out_dir}{timestamp}.npy", corners)
						print(colored("saving data", 'green'))

					else:
						print(colored("this position was aleady saved",'red'))
				else:
					print(colored("image not stable or target not detected",'red'))
			elif key == ord('p'):
				# save raw image
				cv2.imwrite(f"IR2_target.png", im_raw)



	finally:
		cv2.destroyAllWindows() #cleanup windows 
		connection.close()
		server_socket.close()
		# R.stop_all()

if __name__=='__main__':
	main()
