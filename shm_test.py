import numpy as np
from multiprocessing import Process, Lock
from shared_memory import *
from multiprocessing import cpu_count, current_process
import time, cv2

from read_utils import PolarizationParser, ThermalParser, ZEDParser, LidarParser

# one dimension of the 2d array which is shared
dim = 1000

lock1 = Lock()
lock2 = Lock()
lock3 = Lock()

def add_one(shr_name):

	existing_shm = shared_memory.SharedMemory(name=shr_name)
	np_array = np.ndarray((dim, dim,), dtype=np.int32, buffer=existing_shm.buf)
	lock.acquire()
	np_array[:] = np_array[0] + 1
	lock.release()
	# time.sleep(10) # pause, to see the memory usage in top
	print('added one')
	existing_shm.close()

def create_shared_block():

	# a = np.ones(shape=(dim, dim), dtype=np.int64)  # Start with an existing NumPy array
	a = np.ones(shape=(dim, dim), dtype=np.int32)  # Start with an existing NumPy array

	shm = shared_memory.SharedMemory(create=True, size=a.nbytes)
	# # Now create a NumPy array backed by shared memory
	np_array = np.ndarray(a.shape, dtype=np.int32, buffer=shm.buf)
	np_array[:] = a[:]  # Copy the original data into shared memory
	return shm, np_array

def polar_reader(shr_name, width = 1216, height = 1024):

	parser = PolarizationParser()
	parser.start_all()

	existing_shm = shared_memory.SharedMemory(name=shr_name)
	print(shr_name)
	np_array = np.ndarray((height, width), dtype=np.uint8, buffer=existing_shm.buf)

	while True:
		data, addr = parser.socket_in.recvfrom(1000)
		parser.parse_packet(data)
		if parser.image is not None and parser.image_ok:
			lock1.acquire()
			np_array[:]=parser.image[:]
			lock1.release()

def thermal_reader(shr_name, height=288, width=384):
	parser = ThermalParser()
	parser.start_all()
	print(shr_name)

	existing_shm = shared_memory.SharedMemory(name=shr_name)
	np_array = np.ndarray((height, width), dtype=np.uint8, buffer=existing_shm.buf)

	while True:
		data, addr = parser.socket_in.recvfrom(1000)
		parser.parse_packet(data)
		if parser.image is not None and parser.image_ok:
			lock2.acquire()
			np_array[:]=parser.image[:]
			lock2.release()

def zed_reader(shr_name, height=621, width=1104):
	parser = ZEDParser()
	parser.start_all()
	print(shr_name)

	existing_shm = shared_memory.SharedMemory(name=shr_name)
	np_array = np.ndarray((height, width, 3), dtype=np.uint8, buffer=existing_shm.buf)

	while True:
		data, addr = parser.socket_in.recvfrom(1000)
		parser.parse_packet(data)
		if parser.image is not None and parser.image_ok:
			lock3.acquire()
			parser.image = cv2.cvtColor(parser.image, cv2.COLOR_BGR2RGB)
			np_array[:]=parser.image[:]
			lock3.release()
			parser.image_ok = False

def create_shm(width, height, depth=1):
	if depth==1:
		a = np.zeros(shape=(height, width), dtype=np.uint8)
	else:
		a = np.zeros(shape=(height, width, depth), dtype=np.uint8)
	shm = shared_memory.SharedMemory(create=True, size=a.nbytes)
	np_array = np.ndarray(a.shape, dtype=np.uint8, buffer=shm.buf)
	np_array[:] = a[:]
	return shm, np_array

def main():

	polar_width = 1216
	polar_height = 1024

	thermal_width = 384
	thermal_height = 288

	# zed_width = 2208
	# zed_height = 1242
	zed_width = 1104
	zed_height = 621

	shr_polar, np_array_polar = create_shm(polar_width, polar_height)
	shr_thermal, np_array_thermal = create_shm(thermal_width, thermal_height)
	shr_zed, np_array_zed = create_shm(zed_width, zed_height, 3)

	# print("shr_polar", shr_polar.name)
	# print("shr_thermal", shr_thermal.name)

	p1 = Process(target=polar_reader, args=(shr_polar.name,))
	p1.start()

	p2 = Process(target=thermal_reader, args=(shr_thermal.name,))
	p2.start()

	p3 = Process(target=zed_reader, args=(shr_zed.name,))
	p3.start()

	key = -1
	while key & 0xFF != ord("q"):
		cv2.imshow("flir", np_array_polar)
		cv2.imshow("thermal", np_array_thermal)
		cv2.imshow("zed", np_array_zed)
		key = cv2.waitKey(1)

	p1.terminate()
	p2.terminate()
	p3.terminate()

	p1.join()
	p2.join()
	p3.join()

	shr_polar.close()
	shr_polar.unlink()
	shr_thermal.close()
	shr_thermal.unlink()
	shr_zed.close()
	shr_zed.unlink()

def test():

	print("creating shared block")
	shr, np_array = create_shared_block()

	print(np_array)

	n = 4
	n = cpu_count()

	processes = []
	for i in range(n):
		_process = Process(target=add_one, args=(shr.name,))
		processes.append(_process)
		_process.start()

	for _process in processes:
		_process.join()

	print("Final array")
	print(np_array[:10])
	print(np_array[10:])

	shr.close()
	shr.unlink()

def lidar_reader(shr_name, N = 60000):

	parser = LidarParser()
	parser.start_all()

	existing_shm = shared_memory.SharedMemory(name=shr_name)
	np_array = np.ndarray((N, 4), dtype=np.float32, buffer=existing_shm.buf)

	while True:
		data, addr = parser.socket_in.recvfrom(1000)
		parser.parse_packet(data)
		if parser.pc is not None and parser.data_ok:
			lock1.acquire()
			pc = parser.pc
			print(pc.shape, pc.dtype)
			# print(np_array.shape, np_array.dtype)
			# np_array[:]=parser.pc[:]
			np_array[:pc.shape[0],:]=pc
			np_array[pc.shape[0]:, :] = 0
			lock1.release()

def create_shm_lidar(N):
	a = np.zeros(shape=(N, 4), dtype=np.float32)

	shm = shared_memory.SharedMemory(create=True, size=a.nbytes)
	np_array = np.ndarray(a.shape, dtype=np.float32, buffer=shm.buf)
	np_array[:] = a[:]
	return shm, np_array

def lidar_test():

	lidar_width = 4
	lidar_height = 60000

	shr_lidar, np_array_lidar = create_shm_lidar(lidar_height)

	p1 = Process(target=lidar_reader, args=(shr_lidar.name,))
	p1.start()

	while True:
		pass
		# print(np_array_lidar.shape, np_array_lidar.dtype)
		# print(np_array_lidar[:10, :])

if __name__ == "__main__":
	# test()
	# main()
	lidar_test()