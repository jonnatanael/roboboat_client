from socket import *
# import time, datetime, math
import numpy as np
# import datetime
import time, threading
import cv2, os
import datetime


from read_utils import RGBParser, LidarParser
# from live_client import load_camera_calibration

from calibration_utils import *

thread_stop = False
lidar = None
pc = None

side = 'left'
side = 'right'

calib_fn = f'helper_calibrations/calibration_stereo_front_{side}_helper.yaml'

(width, height, M, D) = load_camera_calibration(calib_fn)
R_lidar, T_lidar, _ = load_lidar_calibration(f"helper_calibrations/calibration_lidar_stereo_front_{side}.yaml")

patternsize = (3,5)

n_iter = 1000
repr_err = 0.001
err_thr = 0.5
err_thr = 1.0

def worker():
	global thread_stop, pc, lidar

	while True:
		# print("lidar thread")
		data, addr = lidar.socket_in.recvfrom(63000)
		# print(addr)
		lidar.parse_packet(data)
		if lidar.pc is not None:
			# print(len(R.pc))
			pc = lidar.pc
			# pc = pc[:,0:3]
			# print(pc)

		# print(thread_stop)

		if thread_stop:
			break

gray_values = np.arange(256, dtype=np.uint8)[::-1]
color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_TURBO).reshape(256, 3)))

def get_colors(points, rnge):
	global gray_values, color_values	

	colors = []
	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)
	# rnge = np.max(dist)
	idx = (dist/rnge)*255
	for i in idx:
		colors.append(color_values[int(i)])

	return np.array(colors,dtype=np.uint8)

def project_lidar_points(pc, im, R, t, M, D, skip=1, rn=1e6, l2im=np.array([[0, 1, 0], [0, 0, -1], [-1, 0, 0]])):
	pc = pc[::skip, 0:3].T

	pc = l2im.dot(pc)

	pc = pc[:,pc[2,:]>0] # remove points behind the camera

	dst = np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)

	if pc.shape[1]==0:
		return pc, None, np.array([])

	colors = get_colors(pc, rn)

	pts, _ = cv2.projectPoints(pc, R, t, M, distCoeffs=D)
	pts = pts[:,0,:]
	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<im.shape[1]-1) & (pts[:,1]<im.shape[0]-1) # create mask for valid pixels

	pts = pts[mask,:]
	colors = colors[mask,:]
	dst = dst[mask]

	# colors = [(x[2]/255,x[1]/255,x[0]/255) for x in colors]
	colors = [(x[0]/255,x[1]/255,x[2]/255) for x in colors]

	return pts, colors, dst

def main():
	global thread_stop, pc, lidar

	window_name = f"stereo side {side}"
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

	R = RGBParser('side')
	R.start_sending()

	if side=='left':
		l2im = np.array([[0, 1, 0], [0, 0, -1], [-1, 0, 0]]) # from lidar to left side camera
	else:
		l2im = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]]) # from lidar to right side camera

	lidar = LidarParser()
	# lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
	lidar.start_all()

	t = threading.Thread(target=worker)
	t.start()

	ts_prev = None
	detect_target = True
	detect_target = False

	grid = get_grid()
	cnt = 0

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(60000)
		R.parse_packet(data)
		# print(data)

		if R.image is not None and R.image_ok:
			# print(R.image.shape)
			ts = datetime.datetime.now()


			# R.image = cv2.undistort(R.image, M, D)
			im = R.image.copy()
			print(im.shape)
			im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			R.image_ok = False

			if side=='left':
				im = im[:, :im.shape[1]//2]				
			else:				
				im = im[:, im.shape[1]//2:]

			print(im.shape)


			# print(im.shape)
			if detect_target:
				corners = find_corners(im)
				# print(corners)
				if corners is not None:

					_, rvec, tvec, _ = cv2.solvePnPRansac(grid, corners, M, D, iterationsCount = n_iter, reprojectionError = repr_err)
					pts, _ = cv2.projectPoints(grid, rvec, tvec, M, distCoeffs=D)

					e = np.mean(np.abs(pts-corners))
					print(e)

					if e>err_thr:
						timestamp = int(round(ts.timestamp()))

						cv2.imwrite(f"{out_dir}{timestamp}_{e:.3f}.jpg", im)

					
					cv2.drawChessboardCorners(im, patternsize, corners, True)
					
					
			if pc is not None:
				ldr = pc.copy()
				print(ldr.shape)

				# print(ldr.shape)
				pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M, D, rn=100, l2im=l2im)

				if pts.shape[0]!=0 and pts is not None:
					# print(pts.shape)
					for point, clr in zip(pts, colors):
						# for point in pts:
						point = (int(point[0]), int(point[1]))
						# clr = (0,0,255)
						# print(clr)
						clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
						# print(clr, type(clr[0]), type(clr))
						im = cv2.circle(im, point,4, clr, -1)
					

			cv2.imshow(window_name, im)

			# calculate fps
			# if ts_prev is not None:
			# 	dt = ts-ts_prev
			# 	delay = dt.total_seconds() * 1000
			# 	print(delay)
			# 	if delay!=0:
			# 		fps = 1000/delay
			# 		print(f"FPS: {fps}")
			# ts_prev = ts

			# if corners is not None:
			# 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)
			

			if cv2.waitKey(1) & 0xFF == ord('q'):
				thread_stop=True
				R.stop_all()
				t.join()
				break
			# break

	# R.stop_sending()

if __name__=='__main__':
	main()