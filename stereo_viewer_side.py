import numpy as np
import socket
import cv2, struct, io, os, sys
from PIL import Image
import subprocess
from datetime import datetime
from utils import get_local_IP

from read_utils import LidarParser

patternsize = (3,5)

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

def main():

	print(sys.argv)

	camera = 0
	# camera = 1

	if len(sys.argv)>1:
		camera = sys.argv[1]

	local_IP = '192.168.90.14'
	# local_IP = '192.168.90.79'
	local_IP = get_local_IP()

	# stolen from: https://stackoverflow.com/questions/45924220/netcat-h-264-video-from-raspivid-into-opencv

	# Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
	# all interfaces)
	server_socket = socket.socket()
	server_socket.bind(('0.0.0.0', 1200))
	server_socket.listen(0)
	 
	start_time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
	print("date and time: ", start_time)

	out_dir = 'stereo_out/'+start_time+'/'

	cnt=0
	write = False
	project_lidar = True
	ts_prev = None

	if write:
		try:
			os.makedirs(out_dir)
		except OSError as error:
			print(error)
			print("Directory '%s' can not be created")

	# start raspivid on RPi
	IP = '192.168.90.71'
	command = f"python camera_stream.py {camera} {local_IP}"
	subprocess.call(['ssh', '-t', '-f', "pi@"+IP, "nohup", command], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

	# Accept a single connection and make a file-like object out of it
	connection = server_socket.accept()[0].makefile('rb')
	
	try:

		R = LidarParser()
		R.set_parameters(fov_start=0, fov_stop=180, azimuth_filter=1, elevation_filter=1)

	
		R.start_all()

		while True:
			# print("waiting")
			# Read the length of the image as a 32-bit unsigned int. If the
			# length is zero, quit the loop
			image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]
			if not image_len:
				break
			# Construct a stream to hold the image data and read the image
			# data from the connection
			image_stream = io.BytesIO()
			image_stream.write(connection.read(image_len))
			# Rewind the stream, open it as an image with opencv and do some
			# processing on it
			image_stream.seek(0)
			image = Image.open(image_stream)

			data = np.fromstring(image_stream.getvalue(), dtype=np.uint8)
			im = cv2.imdecode(data, 1)
			# print(im.shape)

			ts = datetime.now()
			if ts_prev is not None:
				dt = ts-ts_prev
				fps = 1/dt.total_seconds()
				print(f"FPS stereo side: {fps}")
			ts_prev = ts

			if write:
				cv2.imwrite(f'{out_dir}{str(cnt)}.jpg',im)

			# fix contrast
			if camera==0:
				# https://stackoverflow.com/a/58211607
				# alpha 1.0-3.0
				# beta 0-100
				im = cv2.convertScaleAbs(im, alpha=1, beta=0)

			corners = find_corners(im)
			# print(corners)

			# while True:
			# 	data, addr = R.socket_in.recvfrom(1000)
			# 	# print(addr)
			# 	R.parse_packet(data)

			# print(R.pc)

			if corners is not None:
				im = cv2.drawChessboardCorners(im, patternsize, corners, True)

			cv2.imshow("Frame",im)

			

			if cv2.waitKey(1) & 0xFF == ord('q'):
				break

			cnt+=1

			# cv2.destroyAllWindows() #cleanup windows 
	finally:
		connection.close()
		server_socket.close()
		R.stop_all()



if __name__=='__main__':
	main()
