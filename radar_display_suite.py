from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt, QTimer, QObject, QThread, pyqtSignal
import cv2
import sys, time, os
import numpy as np
import pyqtgraph.opengl as gl
import pyqtgraph as pg

from gui_utils import CompassWidget
from read_utils import RadarParser, ZEDParser

global pc

pc = None

class RadarWorker(QObject):
	global pc
	finished = pyqtSignal()
	data = pyqtSignal(object)

	def __init__(self):
		super(RadarWorker, self).__init__()
		self.parser = RadarParser()

	def run(self):
		# self.parser.start_all()
		self.parser.start_sending()

		while True:
			data, addr = self.parser.socket_in.recvfrom(1000)
			self.parser.parse_packet(data)
			if self.parser.pc is not None and len(self.parser.pc)!=0:
				self.data.emit(self.parser.pc)
				pc = self.parser.pc.copy()

class ZedWorker(QObject):
	finished = pyqtSignal()
	data = pyqtSignal(object)

	def __init__(self):
		super(ZedWorker, self).__init__()
		self.parser = ZEDParser()

		self.im = None
		self.ts_prev = None

	def run(self):
		fps = 0

		while True:
			# print("zed loop")
			data, addr = self.parser.socket_in.recvfrom(1000)
			self.parser.parse_packet(data)
			if self.parser.image is not None and self.parser.image_ok:
				im = self.parser.image
				# print(im.shape)

				# ts = datetime.datetime.now()
				# if self.ts_prev is not None:
				# 	dt = ts-self.ts_prev
				# 	fps = 1/dt.total_seconds()
				# 	# print(f"FPS zed: {fps}", end='\r')
				# self.ts_prev = ts

				# # alpha = 1.5 # Contrast control (1.0-3.0)
				# # beta = 0 # Brightness control (0-100)

				# # im = cv2.convertScaleAbs(im, alpha=alpha, beta=beta)

				# cv2.putText(im, str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 4, cv2.LINE_AA)
				# cv2.putText(im, str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

				self.data.emit(im)
				# self.parser.image_ok = False

def load_lidar_calibration(filename):
	if os.path.isfile(filename):
		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		R = fs.getNode("R").mat()
		T = fs.getNode("T").mat()
		
		return (R, T)
	else:
		print("calibration file not found!")

gray_values = np.arange(256, dtype=np.uint8)[::-1]
color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_TURBO).reshape(256, 3)))

def get_colors(points, rnge):
	global gray_values, color_values	

	colors = []
	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)
	# rnge = np.max(dist)
	idx = (dist/rnge)*255
	for i in idx:
		colors.append(color_values[int(i)])

	return np.array(colors,dtype=np.uint8)

def project_radar_points(pc, im, R, t, M, D, skip=1, rn=1e6):
	# l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
	l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])

	# print(pc)

	dst = np.sqrt(pc[:,0]**2+pc[:,1]**2+pc[:,2]**2)
	idx = dst<rn

	# pc = pc[:, 0:3].T

	# pc = pc.astype(float)*1e-3

	# print(pc.shape)

	pc = pc[idx,:]

	speed = pc[:, 3]
	rcs = pc[:, 4]
	pc = pc[:, 0:3].T

	# pc = pc[:,pc[1,:]>0] # remove points behind the camera
	pc = l2im.dot(pc)


	

	if pc.shape[1]==0:
		return pc, None, np.array([])

	# print(pc.shape)

	colors = get_colors(pc, rn)

	pts, _ = cv2.projectPoints(pc, R, t, M, distCoeffs=D)
	pts = pts[:,0,:]
	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<im.shape[1]-1) & (pts[:,1]<im.shape[0]-1) # create mask for valid pixels

	pts = pts[mask,:]
	colors = colors[mask,:]
	speed = speed[mask]
	rcs = rcs[mask]

	# colors = [(x[2]/255,x[1]/255,x[0]/255) for x in colors]
	colors = [(x[0]/255,x[1]/255,x[2]/255) for x in colors]

	return pts, speed, rcs, colors

def load_camera_calibration(filename):

	if os.path.isfile(filename):

		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		sz = fs.getNode("imageSize")
		M = fs.getNode("cameraMatrix").mat()
		D = fs.getNode("distCoeffs").mat()
		if sz.isSeq(): # za Rokov format, ki lahko vsebuje sezname
			width = sz.at(0).real()
			height = sz.at(1).real()
		else:
			sz = sz.mat()
			height = sz[0][0]
			width = sz[1][0]
		return (width, height, M,D)
	else:
		print("calibration file not found!")

def eulerAnglesToRotationMatrix(theta):
	# Calculates Rotation Matrix given euler angles.
	theta = [np.radians(x) for x in theta]
	
	R_x = np.array([[1, 0, 0],
					[0, np.cos(theta[0]), -np.sin(theta[0]) ],
					[0, np.sin(theta[0]), np.cos(theta[0]) ]
					])
		
		
					
	R_y = np.array([[np.cos(theta[1]), 0,np.sin(theta[1]) ],
					[0, 1,0],
					[-np.sin(theta[1]),0,np.cos(theta[1]) ]
					])
				
	R_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
					[np.sin(theta[2]), np.cos(theta[2]), 0],
					[0, 0,1]
					])
					
	R = np.dot(R_x, np.dot( R_y, R_z ))

	return R

class RadarWidget(pg.GraphicsWindow):
	def __init__(self, parent=None):
		super().__init__(parent=parent)

		self.layout = QtWidgets.QVBoxLayout()		
		self.setLayout(self.layout)

		self.gl = gl.GLViewWidget()
		self.gl.setCameraPosition(pos=QtGui.QVector3D(-0.6320943236351013, 12.38536262512207, 14.325300216674805), distance=97.87839624836319, elevation=np.degrees(0.6108652381980153), azimuth=np.degrees(-1.5882496193148399))
		self.layout.addWidget(self.gl)
		self.gl.show()
		self.grid = gl.GLGridItem()
		self.gl.addItem(self.grid)

		# self.sp1 = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
		# self.gl.addItem(self.sp1)

		self.sp2 = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
		self.gl.addItem(self.sp2)

		self.draw_frustum()
		# self.draw_circle(100, 100)

		# image widget for zed
		# self.layout.addWidget(self.gl)

		groupBox = QtWidgets.QGroupBox()
		hbox = QtWidgets.QHBoxLayout()
		self.image_frame_zed = QtWidgets.QLabel()
		hbox.addWidget(self.image_frame_zed)
		groupBox.setLayout(hbox)
		self.layout.addWidget(groupBox)

		# threads
		self.thread_radar = QThread()
		self.worker_radar = RadarWorker()
		self.worker_radar.moveToThread(self.thread_radar)

		self.thread_zed = QThread()
		self.worker_zed = ZedWorker()
		self.worker_zed.moveToThread(self.thread_zed)

		self.thread_radar.started.connect(self.worker_radar.run)
		self.worker_radar.data.connect(self.update_radar)
		self.thread_radar.start()

		self.thread_zed.started.connect(self.worker_zed.run)
		self.worker_zed.data.connect(self.update_zed)

		self.thread_zed.start()

		self.pc = None

		(self.width, self.height, self.M, self.D) = load_camera_calibration('calibration_zed.yaml')
		self.M/=4
		self.M[-1,-1]=1

		# self.dt = 0.025
		# self.da = 0.6

		self.dt = 0.05
		self.da = 2.0

		self.dx = self.dy = self.dz = self.dt
		self.dr = self.dp = self.dw = self.da

		self.offx = 0.08
		self.offy = -0.5
		self.offz = 0.0
		self.roll = 0.0
		self.pitch = 0.0
		self.yaw = 0.0

	def draw_frustum(self):

		fovx = np.radians(50)
		fovy = np.radians(15)
		rn = 100

		p0 = (0,0,0)
		p1 = (rn*np.sin(fovx), rn*np.cos(fovx), rn*np.sin(fovy/2))
		p2 = (-rn*np.sin(fovx), rn*np.cos(fovx), rn*np.sin(fovy/2))
		# ln1 = gl.GLLinePlotItem(pos=np.array([p0, p1]), width=1, antialias=False)
		ln1 = gl.GLLinePlotItem(pos=np.array([p0, p1, p2, p0]), width=1, antialias=False)
		self.gl.addItem(ln1)

		p3 = (rn*np.sin(fovx), rn*np.cos(fovx), -rn*np.sin(fovy/2))
		p4 = (-rn*np.sin(fovx), rn*np.cos(fovx), -rn*np.sin(fovy/2))
		ln2 = gl.GLLinePlotItem(pos=np.array([p0, p3, p4, p0]), width=1, antialias=False)
		self.gl.addItem(ln2)

		self.gl.addItem(gl.GLLinePlotItem(pos=np.array([p1,p3]), width=1, antialias=False))
		self.gl.addItem(gl.GLLinePlotItem(pos=np.array([p2,p4]), width=1, antialias=False))

	def keyPressEvent(self, event):
		# print(event, self)
		if event.key() == QtCore.Qt.Key_Q:
			self.close()
		if event.key() == QtCore.Qt.Key_E:
			self.offx-=self.dx
		if event.key() == QtCore.Qt.Key_R:
			self.offx+=self.dx
		if event.key() == QtCore.Qt.Key_S:
			self.offy-=self.dy
		if event.key() == QtCore.Qt.Key_D:
			self.offy+=self.dy
		if event.key() == QtCore.Qt.Key_X:
			self.offz-=self.dz
		if event.key() == QtCore.Qt.Key_C:
			self.offz+=self.dz
		if event.key() == QtCore.Qt.Key_U:
			self.roll-=self.dr
		if event.key() == QtCore.Qt.Key_I:
			self.roll+=self.dr
		if event.key() == QtCore.Qt.Key_J:
			self.pitch-=self.dp
		if event.key() == QtCore.Qt.Key_K:
			self.pitch+=self.dp
		if event.key() == QtCore.Qt.Key_N:
			self.yaw-=self.dw
		if event.key() == QtCore.Qt.Key_M:
			self.yaw+=self.dw

	def closeEvent(self, event):
		# self.worker.parser.stop_all()
		# self.worker.parser.stop_sending()
		# event.accept()
		pass

	def draw_circle(self, n=200, rn = 33):

		# circle
		N = np.arange(0,2*np.pi,2*np.pi/float(n))
		# rn = 33
		x = np.sin(N)*rn
		y = np.cos(N)*rn
		z = np.zeros_like(N)
		pts = np.vstack((x,y,z)).T

		pts = np.append(pts, np.array([pts[0,:]]), axis=0)


		ln = gl.GLLinePlotItem(pos=pts, width=1, antialias=False, color=(0,0,1,1))
		self.gl.addItem(ln)

	def update_zed(self, im):
		# print(im.shape)
		point_size = 3

		if self.pc is not None:

			lidar_calib = {'x': self.offx, 'y': self.offy, 'z': self.offz, 'yaw': self.yaw, 'pitch': self.pitch, 'roll': self.roll}
			R_radar = eulerAnglesToRotationMatrix([lidar_calib['pitch'], lidar_calib['yaw'], lidar_calib['roll']]) # TODO load this from yaml file
			T_radar = np.array([lidar_calib['x'], lidar_calib['y'], lidar_calib['z']])

			# im = cv2.resize(im, (int(width1), int(height1)))

			# R_radar, T_radar = load_lidar_calibration('calibration_lidar_zed_2.yaml')
			# R_radar = np.eye(3)
			# T_radar = np.zeros(3)

			# pts, colors, _ = project_radar_points(self.pc, im, R_radar, T_radar, self.M, np.array([]))
			pts, speed, rcs, colors = project_radar_points(self.pc, im, R_radar, T_radar, self.M, np.array([]), rn=300)

			# print(pts.shape)
			# print(speed.shape)
			# print(colors.shape)

			if pts.shape[0]!=0 and pts is not None:
				# print(pts.shape)
				for point, clr, sp, RCS in zip(pts, colors, speed, rcs):
					point = (int(point[0]), int(point[1]))
					clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))

					sz = int(np.abs(RCS)*0.5)
					# if RCS>0:
					# 	continue
					# print(sz)
					sz = point_size

					if sp==0:
						continue

					# im = cv2.circle(im, point, point_size, clr, thickness=cv2.FILLED, lineType=cv2.LINE_AA) # like lidar
					im = cv2.circle(im, point, sz, (0,255,0) if sp==0.0 else (255,0,0), thickness=cv2.FILLED, lineType=cv2.LINE_AA) # like lidar

				print(f"x: {self.offx:.2f}, y: {self.offy:.2f}, z: {self.offz:.2f}, roll: {self.roll:.2f}, pitch: {self.pitch:.2f}, yaw: {self.yaw:.2f}", end='\r')


		self.image_zed = QtGui.QImage(im.data, im.shape[1], im.shape[0], im.strides[0], QtGui.QImage.Format_RGB888 )
		pixmap = QtGui.QPixmap.fromImage(self.image_zed)
		pixmap = pixmap.scaled(self.image_frame_zed.size(), Qt.KeepAspectRatio)
		self.image_frame_zed.setPixmap(pixmap)

	def update_radar(self, data):

		data[:,0]=-data[:,0] # flip x axis

		self.pc = data.copy()
		# print(self.pc.shape)

		# self.sp1.setData(pos=np.array([[0,0,0]]))
		# color = np.zeros((1,3), dtype=np.float32)
		# color[:,0]=1
		# self.sp1.setData(color=color, size=20)

		# print(data)

		# data = data[data[:,-1]<0,:]
		# data[:,-1]=np.abs(data[:,-1])

		# print(data)

		data = data[data[:,-2]>0,:]


		

		self.sp2.setData(pos=data[:,0:3])
		color = np.zeros((data.shape[0],4), dtype=np.float32)
		color[:,1]=1

		speed = data[:,-2]
		rcs = data[:,-1]

		color[:,-1]=1

		# print(speed)
		# print(rcs)

		# color non-static points with red
		for i,x in enumerate(speed):

			if x!=0.0:
				color[i,:]=(1,0,0,1)

		sizes = data[:,-1]
		# size = np.random.randint(0, 20, sizes.shape)

		self.sp2.setData(color=color, size = 10)
		# self.sp2.setData(color=color, size = sizes)

if __name__ == '__main__':

	app = QtGui.QApplication(sys.argv)
	radar_widget = RadarWidget()
	radar_widget.show()

	timer = QTimer()
	timer.timeout.connect(lambda: None)
	timer.start(100)

	if pc is not None:
		print(pc.shape)

	app.exec_()
