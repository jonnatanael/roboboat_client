from read_utils import ZEDParser, DepthParser
import signal, sys, struct, datetime
import numpy as np
import cv2, time
from socket import *

start_time = time.time()
def start_timing():
	global start_time

	start_time = time.time()

def end_timing(s=''):


	print(f'{s} took: {(time.time() - start_time)} seconds')

	# print("--- %s seconds ---" % (time.time() - start_time))

def main():
	R = DepthParser()

	ts_prev = None

	# R.start_all()

	cv2.namedWindow("depth", cv2.WINDOW_NORMAL);

	while True:

		# R.start_all()
		# print("loop")

		# continue

		# print(f'{R.socket_in=}')


		# data, addr = R.socket_in.recvfrom(33000)

		# start_timing()

		data, addr = R.socket_in.recvfrom(65000)
		

		# end_timing("recvfrom")

		# print(data)
		# print(addr)
		R.parse_packet(data)
		
		if R.image is not None and R.image_ok:
		
			im = R.image.copy()
			print(f'{im.shape=}')


			ts = datetime.datetime.now()
			if ts_prev is not None:
				dt = ts-ts_prev
				fps = 1/dt.total_seconds()
				print(f"FPS zed: {fps}")
			ts_prev = ts

			# corners = find_corners(im)

			# im = cv2.cvtColor(im,cv2.COLOR_GRAY2RGB)

			# if corners is not None:
			# 	im = cv2.drawChessboardCorners(im, (3,5), corners, True)				

			cv2.imshow("depth", im)
			key = cv2.waitKey(1)

			if key & 0xFF == ord("q"):
				break

			# cv2.imwrite("zed_image.jpg", im)

			# print(im.shape)

			R.image_ok = False

	# R.stop_all()

def test():

	socket_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
	socket_in.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	socket_in.bind(('192.168.90.14', 32768))
	# socket_in.bind(('0.0.0.0', 32768))

	while True:

		# R.start_all()
		print("loop")

		# continue

		print(f'{socket_in=}')

		data, addr = socket_in.recvfrom(65000)
		# data, addr = socket_in.recvfrom(1500)
		# print(data)
		print(addr)


if __name__=='__main__':
	main()

	# test()
