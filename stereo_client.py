import socket, math
import numpy as np
import struct, zlib, binascii, time, datetime, signal, sys, cv2
from read_utils import RGBParser
from socket import *

# R = RGBParser(side='front')
R = RGBParser(side='side')

def sigint_handler(sig, frame):

	# R.stop_all()
	sys.exit()

def calculate_port_number_for_sending():

	pass

	# lidar example
	sensor_type = 4-1
	sensor_subtype = 1-1 # data
	sensor_number = 1-1
	data_direction = 1-1
	payload_type = 4-1

	# stereo front
	sensor_type = 1-1
	sensor_subtype = 1-1 # data
	sensor_number = 1-1
	data_direction = 1-1
	payload_type = 1-1

	# stereo side
	sensor_type = 1-1
	sensor_subtype = 1-1 # data
	sensor_number = 2-1
	data_direction = 1-1
	payload_type = 1-1

	# # zed
	# sensor_type = 1-1
	# sensor_subtype = 1-1 # data
	# sensor_number = 3-1
	# data_direction = 1-1
	# payload_type = 1-1

	# port_number = '{0:04b}'.format(sensor_type-1)

	decimals = [1,payload_type,data_direction,sensor_number,sensor_subtype,sensor_type]
	bit_lengths = [1,4,1,3,3,4]

	binary_string = ''
	for decimal, bit_length in zip(decimals, bit_lengths):
		# decimal-=1
		print(f'{decimal=}')
		print(f'{bit_length=}')
		binary_format = f'{{:0{bit_length}b}}'  # Create format string for binary conversion with padding
		binary_string += binary_format.format(decimal)

	print(binary_string)

	a = int(binary_string, 2)

	print(a)

def main():

	signal.signal(signal.SIGINT, sigint_handler)


	while True:
		data, addr = R.socket_in.recvfrom(63000)
		R.parse_packet(data)
		if R.image is not None and R.image_ok:
			im = R.image.copy()

			cv2.imshow("img", im)
			key = cv2.waitKey(1)

			if key & 0xFF == ord("q"):
				break

			R.image_ok = False

if __name__=='__main__':
	main()
	# calculate_port_number_for_sending()
