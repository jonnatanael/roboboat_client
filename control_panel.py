from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt, QTimer, QObject, QThread, pyqtSignal
from qtwidgets import Toggle
import sys, time, os, binascii, struct, signal
from socket import *
from utils import get_local_IP
# from darktheme.widget_template import DarkPalette

# crazy workaround for windows taskbar icon, hopefully doesn't crash on UNIX
# https://stackoverflow.com/a/1552105
import ctypes
myappid = 'a' # arbitrary string
if os.name == 'nt':
	ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

from read_utils import GPSParser, ThermalParser, RadarParser, PolarizationParser, LidarParser, data_direction, payload_types, sensor_subtypes, DepthParser, RGBParser, ZEDParser
from read_utils import system_params, electrical_params, sync_params, magic, hdr, gps_params, thermal_params, flir_params

from telemetry_display_demo import PowerBar

# NOTE: LED icons shamelessly stolen from https://github.com/akej74/grid-control/blob/master/grid-control/ui/mainwindow.py

import subprocess, platform

led_size = 30 # this is bad and should be changed

# run data splitter in background
proc = subprocess.Popen(["python", "data_splitter.py"])

def ping(host, t = 1):
	"""
	Returns True if host (str) responds to a ping request.
	Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
	"""

	# Option for the number of packets as a function of
	param = '-n' if platform.system().lower()=='windows' else '-c'
	timeout = t*1000 if platform.system().lower()=='windows' else t

	# Building the command. Ex: "ping -c 1 google.com"
	command = ['ping', param, '1', '-w', str(timeout) ,host]

	return subprocess.call(command, stdout=open(os.devnull, 'wb')) == 0

class PingChecker(QObject):
	progress = pyqtSignal(object)

	def __init__(self):
		super(PingChecker, self).__init__()

	def pingOk(self, sHost, timeout=1):
		return ping(sHost)
		
	def run(self):

		green_led = QtGui.QPixmap("icons/led-green.png").scaled(led_size, led_size, Qt.KeepAspectRatio, Qt.FastTransformation)
		red_led = QtGui.QPixmap("icons/led-red.png").scaled(led_size, led_size, Qt.KeepAspectRatio, Qt.FastTransformation)

		while True:
			for i, (sensor_name, info) in enumerate(sensor_info.items()):
				res = ping(info['IP'])
				sensor_info[sensor_name]['ping'] = res
				box = info['control_box']
				b = box.findChild(QtWidgets.QLabel, sensor_name+'_led')
				if res:
					b.setPixmap(green_led)

					# additionally, send parameter request
					if info['parser']:
						# print(f"{sensor_name} asking for params")
						# info['parser'].get_parameters()
						pass
				else:
					b.setPixmap(red_led)
			time.sleep(1)

class StatusChecker(QObject):

	def run(self):
		# runs continuously and checks for new parameters or telemetry data, then displays it
		while True:
			diff = 0
			for i, (sensor_name, info) in enumerate(sensor_info.items()):
				# print(sensor_name)
				p = info['parser']

				# subprocess.call(command, stdout=open(os.devnull, 'wb'))
				# command = f"scp {config_file} {v['username']}@{sensor_IP}:/tmp/tmp.conf"
				# subprocess.call(command.split(' '), stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
				# print(info)
				# TODO check directory size
				command = "du -s /mnt/SSD"
				try:
					proc = subprocess.check_output(['ssh', '-T', '-f', f"{info['username']}@{info['IP']}", "nohup", command], stderr=subprocess.DEVNULL, timeout=1).decode()
					sz = int(proc.split("\t")[0])
				except:
					sz = None
					pass
					# print(f"sensor at {info['IP']} not responding")

				# print(sz)

				# if sensor_name=='zed':
					# print(sz, info['size'])
				# print(proc)
				# print(proc.split('\t')[0])
				
				# print(sensor_name, sz)
				

				# if 
				if info['size'] is not None and sz is not None:
					diff = sz-info['size']
					# print(f"{sz} {info['size']} diff: {diff}")
					# if diff==0:
						# print(f"sensor {sensor_name} not recording")

				green_led = QtGui.QPixmap("icons/led-green.png").scaled(led_size, led_size, Qt.KeepAspectRatio, Qt.FastTransformation)
				red_led = QtGui.QPixmap("icons/led-red.png").scaled(led_size, led_size, Qt.KeepAspectRatio, Qt.FastTransformation)
				box = info['control_box']
				# print(box)
				b = box.findChild(QtWidgets.QLabel, sensor_name+'_recording')
				# if res:
				# 	b.setPixmap(green_led)

				# 	# additionally, send parameter request
				# 	if info['parser']:
				# 		# print(f"{sensor_name} asking for params")
				# 		# info['parser'].get_parameters()
				# 		pass
				# else:
				# 	b.setPixmap(red_led)
				if diff!=0:
					b.setPixmap(green_led)
				else:
					b.setPixmap(red_led)


				info['size']=sz

				# info['size']=

				if p is not None:
					p.get_parameters() # send parameter request

					if p.parameters is not None:
						box = info['param_box']

						for k,v in info['params'].items():
							if k=='command':
								continue				
							x = box.findChildren(QtWidgets.QLabel, k+'_label')
							x[0].setText(str(p.parameters[k]))

					if p.system_data is not None:
						box = info['telem_box']
						for i, (k,v) in enumerate(system_params.items()):
							x = box.findChildren(QtWidgets.QLabel, k+'_label')
							x[0].setText(str(p.system_data[k]))
							if (k.startswith('cpu') or k.startswith('gpu')) : # skip last 4 cores if they aren't present
								if p.system_data[k]==255:
									x[0].parent().hide()
								else:
									x[0].parent().show()

								# get powerbar widget
								y = box.findChildren(PowerBar)
								y[i]._dial.setValue(p.system_data[k])
							if k.startswith('SSD'):
								if p.system_data[k]==65535:
									x[0].setText("NO SSD")

					if p.electrical_data is not None:
						box = info['telem_box']
						for k,v in electrical_params.items():
							x = box.findChildren(QtWidgets.QLabel, k+'_label')
							x[0].setText(str(p.electrical_data[k]))

					if p.sync_data is not None:
						box = info['telem_box']
						# print(dir(box))
						
						# xx = QtGui.QIcon('icons/led-green.png')
						# print(box.parent().tabText(i))
						sync_ok = False
						for k,v in sync_params.items():
							x = box.findChildren(QtWidgets.QLabel, k+'_label')
							value = p.sync_data[k]
							# print(k,v,value,0, len(value))
							x[0].setText(str(value))
							if float(value)>=0 and float(value)<20:
								sync_ok=True
								# x[0].setStyleSheet("color: white")
								x[0].setStyleSheet("color: black")
							else:
								x[0].setStyleSheet("color: red")

						idx = box.parent().parent().indexOf(box)
						if not sync_ok:
							box.parent().parent().setTabIcon(idx, QtGui.QIcon('icons/led-red.png'))
						else:
							box.parent().parent().setTabIcon(idx, QtGui.QIcon('icons/led-green.png'))

			time.sleep(1)

class StatusCheckerReceiver(QObject):
	# checks on port 27500 for telemetry and parameters data, saves it to corresponding parser

	def __init__(self):
		super(StatusCheckerReceiver, self).__init__()

		self.socket_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
		self.socket_in.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)		
		IP = get_local_IP()
		# IP = '192.168.91.255'
		self.socket_in.bind((IP, 4200)) # this is set automatically from some config or sth


	def run(self):
		while True:
			data, addr = self.socket_in.recvfrom(1000) # listen on local IP
			# print(data)
			self.parse_packet(data, addr)

	def check_crc(self, crc, data):
		a = binascii.crc32(data).to_bytes(4,'big')		
		return crc==a

	def parse_fields(self, info, data):
		# info is a dict of response information such as offset and type, data is a byte string
		for k,v in data.items():
			c = info[k][-1]
			if c=='bits' or c=='char' or c=='bool' or c=='uint8':
				data[k]=int.from_bytes(v, 'big')
			elif c=='float':
				data[k]=struct.unpack('>f', v)[0]
				data[k]=f'{data[k]:9.2f}' # limit floats to 2 decimal places
			elif c=='uint32':
				data[k]=struct.unpack('!I', v)[0]
			elif c=='uint16':
				data[k]=struct.unpack('!H', v)[0]
			elif c=='string':
				data[k]=v.decode("utf-8")

	def parse_message(self, payload, params):
		crc = payload[-4:]
		data = payload[2:-4]
		res = {}

		if not self.check_crc(crc, data):
			print("CRC failed", self, data)
			return None

		for k,v in params.items():
			o = v[0]
			ln = v[1]
			res[k]=data[o:o+ln]

		self.parse_fields(params, res)
		return res

	def get_header_info(self,data):
		res = {}

		for k,v in hdr.items():
			o = v[0]
			ln = v[1]		
			if k =='payload':
				res[k]=data[o:o+(len(data)-13)]
			else:
				res[k]=int.from_bytes(data[o:o+ln], 'little')
		return res

	def find_parser(self, stype, snum):
		for k,v in sensor_info.items():
			if v['sensor_type']==stype and v['sensor_number']==snum:
				return v['parser']

	def parse_packet(self, packet, addr):
		# this parses system, telemetry and electrical data
		hdr_info = self.get_header_info(packet)
		

		p = self.find_parser(hdr_info['sensor_type'],hdr_info['sensor_number'])
		
		if p is None:
			return

		if hdr_info['direction']==data_direction['from sensor'] and hdr_info['payload_type']==payload_types['raw']:
			p.parse_parameters(hdr_info['payload'])

		elif hdr_info['direction']==data_direction['from sensor'] and hdr_info['sensor_subtype']==sensor_subtypes['telemetry']:

			if hdr_info['payload_type']==payload_types['electrical'] and p is not None:
				p.electrical_data = self.parse_message(hdr_info['payload'], electrical_params)
			elif hdr_info['payload_type']==payload_types['system'] and p is not None:
				p.system_data = self.parse_message(hdr_info['payload'], system_params)
			elif hdr_info['payload_type']==payload_types['timesync'] and p is not None:
				p.sync_data = self.parse_message(hdr_info['payload'], sync_params)

		elif hdr_info['direction']==data_direction['from sensor'] and hdr_info['sensor_subtype']==sensor_subtypes['parameter'] and hdr_info['payload_type']==payload_types['system']:
			p.parse_parameters(hdr_info['payload'])			

sensor_types = {1: 'rgb camera', 2: 'polarization camera', 3: 'thermal camera', 4: 'lidar', 5: 'radar', 6: 'GPS/IMU', 7: 'depth camera'}

sensor_info = {
	'jetson': {'IP': '192.168.90.70', 'IP_VPN': '10.8.0.9', 'parser': ZEDParser(), 'params': None, 'sensor_type': 1, 'sensor_number': 3,'username': 'jetson', 'size':None},
	'stereo front': {'IP': '192.168.90.74', 'IP_VPN': '10.8.0.5', 'parser': RGBParser('front'), 'params': None, 'sensor_type': 1, 'sensor_number': 1,'username': 'pi', 'size':None},
	'stereo side': {'IP': '192.168.90.71', 'IP_VPN': '10.8.0.4', 'parser': RGBParser('side'), 'params': None, 'sensor_type': 1, 'sensor_number': 2,'username': 'pi', 'size':None},
	'thermal camera': {'IP': '192.168.90.72', 'IP_VPN': '10.8.0.6', 'parser': ThermalParser(), 'params': thermal_params, 'sensor_type': 3, 'sensor_number': 1,'username': 'pi', 'size':None},
	'polarization camera': {'IP': '192.168.90.73', 'IP_VPN': '10.8.0.7', 'parser': PolarizationParser(), 'params': flir_params, 'sensor_type': 2, 'sensor_number': 1,'username': 'pi', 'size':None},
	'lidar': {'IP': '192.168.90.75', 'IP_VPN': '10.8.0.3', 'parser': LidarParser(), 'params': None, 'sensor_type': 4, 'sensor_number': 1,'username': 'pi', 'size':None},
	'radar': {'IP': '192.168.90.76', 'IP_VPN': '10.8.0.8', 'parser': RadarParser(), 'params': None, 'sensor_type': 5, 'sensor_number': 1,'username': 'pi', 'size':None},
	'GPS/IMU': {'IP': '192.168.90.77', 'IP_VPN': '10.8.0.2', 'parser': GPSParser(), 'params': None, 'sensor_type': 6, 'sensor_number': 1,'username': 'pi', 'size':None},
}

for k,v in sensor_info.items():
	v['parser'].socket_in.close()

colors = ['#00FF00','#11FF00','#22FF00','#33FF00','#44FF00','#55FF00','#66FF00','#77FF00','#88FF00','#99FF00','#AAFF00','#BBFF00','#CCFF00','#DDFF00','#EEFF00','#FFFF00','#FFEE00','#FFDD00','#FFCC00','#FFBB00','#FFAA00','#FF9900','#FF8800','#FF7700','#FF6600','#FF5500','#FF4400','#FF3300','#FF2200','#FF1100','#FF0000']

class DavimarControlPanel(QtWidgets.QWidget):
	def __init__(self, parent=None):
		super(DavimarControlPanel, self).__init__(parent)

		# if os.name != 'nt':
		# 	self.setPalette(DarkPalette())

		self.main_layout = QtWidgets.QVBoxLayout()
		self.setWindowTitle("Davimar control panel") 

		label = QtWidgets.QLabel("Control Panel")
		label.setAlignment(Qt.AlignCenter)
		self.main_layout.addWidget(label)

		# add tabs
		self.tabs = QtWidgets.QTabWidget()
		self.control_tab = QtWidgets.QWidget()
		self.params_tab = QtWidgets.QWidget()
		self.telem_tab = QtWidgets.QWidget()
		self.shutdown_tab = QtWidgets.QWidget()

		self.tabs.addTab(self.control_tab, "Control")
		self.tabs.addTab(self.params_tab, "Parameters")
		self.tabs.addTab(self.telem_tab, "Telemetry")
		self.tabs.addTab(self.shutdown_tab, "Shutdown")

		self.control_tab.layout = QtWidgets.QVBoxLayout()
		self.params_tab.layout = QtWidgets.QVBoxLayout()
		self.telem_tab.layout = QtWidgets.QVBoxLayout()
		self.shutdown_tab.layout = QtWidgets.QVBoxLayout()
		self.control_tab.setLayout(self.control_tab.layout)
		self.params_tab.setLayout(self.params_tab.layout)
		self.telem_tab.setLayout(self.telem_tab.layout)
		self.shutdown_tab.setLayout(self.shutdown_tab.layout)

		self.main_layout.addWidget(self.tabs)

		# CONTROL TAB

		# https://www.learnpyqt.com/widgets/pyqt-toggle-widget/
		self.control_tab.layout.addWidget(QtWidgets.QLabel('Start data acquisition'))
		toggle_all = Toggle()
		toggle_all.setObjectName('toggle_all')
		toggle_all.clicked.connect(self.handle_toggle_all)
		toggle_all.setFixedWidth(70)


		self.control_tab.layout.addWidget(toggle_all)

		self.framex = QtWidgets.QFrame()

		for sensor_name, info in sensor_info.items():
			# add groupbox
			groupBox = QtWidgets.QGroupBox(self.framex)
			groupBox.setObjectName(sensor_name+'_acq')
			# groupBox.setGeometry(QtCore.QRect(20, 20, 20, 20))
			hbox = QtWidgets.QHBoxLayout()

			# label
			label = QtWidgets.QLabel(sensor_name)
			# label.setFixedWidth(200)
			hbox.addWidget(label)

			label = QtWidgets.QLabel(info['IP'])
			# label.setFixedWidth(200)
			hbox.addWidget(label)

			hbox.addWidget(QtWidgets.QLabel("ping"))

			led = QtWidgets.QLabel()
			# led.setGeometry(QtCore.QRect(200, 200, 200, 200))
			# led.setScaledContents(True)
			# led.setPixmap(QtGui.QPixmap("icons/led-red.png"))
			led.setPixmap(QtGui.QPixmap("icons/led-red.png").scaled(led_size, led_size, Qt.KeepAspectRatio, Qt.FastTransformation))
			# led.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)	
			led.setObjectName(sensor_name+'_led')
			hbox.addWidget(led)

			# toggle button (likely for acquisition, but should not be available for all sensors)
			toggleBtn = Toggle()
			toggleBtn.clicked.connect(self.handle_toggle)
			toggleBtn.setObjectName('acq_toggle')
			hbox.addWidget(toggleBtn)
			toggleBtn.setFixedWidth(70)

			led = QtWidgets.QLabel()
			hbox.addWidget(QtWidgets.QLabel("recording"))
			led.setPixmap(QtGui.QPixmap("icons/led-red.png").scaled(led_size, led_size, Qt.KeepAspectRatio, Qt.FastTransformation))
			led.setObjectName(sensor_name+'_recording')
			hbox.addWidget(led)

			led = QtWidgets.QLabel()

			sensor_info[sensor_name]['control_box']=groupBox
			groupBox.setLayout(hbox)

			self.control_tab.layout.addWidget(groupBox)

		# PARAMETERS TAB
		parameters_tabs = QtWidgets.QTabWidget()

		for sensor_name, info in sensor_info.items():

			params = info['params']

			if params is not None:

				tab = QtWidgets.QWidget()
				parameters_tabs.addTab(tab, sensor_name)
				tab.layout = QtWidgets.QVBoxLayout()
				tab.setLayout(tab.layout)

				# add groupbox
				groupBox = QtWidgets.QGroupBox(self.framex)
				groupBox.setObjectName(sensor_name+'_param')
				outer_box = QtWidgets.QVBoxLayout()

				# label
				label = QtWidgets.QLabel(sensor_name)
				# label.setFixedWidth(200)
				outer_box.addWidget(label)

				# print(params)
				
				for i,(k,v) in enumerate(params.items()):
					# print(i)
					if k=='command':
						continue
					groupBox2 = QtWidgets.QGroupBox()
					inner_box = QtWidgets.QHBoxLayout()

					label1 = QtWidgets.QLabel(k)
					label2 = QtWidgets.QLabel("placeholder")
					label2.setObjectName(k+'_label')

					inner_box.addWidget(label1)
					inner_box.addWidget(label2)
					groupBox2.setLayout(inner_box)
					outer_box.addWidget(groupBox2)

				sensor_info[sensor_name]['param_box']=groupBox
				groupBox.setLayout(outer_box)

				# self.params_tab.layout.addWidget(groupBox)
				tab.layout.addWidget(groupBox)

		self.params_tab.layout.addWidget(parameters_tabs)

		# TELEMETRY TAB

		telemetry_tabs = QtWidgets.QTabWidget()

		for sensor_name, info in sensor_info.items():
			# add groupbox
			# groupBox = QtWidgets.QGroupBox(self.framex)
			# groupBox.setObjectName(sensor_name+'_telem')
			# hbox = QtWidgets.QHBoxLayout()

			# label
			# label = QtWidgets.QLabel(sensor_name)
			# label.setFont(QtGui.QFont('Arial', 5))
			# label.setFixedWidth(200)
			# hbox.addWidget(label)

			tab = QtWidgets.QWidget()
			tab.setObjectName(sensor_name+'_tab')
			# xx = QtGui.QIcon('icons/led-green.png')
			xx = QtGui.QIcon('icons/led-red.png')
			telemetry_tabs.addTab(tab, xx, sensor_name)
			tab.layout = QtWidgets.QVBoxLayout()
			tab.setLayout(tab.layout)

			# create systems box
			groupBox_system = QtWidgets.QGroupBox()
			groupBox_system.setObjectName(sensor_name+'_telem_system')
			vbox = QtWidgets.QVBoxLayout()

			# add system labels
			l = QtWidgets.QLabel("system")
			# l.setFont(QtGui.QFont('Arial', 5))
			sys_params_box = QtWidgets.QGroupBox()
			sys_params_box_layout = QtWidgets.QHBoxLayout()
			sys_params_box_layout.addWidget(l)
			sys_params_box.setLayout(sys_params_box_layout)
			for i,(k,v) in enumerate(system_params.items()):
				# print(k,v)
				groupBox2 = QtWidgets.QGroupBox()
				hbox2 = QtWidgets.QVBoxLayout()
				label1 = QtWidgets.QLabel(k)
				label2 = QtWidgets.QLabel("placeholder")
				label2.setObjectName(k+'_label')
				# label1.setFont(QtGui.QFont('Arial', 5))
				# label2.setFont(QtGui.QFont('Arial', 5))
				label1.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
				label1.setAlignment(Qt.AlignCenter)
				label2.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)				
				label2.setAlignment(Qt.AlignCenter)

				hbox2.addWidget(label1)
				hbox2.addWidget(label2)
				groupBox2.setLayout(hbox2)
				sys_params_box_layout.addWidget(groupBox2)

				if 'cpu' in k or 'gpu' in k:
					bar = PowerBar(steps=100); bar.setBarPadding(2); bar.setBarSolidPercent(1)
					bar.setColors(colors)
					hbox2.addWidget(bar)

				tab.layout.addWidget(sys_params_box)

			# create electrical box
			# # add electrical labels
			l = QtWidgets.QLabel("electrical")
			# l.setFont(QtGui.QFont('Arial', 5))
			electr_params_box = QtWidgets.QGroupBox()
			electr_params_box_layout = QtWidgets.QHBoxLayout()
			electr_params_box_layout.addWidget(l)
			electr_params_box.setLayout(electr_params_box_layout)
			for i,(k,v) in enumerate(electrical_params.items()):
				# print(k,v)
				groupBox2 = QtWidgets.QGroupBox()
				hbox2 = QtWidgets.QVBoxLayout()
				label1 = QtWidgets.QLabel(k)
				label2 = QtWidgets.QLabel("placeholder")
				label2.setObjectName(k+'_label')
				# label1.setFont(QtGui.QFont('Arial', 5))
				# label2.setFont(QtGui.QFont('Arial', 5))
				label1.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
				label1.setAlignment(Qt.AlignCenter)
				label2.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)				
				label2.setAlignment(Qt.AlignCenter)

				hbox2.addWidget(label1)
				hbox2.addWidget(label2)
				groupBox2.setLayout(hbox2)
				electr_params_box_layout.addWidget(groupBox2)
				tab.layout.addWidget(electr_params_box)

			# create sync box
			# add sync labels
			l = QtWidgets.QLabel("sync")
			# l.setFont(QtGui.QFont('Arial', 5))
			sync_params_box = QtWidgets.QGroupBox()
			sync_params_box_layout = QtWidgets.QHBoxLayout()
			sync_params_box_layout.addWidget(l)
			sync_params_box.setLayout(sync_params_box_layout)
			for i,(k,v) in enumerate(sync_params.items()):
				# print(k,v)
				groupBox2 = QtWidgets.QGroupBox()
				hbox2 = QtWidgets.QVBoxLayout()
				label1 = QtWidgets.QLabel(k)
				label2 = QtWidgets.QLabel("placeholder")
				label2.setObjectName(k+'_label')
				# label1.setFont(QtGui.QFont('Arial', 5))
				# label2.setFont(QtGui.QFont('Arial', 5))
				label1.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
				label1.setAlignment(Qt.AlignCenter)
				label2.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)				
				label2.setAlignment(Qt.AlignCenter)

				hbox2.addWidget(label1)
				hbox2.addWidget(label2)
				groupBox2.setLayout(hbox2)
				sync_params_box_layout.addWidget(groupBox2)
				tab.layout.addWidget(sync_params_box)

			# .params_tab.setLayout(self.params_tab.layout)
			# .telem_tab.setLayout(self.telem_tab.layout)

			# sensor_info[sensor_name]['telem_box']=groupBox

			sensor_info[sensor_name]['telem_box']=tab
			# groupBox.setLayout(hbox)
			# self.telem_tab.layout.addWidget(groupBox)

		self.telem_tab.layout.addWidget(telemetry_tabs)

		# self.setLayout(self.main_layout)
		# return

		# REBOOT/SHUTDOWN TAB

		reboot_all = QtWidgets.QPushButton("Reboot all")
		reboot_all.setStyleSheet("background-color: red")
		reboot_all.setMaximumWidth(200)
		reboot_all.setObjectName('reboot_all')
		reboot_all.clicked.connect(self.handle_reboot_all)
		self.shutdown_tab.layout.addWidget(reboot_all)

		shutdown_all = QtWidgets.QPushButton("Shutdown all")
		shutdown_all.setStyleSheet("background-color: red")
		shutdown_all.setMaximumWidth(200)
		shutdown_all.setObjectName('shutdown_all')
		shutdown_all.clicked.connect(self.handle_shutdown_all)
		self.shutdown_tab.layout.addWidget(shutdown_all)

		for sensor_name, info in sensor_info.items():
			# add groupbox
			groupBox = QtWidgets.QGroupBox(self.framex)
			groupBox.setObjectName(sensor_name+'_sdn')
			# groupBox.setGeometry(QtCore.QRect(20, 20, 20, 20))
			hbox = QtWidgets.QHBoxLayout()

			# label
			label = QtWidgets.QLabel(sensor_name)
			# label.setFixedWidth(200)
			hbox.addWidget(label)

			rebootBtn = QtWidgets.QPushButton("Reboot")
			rebootBtn.setStyleSheet("background-color: red")
			rebootBtn.setMaximumWidth(200)
			rebootBtn.clicked.connect(self.handle_reboot)
			hbox.addWidget(rebootBtn)

			shutdownBtn = QtWidgets.QPushButton("Shutdown")
			shutdownBtn.setStyleSheet("background-color: red")
			shutdownBtn.setMaximumWidth(200)
			shutdownBtn.clicked.connect(self.handle_shutdown)
			hbox.addWidget(shutdownBtn)

			groupBox.setLayout(hbox)

			self.shutdown_tab.layout.addWidget(groupBox)

		# telemetry_tabs = QtWidgets.QTabWidget()
		# self.telem_tab.layout.addWidget(telemetry_tabs)

		# ping check
		self.thread = QThread()
		self.worker = PingChecker()
		self.worker.moveToThread(self.thread)
		self.thread.started.connect(self.worker.run)
		self.thread.start()

		# status check		
		self.thread2 = QThread()
		self.worker2 = StatusChecker()
		self.worker2.moveToThread(self.thread2)
		self.thread2.started.connect(self.worker2.run)
		self.thread2.start()

		# # StatusCheckerReceiver
		self.thread3 = QThread()
		self.worker3 = StatusCheckerReceiver()
		self.worker3.moveToThread(self.thread3)
		self.thread3.started.connect(self.worker3.run)
		self.thread3.start()

		self.setLayout(self.main_layout)

		app.setWindowIcon(QtGui.QIcon('icons/davimar.png'))

	def closeEvent(self, event):

		# quit_msg = "Are you sure you want to exit the program?"
		# reply = QtWidgets.QMessageBox.question(self, 'Message', 
		# 				 quit_msg, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)

		# if reply == QtWidgets.QMessageBox.Yes:
		# 	event.accept()
		# else:
		# 	event.ignore()

		if len(sys.argv)==1:
			for k,v in sensor_info.items():
				# print(k,v)
				p = v['parser']
				p.stop_acquisition()

		proc.kill()

	def handle_toggle(self, a):
		name = self.sender().parent().objectName()[:-4]
		
		p = sensor_info[name]['parser']

		# print(f"{'starting' if a else 'stopping'} acquisition on {name}".upper())

		if p:
			if a:
				p.start_acquisition()
			else:
				p.stop_acquisition()

	def handle_reboot(self, a):
		name = self.sender().parent().objectName()[:-4]
		p = sensor_info[name]['parser']
		if p:
			print("reboot:", name)
			p.reboot()

	def handle_shutdown(self, a):
		name = self.sender().parent().objectName()[:-4]
		p = sensor_info[name]['parser']
		if p:
			print("shutdown:", name)
			p.shutdown()

	def handle_toggle_all(self, a):
		name = self.sender().objectName()
		clicked = pyqtSignal()

		print(f"{'starting' if a else 'stopping'} acquisition on all sensors".upper())

		for i, (sensor_name, info) in enumerate(sensor_info.items()):

			box = info['control_box']
			b = box.findChild(Toggle, 'acq_toggle')
			
			# change toggle button position
			b.setChecked(a)
			# send signal to toggle button
			b.clicked.emit(a)
			
	def handle_reboot_all(self, a):
		print("rebooting all sensors".upper())
		name = self.sender().objectName()
		clicked = pyqtSignal()

		for i, (sensor_name, info) in enumerate(sensor_info.items()):
			# print(i, sensor_name)

			p = info['parser']
			# preventively send stop_acquisition command
			p.stop_all()
			p.reboot()

	def handle_shutdown_all(self, a):
		print("shutting down all sensors".upper())
		name = self.sender().objectName()
		clicked = pyqtSignal()

		for i, (sensor_name, info) in enumerate(sensor_info.items()):
			# print(i, sensor_name)

			p = info['parser']
			p.stop_all()
			p.shutdown()

def sigint_handler(*args):

	for k,v in sensor_info.items():
		# print(k,v)
		p = v['parser']
		p.stop_acquisition()

	time.sleep(1)

	QtWidgets.QApplication.quit() # quit gui
	proc.kill() # kill data splitter process

if __name__ == '__main__':
	signal.signal(signal.SIGINT, sigint_handler)
	app = QtWidgets.QApplication(sys.argv)

	if len(sys.argv)==2 and sys.argv[1].lower()=='vpn':
		for k,v in sensor_info.items():
			v['IP']=v['IP_VPN']

	display_image_widget = DavimarControlPanel()
	display_image_widget.show()

	timer = QTimer()
	timer.timeout.connect(lambda: None)
	timer.start(100)

	sys.exit(app.exec_())
