from socket import *
# import time, datetime, math
import numpy as np
# import struct, zlib, binascii
# import pptk
import time
import cv2, os, signal, sys
from read_utils import PolarizationParser, ThermalParser, ZEDParser
import threading
from numpy.linalg import inv

thread_stop = False
im_polar = None
polar = None

from projection_utils import *

def rt_to_projection_matrix(R,t):
	P = np.hstack((R,t))
	P = np.vstack((P,np.array((0,0,0,1))))

	return P

def rotationMatrixToEulerAngles(R) :

	sy = np.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])

	singular = sy < 1e-6

	if not singular :
		x = np.arctan2(R[2,1] , R[2,2])
		y = np.arctan2(-R[2,0], sy)
		z = np.arctan2(R[1,0], R[0,0])
	else :
		x = np.arctan2(-R[1,2], R[1,1])
		y = np.arctan2(-R[2,0], sy)
		z = 0

	return np.array([x, y, z])

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

def get_grid(): # for calibration, not needed really
	# create grid

	objectPoints= []
	grid_size = 0.3
	rows, cols = 3, 6

	z = 0
	off = grid_size/2

	for i in range(rows):
		row = []
		for j in range(cols):
			
			if j%2==0:
				p = (-j*off,i*(-grid_size),z)
				row.append(p)
			else:
				if i<rows-1:
					p = (-j*(off),-(i*(off*2)+off),z)
					row.append(p)

		if i<rows-1:
			row = row[::2]+row[1::2]

		objectPoints.extend(row)


	objectPoints= np.array(objectPoints).astype('float32')
	objectPoints[:,0]*=-1
	objectPoints[:,1]*=-1

	return objectPoints

def project_image(im1s, im2s, P, M1, M2, t = 1):
	# P should be the R|t matrix between the cameras

	i,j = np.meshgrid(np.arange(im1s[1]),np.arange(im1s[0]))
	pixels = np.vstack((i.flatten(),j.flatten())).T

	Z = np.ones_like(i) # ones for Z, because of normalized coordinates
	p = np.vstack((i.flatten(),j.flatten(),Z.flatten()))
	p = (inv(M1) @ p).T # unproject pixels to normalized coordinates
	# print(P.shape)


	# z = 2.7
	p = p*t # include distance to calibration pattern
	
	# project points

	# extend with 4th dimension
	p = np.hstack((p,np.ones((p.shape[0],1))))
	# PM = np.dot(P2, inv(P1)) # precalculate transformation matrices
	P_ = P @ p.T # one line precalculated
	P_/=P_[-1]
	P_ = P_[0:3]

	# R = P_[0:3,0:3]
	# T = P_[:3,-1]

	# print(R,T)

	# ts2 = datetime.now(); delta = (ts2-ts1).total_seconds()*1000; print(f"time: {delta} ms"); ts1 = datetime.now()


	# pts, _ = cv2.projectPoints(P_, np.eye(3), np.zeros(3), M2, distCoeffs=None)
	# pts = pts[:,0,:]

	# print(pts.shape)

	pts_ = M2 @ P_
	pts_[0,:]/=pts_[-1,:]
	pts_[1,:]/=pts_[-1,:]
	pts_ = pts_[:2,:].T
	pts = pts_

	# ts2 = datetime.now(); delta = (ts2-ts1).total_seconds()*1000; print(f"time: {delta} ms"); ts1 = datetime.now()	

	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<im2s[1]-1) & (pts[:,1]<im2s[0]-1) # create mask for valid pixels
	pixels = pixels[mask,:]
	pts = pts[mask,:].astype(np.float32)

	# print("pts", pts.shape)
	# print("pts", np.unique(pts.astype(np.uint16)))

	# ts2 = datetime.now(); delta = (ts2-ts1).total_seconds()*1000; print(f"time: {delta} ms"); ts1 = datetime.now()


	# set map
	mp = np.zeros((i.shape[0],j.shape[1],2),dtype=np.float32)-1
	
	i = pts[:,1]
	j = pts[:,0]
	x = pixels[:,1]
	y = pixels[:,0]

	mp[x,y,0]=j
	mp[x,y,1]=i

	# ts2 = datetime.now(); delta = (ts2-ts1).total_seconds()*1000; print(f"time: {delta} ms"); ts1 = datetime.now()

	mpx = np.zeros((im2s[0],im2s[1],2), dtype=np.float32)-1

	# print(mp.shape, mpx.shape)

	# print(np.unique(i))
	# print(np.unique(j))

	i = i.astype(np.uint16)
	j = j.astype(np.uint16)

	# print(i.shape, j.shape, x.shape, y.shape)

	z = np.vstack((i,j,x,y)).T
	# print(z)

	unique_rows = np.unique(z, axis=0)

	# print("z", z.shape)
	# print("unique_rows", unique_rows.shape)

	mpx[i,j,0]=y
	mpx[i,j,1]=x

	# print("mpx", mpx.shape, mpx.dtype)

	# mpx = cv2.resize(mpx, (mpx.shape[1], mpx.shape[0]), interpolation = cv2.INTER_CUBIC)

	# print("mpx", mpx.shape, mpx.dtype)


	return mp, mpx

def load_lidar_calibration(filename):
	if os.path.isfile(filename):
		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		R = fs.getNode("R").mat()
		T = fs.getNode("T").mat()
		C = fs.getNode("C").mat()
		
		return (R, T, C)
	else:
		print("calibration file not found!")

# def worker():
# 	global thread_stop, im_thermal, thermal

# 	while True:
# 		# print("lidar thread")
# 		data, addr = thermal.socket_in.recvfrom(1000)
# 		# print(addr)
# 		thermal.parse_packet(data)
# 		if thermal.image is not None and thermal.image_ok:
# 			# print(len(thermal.pc))
# 			im_thermal = thermal.image

# 		# print(thread_stop)

# 		if thread_stop:
# 			break
# 	return

def worker():
	global thread_stop, im_polar, polar

	while True:
		# print("lidar thread")
		data, addr = polar.socket_in.recvfrom(1000)
		# print(addr)
		polar.parse_packet(data)
		# if polar.image is not None and polar.image_ok:
			# print(len(polar.pc))
			# im_polar = polar.image.copy()
			# im_polar = cv2.resize(im_polar, None, fx=2, fy=2)

			# polar.image_ok = False

		# print(thread_stop)

		if thread_stop:
			print("stopping thread")
			break
	return

# TODO get mapping between the images from calibration (transitive, i.e. from polar to zed and from zed to thermal or vice versa)


zed = None

def signal_handler(sig, frame):
	print('You pressed Ctrl+C!')
	global zed, polar, thread_stop
	thread_stop=True
	print("signal stopping")
	# zed.stop_acquisition()
	# polar.stop_all()
	# sys.exit()

# exit()

def main():

	global zed, thread_stop, im_polar, polar

	signal_a = signal.signal(signal.SIGINT, signal_handler)

	# while True:
	# 	# print("loop")
	# 	pass

	zed = ZEDParser()
	polar = PolarizationParser()

	zed.start_acquisition()
	polar.start_all()

	# start thread for polar
	t = threading.Thread(target=worker)
	t.start()


	alpha=0.5
	calculate_map = True
	calculate_map = False

	# parameters
	offx= 0.0; offy= 0.0; offz= 0.0; roll = 0.0; pitch = 0.0; yaw = 0.0
	D = 2.0
	# D = 200
	# D = int(1e6)

	dt = 0.01
	da = 0.5
	dd = 0.1

	dx = dy = dz = dt
	dr = dp = dw = da

	# (M1, M2, D1, D2, R1, t1, R2, t2) = load_calibration_data('calibration_zed_polarization_camera.yaml')
	# P_zed = rt_to_projection_matrix(R1,t1)
	# P_polar = rt_to_projection_matrix(R2,t2)

	R_zed, T_zed, C_zed = load_lidar_calibration(f"/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_lidar_zed.yaml")
	R_polar, T_polar, C_polar = load_lidar_calibration(f"/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_lidar_polarization_camera.yaml")

	# P_zed = rt_to_projection_matrix(R_zed, T_zed.T)
	# P_polar = rt_to_projection_matrix(R_polar, T_polar.T)

	# P_polar_zed = np.dot(P_polar, inv(P_zed)) # zed -> polar
	# P_polar_zed = P_polar @ inv(P_zed) # zed -> polar
	# P_polar_zed = inv(P_polar) @ P_zed
	# P_polar_zed = P_zed @ inv(P_polar)
	# P_polar_zed = inv(P_zed) @ P_polar

	P1 = rt_to_projection_matrix(R_zed, T_zed)
	P2 = rt_to_projection_matrix(R_polar, T_polar)

	# print(P_polar_zed)

	(width1, height1, M1, D1) = load_camera_calibration('/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_zed_helper.yaml')
	(width2, height2, M2, D2) = load_camera_calibration('/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_polarization_camera_helper.yaml')
	# (width2, height2, M2, D2) = load_camera_calibration('calibration_thermal_camera.yaml')

	print(width1, height1)
	print(width2, height2)

	# return

	im1 = np.ones((int(height1), int(width1)))
	im2 = np.zeros((int(height2), int(width2)))

	calculate_map = True

	grid = get_grid()
	grid[:,0]*=-1
	grid[:,1]*=-1

	mean_translation_error = 0
	mean_rotation_error = 0
	font = cv2.FONT_HERSHEY_SIMPLEX
	font_scale = 2

	key = -1
	while key & 0xFF != ord("q"):
		# continue
		# zed.get_parameters()
		data, addr = zed.socket_in.recvfrom(1000)
		zed.parse_packet(data)
		# print(data)

		# if zed.image is not None and zed.image_ok and im_thermal is not None:
		if zed.image is not None and zed.image_ok:

			if calculate_map:

				# P_polar_zed = np.dot(P_polar, inv(P_zed))

				# R = eulerAnglesToRotationMatrix([pitch, yaw, roll])
				# T = np.array([[offx, offy, offz]])
				# print(R,t,R.shape, t.shape)
				# P_polar_zed = rt_to_projection_matrix(R,T)

				a,b,c = rotationMatrixToEulerAngles(R_polar)
				a = np.degrees(a)
				b = np.degrees(b)
				c = np.degrees(c)

				a+=pitch
				b+=yaw
				c+=roll

				R_polar_ = eulerAnglesToRotationMatrix((a,b,c))

				T_polar_ = T_polar.copy()				
				T_polar_[0,0]+=offx
				T_polar_[1,0]+=offy
				T_polar_[2,0]+=offz

				print((a,b,c))
				print(T_polar_.T)


				P2 = rt_to_projection_matrix(R_polar_, T_polar_)

				P12 = P2 @ inv(P1)
				mp_zed_polar, mpx = project_image(im1.shape, im2.shape, P12, M1.copy(), M2.copy(), t = D)

				# mp_polar_zed, _ = project_image(im1.shape, im2.shape, P_polar_zed, M1, M2, t = D)
				# print("mp_zed_polar", mp_zed_polar.shape)
				# mp_polar_zed = cv2.resize(mp_polar_zed, (int(width1), int(height1)))

				calculate_map = False

			im_zed = zed.image.copy()
			im_zed = cv2.cvtColor(im_zed, cv2.COLOR_RGB2BGR)
			zed.image_ok = False
			im_zed = cv2.resize(im_zed, (int(width1), int(height1)))

			# print("zed", im_zed.shape)

			# print(zed.image.shape)
			if polar.image is not None and polar.image_ok:
				im_polar = polar.image.copy()
				polar.image_ok = False

				im_polar = cv2.resize(im_polar, (int(width2), int(height2)))

				im_polar = cv2.undistort(im_polar, M2, D2)
				print("im_polar", im_polar.shape)

				# get corners for zed
				c1 = find_corners(cv2.resize(im_zed, None, fx=0.5, fy=0.5))				
				c2 = find_corners(cv2.resize(im_polar, None, fx=0.5, fy=0.5))

				if c1 is not None and c2 is not None:
					c1 = c1*2
					c2 = c2*2


					_, rvec1, tvec1 = cv2.solvePnP(grid, c1, M1, np.array([]), flags=cv2.SOLVEPNP_ITERATIVE)
					_, rvec2, tvec2 = cv2.solvePnP(grid, c2, M2, np.array([]), flags=cv2.SOLVEPNP_ITERATIVE)

					R_target_1 = cv2.Rodrigues(rvec1)[0]
					P_target_1 = rt_to_projection_matrix(R_target_1, tvec1)
					# print("P_target_1", P_target_1)

					R_target_2 = cv2.Rodrigues(rvec2)[0]
					P_target_2 = rt_to_projection_matrix(R_target_2, tvec2)
					# print("P_target_2", P_target_2)

					P12 = P2 @ inv(P1)
					P12_target = P_target_2 @ inv(P_target_1)

					# print("P12", P12)
					# print("P12_target", P12_target)

					pos_error = np.mean(np.abs(P12-P12_target))
					# print("raw error", np.abs(P12-P12_target))
					# print("pos_error", pos_error)

					r = rotationMatrixToEulerAngles(P12[:3,:3])
					r_target = rotationMatrixToEulerAngles(P12_target[:3,:3])

					# print("roll,pitch,yaw ", r )
					# print("roll_target,pitch_target,yaw_target", r_target )

					err_T = np.abs(P12[:,-1]-P12_target[:,-1])
					# print("err_T", err_T)

					err_R = np.abs(r-r_target)

					mean_translation_error = np.mean(err_T)
					mean_rotation_error = np.mean(err_R)

					print("mean_translation_error", mean_translation_error)
					print("mean_rotation_error", mean_rotation_error)


				# print("polar", im_polar.shape)
				# im_polar = cv2.resize(im_polar, None, fx=0.5, fy=0.5)
				polar_to_zed = cv2.remap(im_polar, mp_zed_polar, None, cv2.INTER_LINEAR)
				polar_to_zed = cv2.cvtColor(polar_to_zed, cv2.COLOR_GRAY2BGR)

				# print(polar_to_zed.shape)
				final = cv2.addWeighted(im_zed, alpha, polar_to_zed, (1-alpha), 0.0)
				

				offy_text = final.shape[0]//15;	offx_text = final.shape[1]//2
				cv2.putText(final, "translation error: "+str(mean_translation_error.round(4)), (offx_text, offy_text), font, font_scale, (0, 0, 0), 5, cv2.LINE_AA)
				cv2.putText(final, "translation error: "+str(mean_translation_error.round(4)), (offx_text, offy_text), font, font_scale, (255, 255, 255), 3, cv2.LINE_AA)
				offy_text = 2*(final.shape[0]//15);	offx_text = final.shape[1]//2
				cv2.putText(final, "rotation error: "+str(mean_rotation_error.round(4)), (offx_text, offy_text), font, font_scale, (0, 0, 0), 5, cv2.LINE_AA)
				cv2.putText(final, "rotation error: "+str(mean_rotation_error.round(4)), (offx_text, offy_text), font, font_scale, (255, 255, 255), 3, cv2.LINE_AA)


				cv2.imshow("image",final)
				cv2.imshow("zed",im_zed)
				cv2.imshow("flir",im_polar)
			else:
				pass
				# cv2.imshow("zed",im_zed)
				# cv2.imshow("flir",im_polar)

			# cv2.imshow("zed",im_zed)
			# cv2.imshow("flir",im_polar)

				

			# im_zed = cv2.resize(im_zed, None, fx=4, fy=4)
			
			# print(im.shape)

			

			# print(im_zed.shape)
			# print(polar_to_zed.shape)
			

			# 
			# cv2.imshow("im_zed",im_zed)
			# cv2.imshow("polar",im_polar)
			# cv2.imshow("map",polar_to_zed)
			# cv2.imshow("map",polar_to_zed)
			

			# print("polar", im.shape)
			# print("mapped", thermal_to_polazed.shape)

			# # if corners is not None:
			# # 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)		

			# if cv2.waitKey(1) & 0xFF == ord('q'):
				# thread_stop=True
				# zed.stop_all()
				# thermal.stop_all()
				# t.join()
				# break
			# break

			key = cv2.waitKey(1)
			
			if key & 0xFF == ord("e"):
				offx-=dx
				calculate_map = True
			elif key & 0xFF == ord("r"):
				offx+=dx
				calculate_map = True
			elif key & 0xFF == ord("s"):
				offy-=dy
				calculate_map = True
			elif key & 0xFF == ord("d"):
				offy+=dy
				calculate_map = True
			elif key & 0xFF == ord("x"):
				offz-=dz
				calculate_map = True
			elif key & 0xFF == ord("c"):
				offz+=dz
				calculate_map = True

			elif key & 0xFF == ord("u"):
				roll-=dr
				calculate_map = True
			elif key & 0xFF == ord("i"):
				roll+=dr
				calculate_map = True
			elif key & 0xFF == ord("j"):
				pitch-=dp
				calculate_map = True
			elif key & 0xFF == ord("k"):
				pitch+=dp
				calculate_map = True
			elif key & 0xFF == ord("n"):
				yaw-=dw
				calculate_map = True
			elif key & 0xFF == ord("m"):
				yaw+=dw
				calculate_map = True
			elif key & 0xFF == ord("+"):
				# print("+")
				D+=dd
				calculate_map = True
				# azi_filter = max(1,azi_filter-1)
				# print(azi_filter)
				# lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("-"):
				# print("-")
				D-=dd
				calculate_map = True

			# print(f"D: {D:.2f}, x: {offx:.2f}, y: {offy:.2f}, z: {offz:.2f}, roll: {roll:.2f}, pitch: {pitch:.2f}, yaw: {yaw:.2f}", end='\r')

		if thread_stop:
			break

			

	thread_stop=True
	zed.stop_acquisition()
	polar.stop_all()
	
	t.join()

if __name__=='__main__':
	main()