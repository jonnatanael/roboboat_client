########################################################################
#
# Copyright (c) 2021, STEREOLABS.
#
# All rights reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

"""
	Read a stream and display the left images using OpenCV
"""
import sys
import pyzed.sl as sl
import cv2, threading, signal
import subprocess

IP = '192.168.90.70'
command_kill = "sudo pkill -f stream_zed.py"

def signal_handler(sig, frame):
	global cam
	cam.close()
	subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_kill], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	exit()

def main():

	global cam

	a_signal = signal.signal(signal.SIGINT, signal_handler)

	
	command_start = f"python stream_zed.py"
	

	subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_start], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

	init = sl.InitParameters()
	# init.camera_resolution = sl.RESOLUTION.HD720
	init.depth_mode = sl.DEPTH_MODE.PERFORMANCE

	ip = "192.168.90.70"
	init.set_from_stream(ip)
	# if (len(sys.argv) > 1) :
	# 	ip = sys.argv[1]
	# 	init.set_from_stream(ip)
	# else :
	# 	print('Usage : python3 streaming_receiver.py ip')
	# 	exit(1)

	cam = sl.Camera()
	status = cam.open(init)
	if status != sl.ERROR_CODE.SUCCESS:
		print("fail")
		print(repr(status))
		exit(1)

	runtime = sl.RuntimeParameters()
	mat = sl.Mat()

	alpha = 0.5

	key = ''
	print("  Quit : CTRL+C\n")
	while key != 113:
		err = cam.grab(runtime)
		if (err == sl.ERROR_CODE.SUCCESS) :
			cam.retrieve_image(mat, sl.VIEW.LEFT)
			im = mat.get_data()
			im = im[...,0:3]
			# im = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
			print(im.shape)
			cv2.imshow("ZED", im)

			key = cv2.waitKey(1)
		else :
			key = cv2.waitKey(1)

	subprocess.call(['ssh', '-t', '-f', "jetson@"+IP, "nohup", command_kill], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	cam.close()

if __name__ == "__main__":
	main()