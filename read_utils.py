from socket import *
import struct, zlib, binascii, time, datetime, signal, sys, cv2
import numpy as np
import inspect
import subprocess
from utils import get_local_IP

def set_bit(value, bit):
    return value | (1<<bit)

def clear_bit(value, bit):
    return value & ~(1<<bit)

magic = b'\x7f\x07\xea\x5a\x3f\x98\x61\xab'
SOF = '87a871df3f826d06'
EOF = 'b0d8485890688b4d'


hdr = {
	'magic': [0, 8],
	'sensor_type': [8, 1],
	'sensor_subtype': [9, 1],
	'sensor_number':[10, 1],
	'direction':[11, 1],
	'payload_type':[12, 1],
	'payload_length_type':[13, 1],
	'payload': [14, -1]
}

sensor_types = { 'rgb_camera': 1, 'polarization_camera': 2, 'thermal_camera': 3, 'lidar': 4, 'radar': 5, 'imu_gps': 6, 'depth': 7}
sensor_subtypes = { 'data': 1, 'command': 2, 'request': 3, 'parameter': 4, 'telemetry': 5}
data_direction = { 'from sensor': 1, 'to sensor': 2}
payload_types = { 'raw': 1, 'jpeg': 2, 'h264': 3, 'pointcloud': 4, 'imu/gps': 5, 'electrical': 6, 'system': 7, 'timesync': 8}
payload_length_types = {'short': 1, 'long': 2}

# GPS
gps_params = {
	'timestamp1': [0, 4, 'uint32'],
	'timestamp2': [4, 4, 'uint32'],
	'msg_counter': [8, 4, 'uint32'],
	'latitude': [12, 4, 'float'],
	'longitude': [16, 4, 'float'],
	'MGRS': [20, 15, 'string'],
	'altitude': [35, 4, 'float'],
	'roll': [39, 4, 'float'],
	'pitch': [43, 4, 'float'],
	'yaw': [47, 4, 'float'],
	'course': [51, 4, 'float'],
	'mode': [55, 2, 'uint16'],
	'accuracy': [57, 4, 'float'],
	'status': [61, 2, 'uint16'],
}

gps_command = { # order is important
	# first is offset in messages, second is length, third is type, fourth is command byte, fifth is command bit
	'command': [0, 2, 'uint16', None, None]
}

# thermal
thermal_params = {
	'command': [0, 1, 'bits'],
	'frame_rate': [1, 4, 'float'],
	'n_images': [5, 4, 'uint32'],
	'shutterless':[9, 1, 'bool'],
	'auto_gain_control':[10, 1, 'uint8'],
	'acquisition':[11, 1, 'bool'],
	'chunk_size': [12, 2, 'uint16'],
	'JPEG_compression': [14, 1, 'char']
}

thermal_command = {
	# first is offset in messages, second is length, third is type, fourth is command byte, fifth is command bit
	'command1': [0, 1, 'bits', None, None],
	'command2': [1, 1, 'bits', None, None],
	'frame_rate': [2, 4, 'float', 0, 0],
	'num_images': [6, 4, 'uint32', 0, 1],
	'shutterless': [10, 1, 'bool', 0, 2],
	'auto_gain_control': [11, 1, 'uint8', 0, 3],
	'acquisition': [12, 1, 'bool', 0, 4],
	'chunk_size': [13, 2, 'uint16', 0, 5],
	'JPEG_compression': [15, 1, 'char', 0, 6],
	'reboot_pi': [16, 1, 'char', 0, 7],
	'sending': [17, 1, 'uint8', 1, 0]
}

# polarization
flir_command = {
	# NOTE: command bits should be set based on parameters
	'command1': [0, 1, 'bits', None, None],
	'command2': [1, 1, 'bits', None, None],
	'gain': [2, 4, 'float', 0, 0],
	'pixel_format': [6, 4, 'float', 0, 1],
	'polarization_algorithm': [10, 4, 'float', 0, 2],
	'frame_rate': [14, 4, 'float', 0, 3],
	'num_images': [18, 4, 'uint32', 0, 4],
	'acquisition': [22, 1, 'uint8', 0, 5],
	'chunk_size': [23, 2, 'uint16', 0, 6],
	'JPEG_compression': [25, 1, 'char', 1, 0],
	'reboot': [26, 1, 'char', 0, 7],
	'sending': [27, 1, 'uint8', 1, 1]
}

flir_params = {
	'command': [0, 1, 'bits'],
	'gain': [1, 4, 'float'],
	'pixel_format': [5, 4, 'float'], # 1 or 2
	'polarization_algorithm':[9, 4, 'float'], # from 1 to 11
	'frame_rate':[13, 4, 'float'],
	'num_images':[17, 4, 'uint32'],
	'acquisition': [21, 1, 'bool'],
	'chunk_size': [22, 2, 'uint16'],
	'JPEG_compression': [24, 1, 'char']
}

def print_header(hdr, filter_id=5):
	a = int.from_bytes(hdr['sensor_type'][-1], 'little')

	sensor_type = int.from_bytes(hdr['sensor_type'][-1], 'little')

	if sensor_type!=filter_id:
		return

	print('Sensor type: ', sensor_types[sensor_type])
	print('Sensor subtype: ', sensor_subtype[int.from_bytes(hdr['sensor_subtype'][-1], 'little')])
	print('Sensor number: ', int.from_bytes(hdr['sensor_number'][-1], 'little'))
	print('Data direction: ', data_direction[int.from_bytes(hdr['direction'][-1], 'little')])
	print('Payload type: ', payload_types[int.from_bytes(hdr['payload_type'][-1], 'little')])
	print('Payload length type: ', payload_types[int.from_bytes(hdr['payload_length_type'][-1], 'little')])
	print('',)

class PacketParser():

	# def __init__(self, sensor_type, in_port=4200, out_port=4200, IP='192.168.90.255'):
	def __init__(self, sensor_type, in_port=4200, out_port=4200, IP='192.168.91.255'):
		self.buffer = bytearray()
		self.parameters = None

		self.data_length = -1
		self.chunks = []
		self.offsets = []

		self.electrical_data = None
		self.system_data = None
		self.sync_data = None

		self.sensor_type = sensor_type
		self.in_port = in_port
		self.out_port = out_port
		self.IP = IP
		# self.IP = '127.0.0.1'
		self.started = False

		self.setup_socket()

		self.signal = signal.signal(signal.SIGINT, self.signal_handler)

		# print("initing PacketParser")
		# print(f'{sensor_type=}')
		# print(f'{in_port=}')
		# print(f'{out_port=}')
		# print(f'{IP=}')

	def signal_handler(self, sig, frame):
		# handler za ctrl+c
		print("CTRL + C pressed, ", sig, frame)
		sys.exit()

	def send_message(self, msg):
		self.socket_out.sendto(msg, (self.IP, self.out_port))

	def setup_socket(self, use_local=True):
		if use_local:
			IP = get_local_IP()
		else:
			IP = self.IP
		# print(f'{use_local=}')

		# print(f'setting up socket for IP: {IP=}')
		self.socket_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
		self.socket_in.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)		
		self.socket_in.bind((IP, self.in_port))

		self.socket_out = socket(AF_INET, SOCK_DGRAM)		
		self.socket_out.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
		self.socket_out.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

	def parse_fields(self, info, data):
		# info is a dict of response information such as offset and type, data is a byte string
		for k,v in data.items():
			c = info[k][-1]
			if c=='bits' or c=='char' or c=='bool' or c=='uint8':
				data[k]=int.from_bytes(v, 'big')
			elif c=='float':
				data[k]=struct.unpack('>f', v)[0]
			elif c=='uint32':
				data[k]=struct.unpack('!I', v)[0]
			elif c=='uint16':
				data[k]=struct.unpack('!H', v)[0]
			elif c=='string':
				data[k]=v.decode("utf-8")

	def compose_message(self, sensor_subtype, sensor_number, sensor_direction, payload_type, payload_length_type, data):
		sensor_type = sensor_types[self.sensor_type].to_bytes(1,'big')
		# print(f'{sensor_type=}')
		sensor_subtype = sensor_subtype.to_bytes(1,'big')
		# print(f'{sensor_subtype=}')
		sensor_number = sensor_number.to_bytes(1,'big')
		# print(f'{sensor_number=}')
		sensor_direction = sensor_direction.to_bytes(1,'big')
		# print(f'{sensor_direction=}')
		payload_type = payload_type.to_bytes(1,'big')
		# print(f'{payload_type=}')
		payload_length_type = payload_length_type.to_bytes(1,'big')
		# print(f'{payload_length_type=}')
		# print(f"{data=}")

		crc = struct.pack("!I", binascii.crc32(data))
		# print("crc", crc.hex())
		# print("data", data.hex())
		length = len(data)
		# print(f'{length=}')
		# print(f'{len(data).to_bytes(2,"big")=}')
		header = magic+sensor_type+sensor_subtype+sensor_number+sensor_direction+payload_type+payload_length_type
		# print(f'{len(header)=}')
		# print(f'{len(data).to_bytes(2,"big")=}')
		# print(f'{crc=}')
		# print(f'{int.from_bytes(crc, "little")=}')

		m = header+len(data).to_bytes(2,'big')+data+crc 

		# print(f"{m=}")
		# print(f"{len(m)=}")


		return m

	def get_header_info(self,data):
		res = {}

		for k,v in hdr.items():
			# print(k,v)
			o = v[0]
			ln = v[1]		
			if k =='payload':
				res[k]=data[o:o+(len(data)-13)]
			else:
				res[k]=int.from_bytes(data[o:o+ln], 'little')
		return res

	def get_parameters(self):
		pass # placeholder

	def start_acquisition(self):
		print("start_acquisition placeholder")
		pass # placeholder

	def stop_acquisition(self):
		print("stop_acquisition placeholder")		
		pass # placeholder

	def set_default_parameters(self):
		print("default params")
		pass # placeholder

	def reboot(self):
		print("reboot placeholder")
		pass # placeholder

	def shutdown(self):
		print("shutdown placeholder")
		pass # placeholder

	def start_all(self):
		print("start_all placeholder")
		pass # placeholder

	def stop_all(self):
		print("stop_all placeholder")
		pass # placeholder

	def check_crc(self, crc, data):
		a = binascii.crc32(data).to_bytes(4,'big')
		# a = binascii.crc32(data).to_bytes(4,'little')
		if crc!=a:
			print("CRC incorrect, check it")
			print(crc.hex(), a.hex())
		
		return crc==a

	def parse_packet(self, packet):
		hdr_info = self.get_header_info(packet)
		# print(hdr_info)

		payload = hdr_info['payload']
		if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor']:
			# print(hdr_info)
			self.parse_payload(payload)

	def parse_payload(self, payload):
		
		sof = payload[0:8].hex()
		eof = payload[-12:-4].hex()

		# print(f'{sof=}')
		# print(f'{SOF=}')


		if sof == SOF:
			self.started = True
			# print("SOF")
			# self.buffer = bytearray()
			self.chunks = []
			self.offsets = []
			# self.pc = []

			data_len = struct.unpack('!I', payload[8:8+4])[0]
			data = payload[12:]

			# print("new buffer")
			# start_timing()
			self.buffer = bytearray(data_len)
			# end_timing("bytearray")
			
			# print(len(self.buffer))
			self.current_position = 0


			# check if eof exists at the end of data
			eof2 = payload[12+data_len:12+data_len+8].hex()

			if eof2 == EOF: # if yes, this is the only payload
				crc = payload[12+data_len+8:12+data_len+8+4]
				self.buffer = bytearray(payload[12:12+data_len])
				self.data_length = data_len

				if self.check_crc(crc, self.buffer):
					self.parse_data()
				else:
					print("CRC check failed")
					# print("actual length: ", len(self.buffer), " expected length: ", self.data_length)
					pass

				return
			else:
				self.data_length = data_len
				self.started = True
				# self.buffer.extend(data)
				self.buffer[self.current_position:self.current_position+len(data)]=data
				self.current_position+=len(data)

		elif eof == EOF:
			# print("EOF")
			if not self.started:
				return

			chunk = struct.unpack('!h', payload[0:2])[0]
			offset = struct.unpack('!I', payload[2:2+4])[0]
			data = payload[6:-12] # from byte 12 to end minus EOF and CRC
			crc = payload[-4:]
			# self.buffer.extend(data) # ONLY APPEND DATA
			self.buffer[self.current_position:self.current_position+len(data)]=data
			self.current_position+=len(data)
			# self.chunks.append(chunk)
			# self.offsets.append(offset)

			if self.check_crc(crc, self.buffer):		
				self.parse_data()
			else:
				# print("actual length: ", len(self.buffer), " expected length: ", self.data_length)
				pass

			self.started = False

		else:
			if not self.started:
				return
			# print("else")

			chunk = struct.unpack('!h', payload[0:2])[0]
			# print(chunk)
			offset = struct.unpack('!I', payload[2:2+4])[0]
			# print(offset)

			data = payload[6:]
			# self.buffer.extend(data) # ONLY APPEND DATA, no chunk number or offset!
			self.buffer[self.current_position:self.current_position+len(data)]=data
			self.current_position+=len(data)
			# self.chunks.append(chunk)
			# self.offsets.append(offset)

class GPSParser(PacketParser):

	# def __init__(self, in_port = 27161):
	def __init__(self, in_port = 4200):
		super().__init__('imu_gps', in_port)

		self.data = None

		self.IP = '192.168.90.77'

	def signal_handler(self, sig, frame):
		self.stop_all()
		sys.exit()

	def start_acquisition(self):
		self.set_parameters(acquisition=1)

		# start backup compass and gps scripts
		command = './gps_script.sh'
		subprocess.call(['ssh', '-T', '-f', "pi@"+self.IP, "nohup", command])

		command = './compass_script.sh'
		subprocess.call(['ssh', '-T', '-f', "pi@"+self.IP, "nohup", command])


	def stop_acquisition(self):
		self.set_parameters(acquisition=0)

		subprocess.call(['ssh', '-T', "pi@"+self.IP, 'sudo pkill -INT -f gps_script.sh'])
		subprocess.call(['ssh', '-T', "pi@"+self.IP, 'sudo pkill -INT -f compass_script.sh'])

	def start_sending(self):
		self.set_parameters(sending=1)

	def stop_sending(self):
		self.set_parameters(sending=0)

	def start_all(self):
		self.set_parameters(acquisition=1, sending=1)

	def stop_all(self):
		self.set_parameters(acquisition=0, sending=0)

	def reboot(self):
		self.set_parameters(reboot=True)
		pass 

	def shutdown(self):
		self.set_parameters(shutdown=True)
		pass

	def set_parameters(self, acquisition=None, sending=None, calibration=None, reboot=None, shutdown=None):
		gps_command = { # value is the relevant bit position
			'acquisition': 0,
			'sending': 1,
			'calibration': 2,
			'reboot': 3,
			'shutdown': 4,
		}

		command_array = bytearray(b'\0'*2)
		command_byte = 0
		value_byte = 0

		for k,v in locals().items(): # be careful of variable names here, otherwise works fine
			if v is not None and k in gps_command:

				c = gps_command[k]
				command_byte = set_bit(command_byte, c)
				if v:
					value_byte = set_bit(value_byte, c)


		command_byte = command_byte.to_bytes(1, byteorder='big')
		value_byte = value_byte.to_bytes(1, byteorder='big')
		command_array[0:1]=command_byte
		command_array[1:2]=value_byte

		m = self.compose_message(sensor_subtypes['command'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command_array)

		self.send_message(m)

	def parse_packet(self, packet):
		hdr_info = self.get_header_info(packet)
		# print(hdr_info)
		payload = hdr_info['payload']
		if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor']:
			if hdr_info['payload_type']==payload_types['imu/gps']:
				self.data = self.parse_gps_data(payload)

	def parse_gps_data(self, data):
		# print(data, len(data))
		data_len = data[0:2].hex()
		crc = data[-4:]
		data = data[2:-4]

		if not self.check_crc(crc, data):
			print("CRC failed"+self)

		res = {}
		for k,v in gps_params.items():
			o = v[0]
			ln = v[1]
			res[k]=data[o:o+ln]

		self.parse_fields(gps_params, res)

		res['latitude'] = np.degrees(res['latitude'])
		res['longitude'] = np.degrees(res['longitude'])

		res['roll'] = np.degrees(res['roll'])
		res['pitch'] = np.degrees(res['pitch'])
		res['yaw'] = np.degrees(res['yaw'])

		# convert MGRS to fields
		res['UTM_zone'] = res['MGRS'][0:2]
		res['UTM_band'] = res['MGRS'][2]
		res['MGRS_square'] = res['MGRS'][3:5]
		res['easting'] = res['MGRS'][5:10]
		res['northing'] = res['MGRS'][10:15]

		return res

class RadarParser(PacketParser):

	def __init__(self, in_port = 4200):


		self.in_port = in_port

		super().__init__('radar', self.in_port)
		self.pc = np.zeros((0,5))


	def signal_handler(self, sig, frame):
		# handler za ctrl+c
		self.stop_all()
		sys.exit()

	def start_acquisition(self):
		self.set_parameters(acquisition=int(True))

	def stop_acquisition(self):
		self.set_parameters(acquisition=int(False))

	def start_sending(self):
		self.set_parameters(sending=int(True))

	def stop_sending(self):
		self.set_parameters(sending=int(False))

	def start_all(self):
		self.set_parameters(acquisition=int(True), sending=int(True))

	def stop_all(self):
		self.set_parameters(acquisition=int(False), sending=int(False))

	def reboot(self):
		self.set_parameters(reboot=int(True))

	def shutdown(self):
		self.set_parameters(shutdown=int(True))

	def set_parameters(self, chunk_size=None, reboot=None, shutdown=None, acquisition=None, sending=None):
		# reboot, shutdown and sending should be either 0 or 1

		radar_command = {
			# name, offset, length, type, related bit in command byte
			'command': [0, 1, 'bits', None],
			'chunk_size': [1, 2, 'uint16', 0],
			'reboot': [3, 1, 'uint8', 1],
			'shutdown': [4, 1, 'uint8', 2],
			'acquisition': [5, 1, 'uint8', 3],
			'sending': [6, 1, 'uint8', 4],
		}		

		length = sum([v[1] for v in radar_command.values()])
		command_array = bytearray(b'\0'*length)
		command_byte = 0

		# check whether function parameters are set
		for k,v in locals().items(): # be careful of variable names here, otherwise works fine
			if v is not None and k in radar_command:
				c = radar_command[k]
				value = int(v).to_bytes(c[1], byteorder='big')
				command_array[c[0]:c[0]+c[1]]=value
				command_byte = set_bit(command_byte, c[3])

		command_byte = command_byte.to_bytes(1, byteorder='big')
		command_array[0:1]=command_byte

		m = self.compose_message(sensor_subtypes['command'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command_array)

		self.send_message(m)

	def parse_data(self):
		data = self.buffer

		if len(data)!=self.data_length:
			print("Data length mismatch")

		# number of points
		n_points = data[0]		
		offset = 1
		point_len = 20

		# print(n_points)

		self.pc = np.zeros((0,5))

		for n in range(n_points):
			point = data[offset:offset+point_len]
			# print(point)
			px,py,pz,v,rcs = [struct.unpack('!f', point[i*4:(i*4)+4])[0] for i in range(5)]
			# self.pc.append([px, py, pz, v, rcs])
			# p = np.array([[px, py, pz, v, rcs]])

			# print(p)

			pr = px
			pa = np.deg2rad(py)
			pe = np.deg2rad(pz)

			# študent
			x = pr * np.cos(pe) * np.sin(pa)
			y = pr * np.cos(pe) * np.cos(pa)				
			z = pr * np.sin(pe)

			# x = pr * np.sin(pa) * np.cos(pe)
			# y = pr * np.sin(pa) * np.sin(pe)				
			# z = pr * np.cos(pa)

			p = np.array([[x, y, z, v, rcs]])

			# print(p.shape)
			self.pc = np.append(self.pc, p, axis=0)
			# sel
			offset+=point_len

		# print(len(self.pc))
		# print("2", self.pc.shape)
		# max_pts = 5000
		# if self.pc.shape[0]>max_pts:
			# self.pc = self.pc[-max_pts:,:]

		# self.pc = np.asarray(self.pc)

class ThermalParser(PacketParser):

	# def __init__(self, in_port = 27131):
	def __init__(self, in_port = 34818):
	# def __init__(self, in_port = 53470):
	# def __init__(self, in_port = 4200):


		# print("in_port", in_port)

		super().__init__('thermal_camera', in_port)

		self.image = None
		self.image_ok = False

		self.params = {
			'frame_rate': 10,
			'n_images': 0,
			'shutterless': 1,
			'auto_gain_control': 1,
			'chunk_size': 512,
			'JPEG_compression': 80
		}

		# self.set_default_parameters()
		self.signal = signal.signal(signal.SIGINT, self.signal_handler)

	def signal_handler(self, sig, frame):
		# handler za ctrl+c
		self.stop_all()
		sys.exit()

	def start_acquisition(self):
		self.set_parameters(acquisition=True, sending=False)

	def stop_acquisition(self):
		self.set_parameters(acquisition=False, sending=False)

	def start_sending(self):
		self.set_parameters(sending=True)

	def stop_sending(self):
		self.set_parameters(sending=False)

	def start_all(self):
		print("start all")
		self.set_parameters(sending=True, acquisition=True)

	def stop_all(self):
		self.set_parameters(sending=False, acquisition=False)

	def reboot(self):
		self.set_parameters(reboot_pi=1)

	def shutdown(self):
		self.set_parameters(reboot_pi=2)

	def set_default_parameters(self):
		self.set_parameters(frame_rate=self.params['frame_rate'], num_images=self.params['n_images'], shutterless=self.params['shutterless'], auto_gain_control=self.params['auto_gain_control'], JPEG_compression=self.params['JPEG_compression'], chunk_size=self.params['chunk_size'])

	def get_parameters(self):
		command = b'\xff'

		m = self.compose_message(sensor_subtypes['request'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command)
		# print(m)
		self.send_message(m)

	def set_parameters(self, acquisition=None, sending=None, frame_rate=None, num_images=None, shutterless=None, auto_gain_control=None, JPEG_compression=None, reboot_pi=None, chunk_size=None):

		length = sum([v[1] for v in thermal_command.values()])

		# set up empty command bytes and data
		command_byte1 = 0
		command_byte2 = 0
		command = bytearray(b'\0'*length)

		# iterate through, set command bits and command values
		for k,v in locals().items():
			if v is not None and k in thermal_command:
				c = thermal_command[k]
				# set command bytes
				if c[2]=='float':
					value = struct.pack(">f", v)
					command[c[0]:c[0]+c[1]]=value
				else:
					value = int(v).to_bytes(c[1], byteorder='big')
					command[c[0]:c[0]+c[1]]=value
				# set bits
				if c[3]==0:
					command_byte1=set_bit(command_byte1, c[4])
				else:
					command_byte2=set_bit(command_byte2, c[4])
		
		command_byte1 = command_byte1.to_bytes(1, byteorder='big')
		command[0:1]=command_byte1
		command_byte2 = command_byte2.to_bytes(1, byteorder='big')
		command[1:2]=command_byte2

		# print(f'{command_byte1=}')
		# print(f'{command_byte2=}')
		# print(f'{length=}')
		# print(f'{sending=}')
		
		m = self.compose_message(sensor_subtypes['command'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command)

		# print(m)
		# print(len(m))

		self.send_message(m)

	def check_crc(self, crc, data):
		a = binascii.crc32(data).to_bytes(4,'big')
		if crc!=a:
			print("CRC incorrect, check it")
			print(crc.hex(), a.hex())
		
		return crc==a

	def parse_packet(self, packet):
		hdr_info = self.get_header_info(packet)
		payload = hdr_info['payload']
		if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor']:
			# if raw sensor, parse parameters
			if hdr_info['payload_type']==payload_types['raw']:
				res = self.parse_parameters(payload)
				self.parameters = res
			else:
				self.parse_payload(payload)

	def parse_parameters(self, data):
		data_len = data[0:2].hex()
		crc = data[-4:]
		data = data[2:-4]

		if not self.check_crc(crc, data):
			print("CRC failed"+self)

		res = {}

		for k,v in thermal_params.items():
			o = v[0]
			ln = v[1]
			res[k]=data[o:o+ln]

		self.parse_fields(thermal_params, res)
		self.parameters = res
		return res

	def parse_data(self):
		data = self.buffer

		timestamp = struct.unpack("<I", data[0:4])[0]
		timestamp2 = struct.unpack("<H", data[4:6])[0]
		counter = struct.unpack("<i", data[6:10])[0]
		# print(counter)
		# min_value = struct.unpack("<h", data[6:8])[0]
		# max_value = struct.unpack("<h", data[8:10])[0]
		# print(min_value, max_value)
		jpeg_data = data[10:]

		if len(data)!=self.data_length:
			print("Data length mismatch")
		else:
			nparr = np.frombuffer(bytes(jpeg_data), np.uint8)
			# self.image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
			# self.image = cv2.imdecode(nparr, cv2.IMREAD_GRAYSCALE)
			self.image = cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)

			# v1 = int((min_value/65636)*255)
			# v2 = int((max_value/65636)*255)
			# print(v1,v2)

			self.image_ok = True

class PolarizationParser(PacketParser):

	# def __init__(self, in_port = 27121):
	def __init__(self, in_port = 34817):

		super().__init__('polarization_camera', in_port)

		self.parameters = None
		# self.display = False
		# self.display = True
		self.image = None
		self.image_ok = False

		self.params = {
			# 'gain': 50,
			'gain': 30,
			'pixel_format': 2,
			'polarization_algorithm': 3,
			'frame_rate': 10,
			'num_images': 0,
			'chunk_size': 512,
			'JPEG_compression': 80
		}

		self.set_default_parameters()

	def signal_handler(self, sig, frame):
		# handler za ctrl+c
		self.stop_all()
		sys.exit()

	def start_acquisition(self):
		self.set_parameters(acquisition=int(True))

	def stop_acquisition(self):
		self.set_parameters(acquisition=int(False))

	def start_sending(self):
		self.set_parameters(sending=int(True))

	def stop_sending(self):
		self.set_parameters(sending=int(False))

	def start_all(self):
		self.set_parameters(sending=int(True), acquisition=int(True))

	def stop_all(self):
		self.set_parameters(sending=int(False), acquisition=int(False))

	def reboot(self):
		self.set_parameters(reboot=1)
		pass

	def shutdown(self):
		self.set_parameters(reboot=2)
		pass

	def check_crc(self, crc, data):
		a = binascii.crc32(data).to_bytes(4,'big')
		# a = binascii.crc32(data).to_bytes(4,'little')
		if crc!=a:
			print("CRC incorrect, check it")
			print(crc.hex(), a.hex())
		
		return crc==a

	def set_default_parameters(self):
		self.set_parameters(gain=self.params['gain'], pixel_format=self.params['pixel_format'], polarization_algorithm=self.params['polarization_algorithm'], frame_rate=self.params['frame_rate'], num_images=self.params['num_images'], chunk_size=self.params['chunk_size'], JPEG_compression=self.params['JPEG_compression'])

	def set_parameters(self, gain=None, pixel_format=None, polarization_algorithm=None, frame_rate=None, num_images=None, acquisition=None, chunk_size=None, JPEG_compression=None, reboot=None, sending=None):
		# print("set_parameters")
		length = sum([v[1] for v in flir_command.values()])

		command_byte1 = 0
		command_byte2 = 0
		command_array = bytearray(b'\0'*length)

		for k,v in locals().items():
			if v is not None and k in flir_command:
				c = flir_command[k]
				# set command bytes
				if c[2]=='float':
					value = struct.pack(">f", v)
					command_array[c[0]:c[0]+c[1]]=value
				else:
					value = int(v).to_bytes(c[1], byteorder='big')
					command_array[c[0]:c[0]+c[1]]=value
				# set bits
				if c[3]==0:
					command_byte1=set_bit(command_byte1, c[4])
				else:
					command_byte2=set_bit(command_byte2, c[4])

		command_byte1 = command_byte1.to_bytes(1, byteorder='big')
		command_array[0:1]=command_byte1
		command_byte2 = command_byte2.to_bytes(1, byteorder='big')
		command_array[1:2]=command_byte2

		m = self.compose_message(sensor_subtypes['command'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command_array)
		self.send_message(m)

	def get_parameters(self):
		command = b'\xff'

		m = self.compose_message(sensor_subtypes['request'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command)
		self.send_message(m)

	def parse_data(self):
		data = self.buffer
		# print(len(data))

		if len(data)!=self.data_length:
			print("Data length mismatch")
		else:
			jpeg_data = data # apparently I removed the timestamp for sending
			nparr = np.frombuffer(bytes(jpeg_data), np.uint8)
			# self.image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
			self.image = cv2.imdecode(nparr, cv2.IMREAD_GRAYSCALE)
			self.image_ok = True

	def parse_packet(self, packet):
		hdr_info = self.get_header_info(packet)
		payload = hdr_info['payload']

		if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor']:
			if hdr_info['sensor_subtype']==sensor_subtypes['parameter']:
				res = self.parse_parameters(payload)
				self.parameters = res
			else:
				# print("parsing data")
				self.parse_payload(payload)

	def print_parameters(self):
		if self.parameters is None:
			return
		print("polarization camera parameters: ")
		for k,v in self.parameters.items():
			if k!='command':
				print(k,v)
		print("")

	def parse_parameters(self, data):
		# print("parsing polar parameters")
		data_len = data[0:2].hex()
		crc = data[-4:]
		data = data[2:-4]

		if not self.check_crc(crc, data):
			print("CRC failed")

		res = {}

		for k,v in flir_params.items():
			o = v[0]
			ln = v[1]
			res[k]=data[o:o+ln]

		self.parse_fields(flir_params, res)

		self.parameters = res

		return res

lidar_command = {
	# NOTE: command bits should be set based on parameters
	'command1': [0, 1, 'bits', None, None],
	'return_mode': [1, 1, 'char', 0],
	'RPM': [2, 2, 'uint16', 1],
	'azimuth_filter': [4, 1, 'uint8', 2], # returns every n-th horizontal point
	'elevation_filter': [5, 1, 'uint8', 3], # returns every n-th beam
	'fov_start': [6, 2, 'uint16', 4],
	'fov_stop': [8, 2, 'uint16', 5],
	'sending': [10, 1, 'uint8', 6],
	'reboot': [11, 1, 'uint8', 7],
}

lidar_points_packet = {
	'return_mode' : [0, 1, 'uint8'],
	'azimuth_filtering' : [1, 1, 'uint8'],
	'elevation_filtering' : [2, 1, 'uint8'],
	'fov_start' : [3, 2, 'uint16'],
	'fov_stop' : [5, 2, 'uint16'],
	'n_packets' : [7, 2, 'uint16'],
	'n_points' : [9, 2, 'uint16']
}

class LidarParser(PacketParser):

	# def __init__(self, in_port = 27141):
	def __init__(self, in_port = 38915):
	# def __init__(self, in_port = 4200):


		super().__init__('lidar', in_port)
		self.setup_socket()

		self.data = None
		self.signal = signal.signal(signal.SIGINT, self.signal_handler)
		self.pc = None
		self.data_ok = False

		self.params = {
			'return_mode' : 2,
			'rpm' : 600,
			'azimuth_filter' : 1,
			'elevation_filter' : 1,
			# 'fov_start' : 0,
			# 'fov_stop' : 360,
			'fov_start' : 320,
			'fov_stop' : 40,
		}

		# self.set_default_parameters()
		self.ts_prev = None

	def parse_packet(self, packet):
		hdr_info = self.get_header_info(packet)
		# print(hdr_info)
		payload = hdr_info['payload']
		# print(hdr_info['sensor_type'], sensor_types[self.sensor_type], hdr_info['direction'], data_direction['from sensor'])
		if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor']:

			if hdr_info['payload_type']==payload_types['pointcloud']:
				self.parse_payload(payload)
			else:
				# print(hdr_info)
				# print(hdr_info['sensor_type'], sensor_types[self.sensor_type], hdr_info['direction'], data_direction['from sensor'], hdr_info['sensor_subtype'])

				# print('parsing params')
				self.parse_parameters(payload)

			# if hdr_info['sensor_subtype']==sensor_subtypes['parameter']:
			# 	self.parse_parameters(payload)
			# else:
				

	def parse_parameters(self, payload):
		print(payload)
		pass

	def signal_handler(self, sig, frame):
		self.stop_all()
		sys.exit()

	def acquisition_byte(self, acquisition=0, sending=0):
		res = 0
		if acquisition:
			res = set_bit(res, 0)
		if sending:
			res = set_bit(res, 1)

		res = res.to_bytes(1, byteorder='big')
		# print(res)
		return res

	def start_sending(self):
		self.set_parameters(sending=1)

	def stop_sending(self):
		self.set_parameters(sending=0)

	def start_acquisition(self):
		self.set_parameters(acquisition=1)

	def stop_acquisition(self):
		self.set_parameters(acquisition=0)

	def start_all(self):
		self.set_parameters(sending=1, acquisition=1) # actually start all
		# self.set_parameters(sending=0, acquisition=1) # just start acquisition

	def stop_all(self):
		self.set_parameters(sending=0, acquisition=0)

	def reboot(self):
		self.set_parameters(reboot=1)

	def shutdown(self):
		self.set_parameters(reboot=2)

	def set_default_parameters(self):
		# print(self.params['fov_start'])
		# print(self.params['fov_stop'])
		self.set_parameters(azimuth_filter=self.params['azimuth_filter'], elevation_filter=self.params['elevation_filter'], fov_start=self.params['fov_start'], fov_stop=self.params['fov_stop'])

	def set_parameters(self, return_mode=None, rpm=None, azimuth_filter=None, elevation_filter=None, fov_start=None, fov_stop=None, acquisition=None, sending=None, reboot=None):
		# NOTE: by design, only one of the parameters should be not None at a time

		length = sum([v[1] for v in lidar_command.values()])
		command_array = bytearray(b'\0'*length)
		command_byte = 0

		# check whether function parameters are set
		for k,v in locals().items(): # be careful of variable names here, otherwise works fine
			if v is not None and k in lidar_command:
				if k == 'sending' or k=='acquisition':
					continue
				# print(k,v)
				c = lidar_command[k]
				value = int(v).to_bytes(c[1], byteorder='big')
				command_array[c[0]:c[0]+c[1]]=value
				command_byte = set_bit(command_byte, c[3])

		# set sending byte separately, TODO this should be reworked a bit
		if (acquisition is not None) or (sending is not None):

			command_byte = set_bit(command_byte, 6)

			data_byte = 0

			if acquisition is not None:

				# print("acquisition")
				# set bit 0 to 1
				# set bit 1 to acquisition
				data_byte = set_bit(data_byte, 0)
				if acquisition==0:
					# print("acq 0")
					data_byte = clear_bit(data_byte, 1)
				else:
					# print("acq 1")

					data_byte = set_bit(data_byte, 1)

			if sending is not None:
				# print("sending")

				data_byte = set_bit(data_byte, 4)


				if sending==0:
					print("sending 0")

					data_byte = clear_bit(data_byte, 5)
				else:
					print("sending 1")

					data_byte = set_bit(data_byte, 5)
				# set bit 4 to 1
				# set bit 5 to sending

			data_byte = data_byte.to_bytes(1, byteorder='big')

			command_array[10:11]=data_byte
			# print(f'{data_byte=}')

		command_byte = command_byte.to_bytes(1, byteorder='big')
		command_array[0:1]=command_byte

		# print(f'{length=}')


		m = self.compose_message(sensor_subtypes['command'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command_array)

		# print(f'{len(m)=}')
		# print(f'{m=}')

		self.send_message(m)

	def parse_data(self):
		data = self.buffer
		data_info = data[0:11]
		points = data[11:]

		res = {}

		for k,v in lidar_points_packet.items():
			o = v[0]
			ln = v[1]
			res[k]=data[o:o+ln]

		self.parse_fields(lidar_points_packet, res)

		# print(res)
		# print(f'{len(points)=}')

		self.parse_points(res, points)

	def parse_points(self, res, data):
		f = 1e-3 # from mm to m, I guess
		pc = []
		offsets = [(8+(7*res['n_points']))*n for n in range(0,res['n_packets'])]
		# print(len(offsets))
		# print(f'{len(data)=}')

		# print(res['n_packets'], res['n_points'])

		# self.pc = None
		# self.data_ok = False
		
		for off in offsets:
			try:
				ts = struct.unpack("!d", data[off:off+8])[0]
				chunk = data[off+8:off+8+res['n_points']*7]

				# print(res['n_points'])
				
				beam = 0
				ret = 0
				for i in range(res['n_points']-1):

					s = i*7
					if s+2>len(chunk):
						continue
					# print(len(chunk), s)
					px = struct.unpack("!h", chunk[s:s+2])[0]*f
					py = struct.unpack("!h", chunk[s+2:s+4])[0]*f
					pz = struct.unpack("!h", chunk[s+4:s+6])[0]*f
					# refl = struct.unpack("!h", chunk[s+6:s+8])[0]*f
					# print(refl)
					# print(x)
					# print(px,py,pz)

					

					pc.append([px, py, pz, beam])

					# we use dual return, so two sequential points have the same beam
					ret = (ret+1)%2
					if ret==0:
						beam = (beam+1)%16

					# if i < res['n_points']-1:
					
						# print(beam)
			except Exception as e:
				print("exception", e)

		self.pc = np.asarray(pc, dtype=np.float32)
		self.data_ok = True

		ts = datetime.datetime.now()
		if self.ts_prev is not None:
			dt = ts-self.ts_prev
			fps = 1/dt.total_seconds()
			print(f"FPS lidar: {fps}")
		self.ts_prev = ts
	
	def get_parameters(self, status=None, diagnostics=None):
		if status is not None:
			command = b'\x00'
		else:
			command = b'\x01'

		m = self.compose_message(sensor_subtypes['request'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command)
		self.send_message(m)

system_params = {
	'cpu_1': [0, 1, 'uint8'], # 255 means unused or n/a
	'cpu_2': [1, 1, 'uint8'],
	'cpu_3': [2, 1, 'uint8'],
	'cpu_4': [3, 1, 'uint8'],
	'cpu_5': [4, 1, 'uint8'],
	'cpu_6': [5, 1, 'uint8'],
	'cpu_7': [6, 1, 'uint8'],
	'cpu_8': [7, 1, 'uint8'],
	'gpu_load': [8, 1, 'uint8'],
	'ARM_clock': [9, 2, 'uint16'],
	'ARM_temp': [11, 1, 'uint8'],
	'SD_free_space': [12, 1, 'uint8'],
	'SSD_free_space': [13, 2, 'uint16'],
}

electrical_params = {
	'battery_current': [0, 4, 'float'],
	'5V_current': [4, 4, 'float'],
	'12V_current': [8, 4, 'float'],
	'ADC': [12, 4, 'float'],
	'3V3': [16, 4, 'float'],
	'12V_voltage': [20, 4, 'float'],
	'5V_voltage': [24, 4, 'float'],
	'battery_voltage': [28, 4, 'float'],
}

sync_params = {
	'NTP_offset': [0, 4, 'float'],
	'NTP_jitter': [4, 4, 'float']
}

class RGBParser(PacketParser):

	def __init__(self, side):

		if side=='front':
			self.in_port = 32768
		else:
			self.in_port = 32896

		super().__init__('rgb_camera', self.in_port)

		if side=='front':
			self.IP = '192.168.90.74'
			self.number = 1
		elif side=='side':
			self.IP = '192.168.90.71'
			self.number = 2
		else:
			self.IP = '192.168.90.70'
			self.number = 3

		self.side = side
		self.image = None
		self.image_ok = False
		self.data_ok = False

	def signal_handler(self, sig, frame):
		self.stop_all()
		sys.exit()

	def start_acquisition(self):
		command = 'python3 robocoln_2/stereo/stream.py'
		# command = 'python3 robocoln_2/stereo/stream_calibration.py'
		subprocess.call(['ssh', '-T', '-f', "pi@"+self.IP, "nohup", command])

	def stop_acquisition(self):
		subprocess.call(['ssh', '-T', "pi@"+self.IP, 'sudo killall -INT python3'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

	def start_sending(self):
		self.start_acquisition()
		# print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))

	def stop_sending(self):
		self.stop_acquisition()
		# print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))

	def start_all(self):
		self.start_acquisition()
		# print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))

	def stop_all(self):
		self.stop_acquisition()
		# print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))

	def reboot(self):
		subprocess.call(['ssh', '-T', "pi@"+self.IP, 'sudo reboot now'])

	def shutdown(self):
		subprocess.call(['ssh', '-T', "pi@"+self.IP, 'sudo shutdown now'])

	def parse_packet(self, packet):
		hdr_info = self.get_header_info(packet)
		# print(hdr_info)

		payload = hdr_info['payload']
		if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor']:
			# print("parsing")
			self.parse_payload(payload)

	def parse_data(self):
		jpeg_data = self.buffer
		# print(jpeg_data)

		if len(jpeg_data)!=self.data_length:
			print("Data length mismatch")
		else:
			nparr = np.frombuffer(bytes(jpeg_data), np.uint8)
			self.image = cv2.imdecode(nparr, cv2.IMREAD_GRAYSCALE)
			self.image_ok = True
			self.data_ok = True

def convert_16bit_to_colormap(ushort_mat):
    # Normalize the 16-bit matrix to the range [0, 255]
    normalized_mat = cv2.normalize(ushort_mat, None, 0, 255, cv2.NORM_MINMAX)

    # Convert the normalized matrix to 8-bit
    normalized_mat = np.uint8(normalized_mat)

    # Apply a colormap to the normalized 8-bit matrix
    colormap_mat = cv2.applyColorMap(normalized_mat, cv2.COLORMAP_JET)

    return colormap_mat

class DepthParser(PacketParser):
	# NOTE: kinda deprecated

	# def __init__(self, in_port = 27113):
	def __init__(self, in_port = 32774, output_raw=False):

		self.in_port = in_port
		self.image = None
		self.image_ok = False

		self.max_depth = 10
		# self.image_size = (376, 672)
		self.image_size = (320, 576)

		self.output_raw = output_raw

		super().__init__('depth', self.in_port)

	def signal_handler(self, sig, frame):
		self.stop_all()
		sys.exit()

	def start_acquisition(self):
		print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))
		return
		command = "ZED_SVO_Recording /mnt/SSD/$(date -d 'today' +'%Y-%m-%d-%H-%M-%S').svo"
		subprocess.call(['ssh', '-T', '-f', "jetson@"+self.IP, "nohup", command])

	def stop_acquisition(self):
		print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))
		return
		subprocess.call(['ssh', '-T', "jetson@"+self.IP, 'sudo killall ZED_SVO_Recording'])

	def start_sending(self):
		print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))

	def stop_sending(self):
		print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))

	def start_all(self):
		print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))

	def stop_all(self):
		print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))

	def reboot(self):
		print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))
		return
		command = "sudo reboot now"
		subprocess.call(['ssh', '-T', '-f', "jetson@"+self.IP, "nohup", command])

	def shutdown(self):
		print("function {} not implemented for class {}".format(inspect.stack()[0][3],self.__class__.__name__))
		return
		command = "sudo shutdown now"
		subprocess.call(['ssh', '-T', '-f', "jetson@"+self.IP, "nohup", command])

	def parse_data(self):

		png = self.buffer
		# print(f"buffer length {len(self.buffer)}")
		# print(f'{self.output_raw=}')

		if len(png)!=self.data_length:
			print("Data length mismatch")
		else:
			nparr = np.frombuffer(bytes(png), np.uint8)
			# print(f'{nparr.shape=}')
			self.image = cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)
			# print(f'{self.image.shape=} {self.image.dtype=}')

			# normalize
			if not self.output_raw:
				image = self.image.copy()
				image =  cv2.normalize(image, None, 0, 255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
				self.image = cv2.applyColorMap(image, cv2.COLORMAP_PARULA)
			else:
				self.image = self.image.astype(np.float32)


			self.image_ok = True

		return

		data = self.buffer
		self.image = np.frombuffer(bytes(self.buffer), np.uint8)
		# self.image = np.frombuffer(bytes(self.buffer), np.float32)
		# self.image = np.reshape(self.image, self.image_size)
		self.image = np.reshape(self.image, (self.image_size[0], self.image_size[1], 4))

		image = self.image.copy()
		# image[~np.isfinite(image)]=0
		# image/=self.max_depth
		# image=(image*255).astype(np.uint8)

		# image = cv2.applyColorMap(image, cv2.COLORMAP_PARULA)

		# print(f'{image.shape=} {image.dtype=}')

		self.image = image
		self.image_ok = True

start_time = time.time()
def start_timing():
	global start_time

	start_time = time.time()

def end_timing(s=''):


	print(f'{s} took: {(time.time() - start_time):.4f} seconds')

	# print("--- %s seconds ---" % (time.time() - start_time))

class ZEDParser(PacketParser):

	# def __init__(self, in_port = 27113):
	# def __init__(self, in_port = 32774):
	def __init__(self, in_port = 35072):

		self.IP = '192.168.90.70'

		super().__init__('rgb_camera', in_port)

		
		# print(f"{in_port=}")

		self.data = None
		self.signal = signal.signal(signal.SIGINT, self.signal_handler)
		self.image = None
		self.image_ok = False

		self.ts_prev = None


	def signal_handler(self, sig, frame):
		sys.exit()

	def start_acquisition(self):
		self.set_parameters(acquisition=1)

	def stop_acquisition(self):
		self.set_parameters(acquisition=0)

	def start_sending_both(self):
		self.set_parameters(sending_rgb=1, sending_depth=1)

	def start_sending_rgb(self):
		self.set_parameters(sending_rgb=1)

	def start_sending_depth(self):
		self.set_parameters(sending_depth=1)

	def stop_sending_both(self):
		self.set_parameters(sending_rgb=0, sending_depth=0)

	def stop_sending_rgb(self):
		self.set_parameters(sending_rgb=0)

	def stop_sending_depth(self):
		self.set_parameters(sending_depth=0)	

	def start_all(self):
		self.set_parameters(acquisition=1, sending_rgb=1, sending_depth=1)

	def stop_all(self):
		self.set_parameters(acquisition=0, sending_rgb=0, sending_depth=0)

	def reboot(self):
		self.set_parameters(reboot=1)

	def shutdown(self):
		self.set_parameters(shutdown=1)

	def set_resolution(self, height, width):
		self.set_parameters(set_resolution=1, set_width=width, set_height=height)

	def set_parameters(self, chunk_size=None, reboot=None, shutdown=None, acquisition=None, sending_rgb=None, sending_depth=None, set_resolution=None, set_width=None, set_height=None):
		# reboot, shutdown and sending should be either 0 or 1

		zed_command = {
			# name, offset, length, type, related bit in command byte
			'command': [0, 1, 'bits', None],
			'reboot': [1, 1, 'uint8', 0],
			'shutdown': [2, 1, 'uint8', 1],
			'acquisition': [3, 1, 'uint8', 2],
			'sending_rgb': [4, 1, 'uint8', 3],
			'sending_depth': [5, 1, 'uint8', 4],
			'set_resolution': [6, 1, 'uint8', 5],
			'height': [7, 2, 'uint16', None],
			'width': [9, 2, 'uint16', None],

			# 'chunk_size': [1, 2, 'uint16', 0],
			# 'reboot': [3, 1, 'uint8', 1],
			# 'shutdown': [4, 1, 'uint8', 2],
			# 'acquisition': [5, 1, 'uint8', 3],
			# 'sending': [6, 1, 'uint8', 4],
		}		

		length = sum([v[1] for v in zed_command.values()])
		command_array = bytearray(b'\0'*length)
		command_byte = 0

		# print(f'{length=}')
		# print(f'{command_array=}')

		# check whether function parameters are set
		for k,v in locals().items(): # be careful of variable names here, otherwise works fine
			# print(k,v)
			if v is not None and k in zed_command:
				c = zed_command[k]
				value = int(v).to_bytes(c[1], byteorder='big')
				# print(f'{k=}')
				# print(f'{value=}')
				if k=='set_resolution':
					if set_width is not None and set_height is not None:
						command_array[c[0]:c[0]+c[1]]=value
						command_byte = set_bit(command_byte, c[3])

						# set h/w fields
						height_values = zed_command['height']
						width_values = zed_command['width']
						command_array[height_values[0]:height_values[0]+height_values[1]]=int(set_height).to_bytes(height_values[1], byteorder='little')
						command_array[width_values[0]:width_values[0]+width_values[1]]=int(set_width).to_bytes(width_values[1], byteorder='little')
					else:
						print("height and/or width not set")
				else:
					command_array[c[0]:c[0]+c[1]]=value
					command_byte = set_bit(command_byte, c[3])

		command_byte = command_byte.to_bytes(1, byteorder='big')
		command_array[0:1]=command_byte

		# print(f'{command_array=}')
		# print(f'{bytes(command_byte).hex()=}')

		# print(f'{length=}')


		m = self.compose_message(sensor_subtypes['command'], 1, data_direction['to sensor'], payload_types['raw'], payload_length_types['short'], command_array)

		# print(f'{m=}')
		# print(f'{len(m)=}')

		self.send_message(m)

	# def parse_packet(self, packet):
	# 	hdr_info = self.get_header_info(packet)
	# 	self.parse_payload(hdr_info['payload'])

	def parse_packet(self, packet):
		hdr_info = self.get_header_info(packet)
		# print(hdr_info)

		# print(hdr_info.keys())
		# print(f'{hdr_info["sensor_type"]=}')
		# print(f'{hdr_info["sensor_number"]=}')
		# print(f'{hdr_info["direction"]=}')
		# print(f'{hdr_info["payload_type"]=}')
		# print(f'{hdr_info["payload_length_type"]=}')

		payload = hdr_info['payload']

		# if hdr_info['sensor_type']==sensor_types[self.sensor_type]:
		# if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor']:
		if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor'] and hdr_info['payload_type']==payload_types['jpeg']:
			# print(hdr_info.keys())
			# print(f'{hdr_info["sensor_type"]=}')
			# print(f'{hdr_info["sensor_number"]=}')
			# print(f'{hdr_info["direction"]=}')
			# print(f'{hdr_info["payload_type"]=}')
			# print(f'{hdr_info["payload_length_type"]=}')
			self.parse_payload(payload)

	def parse_data(self):

		# data = self.buffer
		# self.image = np.frombuffer(bytes(self.buffer), np.uint8)

		# # self.image = np.asarray(data, dtype=np.uint8)
		# self.image = np.reshape(self.image, (376, 672, 4))
		# # self.image = np.reshape(self.image, (1080, 1920, 4))
		# # print(f'{self.image.shape}')

		# self.image_ok = True

		# # end_timing("image parsing")

		# return

		# print(self.buffer)

		jpeg_data = self.buffer
		# print(f"buffer length {len(self.buffer)}")
		

		if len(jpeg_data)!=self.data_length:
			print("Data length mismatch")
		else:
			nparr = np.frombuffer(bytes(jpeg_data), np.uint8)
			# print(f'{nparr.shape=}')
			self.image = cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)
			# print(f'{self.image.shape=}')

			# self.image = cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB)
			self.image_ok = True

			ts = datetime.datetime.now()
			if self.ts_prev is not None:
				dt = ts-self.ts_prev
				fps = 1/dt.total_seconds()
				# print(f"FPS ZED: {fps}")
			self.ts_prev = ts

		# print(self.image)

class TelemetryParser(PacketParser):

	def __init__(self, sensor_type, sensor_number=1):
		super().__init__(sensor_type)

		# print('TelemetryParser')

		in_port = 4200
		self.IP='192.168.91.255'
		self.sensor_number = sensor_number

		self.setup_socket(use_local=False)

		self.signal = signal.signal(signal.SIGINT, self.signal_handler)

		self.electrical_data = {}
		self.system_data = {}
		self.sync_data = {}

	def signal_handler(self, sig, frame):

		sys.exit()

	def parse_packet(self, packet):
		hdr_info = self.get_header_info(packet)

		# if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['sensor_number']==self.sensor_number:
		# 	print(hdr_info)

		if hdr_info['sensor_type']==sensor_types[self.sensor_type] and hdr_info['direction']==data_direction['from sensor'] and hdr_info['sensor_subtype']==sensor_subtypes['telemetry'] and hdr_info['sensor_number']==self.sensor_number:
			if not hdr_info['sensor_number']==self.sensor_number:
				pass
				# return
				# print(hdr_info)		
			if hdr_info['payload_type']==payload_types['electrical']:
				hdr_info['sensor_number']
				self.electrical_data = self.parse_message(hdr_info['payload'], electrical_params)
			elif hdr_info['payload_type']==payload_types['system']:
				self.system_data = self.parse_message(hdr_info['payload'], system_params)
			elif hdr_info['payload_type']==payload_types['timesync']:
				self.sync_data = self.parse_message(hdr_info['payload'], sync_params)

	def parse_message(self, payload, params):
		# print(payload)
		# print("parse_message")
		crc = payload[-4:]
		data = payload[2:-4]
		data_length = payload[:2]
		# print(f'{data_length=}')
		# print(f'{data=}')
		# print(f'{crc.hex()=}')
		res = {}
		# print(len(data))

		if not self.check_crc(crc, data):
			print("CRC failed")
		else:
			pass
			# print("CRC OK")
		# 	return res

		for k,v in params.items():
			o = v[0]
			ln = v[1]
			res[k]=data[o:o+ln]

		self.parse_fields(params, res)
		return res
