from socket import *
# import time, datetime, math
import numpy as np
# import datetime
import time, threading
from multiprocessing import Process, Lock
from shared_memory import *
from multiprocessing import cpu_count, current_process
import cv2, os, signal, sys
import datetime


from read_utils import PolarizationParser, LidarParser
# from live_client import load_camera_calibration

from calibration_utils import *

thread_stop = False
lidar = None
pc = None

calib_fn = 'helper_calibrations/calibration_polarization_camera_helper.yaml'

(width, height, M, D) = load_camera_calibration(calib_fn)
R_lidar, T_lidar, C_lidar = load_lidar_calibration(f"helper_calibrations/calibration_lidar_polarization_camera.yaml")

patternsize = (3,5)

n_iter = 1000
repr_err = 0.001
err_thr = 0.5
err_thr = 1.0

lock1 = Lock()


# def worker():
# 	global thread_stop, pc, lidar

# 	while True:
# 		# print("lidar thread")
# 		data, addr = lidar.socket_in.recvfrom(1000)
# 		# print(addr)
# 		lidar.parse_packet(data)
# 		if lidar.pc is not None:
# 			# print(len(R.pc))
# 			pc = lidar.pc
# 			# pc = pc[:,0:3]
# 			# print(pc)

# 		# print(thread_stop)

# 		if thread_stop:
# 			break

def lidar_reader(shr_name, N = 60000):

	parser = LidarParser()
	parser.start_all()

	existing_shm = shared_memory.SharedMemory(name=shr_name)
	np_array = np.ndarray((N, 4), dtype=np.float32, buffer=existing_shm.buf)

	def term(a,b):
		parser.stop_all()
		sys.exit()

	signal.signal(signal.SIGTERM, term)

	while True:
		try:
			data, addr = parser.socket_in.recvfrom(1000)
			parser.parse_packet(data)
			if parser.pc is not None and parser.data_ok:
				lock1.acquire()
				pc = parser.pc
				np_array[:pc.shape[0],:]=pc
				np_array[pc.shape[0]:, :] = 0
				lock1.release()
		except (KeyboardInterrupt, SystemExit):
			print("Exiting...")
			break
		

def create_shm_lidar(N):
	a = np.zeros(shape=(N, 4), dtype=np.float32)

	shm = shared_memory.SharedMemory(create=True, size=a.nbytes)
	np_array = np.ndarray(a.shape, dtype=np.float32, buffer=shm.buf)
	np_array[:] = a[:]
	return shm, np_array

def main():
	global thread_stop, pc, lidar

	window_name = "FLIR"
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

	R = PolarizationParser()
	# R.start_sending()
	R.start_all()

	# threading solution
	# lidar = LidarParser()
	# # lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
	# lidar.start_all()

	# t = threading.Thread(target=worker)
	# t.start()

	# multiprocess solution
	lidar_height = 60000
	shr_lidar, pc = create_shm_lidar(lidar_height)
	p1 = Process(target=lidar_reader, args=(shr_lidar.name,))
	p1.start()

	ts_prev = None
	detect_target = True
	detect_target = False

	grid = get_grid()
	cnt = 0

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		# print(data)

		if R.image is not None and R.image_ok:
			# print(R.image.shape)
			ts = datetime.datetime.now()

			# R.image = cv2.undistort(R.image, M, D)
			im = R.image.copy()
			im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			R.image_ok = False



			# print(im.shape)
			if detect_target:
				corners = find_corners(im)
				# print(corners)
				if corners is not None:

					_, rvec, tvec, _ = cv2.solvePnPRansac(grid, corners, M, D, iterationsCount = n_iter, reprojectionError = repr_err)
					pts, _ = cv2.projectPoints(grid, rvec, tvec, M, distCoeffs=D)

					e = np.mean(np.abs(pts-corners))
					print(e)

					if e>err_thr:
						timestamp = int(round(ts.timestamp()))

						cv2.imwrite(f"{out_dir}{timestamp}_{e:.3f}.jpg", im)

					
					cv2.drawChessboardCorners(im, patternsize, corners, True)

			if pc is not None:
				ldr = pc.copy()
				ldr[:,:3] = (C_lidar @ ldr[:,:3].T).T
				ldr = ldr[~np.all(ldr[:,0:3]==0, axis=1),:] # remove zero points

				print(ldr.shape, ldr.dtype)
				pts, colors, _ = project_lidar_points(ldr[:,:3].T, im.shape, R_lidar, T_lidar, M, D, rn=3)


				# print(pts)

				if pts is not None and pts.shape[1]!=0:
					for pt, color in zip(pts,colors):
						# print(pt)
						# c = tuple([int(x*255) for x in color][::-1])
						# c = tuple([int(x*255) for x in color])
						c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)])
						try:						
							cv2.circle(im, (int(pt[0]),int(pt[1])), radius=5, color=c, thickness=-1)
						except:
							pass
					

			cv2.imshow(window_name, im)			

			if cv2.waitKey(1) & 0xFF == ord('q'):
				# thread_stop=True
				R.stop_all()
				# t.join()
				break
			# break

	p1.terminate()
	p1.join()
	shr_lidar.close()
	shr_lidar.unlink()
	R.stop_all()

if __name__=='__main__':
	main()