
import zlib, struct, datetime
import numpy as np
from matplotlib import pyplot as plt
# from mayavi import mlab

header_format = {
	'formatVer': [0, 4, 'int32'],
	'creatTS': [4, 8, 'double'],
	'tagSize': [12, 4, 'int32'],
	'uncomprChunkSize': [16, 4, 'int32'],
	'comprType': [20, 4, 'int32'],
	'txtSize': [24, 4, 'int32'],
	# 'text': [0, 0, ''],
	# '': [0, 0, ''],
}

tag_format = {
	'sysTime': [0, 8, 'double'],
	'intTime': [8, 4, 'uint32'],
	'recNo': [12, 8, 'uint64'],
	'compressedChunkSize': [20, 4, 'int32'],
}

packet_format = {

	'?': [0, 8, ''],
	# 'h': [0, 10, ''],

	# 'a': [0, 8, ''],
	# 'b': [1208, 8, ''],

	# 'ts': [0, 4, 'int32'],	
	
	'blocks': [8, 12*100, ''],
	'ts': [1208, 4, 'uint32'],
	'mode': [1212, 1, ''],
	'model': [1213, 1, ''],	
}

block_format = {
	'flag': [0, 2, ''], # must be \xFF\xEE
	'azimuth': [2, 2, 'uint16'],
	'points': [4, 32*3, '']
}

point_format = {
	'distance': [0, 2, 'uint16'],
	'reflectivity': [2, 1, 'uint8']
}

# chunk = 1024*packet
# packet = 1216 = 8 bytes of sth? + 12 blocks + 8 bytes of metadata

def parse_fields(info, data):
	# info is a dict of response information such as offset and type, data is a byte string
	for k,v in data.items():
		c = info[k][-1]
		if c=='bits' or c=='char' or c=='bool' or c=='uint8':
			data[k]=int.from_bytes(v, 'big')
		elif c=='float':
			data[k]=struct.unpack('>f', v)[0]
		elif c=='double':
			data[k]=struct.unpack('d', v)[0]
		elif c=='uint64':
			data[k]=struct.unpack('q', v)[0]
		elif c=='uint32':
			data[k]=struct.unpack('I', v)[0]
		elif c=='int32':
			data[k]=struct.unpack('i', v)[0]
		elif c=='uint16':
			data[k]=struct.unpack('H', v)[0]
		elif c=='int16':
			data[k]=struct.unpack('h', v)[0]
		elif c=='string':
			data[k]=v.decode("utf-8")

def beam2ele(beam):
	# static inline double beam2ele(int beam) {return rad(beam%2 ? beam : beam - 15);}
	return np.deg2rad(beam-15) if beam%2==0 else np.deg2rad(beam)

def uncompress_chunk(chunk):
	res = []
	chunk = zlib.decompress(chunk, wbits=0)
	# print("decompress len", len(a))
	# print(a.hex())

	ts = struct.unpack('d', chunk[0:8])[0]
	date = datetime.datetime.fromtimestamp(ts)
	print("ts:", ts, date)
	# packet = a[8:]
	packet_len = 1216
	block_size = 100

	for i in range(1024):
		pos = i*packet_len
		P = chunk[pos:pos+packet_len]
		# x = 1216
		# print(P[:x], P[:x].hex())
		packet = parse(packet_format, chunk[pos:pos+packet_len])
		# print(packet['ts'],packet['model'].hex(),packet['mode'].hex(), packet['?'])

		for j in range(12):
			B = packet['blocks'][j*block_size:j*block_size+block_size]
			# az = B[2:4]
			# print(j, struct.unpack('<H', az)[0], struct.unpack('>H', az)[0], struct.unpack('<h', az)[0], struct.unpack('>h', az)[0])
			block = parse(block_format, B)
			azi = block['azimuth']/100
			azi = np.deg2rad(azi)
			# print(block)
			

			# for k in range(32):
			# 	cur_point = block['points'][k*3:k*3+3]
			# 	# print(cur_point)
			# 	point = parse(point_format, cur_point)
			# 	# print((point['distance']*2)*1e-3)
			# 	# print(point)
			# 	# TODO convert to carthesian coordinates
			# 	# a = np.cos(beam2ele())
			# 	x = point['distance']*np.sin(azi)
			# 	y = point['distance']*np.cos(azi)
			# 	# z = np.sin(beam2ele())

			for k in range(16):
				cur_point = block['points'][k*3:k*3+3]
				# print(cur_point)
				point = parse(point_format, cur_point)
				# print((point['distance']*2)*1e-3)
				# print(point)
				# convert to carthesian coordinates
				a = ((point['distance']*2)*1e-3)*np.cos(beam2ele(k))
				x = a*np.sin(azi)
				y = a*np.cos(azi)
				z = ((point['distance']*2)*1e-3)*np.sin(beam2ele(k))

				# print(x,y,z)
				res.append((x,y,z, point['reflectivity']))

				# carthesian
				# TPoint pt = {float(a*sin(azi[j])), float(a*cos(azi[j])), float(r[j]*sin(beam2ele(k))), pkt.blocks[i].points[j*kNBeams+k].reflectivity, unixTime + lidTOH + t};
				# 

		# for j in range(12):
		# 	block_pos = 

		# ts2 = struct.unpack('I', packet['ts'])[0]
		# print(ts2)
		# print(packet['a'], packet['b'])
		# print(packet['a'].hex(), packet['b'].hex())
		# print(packet['ts'])
		# print(packet['h'].hex())
	# print(len(res))
	return np.array(res)

def parse(info, data):
	res = {}
	for k,v in info.items():

		o = v[0]
		ln = v[1]
		res[k]=data[o:o+ln]

	# print(res)
	parse_fields(info, res)

	return res

def draw_cs(ln=1,lw = 20):
	
	black = (0,0,0)

	# draw coordinate system
	# mlab.points3d(0,0,0, color=black, scale_factor = 5)
	l1 = mlab.plot3d([0.0, ln], [0, 0], [0, 0], color=black, line_width=lw)
	l2 = mlab.plot3d([0.0, 0], [0, ln], [0, 0], color=black, line_width=lw)
	l3 = mlab.plot3d([0.0, 0], [0, 0], [0, ln], color=black, line_width=lw)

def ltf_parser(data):
	# print(len(data))
	# print(data[0:4])
	data = data[4:]

	res = parse(header_format, data)

	# print(res)

	txt = data[28:28+res['txtSize']]
	# r = data[28+res['txtSize']:28+res['txtSize']+100]
	# print(txt)

	# return

	header_length = 28+res['txtSize']
	# chunks = 

	pos = header_length

	fig = plt.figure()
		

	while True:
		print(pos)
		tag = data[pos:pos+24]
		tag = parse(tag_format, tag)
		# print(tag)
		chunk_data_pos = pos+24
		newPos = chunk_data_pos+tag['compressedChunkSize']
		# print("newPos ", newPos)
		chunk_data = data[chunk_data_pos:newPos]
		pos = newPos
		points = uncompress_chunk(chunk_data)
		# print(points.shape)

		# matplotlib
		# fig.clf()
		# ax = fig.add_subplot(111, projection='3d')

		# # ax.set_xlim3d(-x_range, x_range)
		# # ax.set_ylim3d(-y_range, y_range)
		# # ax.set_zlim3d(-z_range, z_range)
		# plt.plot(points[:,0],points[:,1],points[:,2], 'r.')
		# plt.plot([0,0],[0,0],[0,0], 'k*')
		# plt.show()
		# plt.pause(0.01)
		# plt.draw()
		# plt.waitforbuttonpress()

		skip = 10

		mlab.clf()
		draw_cs()
		mlab.points3d(points[::skip,0], points[::skip,1], points[::skip,2], color=(1,0,0), scale_factor = 0.1)

		mlab.show()

		# break
		# print(tag['compressedChunkSize'], len(chunk_data))

		if newPos>=len(data):
			break