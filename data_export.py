import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from PIL import Image
import cv2, glob, datetime

from ltf_parser import ltf_parser

data_dir = "F:\\Jon\\Lab\\davimar\\paddys_davimar_test\\"

def read_thermal_data():

	pth = data_dir+'zunanji_test_thermal\\'

	images = sorted(glob.glob(pth+'Images*'))
	indices = sorted(glob.glob(pth+'Index*'))

	im_size = (288, 384)
	start = 5

	# display = False
	display = True
	idx = 3

	im_fn = images[idx]
	idx_fn = indices[idx]

	fn_out = 'videos_thermal\\'+im_fn.split('\\')[-1][:-4]+'.avi'

	fourcc = cv2.VideoWriter_fourcc(*'MJPG') 
	video = cv2.VideoWriter(fn_out, fourcc, 10,(im_size[1],im_size[0]))

	print(im_fn)

	# for i, (im_fn, idx_fn) in enumerate(zip(images, indices)):

	with open(idx_fn) as f:
		lines = f.readlines()
		res = [l.split(',') for l in lines]
		res = [[int(l[0]),int(l[1]),int(l[2])] for l in res]
		data = np.array(res)

	indices = data[start:,0]
	timestamps = data[start:,1]


	for idx in range(0,data.shape[0]):
		print(idx, data.shape[0])
		print(im_fn, idx_fn)

		with open(im_fn, "rb") as f:
			# index = data[idx,0]
			offset = data[idx,2]

			image_bytes = np.fromfile(f, dtype=np.uint16,count=im_size[0]*im_size[1], offset=offset)
		# print(len(image_bytes))

		im = image_bytes.reshape(im_size) #notice row, column format
		# cv2.imshow("a", im)
		# cv2.waitKey(1)
		# color_img = cv2.cvtColor(im*255, cv2.COLOR_GRAY2RGB)
		im = (im/256).astype('uint8')
		im = cv2.merge([im,im,im])

		video.write(im)

	cv2.destroyAllWindows()
	video.release()

		# plt.clf()
		# plt.imshow(im)
		# # plt.show()
		# plt.draw()
		# plt.pause(0.01)
		# # plt.waitforbuttonpress()

def read_polar_data():

	pth = data_dir+'zunanji_test_polar\\'

	images = sorted(glob.glob(pth+'Images*'))
	indices = sorted(glob.glob(pth+'Index*'))
	# im_size = (2432, 2048)
	im_size = (1216, 1024)

	# display = False
	display = True
	start = 5
	idx = 4

	im_fn = images[idx]
	idx_fn = indices[idx]

	fn_out = 'videos_polar\\'+im_fn.split('\\')[-1][:-4]+'.avi'
	# print(fn_out)
	# return
	fourcc = cv2.VideoWriter_fourcc(*'MJPG') 
	# video = cv2.VideoWriter('video.avi', fourcc, 10,(im_size[0],im_size[1]))
	video = cv2.VideoWriter(fn_out, fourcc, 10,(im_size[0],im_size[1]))

	# for i, (im_fn, idx_fn) in enumerate(zip(images, indices)):
		# print(i, idx_fn)
		# if i==0:
		# 	continue
		# res = []
		# plt.clf()
		# print(im_fn, idx_fn)

	with open(idx_fn) as f:
		lines = f.readlines()
		res = [l.split(',') for l in lines]
		res = [[int(l[0]),int(l[1]),int(l[2]),int(l[3])] for l in res]
		data = np.array(res)

		indices = data[start:,0]
		timestamps = data[start:,1]
		offsets = data[start:,2]
		lengths = data[start:,3]


	for idx in range(0,offsets.shape[0]):
		print(idx, offsets.shape[0])
		# print(im_fn, idx_fn)				

		with open(im_fn, "rb") as f:
			index = data[idx,0]
			ts = data[idx,1]
			# offset = data[idx,2]
			offset = offsets[idx]
			length = lengths[idx]					

			print("ts", ts)
			#image_bytes = np.fromfile(f, dtype=np.uint8,count=im_size[0]*im_size[1], offset=offset)
			#image_bytes = np.fromfile(f, dtype=np.uint8,count=offset2-offset, offset=offset)
			image_bytes = np.fromfile(f, dtype=np.uint8,count=length, offset=offset)
			# print(len(image_bytes))

			nparr = np.frombuffer(bytes(image_bytes), np.uint8)
			im = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
			# print(im.shape)
			# I = cv2.cvtColor(im,cv2.COLOR_GRAY2RGB)
			# print(I.size)

			video.write(im)

	cv2.destroyAllWindows()
	video.release()
			
			# plt.clf()
			# plt.imshow(im)
			# # plt.show()
			# plt.draw()
			# plt.pause(0.01)
			# plt.waitforbuttonpress()

def read_radar_data():
	
	pth = data_dir+'zunanji_test_radar\\'

	radar_log = sorted(glob.glob(pth+'RadarLOG*.txt'))
	radar_raw = sorted(glob.glob(pth+'RadarRAW*.txt'))
	radar_ply = sorted(glob.glob(pth+'*.ply'))

	display=False
	display=True

	n = 20
	x_range = n
	y_range = n
	z_range = n

	idx = 1
	lg = radar_log[idx]
	raw = radar_raw[idx]
	ply = radar_ply[idx]

	with open(lg) as f:
		lines = f.readlines()
		res = [l.split(',') for l in lines[1:-1]]
		# print(res[-1])
		res = [[int(l[0]),int(l[1]),int(l[2])-1] for l in res]
		meta = np.array(res)

	with open(raw) as f:
		lines = f.readlines()
		res = [l.split(',') for l in lines[2:]]
		res = [[float(l[0]),float(l[1]),float(l[2]),float(l[3])] for l in res]
		data = np.array(res)

	if display:		
		fig = plt.figure()

		for p in meta:
			plt.clf()
			# print(p)
			ts = p[0]*1e-6
			ln = p[1]
			offset = p[2]
			cur = data[offset:offset+ln]
			# print(ts, p[0], datetime.datetime.fromtimestamp(p[0]))
			# print(date)

			date = datetime.datetime.fromtimestamp(ts)
			# print(cur)				

			ax = fig.add_subplot(111, projection='3d')

			ax.set_xlim3d(-x_range, x_range)
			ax.set_ylim3d(-y_range, y_range)
			ax.set_zlim3d(-z_range, z_range)
			plt.plot(cur[:,0],cur[:,1],cur[:,2], 'r.')
			plt.plot([0,0],[0,0],[0,0], 'k*')
			plt.title(date)
			# plt.show()
			plt.pause(0.01)
			# plt.waitforbuttonpress()

def read_lidar_data():

	pth = data_dir+'zunanji_test_lidar\\'

	files = sorted(glob.glob(pth+'*.ltf'))

	# for fn in files:
	idx = 1
	fn = files[idx]

	with open(fn, 'rb') as f:
		data = f.read()
	ltf_parser(data)

def main():
	# read_polar_data()
	read_thermal_data()
	# read_radar_data()
	# read_lidar_data()

if __name__=='__main__':
	main()