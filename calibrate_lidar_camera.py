import sys, datetime, time
import cv2
import numpy as np
from matplotlib import pyplot as plt

from calibration_utils import *

dt = 0.1
da = 0.5
# da = 0.1
# n_iter = 5000
# n_iter = 2000
# n_iter = 1500
# n_iter = 1000
# n_iter = 200
# n_iter = 100
n_iter = 10
# n_iter = 200
lr = 1e-2
lr2 = 1e-2 # for rotation
# lr = 1e-5
# lr = 1e-3
lidar_thr = 1.5
margin = 1.4
# margin = 0.5
# rn = 3

# def get_colors(points, rnge=100, relative=False, cmap_name='Wistia', cmap_invert=False, cmap_len=65536):
# 	colors = []
# 	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)

# 	if relative:
# 		rnge = np.max(dist)

# 	f = dist/rnge
# 	f = np.clip(f, 0, 1)

# 	cmap = plt.get_cmap(cmap_name)
# 	sm = plt.cm.ScalarMappable(cmap=cmap)
# 	if cmap_invert:
# 		color_range = sm.to_rgba(np.linspace(0, 1, cmap_len))[:,0:3][::-1]
# 	else:
# 		color_range = sm.to_rgba(np.linspace(0, 1, cmap_len))[:,0:3]

# 	idx = f*(cmap_len-1)

# 	for i in idx:
# 		clr = color_range[int(i),:]
# 		colors.append(clr)

# 	return np.array(colors)

# def project_lidar_points(point_cloud, sz, R, t, M, D, rn=1e6, l2im=np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]]), cmap_name='Wistia', cmap_invert=False, cmap_len=65536):
# 	# cur = datetime.now()
# 	pc = point_cloud[:,:3].copy().T

# 	# pc = l2im.dot(pc) # convert from lidar to image cs

# 	# idx = (pc[2,:]>0) # remove points behind camera
# 	# pc = pc[:,idx]	

# 	if pc.shape[1]==0:
# 		# return pc, None, np.array([])
# 		return pc, None, None

# 	colors = get_colors(pc, rn, cmap_name=cmap_name, cmap_invert=cmap_invert, cmap_len=cmap_len)

# 	pts, _ = cv2.projectPoints(pc, R, t, M, distCoeffs=D)
# 	pts = pts[:,0,:]
# 	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<sz[1]-1) & (pts[:,1]<sz[0]-1) # create mask for valid pixels

# 	return pts, colors, mask

def eulerAnglesToRotationMatrix(theta):
	# Calculates Rotation Matrix given euler angles.
	theta = [np.radians(x) for x in theta]
	
	R_x = np.array([[1, 0, 0],
					[0, np.cos(theta[0]), -np.sin(theta[0]) ],
					[0, np.sin(theta[0]), np.cos(theta[0]) ]
					])
		
		
					
	R_y = np.array([[np.cos(theta[1]), 0,np.sin(theta[1]) ],
					[0, 1,0],
					[-np.sin(theta[1]),0,np.cos(theta[1]) ]
					])
				
	R_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
					[np.sin(theta[2]), np.cos(theta[2]), 0],
					[0, 0,1]
					])
					
	R = np.dot(R_x, np.dot( R_y, R_z ))

	return R

def params_to_input(params):
	# ordering is x,y,z,p,y,r
	R = eulerAnglesToRotationMatrix(params[3:]) # TODO load this from yaml file
	T = np.array(params[0:3])

	return R, T

def evaluate_solution_edges(edges, pts):

	pts = pts.astype(np.int32)

	inliers = list(map(lambda x: edges[x[1],x[0]], pts))

	# print(inliers)
	return sum(inliers)

def get_score(R, T, pc, M, edges, l2im=None):
	# disp(pc)
	# print(R, T)
	# x = project_lidar_points(pc, edges.shape, R, T, M, np.array([]))
	# print(x)
	if l2im is not None:
		pts, _, mask = project_lidar_points(pc, edges.shape, R, T, M, np.array([]), l2im=l2im)
	else:
		pts, _, mask = project_lidar_points(pc, edges.shape, R, T, M, np.array([]))

	pts = pts[mask, :]
	s = evaluate_solution_edges(edges, pts)
	return s

def calculate_gradient(params, edges, lidar_data, M, dt, da, l2im=None, dist=1):

	scan = lidar_data[:,0:3]
	beams = lidar_data[:,3]
	# dst = lidar_data[:,4]

	res = []
	
	for i, x in enumerate(params): # translation params
		p = params.copy()
		p[i]+=dt if i<3 else da
		R, T = params_to_input(p)
		if l2im is not None:
			s1 = get_score(R, T, scan, M, edges, l2im=l2im)*dist
		else:
			s1 = get_score(R, T, scan, M, edges)*dist

		p = params.copy()
		p[i]-=dt if i<3 else da
		# print(p)
		R, T = params_to_input(p)
		if l2im is not None:
			s2 = get_score(R, T, scan, M, edges, l2im=l2im)*dist
		else:
			s2 = get_score(R, T, scan, M, edges)*dist


		g = (s2-s1)
		g/=2*dt if i<3 else 2*da


		# print(s1, s2, g)
		res.append(g)

	return np.array(res)

def calibrate(queue, edges, scans, M, R_init, T_init):

	ts = datetime.datetime.now()
	ts = int(round(ts.timestamp()))

	print("calibration start")
	# print(args)

	params = np.zeros(6)
	params[1]=-0.6
	params[3]=-14

	print(len(edges))

	if len(edges)>0:

		for n in range(n_iter):
			print(edges[0].shape)

			gradients = []
			for e, s in zip(edges, scans):
				g = calculate_gradient(params, e[...,0], s, M, dt, da)
				print(g)
				gradients.append(g)

			gradients = np.array(gradients)
			G = np.mean(gradients, axis=0)

			params[:3]-=(lr*g[:3])
			params[3:]-=(lr2*g[3:])

		# for e, s in zip(edges, scans):
		# 	# print(p)
		# 	# cv2.imshow('a', e)
		# 	# cv2.waitKey(0)
		# 	print(e.shape)

		# time.sleep(2)

		print("calibration end")

		R, T = params_to_input(params)

		queue.put((R,T))
	else:
		print("a")
		queue.put((R_init, T_init))

	return

if __name__=='__main__':
	main()