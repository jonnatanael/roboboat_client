from read_utils import ZEDParser
import signal, sys, struct, datetime
import numpy as np
import cv2, time, torch
from ultralytics import YOLO

from socket import *

# run in base conda environment or ensure ultralytics is installed

start_time = time.time()
def start_timing():
	global start_time

	start_time = time.time()

def end_timing(s=''):


	print(f'{s} took: {(time.time() - start_time)} seconds')

	# print("--- %s seconds ---" % (time.time() - start_time))

def main():

	model = YOLO('yolov8l.pt', task='detect')
	print(model.device)
	device = 'cuda' if torch.cuda.is_available() else 'cpu'
	model.to(device)
	print(model.device)


	R = ZEDParser()

	ts_prev = None

	R.start_all()
	# R.stop_all()

	cv2.namedWindow("YOLOv8 Inference", cv2.WINDOW_NORMAL)

	# R.socket_in.setblocking(0)

	sending_depth = False


	# R.set_parameters(set_resolution=1, set_height=100, set_width=100)
	# R.set_parameters(set_resolution=1, set_height=500, set_width=500)
	R.set_parameters(set_resolution=1, set_height=320, set_width=576)
	R.set_parameters(set_resolution=1, set_height=640, set_width=1152)
	# R.set_parameters(set_resolution=1, set_height=1000, set_width=1000)
	# return
	height = 320
	width = 576
	delta_resolution = 50
	# height = 100
	# width = 100
	# height = 1000
	# width = 1000
	# height = 410
	# width = 410
	# x = 100
	# x = 500
	# R.set_resolution(x, x)
	# R.set_resolution(310, 552)
	R.set_resolution(height, width)
	# R.set_resolution(320, 576)
	# R.set_resolution(310*2, 552*2)
	# R.set_resolution(height, width)

	# R.set_parameters(acquisition=1)
	R.set_parameters(sending_rgb=1)
	# R.set_parameters(sending_depth=int(sending_depth))
	R.set_parameters(sending_depth=0)
	# R.set_parameters(sending_depth=1)
	# return

	# R.set_parameters(acquisition=0)

	# return

	while True:

		# R.start_all()
		# print("loop")

		# continue

		# print(f'{R.socket_in=}')


		# data, addr = R.socket_in.recvfrom(33000)

		# start_timing()

		data, addr = R.socket_in.recvfrom(63000)
		R.parse_packet(data)

		# try:
		# 	data, addr = R.socket_in.recvfrom(63000)
		# 	# data, addr = R.socket_in.recvfrom(10000)
		# 	# data, addr = R.socket_in.recvfrom(600)
		# 	R.parse_packet(data)
		# except:
			# pass
		

		# end_timing("recvfrom")

		# print(data)
		# print(addr)
		
		
		if R.image is not None and R.image_ok:
		
			im = R.image.copy()
			print(f'{im.shape=}')

			ts = datetime.datetime.now()
			if ts_prev is not None:
				dt = ts-ts_prev
				fps = 1/dt.total_seconds()
				# print(f"FPS zed: {fps}")
			ts_prev = ts

			

			R.image_ok = False

			results = model.predict(im)
			# print(results)

			annotated_frame = results[0].plot()

			# Display the annotated frame
			cv2.imshow('YOLOv8 Inference', annotated_frame)

			# cv2.imshow("img", annotated_frame)
			# cv2.imshow("results", results)
			key = cv2.waitKey(1)

			if key & 0xFF == ord("q"):
				break
			elif key==ord("e"):
				print("send depth")
				sending_depth = not sending_depth
				R.set_parameters(sending_depth=int(sending_depth))

			elif key==ord("r"):
				R.set_resolution(height, width)
				print("stopping acquisition")
			elif key==ord("t"):
				R.set_resolution(320, 576)
			elif key==43:
				height+=delta_resolution
				width+=delta_resolution
				R.set_resolution(height, width)
			elif key==45:
				height-=delta_resolution
				width-=delta_resolution
				R.set_resolution(height, width)
			else:
				print(key)


	R.stop_all()
	# R.set_parameters(acquisition=int(False))

def test():

	socket_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
	socket_in.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	socket_in.bind(('192.168.90.14', 32768))
	# socket_in.bind(('0.0.0.0', 32768))

	while True:

		# R.start_all()
		print("loop")

		# continue

		print(f'{socket_in=}')

		data, addr = socket_in.recvfrom(65000)
		# data, addr = socket_in.recvfrom(1500)
		# print(data)
		print(addr)


if __name__=='__main__':
	main()

	# test()
