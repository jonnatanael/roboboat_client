

# set up the UDP reader for PC's IP

from socket import *
import sys, time, signal, os, threading, glob, subprocess, fileinput, re
import numpy as np
from utils import get_local_IP

config_files_path = 'config_files/'

sensor_info = {
	'jetson': {'IP': '192.168.90.70', 'IP_VPN': '10.8.0.9', 'sensor_type': 7, 'config_path': '/etc/jetson.conf', 'username': 'jetson', 'sudo': True, 'daemon_name': ''},
	'stereo_front': {'IP': '192.168.90.74', 'IP_VPN': '10.8.0.5', 'sensor_type': 1, 'config_path': '/etc/stereo.conf', 'username': 'pi', 'sudo': True, 'daemon_name': ''},
	'stereo_side': {'IP': '192.168.90.71', 'IP_VPN': '10.8.0.4', 'sensor_type': 1, 'config_path': '/etc/stereo.conf', 'username': 'pi', 'sudo': True, 'daemon_name': ''},
	'thermal_camera': {'IP': '192.168.90.72', 'IP_VPN': '10.8.0.6', 'sensor_type': 3, 'config_path': '/home/pi/Desktop/robocoln_2/TermalnaKamera/configThermal.conf', 'username': 'pi', 'sudo': False, 'daemon_name': 'thermalDaemon'},
	'polarization_camera': {'IP': '192.168.90.73', 'IP_VPN': '10.8.0.7', 'sensor_type': 2, 'config_path': '/home/pi/Desktop/robocoln_2/FLIR/ConfigFile.conf', 'username': 'pi', 'sudo': False, 'daemon_name': 'polarizationDaemon'},
	'lidar': {'IP': '192.168.90.75', 'IP_VPN': '10.8.0.3', 'sensor_type': 4, 'config_path': '/etc/lidar.conf', 'username': 'pi', 'sudo': True, 'daemon_name': 'lidarDaemon'},
	'radar': {'IP': '192.168.90.76', 'IP_VPN': '10.8.0.8', 'sensor_type': 5, 'config_path': 'Desktop/robocoln_2/Radar/configFile.conf', 'username': 'pi', 'sudo': False, 'daemon_name': 'radarDaemon'},
	'GPSIMU': {'IP': '192.168.90.77', 'IP_VPN': '10.8.0.2', 'sensor_type': 6, 'config_path': '/etc/rbgpsimu.conf', 'username': 'pi', 'sudo': True, 'daemon_name': 'GPSIMUDaemon'},
}

def worker(k,v,IP):
	print(f"{k} start")

	config_file = f"{config_files_path}{k}.conf"
	sensor_IP = v['IP']

	if not v['sudo']:
		command = f"scp {config_file} {v['username']}@{sensor_IP}:{v['config_path']}"
		proc = subprocess.call(command.split(' '), stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
	else:
		command = f"scp {config_file} {v['username']}@{sensor_IP}:/tmp/tmp.conf"
		subprocess.call(command.split(' '), stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
		command_move = f"sudo mv /tmp/tmp.conf {v['config_path']}"
		proc = subprocess.call(['ssh', '-T', '-f', f"{v['username']}@{sensor_IP}", "nohup", command_move], stderr=subprocess.DEVNULL)

	# command_cat = f"cat {v['config_path']}"
	# subprocess.call(['ssh', '-T', '-f', f"{v['username']}@{sensor_IP}", "nohup", command_cat], stderr=subprocess.DEVNULL)

	# restart daemon to use new IP
	dn = v['daemon_name']
	if dn:
		command_restart = f"sudo systemctl restart {dn}"
		proc2 = subprocess.call(['ssh', '-T', '-f', f"{v['username']}@{sensor_IP}", "nohup", command_restart], stderr=subprocess.DEVNULL)

	print(f"{k} finish")

def set_davimar_system_IP_parallel(IP):

	threads = []

	for k, v in sensor_info.items():
		config_file = f"{config_files_path}{k}.conf"
		client_address_1 = IP.split('.')[-2]
		client_address_2 = IP.split('.')[-1]

		# print(config_file)

		# with open(config_file) as f:
		# 	lines = f.readlines()
		# 	# print("data", data)
		# 	for l in lines:
		# 		if '192.168.9' in l:
		# 			print(l)


		for line in fileinput.input(config_file, inplace=True):
			if '192.168.9' in line:
				# print(line)
				a = line.split('.')
				a[-2]=client_address_1
				a[-1]=client_address_2
				b = '.'.join(a)+'\n'
				print('{}'.format(b), end='')
			else:
				print('{}'.format(line), end='')

	for k,v in sensor_info.items():
		t = threading.Thread(target=worker, args=[k, v, IP])
		t.start()
		threads.append(t)
		# t.join()

	for t in threads:
		# print("waiting for thread")
		t.join()

def set_davimar_system_IP(IP):

	files = glob.glob(config_files_path+'*')
	# print(files)

	for k, v in sensor_info.items():
		# print(k, v['IP'])
		print(k)

		config_file = f"{config_files_path}{k}.conf"
		print(config_file)

		client_address = IP.split('.')[-1]
		print(client_address)

		# continue

		for line in fileinput.input(config_file, inplace=True):
			if '192.168.90' in line:
				# print(line)
				a = line.split('.')
				a[-1]=client_address

				b = '.'.join(a)+'\n'
				# print("b", b)

				print('{}'.format(b), end='')
			else:
				print('{}'.format(line), end='')


		sensor_IP = v['IP']

		if not v['sudo']:
			# subprocess.call(['ssh', '-T', '-f', "jetson@"+IP, "nohup", command_kill], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
			command = f"scp {config_file} {v['username']}@{sensor_IP}:{v['config_path']}"
			# print(command)
			# command = 'ls -lh'
			subprocess.call(command.split(' '), stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
		else:
			command = f"scp {config_file} {v['username']}@{sensor_IP}:/tmp/tmp.conf"
			# print(command)
			subprocess.call(command.split(' '), stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
			command_move = f"sudo mv /tmp/tmp.conf {v['config_path']}"
			subprocess.call(['ssh', '-T', '-f', f"{v['username']}@{sensor_IP}", "nohup", command_move], stderr=subprocess.DEVNULL)

		# check if file was set ok

		# command_cat = f"cat {v['config_path']}"
		# subprocess.call(['ssh', '-T', '-f', f"{v['username']}@{sensor_IP}", "nohup", command_cat], stderr=subprocess.DEVNULL)

		# TODO restart daemon to use new IP
		dn = v['daemon_name']
		if dn:
			print(dn)
			command_restart = f"sudo systemctl restart {dn}"
			subprocess.call(['ssh', '-T', '-f', f"{v['username']}@{sensor_IP}", "nohup", command_restart], stderr=subprocess.DEVNULL)

def worker_get():
	pass

def get_davimar_configs():

	for k, v in sensor_info.items():
		# print(k, v['IP'])
		print(k)
		sensor_IP = v['IP']

		config_file_local = f"{config_files_path}{k}.conf"
		config_file_remote = f"{v['config_path']}"

		print(config_file_remote)
		print(config_file_local)

		command = f"scp {v['username']}@{sensor_IP}:{config_file_remote} {config_file_local}"
		print(command)
		subprocess.call(command.split(' '), stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

def main():

	local_IP = get_local_IP()
	print("local IP", local_IP)
	# local_IP = '192.168.90.255'
	# local_IP = '255.255.255.255'
	# print(local_IP)

	# set_davimar_system_IP(local_IP)
	set_davimar_system_IP_parallel(local_IP)

	# get_davimar_configs()

	return

if __name__ == '__main__':
	main()
