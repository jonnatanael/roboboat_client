from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt, QTimer, QObject, QThread, pyqtSignal
import cv2
import sys, time
import numpy as np

from read_utils import TelemetryParser
# from powerbar_blur_demo import PowerBar

class _Bar(QtWidgets.QWidget):
	clickedValue = QtCore.pyqtSignal(int)

	def __init__(self, steps, *args, **kwargs):
		super().__init__(*args, **kwargs)

		# self.setSizePolicy(
		# 	QtWidgets.QSizePolicy.MinimumExpanding,
		# 	QtWidgets.QSizePolicy.MinimumExpanding
		# )

		if isinstance(steps, list):
			# list of colours.
			self.n_steps = len(steps)
			self.steps = steps

		elif isinstance(steps, int):
			# int number of bars, defaults to red.
			self.n_steps = steps
			self.steps = ['green'] * steps

		else:
			raise TypeError('steps must be a list or int')

		self._bar_solid_percent = 0.8
		self._background_color = QtGui.QColor('black')
		self._padding = 4.0  # n-pixel gap around edge.

	def sizeHint(self):
		return QtCore.QSize(20,120)

	def paintEvent(self, e):
		painter = QtGui.QPainter(self)

		brush = QtGui.QBrush()
		brush.setColor(self._background_color)
		brush.setStyle(Qt.SolidPattern)
		rect = QtCore.QRect(0, 0, painter.device().width(), painter.device().height())
		painter.fillRect(rect, brush)

		# Get current state.
		dial = self.parent()._dial
		vmin, vmax = dial.minimum(), dial.maximum()
		value = dial.value()

		# Define our canvas.
		d_height = painter.device().height() - (self._padding * 2)
		d_width = painter.device().width() - (self._padding * 2)

		# Draw the bars.
		step_size = d_height / self.n_steps
		bar_height = step_size * self._bar_solid_percent
		# bar_spacer = step_size * (1 - self._bar_solid_percent) / 2
		bar_spacer = 0

		# Calculate the y-stop position, from the value in range.
		pc = (value - vmin) / (vmax - vmin)
		n_steps_to_draw = int(pc * self.n_steps)

		for n in range(n_steps_to_draw):
			brush.setColor(QtGui.QColor(self.steps[n]))
			rect = QtCore.QRect(
				self._padding,
				# self._padding + d_height - ((1 + n) * step_size) + bar_spacer,
				self._padding + d_height - ((n) * step_size) + bar_spacer,
				d_width,
				bar_height+1
			)
			painter.fillRect(rect, brush)

		painter.end()

	def _trigger_refresh(self):
		self.update()

	# def _calculate_clicked_value(self, e):
	# 	parent = self.parent()
	# 	vmin, vmax = parent.minimum(), parent.maximum()
	# 	d_height = self.size().height() + (self._padding * 2)
	# 	step_size = d_height / self.n_steps
	# 	click_y = e.y() - self._padding - step_size / 2

	# 	pc = (d_height - click_y) / d_height
	# 	value = vmin + pc * (vmax - vmin)
	# 	self.clickedValue.emit(value)

	# def mouseMoveEvent(self, e):
	# 	self._calculate_clicked_value(e)

	# def mousePressEvent(self, e):
	# 	self._calculate_clicked_value(e)

class PowerBar(QtWidgets.QWidget):
	def __init__(self, steps=5, *args, **kwargs):
		super().__init__(*args, **kwargs)

		layout = QtWidgets.QVBoxLayout()
		self._bar = _Bar(steps)
		layout.addWidget(self._bar)

		self._dial = QtWidgets.QDial()
		# layout.addWidget(self._dial)

		self.setLayout(layout)

		self._dial.valueChanged.connect(self._bar._trigger_refresh)
		self._bar.clickedValue.connect(self._dial.setValue)

	def setColor(self, color):
		self._bar.steps = [color] * self._bar.n_steps
		self._bar.update()

	def setColors(self, colors):
		self._bar.n_steps = len(colors)
		self._bar.steps = colors
		self._bar.update()

	def setBarPadding(self, i):
		self._bar._padding = int(i)
		self._bar.update()

	def setBarSolidPercent(self, f):
		self._bar._bar_solid_percent = float(f)
		self._bar.update()

	def setBackgroundColor(self, color):
		self._bar._background_color = QtGui.QColor(color)
		self._bar.update()

	def __getattr__(self, name):
		if name in self.__dict__:
			return self[name]

		try:
			return getattr(self._dial, name)
		except AttributeError:
			raise AttributeError(
			  "'{}' object has no attribute '{}'".format(self.__class__.__name__, name)
			)

class Worker(QObject):
	finished = pyqtSignal()
	progress = pyqtSignal(object)

	def __init__(self, sensor):
		super(Worker, self).__init__()
		# self.parser = TelemetryParser('lidar')
		self.parser = TelemetryParser(sensor)

	def run(self):
		while True:
			data, addr = self.parser.socket_in.recvfrom(1000)

			self.parser.parse_packet(data)
			print(self.parser.electrical_data)

			self.progress.emit((self.parser.system_data, self.parser.sync_data, self.parser.electrical_data))

system_names = ['cpu_1', 'cpu_2', 'cpu_3', 'cpu_4', 'cpu_5', 'cpu_6', 'cpu_7', 'cpu_8']
electr_names = ['battery_current', '5V_current', '12V_current', 'ADC', '3V3', '12V_voltage', '5V_voltage', 'battery_voltage']
sync_names = ['NTP_offset', 'NTP_jitter']

class TelemetryWidget(QtWidgets.QWidget):
	def __init__(self, parent=None, sensor=None):
		super(TelemetryWidget, self).__init__(parent)

		self.image_frame = QtWidgets.QLabel()
		self.main_layout = QtWidgets.QVBoxLayout()
		self.system_layout = QtWidgets.QHBoxLayout()
		self.electr_layout = QtWidgets.QHBoxLayout()
		self.sync_layout = QtWidgets.QHBoxLayout()

		self.system_frame = QtWidgets.QFrame()
		self.system_frame.setLayout(self.system_layout)
		self.system_frame.setFrameStyle(QtWidgets.QFrame.Box | QtWidgets.QFrame.Plain)

		self.electr_frame = QtWidgets.QFrame()
		self.electr_frame.setLayout(self.electr_layout)
		self.electr_frame.setFrameStyle(QtWidgets.QFrame.Box | QtWidgets.QFrame.Plain)

		self.sync_frame = QtWidgets.QFrame()
		self.sync_frame.setLayout(self.sync_layout)
		self.sync_frame.setFrameStyle(QtWidgets.QFrame.Box | QtWidgets.QFrame.Plain)
		
		self.setLayout(self.main_layout)
		label = QtWidgets.QLabel("System")
		label.setAlignment(Qt.AlignCenter)
		self.main_layout.addWidget(label)
		self.main_layout.addWidget(self.system_frame)
		# self.main_layout.addLayout(self.system_layout)

		label = QtWidgets.QLabel("Electrical")
		label.setAlignment(Qt.AlignCenter)
		self.main_layout.addWidget(label)
		# self.main_layout.addLayout(self.electr_layout)
		self.main_layout.addWidget(self.electr_frame)

		label = QtWidgets.QLabel("Synchronization")
		label.setAlignment(Qt.AlignCenter)
		self.main_layout.addWidget(label)
		self.main_layout.addWidget(self.sync_frame)	

		for nm in system_names:
			bar = PowerBar(steps=100); bar.setBarPadding(2); bar.setBarSolidPercent(1)
			vl = QtWidgets.QVBoxLayout()
			label = QtWidgets.QLabel(nm)
			label.setAlignment(Qt.AlignCenter)
			vl.addWidget(bar)
			vl.addWidget(label)
			self.system_layout.addLayout(vl)
		
		for nm in electr_names:
			vl = QtWidgets.QVBoxLayout()
			label1 = QtWidgets.QLabel(nm+": ")
			label1.setAlignment(Qt.AlignCenter)
			label2 = QtWidgets.QLabel()
			label2.setAlignment(Qt.AlignCenter)
			vl.addWidget(label1)
			vl.addWidget(label2)
			self.electr_layout.addLayout(vl)

		for nm in sync_names:
			vl = QtWidgets.QVBoxLayout()
			label1 = QtWidgets.QLabel(nm+": ")
			label1.setAlignment(Qt.AlignCenter)
			label2 = QtWidgets.QLabel()
			label2.setAlignment(Qt.AlignCenter)
			vl.addWidget(label1)
			vl.addWidget(label2)
			self.sync_layout.addLayout(vl)

		# thread
		self.thread = QThread()
		self.worker = Worker(sensor)
		self.worker.moveToThread(self.thread)

		self.thread.started.connect(self.worker.run)
		self.worker.progress.connect(self.updateData)
		self.thread.start()

	def updateData(self, data):

		sys_data = data[0]
		sync_data = data[1]
		el_data = data[2]

		for i, nm in enumerate(system_names):
			bar = self.system_layout.itemAt(i).itemAt(0).widget()
			try:
				value = sys_data['cpu_{0}'.format(i+1)]
				value = value if value <= 100 else 0
				bar._dial.setValue(value)
			except:
				pass

		for i,nm in enumerate(electr_names):
			label = self.electr_layout.itemAt(i).itemAt(1).widget()
			try:
				value = el_data[nm]
				label.setText(str(value))
			except:
				pass

		for i,nm in enumerate(sync_names):
			label = self.sync_layout.itemAt(i).itemAt(1).widget()
			try:
				value = sync_data[nm]
				label.setText(str(value))
			except:
				pass

if __name__ == '__main__':
	sensor = None
	if len(sys.argv)==1:
		print("you must specify a sensor! Should be one of rgb_camera, polarization_camera, thermal_camera, lidar, radar, imu_gps, depth")
		exit()
	elif len(sys.argv)==2:
		sensor = sys.argv[1]

	app = QtWidgets.QApplication(sys.argv)
	display_image_widget = TelemetryWidget(sensor=sensor)
	display_image_widget.show()

	timer = QTimer()
	timer.timeout.connect(lambda: None)
	timer.start(100)

	sys.exit(app.exec_())
