

# application that starts sending for all sensors and displays the sent data during recording

# from PyQt5 import QtGui, QtCore, QtWidgets
# from PyQt5.QtCore import Qt, QTimer, QObject, QThread, pyqtSignal, QDateTime
import cv2
import sys, time, signal, os, datetime
import numpy as np
# import pyqtgraph.opengl as gl
# import pyqtgraph as pg

# import urllib.request as urllib2
# from collections import deque

# from gui_utils import CompassWidget
# from read_utils import RadarParser, ThermalParser, PolarizationParser, LidarParser, RGBParser, ZEDParser
from read_utils import ZEDParser, ThermalParser, PolarizationParser, LidarParser
# from openstreetmap_load import deg2num, num2deg
# from gps_utils import gps_get_image

from multiprocessing import Process, Queue, Lock, Pool
from threading import Thread
import asyncio
from shared_memory import *

from calibration_utils import *

# from stereo_client import StereoParser

patternsize = (3,5)

# def find_corners(im, patternsize = (3,5)):

	# ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	# return corners if ret else None

# # blob detector
# params = cv2.SimpleBlobDetector_Params()
# # Setup SimpleBlobDetector parameters.
# # Filter by Area.
# params.filterByArea = True
# params.minArea = 10
# params.maxArea = 18000
# params.minDistBetweenBlobs = 10
# params.filterByColor = True
# params.filterByConvexity = False
# # tweak these as you see fit
# # Filter by Circularity
# params.filterByCircularity = False
# params.minCircularity = 0.2
# detector = cv2.SimpleBlobDetector_create(params)

# def adjust_contrast_brightness(img, contrast:float=1.0, brightness:int=0):
# 	"""
# 	Adjusts contrast and brightness of an uint8 image.
# 	contrast:   (0.0,  inf) with 1.0 leaving the contrast as is
# 	brightness: [-255, 255] with 0 leaving the brightness as is
# 	"""
# 	brightness += int(round(255*(1-contrast)/2))
# 	return cv2.addWeighted(img, contrast, img, 0, brightness)

# offx = 30 # offset for fps counter on images
# label_width = 150

# gray_values = np.arange(256, dtype=np.uint8)[::-1]
# color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_TURBO).reshape(256, 3)))

# def get_colors(points, rnge):
# 	global gray_values, color_values	

# 	colors = []
# 	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)
# 	rnge = np.max(dist)
# 	idx = (dist/rnge)*255
# 	for i in idx:
# 		colors.append(color_values[int(i)])

# 	return np.array(colors,dtype=np.uint8)

# def eulerAnglesToRotationMatrix(theta):
# 	theta = [np.radians(x) for x in theta]
	
# 	R_x = np.array([[1, 0, 0],
# 					[0, np.cos(theta[0]), -np.sin(theta[0]) ],
# 					[0, np.sin(theta[0]), np.cos(theta[0]) ]
# 					])
		
		
					
# 	R_y = np.array([[np.cos(theta[1]), 0,np.sin(theta[1]) ],
# 					[0, 1,0],
# 					[-np.sin(theta[1]),0,np.cos(theta[1]) ]
# 					])
				
# 	R_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
# 					[np.sin(theta[2]), np.cos(theta[2]), 0],
# 					[0, 0,1]
# 					])
					
# 	R = np.dot(R_x, np.dot( R_y, R_z ))

# 	return R

# def project_lidar_points(pc, im, R, t, M, D, skip=1, rn=1e6):
# 	# l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
# 	l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])

# 	pc = pc[::skip, 0:3].T
# 	# pc = pc.astype(float)*1e-3
# 	pc = pc[:,np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)<rn]
# 	pc = pc[:,pc[1,:]>0] # remove points behind the camera
# 	pc = l2im.dot(pc)


# 	dst = np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)

# 	if pc.shape[1]==0:
# 		return pc, None, np.array([])

# 	# print(pc.shape)

# 	colors = get_colors(pc, rn)

# 	pts, _ = cv2.projectPoints(pc, R, t, M, distCoeffs=D)
# 	pts = pts[:,0,:]
# 	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<im.shape[1]-1) & (pts[:,1]<im.shape[0]-1) # create mask for valid pixels

# 	pts = pts[mask,:]
# 	colors = colors[mask,:]
# 	dst = dst[mask]

# 	# colors = [(x[2]/255,x[1]/255,x[0]/255) for x in colors]
# 	colors = [(x[0]/255,x[1]/255,x[2]/255) for x in colors]

# 	return pts, colors, dst

# class ThermalWorker(QObject):
# 	finished = pyqtSignal()
# 	data = pyqtSignal(object)

# 	def __init__(self):
# 		super(ThermalWorker, self).__init__()
# 		self.parser = ThermalParser()
# 		self.ts_prev = None

# 	def run(self):
# 		fps = 0
# 		self.parser.set_parameters(num_images=0, frame_rate=5, auto_gain_control=1)
# 		self.parser.start_sending()

# 		while True:
# 			data, addr = self.parser.socket_in.recvfrom(1000)
# 			# print(data)
# 			self.parser.parse_packet(data)

# 			if self.parser.image is not None:
# 				# print("thermal", self.parser.image.shape)
# 				ts = datetime.datetime.now()

# 				if self.ts_prev is not None:
# 					dt = ts-self.ts_prev
# 					fps = 1/dt.total_seconds()
# 				self.ts_prev = ts

# 				cv2.putText(self.parser.image,str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 4, cv2.LINE_AA)
# 				cv2.putText(self.parser.image,str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

# 				self.data.emit(self.parser.image)
# 				self.parser.image = None

# class PolarizationWorker(QObject):
# 	finished = pyqtSignal()
# 	data = pyqtSignal(object)

# 	def __init__(self):
# 		super(PolarizationWorker, self).__init__()
# 		self.parser = PolarizationParser()
# 		self.ts_prev = None

# 	def run(self):
# 		fps = 0
# 		self.parser.start_sending()

# 		while True:
# 			data, addr = self.parser.socket_in.recvfrom(1000)
# 			self.parser.parse_packet(data)
# 			if self.parser.image is not None:

# 				ts = datetime.datetime.now()
# 				if self.ts_prev is not None:
# 					dt = ts-self.ts_prev
# 					fps = 1/dt.total_seconds()
# 				self.ts_prev = ts

# 				im = self.parser.image

# 				# print("polar", im.shape)

# 				# detect pattern corners if necessary
# 				# if detect_corners:
# 				# # 	# print(image.shape)
# 				# 	corners = find_corners(image)

# 				# 	if corners is not None:
# 				# 		image = cv2.drawChessboardCorners(image, patternsize, corners, True)

# 				alpha = 2 # Contrast control (1.0-3.0)
# 				beta = 0 # Brightness control (0-100)

# 				# im = cv2.convertScaleAbs(im, alpha=alpha, beta=beta)

# 				cv2.putText(im,str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 4, cv2.LINE_AA)
# 				cv2.putText(im,str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

# 				self.data.emit(im)
# 				self.parser.image = None

# class RGBFrontWorker(QObject):
# 	finished = pyqtSignal()
# 	data = pyqtSignal(object)

# 	def __init__(self):
# 		super(RGBFrontWorker, self).__init__()
# 		self.parser = RGBParser('front')

# 		self.im_l = None
# 		self.im_r = None
# 		self.ts_prev = None

# 	def run(self):
# 		fps = 0

# 		while True:
# 			data, addr = self.parser.socket_in.recvfrom(2000)
# 			self.parser.parse_packet(data)
# 			if self.parser.image is not None:
# 				im = self.parser.image

# 				# print("front", im.shape)


# 				ts = datetime.datetime.now()
# 				if self.ts_prev is not None:
# 					dt = ts-self.ts_prev
# 					fps = 1/dt.total_seconds()
# 					# print(f"FPS stereo front: {fps}")
# 				self.ts_prev = ts

# 				alpha = 1.5 # Contrast control (1.0-3.0)
# 				beta = 0 # Brightness control (0-100)

# 				im = cv2.convertScaleAbs(im, alpha=alpha, beta=beta)

# 				# im = cv2.cvtColor(im,cv2.COLOR_GRAY2RGB)

# 				im_l = im[:,0:im.shape[1]//2].copy()
# 				im_r = im[:,im.shape[1]//2:].copy()

# 				cv2.putText(im_l,str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 4, cv2.LINE_AA)
# 				cv2.putText(im_l,str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

# 				self.data.emit((im_l,im_r))
# 				self.parser.image = None

# class RGBSideWorker(QObject):
# 	finished = pyqtSignal()
# 	data = pyqtSignal(object)

# 	def __init__(self):
# 		super(RGBSideWorker, self).__init__()
# 		self.parser = RGBParser('side')

# 	def run(self):
# 		# return
# 		# self.parser.set_parameters(num_images=0, frame_rate=1)
# 		# self.parser.start_sending()

# 		while True:
# 			data, addr = self.parser.socket_in.recvfrom(2000)
# 			self.parser.parse_packet(data)
# 			if self.parser.image is not None and self.parser.image_ok:
# 				im = self.parser.image.copy()

# 				# print("side", im.shape)


# 				alpha = 3 # Contrast control (1.0-3.0)
# 				beta = 0 # Brightness control (0-100)

# 				# im = cv2.convertScaleAbs(im, alpha=alpha, beta=beta)

# 				# for sbs
# 				# im = cv2.cvtColor(im,cv2.COLOR_GRAY2RGB)
# 				im_l = im[:,0:im.shape[1]//2].copy()
# 				im_r = im[:,im.shape[1]//2:].copy()

# 				# for tb
# 				# im_l = im[0:im.shape[0]//2,:].copy()
# 				# im_r = im[im.shape[0]//2:,:].copy()

# 				# cv2.imshow("side", self.parser.image)
# 				# cv2.waitKey(1)
# 				# print(im.shape,im.dtype)
# 				# print(im)

# 				# print(im_l.shape)
# 				# self.parser.image = self.parser.image[:,:self.parser.image.shape[1]//2]
# 				# self.parser.image.strides=(self.parser.image.strides[0]//2,1)
# 				self.data.emit((im_l,im_r))

# 				# self.parser.image = self.parser.image[:,:self.parser.image.shape[1]//2]
# 				# self.parser.image.strides=(self.parser.image.strides[0]//2,1)
# 				# self.data.emit(im)
# 				# self.parser.image = None
# 				self.parser.image_ok = False

# class LidarWorker(QObject):
# 	finished = pyqtSignal()
# 	data = pyqtSignal(object)

# 	def __init__(self):
# 		# print("LIDAR init")
# 		super(LidarWorker, self).__init__()
# 		self.parser = LidarParser()

# 	def run(self):
# 		self.parser.start_sending()

# 		while True:

# 			data, addr = self.parser.socket_in.recvfrom(1000)
# 			self.parser.parse_packet(data)

# 			if self.parser.pc is not None and len(self.parser.pc)!=0 and self.parser.data_ok:
# 				self.data.emit(self.parser.pc)
# 				self.parser.data_ok = False

# class RadarWorker(QObject):
# 	finished = pyqtSignal()
# 	data = pyqtSignal(object)

# 	def __init__(self):
# 		super(RadarWorker, self).__init__()
# 		self.parser = RadarParser()

# 	def run(self):
# 		self.parser.start_sending()

# 		while True:
# 			data, addr = self.parser.socket_in.recvfrom(1000)
# 			self.parser.parse_packet(data)
# 			if self.parser.pc is not None and len(self.parser.pc)!=0:
# 				self.data.emit(self.parser.pc)

# class ZedWorker(QObject):
# 	finished = pyqtSignal()
# 	data = pyqtSignal(object)

# 	def __init__(self):
# 		super(ZedWorker, self).__init__()
# 		self.parser = ZEDParser()

# 		self.im = None
# 		self.ts_prev = None

# 	def run(self):
# 		fps = 0

# 		while True:
# 			data, addr = self.parser.socket_in.recvfrom(1000)
# 			self.parser.parse_packet(data)
# 			if self.parser.image is not None and self.parser.image_ok:
# 				im = self.parser.image


# 				ts = datetime.datetime.now()
# 				if self.ts_prev is not None:
# 					dt = ts-self.ts_prev
# 					fps = 1/dt.total_seconds()
# 					# print(f"FPS zed: {fps}", end='\r')
# 				self.ts_prev = ts

# 				# alpha = 1.5 # Contrast control (1.0-3.0)
# 				# beta = 0 # Brightness control (0-100)

# 				# im = cv2.convertScaleAbs(im, alpha=alpha, beta=beta)

# 				cv2.putText(im, str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 4, cv2.LINE_AA)
# 				cv2.putText(im, str(int(fps)),(10,offx), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

# 				self.data.emit(im)
# 				self.parser.image_ok = False

# sensors = {
# 	'zed': {'IP': '192.168.90.70', 'IP_VPN': '10.8.0.9', 'parser': ZEDParser(), 'params': None, 'sensor_type': 1, 'sensor_number': 3},
# 	'stereo_front': {'IP': '192.168.90.74', 'IP_VPN': '10.8.0.5', 'parser': RGBParser('front'), 'params': None, 'sensor_type': 1, 'sensor_number': 1},
# 	'stereo_side': {'IP': '192.168.90.71', 'IP_VPN': '10.8.0.4', 'parser': RGBParser('side'), 'params': None, 'sensor_type': 1, 'sensor_number': 2},
# 	'thermal_camera': {'IP': '192.168.90.72', 'IP_VPN': '10.8.0.6', 'parser': ThermalParser()},
# 	'polarization_camera': {'IP': '192.168.90.73', 'IP_VPN': '10.8.0.7', 'parser': PolarizationParser(), 'sensor_type': 2, 'sensor_number': 1},
# 	'lidar': {'IP': '192.168.90.75', 'IP_VPN': '10.8.0.3', 'parser': LidarParser(), 'params': None, 'sensor_type': 4, 'sensor_number': 1}, # lidar povzroča težave z drugmimi slikami
# 	'radar': {'IP': '192.168.90.76', 'IP_VPN': '10.8.0.8', 'parser': RadarParser(), 'params': None, 'sensor_type': 5, 'sensor_number': 1},
# 	# 'GPSIMU': {'IP': '192.168.90.77', 'IP_VPN': '10.8.0.2', 'parser': GPSParser(), 'params': None, 'sensor_type': 6, 'sensor_number': 1},
# }

# class ClientWidget(QtWidgets.QWidget):
# 	def __init__(self, parent=None):
# 		super().__init__(parent=parent)

# 		# self.layout = QtWidgets.QHBoxLayout()
# 		self.layout = QtWidgets.QGridLayout()
# 		positions = [(i, j) for i in range(3) for j in range(3)]
		
# 		self.setLayout(self.layout)

# 		self.detect_target = True
# 		self.project_lidar = False
# 		# self.project_lidar = True
# 		self.project_radar = True
# 		self.project_radar = False
# 		self.lidar_scan = None

# 		if self.project_lidar:
# 			(self.width, self.height, self.M, self.D) = load_camera_calibration('calibration_zed.yaml')
# 			self.R_lidar, self.T_lidar = load_lidar_calibration('calibration_lidar_zed_2.yaml')

# 			self.M/=4
# 			self.M[-1,-1]=1


# 		if 'zed' in sensors.keys():
# 			groupBox = QtWidgets.QGroupBox()
# 			groupBox.setMinimumHeight(100)
# 			groupBox.setMaximumHeight(1000)
# 			hbox = QtWidgets.QHBoxLayout()
# 			label = QtWidgets.QLabel('ZED')
# 			self.image_frame_zed = QtWidgets.QLabel()

# 			hbox.addWidget(label)
# 			label.setFixedWidth(label_width)
# 			hbox.addWidget(self.image_frame_zed)

# 			groupBox.setLayout(hbox)
# 			self.layout.addWidget(groupBox, *positions[0])

# 			# threads
# 			self.thread_zed = QThread()
# 			self.worker_zed = ZedWorker()
# 			self.worker_zed.moveToThread(self.thread_zed)

# 			self.thread_zed.started.connect(self.worker_zed.run)
# 			self.worker_zed.data.connect(self.updateZed)
# 			self.thread_zed.start()

# 		if 'thermal_camera' in sensors.keys():
# 			groupBox = QtWidgets.QGroupBox()
# 			groupBox.setMinimumHeight(100)
# 			groupBox.setMaximumHeight(1000)
# 			hbox = QtWidgets.QHBoxLayout()
# 			label = QtWidgets.QLabel('thermal_camera')
# 			label.setFixedWidth(label_width)
# 			self.image_frame_thermal = QtWidgets.QLabel()

# 			hbox.addWidget(label)
# 			hbox.addWidget(self.image_frame_thermal)

# 			groupBox.setLayout(hbox)
# 			self.layout.addWidget(groupBox, *positions[1])

# 			self.thread_thermal = QThread()
# 			self.worker_thermal = ThermalWorker()
# 			self.worker_thermal.moveToThread(self.thread_thermal)

# 			self.thread_thermal.started.connect(self.worker_thermal.run)
# 			self.worker_thermal.data.connect(self.updateThermal)
# 			self.thread_thermal.start()

# 		if 'polarization_camera' in sensors.keys():
# 			groupBox = QtWidgets.QGroupBox()
# 			groupBox.setMinimumHeight(100)
# 			groupBox.setMaximumHeight(1000)
# 			hbox = QtWidgets.QHBoxLayout()
# 			label = QtWidgets.QLabel('polarization_camera')
# 			label.setFixedWidth(label_width)
# 			self.image_frame_polar = QtWidgets.QLabel()

# 			hbox.addWidget(label)
# 			hbox.addWidget(self.image_frame_polar)

# 			groupBox.setLayout(hbox)
# 			self.layout.addWidget(groupBox, *positions[2])

# 			self.thread_polar = QThread()
# 			self.worker_polar = PolarizationWorker()
# 			self.worker_polar.moveToThread(self.thread_polar)

# 			self.thread_polar.started.connect(self.worker_polar.run)
# 			self.worker_polar.data.connect(self.updatePolarization)
# 			self.thread_polar.start()

# 		if 'stereo_front' in sensors.keys():
# 			groupBox = QtWidgets.QGroupBox()
# 			groupBox.setMinimumHeight(100)
# 			groupBox.setMaximumHeight(1000)
# 			hbox = QtWidgets.QHBoxLayout()

# 			self.image_frame_stereo_front_left = QtWidgets.QLabel()
# 			self.image_frame_stereo_front_right = QtWidgets.QLabel()

# 			label = QtWidgets.QLabel('stereo_front')
# 			label.setFixedWidth(label_width)

# 			hbox.addWidget(label)
# 			hbox.addWidget(self.image_frame_stereo_front_left)
# 			hbox.addWidget(self.image_frame_stereo_front_right)

# 			groupBox.setLayout(hbox)
# 			# self.layout.addWidget(groupBox)
# 			self.layout.addWidget(groupBox, *positions[3])

# 			self.thread_stereo_front = QThread()
# 			self.worker_stereo_front = RGBFrontWorker()
# 			self.worker_stereo_front.moveToThread(self.thread_stereo_front)

# 			self.thread_stereo_front.started.connect(self.worker_stereo_front.run)
# 			self.worker_stereo_front.data.connect(self.updateStereoFront)
# 			self.thread_stereo_front.start()

# 		if 'stereo_side' in sensors.keys():

# 			# widgets

# 			# left
# 			groupBox1 = QtWidgets.QGroupBox()
# 			groupBox1.setMinimumHeight(100)
# 			groupBox1.setMaximumHeight(1000)
# 			groupBox1.setMinimumWidth(100)
# 			hbox = QtWidgets.QHBoxLayout()
# 			# hbox = QtWidgets.QVBoxLayout()

# 			self.image_frame_stereo_side_left = QtWidgets.QLabel()			
			
# 			label = QtWidgets.QLabel('stereo_side left')
# 			label.setFixedWidth(label_width)

# 			hbox.addWidget(label)
# 			hbox.addWidget(self.image_frame_stereo_side_left)
			
# 			groupBox1.setLayout(hbox)
# 			# self.layout.addWidget(groupBox)
# 			self.layout.addWidget(groupBox1, *positions[6])			

# 			# right

# 			groupBox2 = QtWidgets.QGroupBox()
# 			groupBox2.setMinimumHeight(100)
# 			groupBox2.setMaximumHeight(1000)
# 			groupBox2.setMinimumWidth(100)
# 			box = QtWidgets.QHBoxLayout()

# 			self.image_frame_stereo_side_right = QtWidgets.QLabel()

# 			label = QtWidgets.QLabel('stereo_side right')
# 			label.setFixedWidth(label_width)

# 			box.addWidget(label)
# 			box.addWidget(self.image_frame_stereo_side_right)

# 			groupBox2.setLayout(box)
# 			self.layout.addWidget(groupBox2, *positions[7])


# 			# threads

# 			self.thread_stereo_side = QThread()
# 			self.worker_stereo_side = RGBSideWorker()
# 			self.worker_stereo_side.moveToThread(self.thread_stereo_side)

# 			self.thread_stereo_side.started.connect(self.worker_stereo_side.run)
# 			self.worker_stereo_side.data.connect(self.updateStereoSide)
# 			self.thread_stereo_side.start()

# 		if 'radar' in sensors.keys():			
# 			groupBox = QtWidgets.QGroupBox()
# 			groupBox.setMinimumHeight(100)
# 			groupBox.setMaximumHeight(1000)
# 			hbox = QtWidgets.QVBoxLayout()
			
# 			label = QtWidgets.QLabel('radar')
# 			label.setFixedWidth(label_width)

# 			hbox.addWidget(label)

# 			self.radarLabel = QtWidgets.QLabel("radarLabel")
# 			hbox.addWidget(self.radarLabel)

# 			groupBox.setLayout(hbox)
# 			self.layout.addWidget(groupBox, *positions[4])

# 			# widgets
# 			# self.gl = gl.GLViewWidget()
# 			# self.layout.addWidget(self.gl*positions[6])
# 			# self.gl.show()
# 			# self.grid = gl.GLGridItem()
# 			# self.gl.addItem(self.grid)

# 			# self.sp1 = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
# 			# self.gl.addItem(self.sp1)
# 			# self.sp2 = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
# 			# self.gl.addItem(self.sp2)

# 			# threads
# 			self.thread_radar = QThread()
# 			self.worker_radar = RadarWorker()
# 			self.worker_radar.moveToThread(self.thread_radar)

# 			self.thread_radar.started.connect(self.worker_radar.run)
# 			self.worker_radar.data.connect(self.updateRadar)
# 			self.thread_radar.start()

# 		if 'lidar' in sensors.keys():
# 			# print("Setting up lidar")
# 			groupBox = QtWidgets.QGroupBox()
# 			groupBox.setMinimumHeight(100)
# 			groupBox.setMaximumHeight(1000)
# 			hbox = QtWidgets.QVBoxLayout()
			
# 			label = QtWidgets.QLabel('lidar')
# 			label.setFixedWidth(label_width)
# 			hbox.addWidget(label)

# 			self.lidarLabel = QtWidgets.QLabel("lidarLabel")
# 			hbox.addWidget(self.lidarLabel)

# 			groupBox.setLayout(hbox)
# 			self.layout.addWidget(groupBox, *positions[5])		

# 			# widgets
# 			# self.gl_lidar = gl.GLViewWidget()
# 			# self.layout.addWidget(self.gl_lidar)
# 			# self.gl_lidar.show()
# 			# self.grid = gl.GLGridItem()
# 			# self.gl_lidar.addItem(self.grid)

# 			# # hbox.addWidget(self.gl_lidar)

# 			# # set size
# 			# # self.gl_lidar.resize(700, 700)
# 			# self.gl_lidar.show()

# 			# self.sp1_lidar = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
# 			# self.gl_lidar.addItem(self.sp1_lidar)
# 			# self.sp2_lidar = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
# 			# self.gl_lidar.addItem(self.sp2_lidar)

# 			# threads
# 			self.thread_lidar = QThread()
# 			self.worker_lidar = LidarWorker()
# 			self.worker_lidar.moveToThread(self.thread_lidar)

# 			self.thread_lidar.started.connect(self.worker_lidar.run)
# 			self.worker_lidar.data.connect(self.updateLidar)
# 			self.thread_lidar.start()

# 		self.timer2=QTimer()
# 		self.timer2.timeout.connect(self.start_sending)
# 		self.timer2.start(5000)

# 		self.signal = signal.signal(signal.SIGINT, self.signal_handler)

# 		self.gps_image = None
# 		self.lat = None
# 		self.lon = None
# 		self.gps_bbox = None
# 		self.gps_size = (1280,720)

# 		N = 1000
# 		self.gps_memory = deque([], N)

# 	def closeEvent(self, event):

# 		if 'GPSIMU' in sensors.keys():
# 			self.worker_gps.parser.stop_sending()

# 		if 'thermal_camera' in sensors.keys():
# 			self.worker_thermal.parser.stop_sending()

# 		if 'polarization_camera' in sensors.keys():
# 			self.worker_polar.parser.stop_sending()

# 		if 'lidar' in sensors.keys():
# 			self.worker_lidar.parser.stop_sending()

# 		if 'radar' in sensors.keys():
# 			self.worker_radar.parser.stop_sending()

# 		event.accept()

# 	def signal_handler(self, sig, frame):
# 		if 'GPSIMU' in sensors.keys():
# 			p = self.worker_gps.parser
# 			p.stop_sending()

# 		if 'thermal_camera' in sensors.keys():
# 			p = self.worker_thermal.parser
# 			p.stop_sending()

# 		if 'polarization_camera' in sensors.keys():
# 			p = self.worker_polar.parser
# 			p.stop_sending()

# 		if 'lidar' in sensors.keys():
# 			self.worker_lidar.parser.stop_sending()

# 		if 'radar' in sensors.keys():
# 			self.worker_radar.parser.stop_sending()

# 		sys.exit()

# 	def start_sending(self):
# 		# start sending for all sensors in a loop (in case they weren't running when the application was started)

# 		if 'GPSIMU' in sensors.keys():
# 			self.worker_gps.parser.start_sending()

# 		if 'thermal_camera' in sensors.keys():
# 			self.worker_thermal.parser.start_sending()

# 		if 'polarization_camera' in sensors.keys():
# 			self.worker_polar.parser.start_sending()

# 		if 'lidar' in sensors.keys():
# 			self.worker_lidar.parser.start_sending()

# 		if 'radar' in sensors.keys():
# 			self.worker_radar.parser.start_sending()

# 	def updateZed(self, im):

# 		if self.detect_target and not self.project_lidar:
# 			corners = find_corners(im)
# 			if corners is not None:
# 				# print("found zed corners")
# 				im = cv2.drawChessboardCorners(im, patternsize, corners, True)

# 		# (width1, height1, M1, D1) = load_camera_calibration('calibration_zed.yaml')
# 		point_size = 1

# 		if self.project_lidar and self.lidar_scan is not None:

# 			# im = cv2.resize(im, (int(width1), int(height1)))			

# 			pts, colors, _ = project_lidar_points(self.lidar_scan, im, self.R_lidar, self.T_lidar, self.M, np.array([]))

# 			if pts.shape[0]!=0 and pts is not None:
# 					# print(pts.shape)
# 					for point, clr in zip(pts, colors):

# 						point = (int(point[0]), int(point[1]))
# 						clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
# 						im = cv2.circle(im, point, point_size, clr, -1)

# 		if self.project_radar:

# 			im = cv2.resize(im, (int(width1), int(height1)))

# 			R_radar, T_radar = load_lidar_calibration('calibration_lidar_zed_2.yaml')

# 			pts, colors, _ = project_lidar_points(self.radar_points, im, R_radar, T_radar, M1, np.array([]))

# 			# print(pts)

# 			if pts.shape[0]!=0 and pts is not None:
# 					# print(pts.shape)
# 					for point, clr in zip(pts, colors):

# 						point = (int(point[0]), int(point[1]))
# 						clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
# 						im = cv2.circle(im, point, point_size, clr, -1)

# 		self.image_zed = QtGui.QImage(im.data, im.shape[1], im.shape[0], im.strides[0], QtGui.QImage.Format_RGB888 )
# 		pixmap = QtGui.QPixmap.fromImage(self.image_zed)
# 		pixmap = pixmap.scaled(self.image_frame_zed.size(), Qt.KeepAspectRatio)
# 		self.image_frame_zed.setPixmap(pixmap)

# 	def updateStereoFront(self, data):
# 		image_l = data[0]
# 		image_r = data[1]

# 		image_l_color = cv2.cvtColor(image_l, cv2.COLOR_GRAY2RGB)
# 		image_r_color = cv2.cvtColor(image_r, cv2.COLOR_GRAY2RGB)

# 		if self.detect_target:
# 			corners_l = find_corners(image_l)

# 			if corners_l is not None:
# 				print("found stereo left corners")				
# 				image_l_color = cv2.drawChessboardCorners(image_l_color, patternsize, corners_l, True)

# 			corners_r = find_corners(image_r)

# 			if corners_r is not None:
# 				print("found stereo right corners 2")
# 				image_r_color = cv2.drawChessboardCorners(image_r_color, patternsize, corners_l, True)

# 		# self.image_stereo_front_left = QtGui.QImage(image_l.data, image_l.shape[1], image_l.shape[0], image_l.strides[0], QtGui.QImage.Format_Grayscale8 )
# 		self.image_stereo_front_left = QtGui.QImage(image_l_color.data, image_l_color.shape[1], image_l_color.shape[0], image_l_color.strides[0], QtGui.QImage.Format_RGB888 )
# 		pixmap = QtGui.QPixmap.fromImage(self.image_stereo_front_left)
# 		pixmap = pixmap.scaled(self.image_frame_stereo_front_left.size(), Qt.KeepAspectRatio)
# 		self.image_frame_stereo_front_left.setPixmap(pixmap)

# 		# self.image_stereo_front_right = QtGui.QImage(image_r.data, image_r.shape[1], image_r.shape[0], image_r.strides[0], QtGui.QImage.Format_Grayscale8 )
# 		self.image_stereo_front_right = QtGui.QImage(image_r_color.data, image_r_color.shape[1], image_r_color.shape[0], image_r_color.strides[0], QtGui.QImage.Format_RGB888 )
# 		pixmap = QtGui.QPixmap.fromImage(self.image_stereo_front_right)
# 		pixmap = pixmap.scaled(self.image_frame_stereo_front_right.size(), Qt.KeepAspectRatio)
# 		self.image_frame_stereo_front_right.setPixmap(pixmap)
		
# 	def updateStereoSide(self, data):
# 		# self.counter_thermal+=1
# 		point_size = 1

# 		image_l = data[0]
# 		image_r = data[1]

# 		image_l_color = cv2.cvtColor(image_l, cv2.COLOR_GRAY2RGB)
# 		image_r_color = cv2.cvtColor(image_r, cv2.COLOR_GRAY2RGB)

# 		# (width, height, M, D) = load_camera_calibration('calibration_zed.yaml')

# 		# R = np.eye(3)
# 		# # R = eulerAnglesToRotationMatrix((0,-90,0))
# 		# T = np.zeros(3)
# 		# # T[0]=-0.4

# 		# ldr = self.lidar_scan.copy()
# 		# ldr[:, [1, 0]] = ldr[:, [0, 1]]

# 		# M/=4
# 		# M[-1,-1]=1

# 		# print(image_r_color.shape)

# 		if self.detect_target:
# 			corners_l = find_corners(image_l)

# 			if corners_l is not None:
# 				print("found side left corners")				
# 				image_l_color = cv2.drawChessboardCorners(image_l_color, patternsize, corners_l, True)

# 			corners_r = find_corners(image_r)

# 			if corners_r is not None:
# 				print("found side right corners 2")
# 				image_r_color = cv2.drawChessboardCorners(image_r_color, patternsize, corners_l, True)

# 		# if self.lidar_scan is not None:

# 		# 	# im = cv2.resize(im, (int(width1), int(height1)))			

# 		# 	pts, colors, _ = project_lidar_points(ldr, image_l_color, R, T, M, np.array([]),rn=10)

# 		# 	if pts.shape[0]!=0 and pts is not None:
# 		# 		# print(pts.shape)
# 		# 		for point, clr in zip(pts, colors):

# 		# 			point = (int(point[0]), int(point[1]))
# 		# 			clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
# 		# 			image_l_color = cv2.circle(image_l_color, point, point_size, clr, -1)


# 		self.image_stereo_side_left = QtGui.QImage(image_l_color.data, image_l_color.shape[1], image_l_color.shape[0], image_l_color.strides[0], QtGui.QImage.Format_RGB888 )
# 		pixmap = QtGui.QPixmap.fromImage(self.image_stereo_side_left)
# 		pixmap = pixmap.scaled(self.image_frame_stereo_side_left.size(), Qt.KeepAspectRatio)
# 		self.image_frame_stereo_side_left.setPixmap(pixmap)

# 		self.image_stereo_side_right = QtGui.QImage(image_r_color.data, image_r_color.shape[1], image_r_color.shape[0], image_r_color.strides[0], QtGui.QImage.Format_RGB888 )
# 		pixmap = QtGui.QPixmap.fromImage(self.image_stereo_side_right)
# 		pixmap = pixmap.scaled(self.image_frame_stereo_side_right.size(), Qt.KeepAspectRatio)
# 		self.image_frame_stereo_side_right.setPixmap(pixmap)

# 	def updateThermal(self, image):

# 		im_inv = 255-image

# 		if self.detect_target:
# 			corners = find_corners(im_inv)
# 			if corners is not None:
# 				# print("found thermal corners")
# 				image = cv2.drawChessboardCorners(image, patternsize, corners, True)

# 		image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

# 		self.image_thermal = QtGui.QImage(image.data, image.shape[1], image.shape[0], image.strides[0], QtGui.QImage.Format_RGB888 )
# 		pixmap = QtGui.QPixmap.fromImage(self.image_thermal)
# 		pixmap = pixmap.scaled(self.image_frame_thermal.size(), Qt.KeepAspectRatio)

# 		self.image_frame_thermal.setPixmap(pixmap)

# 	def updatePolarization(self, image):

# 		image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)


# 		if self.detect_target:
# 			corners = find_corners(image)
# 			# image = cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)
# 			if corners is not None:
# 				# print("found polar corners")
# 				image = cv2.drawChessboardCorners(image, patternsize, corners, True)
# 			else:
# 				_, corners = cv2.findCirclesGrid(image, patternsize, flags=(cv2.CALIB_CB_ASYMMETRIC_GRID + cv2.CALIB_CB_CLUSTERING ), blobDetector=detector)
# 				if corners is not None:
# 					# print("found polar corners 2")
# 					image = cv2.drawChessboardCorners(image, patternsize, corners_l, True)

# 		self.image_polar = QtGui.QImage(image.data, image.shape[1], image.shape[0], image.strides[0], QtGui.QImage.Format_RGB888 )
# 		# self.image_polar = QtGui.QImage(image.data, image.shape[1], image.shape[0], image.strides[0], QtGui.QImage.Format_RGB888 ).rgbSwapped()
# 		pixmap = QtGui.QPixmap.fromImage(self.image_polar)
# 		pixmap = pixmap.scaled(self.image_frame_polar.size(), Qt.KeepAspectRatio)

# 		self.image_frame_polar.setPixmap(pixmap)

# 	def updateLidar(self, data):
# 		# print(data.shape)
# 		# print(data[1:10,:])

# 		# self.sp1_lidar.setData(pos=pts)
# 		# color = np.zeros((pts.shape[0],3), dtype=np.float32)
# 		# color[:,0]=1
# 		# self.sp1_lidar.setData(color=color, size = 5)

# 		# self.sp1_lidar.setData(pos=np.array([[0,0,0]]))
# 		# color = np.zeros((1,3), dtype=np.float32)
# 		# color[:,0]=1
# 		# self.sp1_lidar.setData(color=color, size=20)

# 		self.lidarLabel.setText("points: {:.2f}".format(data.shape[0]))
# 		self.lidar_scan = data

# 		return

# 		skip = 1
# 		d = data[::skip,0:3]

# 		self.sp2_lidar.setData(pos=d)
# 		color = np.zeros((d.shape[0],3), dtype=np.float32)
# 		color[:,1]=1
# 		self.sp2_lidar.setData(color=color, size = 5)

# 		self.lidar_scan = data

# 	def updateRadar(self, data):

# 		self.radarLabel.setText(f"points: {data.shape[0]}")

# 		self.radar_points = data

# 		return

# 		# circle
# 		n = np.arange(0,2*np.pi,2*np.pi/200)
# 		rn = 33
# 		x = np.sin(n)*rn
# 		y = np.cos(n)*rn
# 		z = np.zeros_like(n)
# 		pts = np.vstack((x,y,z)).T

# 		self.sp1.setData(pos=pts)
# 		color = np.zeros((pts.shape[0],3), dtype=np.float32)
# 		color[:,0]=1
# 		self.sp1.setData(color=color, size = 5)

# 		# self.sp1.setData(pos=np.array([[0,0,0]]))
# 		# color = np.zeros((1,3), dtype=np.float32)
# 		# color[:,0]=1
# 		# self.sp1.setData(color=color, size=20)

# 		self.sp2.setData(pos=data[:,0:3])
# 		color = np.zeros((data.shape[0],3), dtype=np.float32)
# 		color[:,1]=1
# 		self.sp2.setData(color=color, size = 5)

def create_shm(shape, type):
	a = np.zeros(shape=shape, dtype=type)
	shm = shared_memory.SharedMemory(create=True, size=a.nbytes)
	np_array = np.ndarray(a.shape, dtype=type, buffer=shm.buf)
	np_array[:] = a[:]
	return shm, np_array

class ZedReader(Process):

	def __init__(self, qi, qo, shr_name):
		super(ZedReader, self).__init__()
		print("initing zed reader")

		self.parser = ZEDParser()
		self.qi = qi
		self.qo = qo
		self.shr_name = shr_name
	
		self.existing_shm = shared_memory.SharedMemory(name=self.shr_name)
		self.np_array = np.ndarray((621, 1104, 3), dtype=np.uint8, buffer=self.existing_shm.buf)

	def run(self):

		while True:			

			if not self.qi.empty():
				x = self.qi.get()
				if x==-1:
					break

			data, addr = self.parser.socket_in.recvfrom(1000)
			self.parser.parse_packet(data)
			if self.parser.image is not None and self.parser.image_ok:
				self.np_array[:] = self.parser.image.copy()[:]
				self.qo.put(1)


		print("zed reader stopped")

lock = Lock()


class ThermalReader(Process):

	def __init__(self, qi, qo, shr_name):
		super(ThermalReader, self).__init__()
		print("initing thermal reader")

		self.parser = ThermalParser()
		self.qi = qi
		self.qo = qo
		self.shr_name = shr_name
	
		self.existing_shm = shared_memory.SharedMemory(name=self.shr_name)
		self.np_array = np.ndarray((288, 384), dtype=np.uint8, buffer=self.existing_shm.buf)

		self.np_lidar = None

	def setup_lidar(self, shr_lidar, M, C, R, T):

		print("setting up lidar shm")
		self.shr_lidar = shr_lidar
		self.lidar_shm = shared_memory.SharedMemory(name=self.shr_lidar)
		self.np_lidar = np.ndarray((60000, 4), dtype=np.float32, buffer=self.lidar_shm.buf)
		print(self.np_lidar.shape)

		self.M = M
		self.C = C
		self.R = R
		self.T = T

		pass

	def run(self):

		while True:		

			if not self.qi.empty():
				x = self.qi.get()
				if x==-1:
					break

			# try:
			data, addr = self.parser.socket_in.recvfrom(1000)
			# print(addr)
			self.parser.parse_packet(data)
			if self.parser.image is not None and self.parser.image_ok:

				# acquire the lock
				

				self.np_array[:] = self.parser.image.copy()[:]
				self.qo.put(1)
				# lock.release()
				# 	print("putting 1")

		print("thermal reader stopped")


def main_single():

	sensor = 'zed'
	sensor = 'thermal_camera'

	calib_fn = f'helper_calibrations/calibration_{sensor}_helper.yaml'
	(width, height, M, D) = load_camera_calibration(calib_fn)
	R, T, C = load_lidar_calibration(f'helper_calibrations/calibration_lidar_{sensor}.yaml')

	# shm_camera, im_camera = create_shm(shape=(int(height), int(width), 3), type=np.uint8)

	q_camera = Queue()
	lock_camera = Lock()

	if sensor=='zed':
		width//=2
		height//=2
		shm_camera, im_camera = create_shm(shape=(int(height), int(width), 3), type=np.uint8)
		p_camera = ZedReader(q_camera, shm_camera.name, lock_camera)

	elif sensor=='thermal_camera':
		shm_camera, im_camera = create_shm(shape=(int(height), int(width)), type=np.uint8)
		p_camera = ThermalReader(q_camera, shm_camera.name, lock_camera)
		p_camera.parser.start_sending()


	p_camera.start()

	while True:

		# print("im_camera.shape", im_camera.shape)

		im = im_camera.copy()
		if sensor=='zed':
			im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

		# cv2.imshow(sensor, im_camera)
		cv2.imshow(sensor, im)
		key = cv2.waitKeyEx(1) & 0xFF
		# key = cv2.waitKeyEx(0) & 0xFF

		if key == ord('q'):

			q_camera.put(-1)
			p_camera.join()
			break

		# 	break
		# q_camera.put(-1)
		# break



	p_camera.join()	
	shm_camera.close()
	shm_camera.unlink()

class PolarReader(Process):

	def __init__(self, qi, qo, shr_name):
		super(PolarReader, self).__init__()
		print("initing polar reader")

		self.parser = PolarizationParser()
		self.qi = qi
		self.qo = qo
		self.shr_name = shr_name
	
		self.existing_shm = shared_memory.SharedMemory(name=self.shr_name)
		self.np_array = np.ndarray((1024, 1216), dtype=np.uint8, buffer=self.existing_shm.buf)

	def run(self):

		while True:		

			# print('waiting')

			if not self.qi.empty():
				x = self.qi.get()
				if x==-1:
					break

			# try:
			data, addr = self.parser.socket_in.recvfrom(512)
			# print(addr)
			self.parser.parse_packet(data)
			if self.parser.image is not None and self.parser.image_ok:
				self.np_array[:] = self.parser.image.copy()[:]
				# print('image ok')
				self.qo.put(1)

		print("polar reader stopped")

class LidarReader(Process):

	def __init__(self, qi, qo, shr_name):
		super(LidarReader, self).__init__()

		self.parser = LidarParser()
		self.qi = qi
		self.shr_name = shr_name

		self.existing_shm = shared_memory.SharedMemory(name=self.shr_name)
		self.np_array = np.ndarray((60000, 4), dtype=np.float32, buffer=self.existing_shm.buf)

	def run(self):

		while True:		

			# print('waiting')

			if not self.qi.empty():
				x = self.qi.get()
				if x==-1:
					break

			# try:
			data, addr = self.parser.socket_in.recvfrom(1000)
			# print(addr)
			self.parser.parse_packet(data)
			if self.parser.pc is not None and self.parser.data_ok:
				# lock.acquire()
				# print("lidar shm", self.shr_name)
				pc = self.parser.pc
				# self.np_array[:] = self.parser.image.copy()[:]
				self.np_array[:pc.shape[0],:] = pc.copy()
				# self.np_array[pc.shape[0]:, :] = 0
				self.parser.data_ok = False

				# lock.release()
				# print('lidar ok')

class DisplayProcess(Process):

	def __init__(self, name, qi, qo, M, C, R, T):
		super(DisplayProcess, self).__init__()

		self.qi = qi
		self.qo = qo
		self.M = M
		self.C = C
		self.R = R
		self.T = T
		self.name = name

	def run():

		while True:

			if not self.qi.empty():
				x = self.qi.get()
				if x==-1:
					break

		print(f"display for {self.name} stopped")

	def display(self, im, ldr, fps):

		print(f'async {self.name}')

		rnge = 50

		# ldr = self.np_lidar.copy()
		# print(ldr.shape)
		# print("thermal lidar shm", self.shr_lidar)
		ldr[:,:3] = (self.C @ ldr[:,:3].T).T
		ldr = ldr[(ldr[:,2]>0),:]
		# print("LIDAR SHAPE", ldr.shape)
		pts, colors, _ = project_lidar_points(ldr[:,:3].T, im.shape, self.R, self.T, self.M, np.array([]), rn=rnge, nonlinear_factor=0.6)
		# print(pts.shape)


		if pts is not None and pts.shape[1]!=0:
			for pt, color in zip(pts,colors):
				# c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)])[::-1]
				c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)])

				try:
					cv2.circle(im, (int(pt[0]),int(pt[1])), radius=1, color=c, thickness=-1)
				except:
					print(pt)		

		if fps!=-1:
			offy = im.shape[0]//10
			offx = im.shape[1]//2
			font = cv2.FONT_HERSHEY_SIMPLEX
			# font = cv2.FONT_HERSHEY_COMPLEX_SMALL
			font_scale = 1
			cv2.putText(im, str(int(fps)), (offx, offy), font, font_scale, (0, 0, 0), 4, cv2.LINE_AA)
			cv2.putText(im, str(int(fps)), (offx, offy), font, font_scale, (255, 255, 255), 2, cv2.LINE_AA)

		self.qo.put(im)

		# cv2.imshow(self.name, im)

def lidar_display(name, im, ldr, M, C, R, T, fps):

	rnge = 50
	ldr[:,:3] = (C @ ldr[:,:3].T).T
	ldr = ldr[(ldr[:,2]>0),:]
	pts, colors, _ = project_lidar_points(ldr[:,:3].T, im.shape, R, T, M, np.array([]), rn=rnge, nonlinear_factor=0.6)

	if pts is not None and pts.shape[1]!=0:
		for pt, color in zip(pts,colors):
			c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)])

			try:
				cv2.circle(im, (int(pt[0]),int(pt[1])), radius=1, color=c, thickness=-1)
			except:
				print(pt)		

	if fps!=-1:
		offy = im.shape[0]//10
		offx = im.shape[1]//2
		font = cv2.FONT_HERSHEY_SIMPLEX
		# font = cv2.FONT_HERSHEY_COMPLEX_SMALL
		font_scale = 1
		cv2.putText(im, str(int(fps)), (offx, offy), font, font_scale, (0, 0, 0), 4, cv2.LINE_AA)
		cv2.putText(im, str(int(fps)), (offx, offy), font, font_scale, (255, 255, 255), 2, cv2.LINE_AA)

	return im

def main():

	project_lidar = True
	rnge = 50
	radius = 3

	sensors = {

		'zed': {'reader': ZedReader, 'radius': 2},
		'thermal_camera': {'reader': ThermalReader, 'radius': 1},
		'polarization_camera': {'reader': PolarReader, 'radius': 3},
		'lidar': {'reader': LidarReader, 'radius': 0},

	}

	calib_dir = '/home/jon/Desktop/davimar_calibration/helper_calibrations/'
	# calib_dir = 'helper_calibrations/'

	for s, v in sensors.items():
		# name = s['name']
		print(s)
		if 'lidar' not in s:
			calib_fn = f'{calib_dir}calibration_{s}.yaml'
			(width, height, M, D) = load_camera_calibration(calib_fn)
			R, T, C = load_lidar_calibration(f'{calib_dir}calibration_lidar_{s}.yaml')

			v['width']=width
			v['height']=height
			v['M']=M
			v['D']=D
			v['R']=R
			v['T']=T
			v['C']=C

			cv2.namedWindow(s, cv2.WINDOW_NORMAL)

		v['queue']=Queue()
		v['res']= None
		v['result'] = None
		if 'zed' in s:
			width//=2
			height//=2
			v['M']*=0.5
			v['M'][-1,-1]=1
			shm_camera, im_camera = create_shm(shape=(int(height), int(width), 3), type=np.uint8)
		elif 'polar' in s:
			shm_camera, im_camera = create_shm(shape=(int(height), int(width)), type=np.uint8)
		elif 'lidar' in s:
			shm_camera, im_camera = create_shm(shape=(60000, 4), type=np.float32)
		else:
			shm_camera, im_camera = create_shm(shape=(int(height), int(width)), type=np.uint8)

		v['im']=im_camera
		v['shm']=shm_camera

		v['cam']=v['reader'](v['queue'], Queue(), v['shm'].name)
		# if 'thermal' not in s:
		v['cam'].start()

		if 'thermal' in s or 'polar' in s or 'lidar' in s:
			print(f"starting sending for {s}")
			v['cam'].parser.start_sending()

		# if not 'lidar' in s:
		# 	v['display'] = DisplayProcess(s, Queue(), M, C, R, T)
		# 	v['display'].daemon = True
		# 	v['display'].start()
			

	display_lidar = True
	# display_lidar = False

	# sensors['thermal_camera']['cam'].setup_lidar(sensors['lidar']['shm'].name, sensors['thermal_camera']['M'], sensors['thermal_camera']['C'], sensors['thermal_camera']['R'], sensors['thermal_camera']['T'])
	# sensors['thermal_camera']['cam'].start()

	# q = Queue()
	# D = DisplayProcess(sensors['thermal_camera']['M'], sensors['thermal_camera']['C'], sensors['thermal_camera']['R'], sensors['thermal_camera']['T'])

	pool = Pool(processes=10)

	while True:

		# print("main loop")

		for s, v in sensors.items():

			if 'lidar' in s:
			# if 'lidar' in s or 'zed' in s:
				continue
			else:
				# v['cam'].parser.start_sending()

				if not v['cam'].qo.empty():
					x = v['cam'].qo.get()
				else:
					continue

				im = v['im'].copy()
				

				if 'zed' in s:
					im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
				else:
					im = cv2.undistort(im, v['M'], v['D'])
					im = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)

				ldr = sensors['lidar']['im'].copy()

				# cv2.imshow(s, im)
				# if 'thermal' in s:
				# print(im.shape)
				# print(ldr.shape)
				# pool.apply_async(v['display'].display, args=(im, ldr, fps))

				# (name, q, im, ldr, M, C, R, T, fps):
				if v['res']==None:
					# get fps
					ts = datetime.datetime.now()
					try:
						delta = (ts-v['ts']).total_seconds()*1000
						fps = 1000/delta
					except:
						fps = -1
					print(f"starting projection {s}")
					v['res'] = pool.apply_async(lidar_display, args=(s, im, ldr, v['M'], v['C'], v['R'], v['T'], fps))
					v['ts']=ts
				# else:
				try:
					im = v["res"].get(timeout=0.1)
					v['result'] = im
					v['res']=None
				except:
					print("timeout")
					
					if v['result'] is not None:
						im = v['result'].copy()

				cv2.imshow(s, im)

				# print(res.get())
				# v['display'].display(im, ldr, fps)
				# t = Thread(target=v['display'].display, args=(im, ldr, fps))				
				# t.start()

		# for s, v in sensors.items():

		# 	if 'lidar' in s:
		# 	# if 'lidar' in s or 'zed' in s:
		# 		continue
		# 	else:
		# 		try:
		# 			im = v["res"].get()
		# 			v['res']=None
		# 			cv2.imshow(s, im)
		# 		except:
		# 			print("timeout")
		# 			pass

		key = cv2.waitKeyEx(1) & 0xFF
		if key == ord('q'):

			for s, v in sensors.items():
				v['queue'].put(-1)
				v['cam'].terminate()
				# if not 'lidar' in s:
				# 	v['display'].terminate()
			break
		elif key == ord('l'):
			display_lidar = not display_lidar

	# print(sensors)

	for s, v in sensors.items():

		# v['cam'].join()	

		v['shm'].unlink()
		v['shm'].close()

class DataReader(Process):

	def __init__(self, name):
		super(DataReader, self).__init__()
		print("initing thermal reader")

		self.parser = ThermalParser()
		self.name = name

		self.parser.start_sending()

	def run(self):

		while True:		

			# if not self.q.empty():
			# 	x = self.q.get()
			# 	if x==-1:
			# 		break

			# try:
			data, addr = self.parser.socket_in.recvfrom(1000)
			# print(addr)
			self.parser.parse_packet(data)
			if self.parser.image is not None and self.parser.image_ok:

				# acquire the lock
				

				im = self.parser.image.copy()
				# print(im.shape)

				cv2.imshow(self.name, im)
				# key = cv2.waitKeyEx(1) & 0xFF

class DisplayProcess(Process):

	def __init__(self, name, qi, qo, M, C, R, T):
		super(DisplayProcess, self).__init__()

		self.qi = qi
		self.qo = qo
		self.M = M
		self.C = C
		self.R = R
		self.T = T
		self.name = name

	def run(self):

		while True:

			if not self.qi.empty():
				im, ldr, fps = self.qi.get()
				if im is None and ldr is None and fps is None:
					break
				else:
					self.display(im, ldr, fps)

	def display(self, im, ldr, fps):

		# print(f"{self.name} in queue", self.qi.qsize())
		# print(f"{self.name} out queue", self.qo.qsize())

		# print(f'async {self.name}')

		ts1 = datetime.datetime.now()

		rnge = 50

		# ldr = self.np_lidar.copy()
		# print(ldr.shape)
		# print("thermal lidar shm", self.shr_lidar)
		ldr[:,:3] = (self.C @ ldr[:,:3].T).T
		ldr = ldr[(ldr[:,2]>0),:]
		# print("LIDAR SHAPE", ldr.shape)
		pts, colors, _ = project_lidar_points(ldr[:,:3].T, im.shape, self.R, self.T, self.M, np.array([]), rn=rnge, nonlinear_factor=0.6)
		# print(pts.shape)


		if pts is not None and pts.shape[1]!=0:
			for pt, color in zip(pts,colors):
				# c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)])[::-1]
				c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)])

				try:
					cv2.circle(im, (int(pt[0]),int(pt[1])), radius=1, color=c, thickness=-1)
				except:
					print(pt)		

		if fps!=-1:
			offy = im.shape[0]//10
			offx = im.shape[1]//2
			font = cv2.FONT_HERSHEY_SIMPLEX
			# font = cv2.FONT_HERSHEY_COMPLEX_SMALL
			font_scale = 1
			cv2.putText(im, str(int(fps)), (offx, offy), font, font_scale, (0, 0, 0), 4, cv2.LINE_AA)
			cv2.putText(im, str(int(fps)), (offx, offy), font, font_scale, (255, 255, 255), 2, cv2.LINE_AA)

		self.qo.put(im)

		ts2 = datetime.datetime.now()

		delta = (ts2-ts1).total_seconds()*1000
		# print(f"{self.name} projection: {delta}")

		# cv2.imshow(self.name, im)

def main2():


	project_lidar = True
	rnge = 50
	radius = 3

	sensors = {

		'zed': {'reader': ZedReader, 'radius': 2},
		'thermal_camera': {'reader': ThermalReader, 'radius': 1},
		'polarization_camera': {'reader': PolarReader, 'radius': 3},
		'lidar': {'reader': LidarReader, 'radius': 0},

	}

	for s, v in sensors.items():
		# name = s['name']
		print(s)
		if 'lidar' not in s:
			calib_fn = f'/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_{s}_helper.yaml'
			(width, height, M, D) = load_camera_calibration(calib_fn)
			R, T, C = load_lidar_calibration(f'/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_lidar_{s}.yaml')

			v['width']=width
			v['height']=height
			v['M']=M
			v['D']=D
			v['R']=R
			v['T']=T
			v['C']=C

			cv2.namedWindow(s, cv2.WINDOW_NORMAL)

		# v['queue']=Queue()
		# v['queue_in']=Queue()
		# v['queue_out']=Queue()
		v['res']= None

		if 'zed' in s:
			width//=2
			height//=2
			v['M']*=0.5
			v['M'][-1,-1]=1
			shm_camera, im_camera = create_shm(shape=(int(height), int(width), 3), type=np.uint8)
		elif 'polar' in s:
			shm_camera, im_camera = create_shm(shape=(int(height), int(width)), type=np.uint8)
		elif 'lidar' in s:
			shm_camera, im_camera = create_shm(shape=(60000, 4), type=np.float32)
		else:
			shm_camera, im_camera = create_shm(shape=(int(height), int(width)), type=np.uint8)

		v['im']=im_camera
		v['shm']=shm_camera

		v['cam']=v['reader'](Queue(), Queue(), v['shm'].name)
		# if 'thermal' not in s:
		v['cam'].start()

		if 'thermal' in s or 'polar' in s or 'lidar' in s:
			print(f"starting sending for {s}")
			v['cam'].parser.start_sending()

		if not 'lidar' in s:
			v['display'] = DisplayProcess(s, Queue(), Queue(), M, C, R, T)
			v['display'].start()
			

	display_lidar = True

	while True:

		# print("main loop")

		for s, v in sensors.items():
			# print(s)

			if 'lidar' in s:
			# if 'lidar' in s or 'zed' in s:
				continue
			else:
				if not v['cam'].qo.empty():
					x = v['cam'].qo.get()
				else:
					continue

				im = v['im'].copy()
				# print(f'image arrived for {s}')		

				if 'zed' in s:
					im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
				else:
					im = cv2.undistort(im, v['M'], v['D'])
					im = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)

				ldr = sensors['lidar']['im'].copy()

				# cv2.imshow(s, im)
				# if 'thermal' in s:
				# print(im.shape)
				# print(ldr.shape)
				# pool.apply_async(v['display'].display, args=(im, ldr, fps))

				# (name, q, im, ldr, M, C, R, T, fps):

				ts = datetime.datetime.now()
				try:
					delta = (ts-v['ts']).total_seconds()*1000
					fps = 1000/delta
				except:
					fps = -1
				v['ts']=ts

				# print(f"{s} in queue", v['display'].qi.qsize())
				# print(f"{s} out queue", v['display'].qo.qsize())
				

				if not v['display'].qo.empty():
					# print(f"{s}: processed image available")
					im_ = v['display'].qo.get()
					cv2.imshow(s, im_)
				else:
					pass
				# 	print("no data")

				if v['display'].qi.empty():
					# print(f'{s}: queue empty, sending data')
					v['display'].qi.put((im.copy(), ldr.copy(), fps))


		key = cv2.waitKeyEx(1) & 0xFF
		if key == ord('q'):

			# for s, v in sensors.items():
				# v['queue'].put(-1)
				
			break
		# elif key == ord('l'):
			# display_lidar = not display_lidar

	# print(sensors)

	print("loop ended, terminating processes")

	for s, v in sensors.items():
		print(f"stopping {s}")

		# v['cam'].qi.put(-1)
				
		# if not 'lidar' in s:
		# 	v['display'].qi.put((None, None, None))

		v['cam'].terminate()

		if not 'lidar' in s:
			v['display'].terminate()


		v['shm'].unlink()
		v['shm'].close()

		# print(v)

if __name__ == '__main__':
	# app = QtGui.QApplication(sys.argv)
	# radar_widget = ClientWidget()
	# radar_widget.show()

	# timer = QTimer()
	# timer.timeout.connect(lambda: None)
	# timer.start(100)

	# app.exec_()

	# main_single()
	main()
	# main2()


	# loop = asyncio.get_event_loop()
	# asyncio.run(main())
