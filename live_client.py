import sys, time, signal, os, datetime, cv2
import numpy as np
from numpy.linalg import inv
from read_utils import ZEDParser, ThermalParser, PolarizationParser, LidarParser, DepthParser

# from multiprocessing import Process, Queue, Lock, Pool, Event
from queue import Empty
import multiprocessing as mp
# from threading import Thread
import asyncio
import signal
import threading
import time
import socket


from calibration_utils import *

stop_event = mp.Event()

def signal_handler(sig, frame):
	print("Ctrl+C detected! Shutting down...")
	stop_event.set()  # Set the stop event to signal threads to stop
	for thread in threading.enumerate():
		if thread is not threading.current_thread():
			thread.join()  # Wait for other threads to finish
	print("Shutdown complete.")
	exit(0)

# Register the signal handler
signal.signal(signal.SIGINT, signal_handler)

class ZedReader(mp.Process):

	def __init__(self, queue, stop_event, timeout=0.5):
		super(ZedReader, self).__init__()
		print("initing zed reader")
		self.parser = ZEDParser()
		self.parser.set_parameters(sending_depth=1)
		self.image = None
		self.queue = queue
		self.stop_event = stop_event
		print(self.parser.socket_in)
		self.ts_prev = None
		self.stop_event = stop_event
		self.parser.socket_in.settimeout(timeout)
		self.fps = None

	def run(self):
		while not self.stop_event.is_set():
			try:
				data, addr = self.parser.socket_in.recvfrom(65000)
				self.parser.parse_packet(data)
			except:
				continue

			if self.parser.image is not None and self.parser.image_ok:
				# print("zed image ok")
				self.image = self.parser.image.copy()
				# print(self.image.shape)
				self.parser.image_ok = False
				self.queue.put(self.image)

				ts = datetime.datetime.now()
				if self.ts_prev is not None:
					dt = ts-self.ts_prev
					self.fps = 1/dt.total_seconds()
					print(f"FPS ZED: {self.fps}")
				self.ts_prev = ts
				# time.sleep(0.1)

			# print("run")
		# self.queue.put(np.array())
		# self.queue.join()
		print("zed reader stopped")

	def stop(self):
		self.stop_event.set()

	def stopped(self):
		return self.stop_event.is_set()

class PolarReader(mp.Process):

	def __init__(self, queue, stop_event, timeout=0.5):
		super(PolarReader, self).__init__()
		print(queue, stop_event)
		
		print("initing polar reader")
		self.parser = PolarizationParser()
		self.image = None
		self.queue = queue
		self.stop_event = stop_event
		print(self.parser.socket_in)
		self.ts_prev = None
		self.stop_event = stop_event
		self.parser.socket_in.settimeout(timeout)
		self.fps = None


	def run(self):
		while not stop_event.is_set():
			try:
				data, addr = self.parser.socket_in.recvfrom(65000)
				self.parser.parse_packet(data)
			except:
				continue

			if self.parser.image is not None and self.parser.image_ok:
				# print("zed image ok")
				self.image = self.parser.image.copy()
				# print(self.image.shape)
				self.parser.image_ok = False
				self.queue.put(self.image)

				ts = datetime.datetime.now()
				if self.ts_prev is not None:
					dt = ts-self.ts_prev
					self.fps = 1/dt.total_seconds()
					print(f"FPS polar: {self.fps}")
				self.ts_prev = ts
				# time.sleep(0.1)

			# print("run")
		# self.queue.put(np.array())
		# self.queue.join()
		print("polar reader stopped")

	def stop(self):
		self.stop_event.set()

	def stopped(self):
		return self.stop_event.is_set()

class ThermalReader(mp.Process):

	def __init__(self, queue, stop_event, timeout=0.5):
		super(ThermalReader, self).__init__()
		print(queue, stop_event)
		
		print("initing thermal reader")
		self.parser = ThermalParser()
		self.image = None
		self.queue = queue
		self.stop_event = stop_event
		print(self.parser.socket_in)
		self.ts_prev = None
		self.stop_event = stop_event
		self.parser.socket_in.settimeout(timeout)
		self.fps = None

	def run(self):
		while not stop_event.is_set():

			try:
				data, addr = self.parser.socket_in.recvfrom(65000)
				self.parser.parse_packet(data)
			except:
				pass

			if self.parser.image is not None and self.parser.image_ok:
				# print("zed image ok")
				self.image = self.parser.image.copy()
				# print(self.image.shape)
				self.parser.image_ok = False
				self.queue.put(self.image)

				ts = datetime.datetime.now()
				if self.ts_prev is not None:
					dt = ts-self.ts_prev
					self.fps = 1/dt.total_seconds()
					# print(f"FPS thermal: {self.fps}")
				self.ts_prev = ts
				# time.sleep(0.1)


		print("thermal reader stopped")

	def stop(self):
		self.stop_event.set()

	def stopped(self):
		return self.stop_event.is_set()

class LidarReader(mp.Process):

	def __init__(self, queue, stop_event, timeout=0.5):
		super(LidarReader, self).__init__()
		print(queue, stop_event)
		
		print("initing lidar reader")
		self.parser = LidarParser()
		self.pc = None
		self.queue = queue
		self.stop_event = stop_event
		print(self.parser.socket_in)
		self.ts_prev = None
		self.stop_event = stop_event
		self.parser.socket_in.settimeout(timeout)
		self.fps = None

	def run(self):
		while not stop_event.is_set():

			try:
				data, addr = self.parser.socket_in.recvfrom(65000)
				self.parser.parse_packet(data)
			except:
				pass



			# self.pc = np.asarray(pc, dtype=np.float32)
			# self.data_ok = True

			if self.parser.pc is not None and self.parser.data_ok:
				# print("zed image ok")
				self.pc = self.parser.pc.copy()
				self.parser.data_ok = False
				self.queue.put(self.pc)

				ts = datetime.datetime.now()
				if self.ts_prev is not None:
					dt = ts-self.ts_prev
					self.fps = 1/dt.total_seconds()
					# print(f"FPS thermal: {self.fps}")
				self.ts_prev = ts

		print("lidar reader stopped")

	def stop(self):
		self.stop_event.set()

	def stopped(self):
		return self.stop_event.is_set()

class DepthReader(mp.Process):

	def __init__(self, queue, stop_event, timeout=0.5, output_raw=False):
		super(DepthReader, self).__init__()
		print(queue, stop_event)
		
		print("initing depth reader")
		self.parser = DepthParser(output_raw=output_raw)
		self.image = None
		self.queue = queue
		self.stop_event = stop_event
		print(self.parser.socket_in)
		self.ts_prev = None
		self.stop_event = stop_event
		self.parser.socket_in.settimeout(timeout)
		self.fps = None

	def run(self):
		while not stop_event.is_set():

			try:
				data, addr = self.parser.socket_in.recvfrom(65000)
				self.parser.parse_packet(data)
			except:
				pass

			if self.parser.image is not None and self.parser.image_ok:
				# print("zed image ok")
				self.image = self.parser.image.copy()
				# print(self.image.shape)
				self.parser.image_ok = False
				self.queue.put(self.image)

				ts = datetime.datetime.now()
				if self.ts_prev is not None:
					dt = ts-self.ts_prev
					self.fps = 1/dt.total_seconds()
					# print(f"FPS thermal: {self.fps}")
				self.ts_prev = ts
				# time.sleep(0.1)

		print("depth reader stopped")

	def stop(self):
		self.stop_event.set()

	def stopped(self):
		return self.stop_event.is_set()

	def set_raw_output(self, output_raw):
		self.parser.output_raw = output_raw

def draw_fps(image, fps):
	# Get the dimensions of the image
	height, width = image.shape[:2]
	
	# Define the text to display
	text = f"{fps:.2f}"
	
	# Define font scale based on image height
	font_scale = height / 300.0  # You can adjust this factor to scale text size
	
	# Define the font and thickness
	font = cv2.FONT_HERSHEY_SIMPLEX
	thickness = max(1, int(height / 500))  # Thickness scales with the image size
	
	# Calculate border thickness
	border_thickness = thickness + 2
	
	# Get the size of the text box
	text_size = cv2.getTextSize(text, font, font_scale, thickness)[0]
	
	# Calculate the position to draw the text (top right corner)
	text_x = width - text_size[0] - 10  # 10 pixels padding from the right
	text_y = 10 + text_size[1]  # 10 pixels padding from the top
	
	# Draw white border text
	cv2.putText(image, text, (text_x, text_y), font, font_scale, (255, 255, 255), border_thickness)
	
	# Draw black text on top
	cv2.putText(image, text, (text_x, text_y), font, font_scale, (0, 0, 0), thickness)

# TODO remap thermal to ZED


def rt_to_projection_matrix(R,t):
	P = np.hstack((R,t))
	P = np.vstack((P,np.array((0,0,0,1))))

	return P

def precalculate_mapping_matrices(im1s, im2s):

	# print(im1s)
	# print(im2s)

	res = {}

	i,j = np.meshgrid(np.arange(im1s[1]),np.arange(im1s[0]))
	# i=i.ravel()
	# j=j.ravel()
	# Z = np.ones((1, i.size)) # ones for Z, because of normalized coordinates

	p = np.ones((4, i.size), dtype=np.float32)
	p[0,:]=i.ravel()
	p[1,:]=j.ravel()

	return p

def project_image(im1s, im2s, P, M1, M2, t = None, cache = None, verbose = False):

	P = P.astype(np.float32)

	M2_ = np.eye(4)
	M2_[:3,:3]=M2

	inv_M1_ = np.eye(4)
	inv_M1_[:3,:3] = inv(M1)
	
	# unroll depth values
	t = t.ravel()
	t = np.expand_dims(t,0)

	if cache is None:
		i,j = np.meshgrid(np.arange(im1s[1]),np.arange(im1s[0]))
		i=i.ravel()
		j=j.ravel()
		p = np.ones((4, i.size), dtype=np.float32)
		p[0,:]=i
		p[1,:]=j
	else:
		p = cache.copy()

	# multiply indices with distance
	p[:3,:]*=t

	# precalculate CS change matrices
	# i.e. unprojection, CS change, and projection to target CS
	P_ = M2_ @ P @ inv_M1_
	P_ = P_.astype(np.float32)
	# multiply points
	pts = P_ @ p

	# convert back to cartesian
	y = pts[2,:]
	pts[0,:]/=y
	pts[1,:]/=y

	# reshape for OpenCV map
	pts = pts.T.reshape((im1s[0],im1s[1],4))

	return pts[...,:2]

(zed_width, zed_height, zed_M, zed_D) = load_camera_calibration('calibrations/calibration_zed.yaml')
# print((zed_width, zed_height, zed_M, zed_D))
zed_R, zed_T, zed_C = load_lidar_calibration('calibrations/calibration_lidar_zed.yaml')

(thermal_width, thermal_height, thermal_M, thermal_D) = load_camera_calibration('calibrations/calibration_thermal_camera.yaml')
# print((thermal_width, thermal_height, thermal_M, thermal_D))
thermal_R, thermal_T, thermal_C = load_lidar_calibration('calibrations/calibration_lidar_thermal_camera.yaml')


P1 = rt_to_projection_matrix(zed_R, zed_T)
P2 = rt_to_projection_matrix(thermal_R, thermal_T)

# P = compose_matrices(P1, P2)
P = P2 @ inv(P1)

p_cache = precalculate_mapping_matrices((int(zed_height), int(zed_width)), (int(thermal_height), int(thermal_width)))

def main():

	sending_depth = True
	remap_thermal = False
	remap_thermal = True

	sensors = {

		# 'zed': {'reader': ZedReader, 'radius': 2},
		# 'depth': {'reader': DepthReader, 'radius': 2},
		'thermal_camera': {'reader': ThermalReader, 'radius': 1},
		# 'polarization_camera': {'reader': PolarReader, 'radius': 3},
		# 'lidar': {'reader': LidarReader, 'radius': 0},

	}

	for s, v in sensors.items():
		v['queue']=mp.Queue(maxsize=1)
		if 'depth' in s:
			v['cam']=v['reader'](v['queue'], stop_event, output_raw = True if remap_thermal else False)
		else:
			v['cam']=v['reader'](v['queue'], stop_event)

		v['cam'].start()
		v['cam'].parser.start_all()
		cv2.namedWindow(s, cv2.WINDOW_NORMAL)


	# if remap_thermal and 'depth' in sensors.keys():
		# print("setting")
		# sensors['depth']['cam'].parser.output_raw = True
		# print(sensors['depth']['cam'].parser.output_raw)
		# sensors['depth']['cam'].set_raw_output(True)

	if 'zed' in sensors.keys():
		sensors['zed']['cam'].parser.set_parameters(sending_depth=sending_depth)

	zed_image = None
	thermal_image = None
	depth_image = None	

	while True:


		for s, v in sensors.items():

			if 'lidar' in s:
				pc = v['queue'].get(timeout=1)
				print(f'{pc.shape=}')


			else:

				try:
					image = v['queue'].get(timeout=1)
				except:
					continue

				if 'zed' in s:
					zed_image = image.copy()
				elif 'thermal' in s:
					thermal_image = image.copy()
				elif 'depth' in s:
					depth_image = image.copy()




				

				fps = v['cam'].fps
				if fps is not None:
					draw_fps(image, fps)

				try:
					cv2.imshow(s, image)
				except Exception as e:
					print(e)

		if remap_thermal and zed_image is not None and depth_image is not None and thermal_image is not None:
			print("ok")

			zed_image = cv2.resize(zed_image, (int(zed_width), int(zed_height)))
			depth_image = cv2.resize(depth_image, (int(zed_width), int(zed_height)), cv2.INTER_NEAREST)*1e-3

			thermal_image = cv2.undistort(thermal_image, thermal_M, thermal_D)
			# thermal_image = cv2.cvtColor(thermal_image, cv2.COLOR_GRAY2RGB)

			# thermal_image = cv2.normalize(thermal_image, dst=None, alpha=0, beta=65535, norm_type=cv2.NORM_MINMAX)
			# thermal_image = cv2.convertScaleAbs(thermal_image, alpha=255/(2**16))

			print(f'{zed_image.shape=} {zed_image.dtype=}')
			print(f'{depth_image.shape=} {depth_image.dtype=}')
			print(f'{thermal_image.shape=} {thermal_image.dtype=}')

			mpx = project_image((int(zed_height), int(zed_width)), (int(thermal_height), int(thermal_width)), P, zed_M, thermal_M, t = depth_image, cache=p_cache)

			print(np.unique(depth_image))

			# 	print(f'{im.shape=}')
			# 	print(f'{thermal.shape=}')
			# 	# print(f'{mpx.shape=}')

			map1 = cv2.remap(thermal_image, mpx, None, cv2.INTER_LINEAR)
			alpha = 0.25
			map1 = cv2.addWeighted(zed_image, alpha, map1, (1-alpha), 0)
			cv2.imshow("map1", map1)
			# cv2.imshow("thermal_image", thermal_image)

		# if not zed_queue.empty():
		# 	# print("getting from queue")
		# 	zed.image = zed_queue.get()
		# 	# print(zed.image)
		# 	if zed.image.shape==():
		# 		continue
		# 	try:
		# 		cv2.imshow("zed", zed.image)
		# 	except Exception as e:
		# 		print(e)

		# if not polar_queue.empty():
		# 	# print("getting from queue")
		# 	polar.image = polar_queue.get()
		# 	# print(polar.image)
		# 	if polar.image.shape==():
		# 		continue
		# 	try:
		# 		cv2.imshow("polar", polar.image)
		# 	except Exception as e:
		# 		print(e)


		key = cv2.waitKey(1)
		if key == 27:  # ESC key to exit
			print("stopping")
			stop_event.set()
			for s, v in sensors.items():
				v['cam'].stop()
			break
		elif key==ord("e"):
			print("send depth")
			sending_depth = not sending_depth
			sensors['zed']['cam'].set_parameters(sending_depth=int(sending_depth))
		# if not zed.is_alive():
		# 	print("dead")
		# 	break  # Break if the ZedReader process has stopped
		# time.sleep(0.1)

	print("cleaning")

	for s, v in sensors.items():
		v['cam'].parser.stop_all()
		v['cam'].join()

	# # Cleanup after exiting the loop
	# stop_event.set()
	# print("stopped", zed.stopped())
	# zed.parser.stop_all()

	# zed.join()  # Wait for the ZedReader process to finish
	# polar.parser.stop_all()

	# polar.join()  # Wait for the ZedReader process to finish
	cv2.destroyAllWindows()

if __name__ == '__main__':
	main()
