from socket import *
# import time, datetime, math
import numpy as np
# import struct, zlib, binascii
# import pptk
import time
import cv2, os
from read_utils import PolarizationParser, ThermalParser
import threading
from numpy.linalg import inv

thread_stop = False
im_thermal = None
thermal = None

from projection_utils import *


def worker():
	global thread_stop, im_thermal, thermal

	while True:
		# print("lidar thread")
		data, addr = thermal.socket_in.recvfrom(1000)
		# print(addr)
		thermal.parse_packet(data)
		if thermal.image is not None and thermal.image_ok:
			# print(len(thermal.pc))
			im_thermal = thermal.image

		# print(thread_stop)

		if thread_stop:
			break
	return

# TODO get mapping between the images from calibration (transitive, i.e. from polar to zed and from zed to thermal or vice versa)


(M1, M2, D1, D2, R1, t1, R2, t2) = load_calibration_data('calibration_zed_polarization_camera.yaml')
P_zed = rt_to_projection_matrix(R1,t1)
P_polar = rt_to_projection_matrix(R2,t2)

(M1, M3, D1, D3, R1, t1, R3, t3) = load_calibration_data('calibration_zed_thermal_camera.yaml')
P_zed = rt_to_projection_matrix(R1,t1)
P_thermal = rt_to_projection_matrix(R3,t3)

P_zed_polar = np.dot(P_polar, inv(P_zed)) # zed -> polar
P_zed_thermal = np.dot(P_thermal, inv(P_zed)) # zed -> thermal

P_thermal_polar = np.dot(P_zed_polar, inv(P_zed_thermal)) # should be from stereo to zed and then to polar


(width1, height1, M1, D1) = load_camera_calibration('calibration_polarization_camera.yaml')
(width2, height2, M2, D2) = load_camera_calibration('calibration_thermal_camera.yaml')

# print(width1, height1)
# print(width2, height2)

im1 = np.ones((int(height1), int(width1)))
im2 = np.zeros((int(height2), int(width2)))


# mp_stereo_polar = project_image_final(im1, im2, P_thermal_polar, M1, M2, t = t1[0,2])
# mp_stereo_polar = project_image_final(im1, im2, P_thermal_polar, M1, M2, t = t2[0,-1])
mp_stereo_polar = project_image_final(im1, im2, P_thermal_polar, M1, M2, t = 2)

print(mp_stereo_polar.shape)
print(im1.shape)
print(im2.shape)

# exit()

def main():

	global thread_stop, im_thermal, thermal

	R = PolarizationParser()
	thermal = ThermalParser()


	print(R.sensor_type)

	#R.set_parameters(frame_rate=2, num_images=0)
	# R.set_parameters(frame_rate=10, num_images=0, JPEG_compression=10, gain=15)
	R.set_parameters(frame_rate=10, num_images=0, JPEG_compression=10, gain=10, pixel_format=2)
	# N = 10
	# R.set_parameters(frame_rate=N, num_images=N, JPEG_compression=10, gain=10)
	# time.sleep(1)
	# R.set_parameters(acquisition=True, sending=True)
	# R.set_parameters(acquisition=True, sending=False)
	thermal.start_all()
	R.start_all()

	# start thread for thermal
	t = threading.Thread(target=worker)
	t.start()

	# return
	# R.start_sending()

	# R.get_parameters()

	# R.display = True

	alpha=0.5

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		# print(data)

		if R.image is not None and R.image_ok and im_thermal is not None:
			# print(R.image.shape)
			# print(im_thermal.shape)

			R.image = cv2.resize(R.image, None, fx=2, fy=2)

			thermal_to_polar = cv2.remap(im_thermal, mp_stereo_polar, None, cv2.INTER_LINEAR)

			final = cv2.addWeighted(R.image, alpha, thermal_to_polar, (1-alpha), 0.0)

			cv2.imshow("FLIR",R.image)
			# cv2.imshow("thermal",im_thermal)
			cv2.imshow("map",thermal_to_polar)
			cv2.imshow("final",final)

			print("polar", R.image.shape)
			print("mapped", thermal_to_polar.shape)

			# if corners is not None:
			# 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)
			R.image_ok = False

			if cv2.waitKey(1) & 0xFF == ord('q'):
				# thread_stop=True
				# R.stop_all()
				# thermal.stop_all()
				# t.join()
				break
			# break

	thread_stop=True
	R.stop_all()
	thermal.stop_all()
	
	t.join()

if __name__=='__main__':
	main()