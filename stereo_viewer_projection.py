import numpy as np
import socket
import cv2, struct, io, os
from PIL import Image
import subprocess
from datetime import datetime
import threading

from read_utils import LidarParser
from lidar_calibration_qt import eulerAnglesToRotationMatrix

R = LidarParser()
R.set_parameters(fov_start=0, fov_stop=270, azimuth_filter=8, elevation_filter=8)	
R.start_all()

thread_stop = False

pc = None
velo = None

def load_camera_calibration(filename):

	if os.path.isfile(filename):

		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		# M1 = fs.getNode("M1").mat()
		# M2 = fs.getNode("M2").mat()
		# D1 = fs.getNode("D1").mat()
		# D2 = fs.getNode("D2").mat()
		# R = fs.getNode("R").mat()
		# T = fs.getNode("T").mat()
		sz = fs.getNode("imageSize")
		# for a in sz:
		# 	print(a)
		M = fs.getNode("cameraMatrix").mat()
		D = fs.getNode("distCoeffs").mat()
		width = sz.at(0).real()
		height = sz.at(1).real()
		return (width, height, M,D)
		# return 1
	else:
		print("calibration file not found!")

def worker():
	global thread_stop, pc, R

	while True:
		# print("lidar thread")
		data, addr = R.socket_in.recvfrom(1000)
		# print(addr)
		R.parse_packet(data)
		if R.pc is not None:
			# print(len(R.pc))
			pc = R.pc

		# print(thread_stop)

		if thread_stop:
			break
	return

patternsize = (3,5)

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

gray_values = np.arange(256, dtype=np.uint8)
color_values = list(map(tuple, cv2.applyColorMap(gray_values, cv2.COLORMAP_HOT).reshape(256, 3)))

def get_colors(points, rnge):
	global gray_values, color_values	

	colors = []

	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)
	# print(points.shape, dist.shape)


	idx = (dist/rnge)*255

	for i in idx:
		colors.append(color_values[int(i)])


	return np.array(colors,dtype=np.uint8)

def main():
	global thread_stop, pc, R, velo

	

	# print(gray_values)
	# print(color_values)

	# return

	# setup calibration
	width, height, M, D = load_camera_calibration('calibration_stereo_left_full_resolution.yml')
	# lidar_calib = {'x': 0.0, 'y': -1.6, 'z': 1.6, 'yaw': -23, 'pitch': 0, 'roll': 0}
	lidar_calib = {'x': 0.2, 'y': -0.33, 'z': -0.15, 'yaw': 0, 'pitch': 0, 'roll': 1.5}
	l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
	rnge = 10
	R_velo = eulerAnglesToRotationMatrix([lidar_calib['pitch'], lidar_calib['yaw'], lidar_calib['roll']])
	T_velo = np.array([lidar_calib['x'], lidar_calib['y'], lidar_calib['z']])

	# print(M)
	# return

	# TODO also fire up lidar and get data

	# camera = 0
	camera = 1
	local_IP = '192.168.90.14'
	# local_IP = '192.168.90.79'

	# stolen from: https://stackoverflow.com/questions/45924220/netcat-h-264-video-from-raspivid-into-opencv

	# Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
	# all interfaces)
	server_socket = socket.socket()
	server_socket.bind(('0.0.0.0', 1200))
	server_socket.listen(0)

	t = threading.Thread(target=worker)
	t.start()

	# datetime object containing current date and time
	# now = 
	 
	start_time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
	print("date and time: ", start_time)

	out_dir = 'stereo_out/'+start_time+'/'

	cnt=0
	write = False
	project_lidar = True

	if write:
		try:
			os.makedirs(out_dir)
		except OSError as error:
			print(error)
			print("Directory '%s' can not be created")

	# start raspivid on RPi
	IP = '192.168.90.71'
	command = f"python camera_stream.py {camera} {local_IP}"
	subprocess.call(['ssh', '-t', '-f', "pi@"+IP, "nohup", command], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

	# Accept a single connection and make a file-like object out of it
	connection = server_socket.accept()[0].makefile('rb')
	
	# try:

	R = LidarParser()
	R.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=8, elevation_filter=8)
	R.start_all()

	while True:
		# print("image thread")
		# print("waiting")
		# Read the length of the image as a 32-bit unsigned int. If the
		# length is zero, quit the loop
		image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]
		if not image_len:
			break
		# Construct a stream to hold the image data and read the image
		# data from the connection
		image_stream = io.BytesIO()
		image_stream.write(connection.read(image_len))
		# Rewind the stream, open it as an image with opencv and do some
		# processing on it
		image_stream.seek(0)
		image = Image.open(image_stream)

		data = np.fromstring(image_stream.getvalue(), dtype=np.uint8)
		im = cv2.imdecode(data, 1)

		# print(im.shape)
		im = cv2.undistort(im, M, D)

		if write:
			cv2.imwrite(f'{out_dir}{str(cnt)}.jpg',im)

		# fix contrast
		if camera==0:
			# https://stackoverflow.com/a/58211607
			# alpha 1.0-3.0
			# beta 0-100
			im = cv2.convertScaleAbs(im, alpha=1, beta=0)

		# corners = find_corners(im)
		# print(corners)

		# while True:
		# data, addr = R.socket_in.recvfrom(1000)
		# # print(addr)
		# R.parse_packet(data)

		# print(R.pc)
		skip = 1
		velo = pc.copy()
		
		if velo is not None:

			if len(velo)>0:
			
				velo = velo[::skip, 0:3].T
				# print(len(pc))
				# print("pc", pc.shape)

				# img = im.copy()
				

				# print("velo", velo.shape)
				velo = velo[:,np.sqrt(velo[0,:]**2+velo[1,:]**2+velo[2,:]**2)<rnge]
				velo = velo[:,velo[1,:]>0] # remove points behind the camera				
				velo = l2im.dot(velo)
				# continue

				# print("velo 2", velo.shape)

				# print(velo[0,:].shape, np.mean(velo[0,:]))
				# print(velo[1,:].shape, np.mean(velo[1,:]))
				# print(velo[2,:].shape, np.mean(velo[2,:]))

			
				if len(velo)>0:
					pts, _ = cv2.projectPoints(velo, R_velo, T_velo, M, distCoeffs=np.array([]))

					colors = get_colors(velo, rnge=rnge)

					# print(pts.shape, colors.shape)

					pts = pts[:,0,:]
					colors = colors[pts[:,0]>0]
					pts = pts[pts[:,0]>0]
					colors = colors[pts[:,1]>0]
					pts = pts[pts[:,1]>0]
					colors = colors[pts[:,0]<width]
					pts = pts[pts[:,0]<width]
					colors = colors[pts[:,1]<height]
					pts = pts[pts[:,1]<height]

					# colors = get_colors(velo, rnge=rnge)
					# print()

					# print(len(pts), len(colors))

					for point, clr in zip(pts, colors):
					# for point in pts:
						point = (int(point[0]), int(point[1]))
						# clr = (0,0,255)
						# print(clr)
						clr = (int(clr[0]), int(clr[1]), int(clr[2]))
						# print(clr, type(clr[0]), type(clr))
						im = cv2.circle(im,point,4, clr, -1)

		
			# cv2.imshow("Frame",img)
			
		# else:

		cv2.imshow("Frame",im)

		# if corners is not None:
		# 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)
		

		if cv2.waitKey(1) & 0xFF == ord('q'):
			# thread_stop=True
			# R.stop_all()
			# t.join()
			break

		cnt+=1

		# cv2.destroyAllWindows() #cleanup windows 
	# finally:
	thread_stop=True
	connection.close()
	server_socket.close()
	R.stop_all()
	
	t.join()



if __name__=='__main__':
	main()