# a general script for mapping one camera image onto another (if both camera-lidar calibrations are known)

from socket import *
# import time, datetime, math
import numpy as np
# import struct, zlib, binascii
# import pptk
import time
import cv2, os, signal, sys
from read_utils import PolarizationParser, ThermalParser, ZEDParser, RGBParser
import threading
from numpy.linalg import inv

from projection_utils import *



sensor1 = 'zed'
# sensor1 = 'thermal_camera'
# sensor1 = 'stereo_front_left'
# sensor2 = 'stereo_front_left'
# sensor2 = 'stereo_front_right'
sensor2 = 'polarization_camera'

parsers = {'zed': ZEDParser, 'polarization_camera': PolarizationParser, 'thermal_camera': ThermalParser, 'stereo_front_left': RGBParser, 'stereo_front_right': RGBParser}

thread_stop = False

def worker(parser):

	global thread_stop

	while not thread_stop:
		data, addr = parser.socket_in.recvfrom(1000)
		# print(data)
		parser.parse_packet(data)

def main():

	global thread_stop

	s1 = parsers[sensor1]()
	s2 = parsers[sensor2]()

	(width1, height1, M1, D1) = load_camera_calibration(f'helper_calibrations/calibration_{sensor1}_helper.yaml')
	(width2, height2, M2, D2) = load_camera_calibration(f'helper_calibrations/calibration_{sensor2}_helper.yaml')

	# print(width1, height1)
	# print(width2, height2)

	# return

	# print(s1)

	# return

	# zed = ZEDParser()
	# thermal = ThermalParser()
	# polar = PolarizationParser()

	# zed.start_acquisition()
	# polar.start_all()

	s1.start_all()
	s2.start_all()

	t1 = threading.Thread(target=worker, args=[s1])
	t1.start()

	t2 = threading.Thread(target=worker, args=[s2])
	t2.start()

	key = -1
	while key & 0xFF != ord("q"):

		key = cv2.waitKey(1)

		if s1.image_ok:
			im1 = s1.image.copy()
			# print(im1.shape)

			cv2.imshow(sensor1, im1)

		if s2.image_ok:
			im2 = s2.image.copy()
			# print(im2.shape)

			cv2.imshow(sensor2, im2)

	thread_stop = True

	print("main loop finished")

	t1.join()
	t2.join()

	s1.stop_all()
	s2.stop_all()

	

if __name__=='__main__':
	main()

