from socket import *
# import time, datetime, math
import numpy as np
# import datetime
import time, threading
import cv2, os
import datetime
from matplotlib import pyplot as plt


from read_utils import ThermalParser, LidarParser
# from live_client import load_camera_calibration

from calibration_utils import *

def load_lidar_calibration(filename):
	if os.path.isfile(filename):
		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		R = fs.getNode("R").mat()
		T = fs.getNode("T").mat()
		C = fs.getNode("C").mat()
		
		return (R, T, C)
	else:
		print("calibration file not found!")

thread_stop = False
lidar = None
pc = None

calib_fn = 'helper_calibrations/calibration_thermal_camera_helper.yaml'

(width, height, M, D) = load_camera_calibration(calib_fn)

# f = 0.2
# M[0,0]*=f
# M[1,1]*=f



patternsize = (3,5)

# n_iter = 1000
# repr_err = 0.001
# err_thr = 0.5
# err_thr = 1.0

def worker():
	global thread_stop, pc, lidar

	while True:
		# print("lidar thread")
		data, addr = lidar.socket_in.recvfrom(1000)
		# print(addr)
		lidar.parse_packet(data)
		if lidar.pc is not None:
			# print(len(R.pc))
			pc = lidar.pc
			# pc = pc[:,0:3]
			# print(pc)

		# print(thread_stop)

		if thread_stop:
			break

	lidar.stop_sending()

def get_colors(points, rnge=100, relative=False, cmap_name='Wistia', cmap_invert=False, cmap_len=256):
	colors = []
	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)

	if relative:
		rnge = np.max(dist)

	f = dist/rnge
	f = np.clip(f, 0, 1)

	cmap = plt.get_cmap(cmap_name)
	sm = plt.cm.ScalarMappable(cmap=cmap)
	if cmap_invert:
		color_range = sm.to_rgba(np.linspace(0, 1, cmap_len))[:,0:3][::-1]
	else:
		color_range = sm.to_rgba(np.linspace(0, 1, cmap_len))[:,0:3]

	idx = f*(cmap_len-1)

	for i in idx:
		clr = color_range[int(i),:]
		colors.append(clr)

	return np.array(colors)

def project_lidar_points(point_cloud, sz, R, t, M, D, rn=1e6, cmap_name='Wistia', cmap_invert=False, cmap_len=65536):
		pc = point_cloud[:,:3].copy().T
		idx = (pc[2,:]>0) # remove points behind camera
		pc = pc[:,idx]

		if pc.shape[1]==0:
			return pc, None, None

		colors = get_colors(pc, rn, cmap_name=cmap_name, cmap_invert=cmap_invert, cmap_len=cmap_len)

		pts, _ = cv2.projectPoints(pc, R, t, M, distCoeffs=D)
		pts = pts[:,0,:]
		mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<sz[1]-1) & (pts[:,1]<sz[0]-1) # create mask for valid pixels

		return pts, colors, mask

def main():
	global thread_stop, pc, lidar

	R_lidar, T_lidar, C_lidar = load_lidar_calibration(f"helper_calibrations/calibration_lidar_thermal_camera.yaml")

	# R_lidar = eulerAnglesToRotationMatrix((0,14,0))

	roll, pitch, yaw = rotationMatrixToEulerAngles(R_lidar)
	offx, offy, offz = T_lidar[:,0]

	offx = -0.05; 	offy = -0.66; 	offz = -0.1; pitch = -11; 	roll = 0; 	yaw = 1.0; 	
	offx = 0.05; 	offy = -0.7; 	offz = -0.15; pitch = -14; 	roll = 0; 	yaw = -1.0; 	
	print(offx, offy, offz, roll, pitch, yaw)

	# offx = 0.07; offy = -0.73; offz = 0.21;
	# roll = 0.80; pitch = -9.10; yaw = 0.30

	dt = 0.025
	da = 0.3

	dx = dy = dz = dt
	dr = dp = dw = da

	window_name = "thermal"
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

	R = ThermalParser()
	# R.start_sending()
	R.start_all()

	lidar = LidarParser()
	# lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
	lidar.start_all()
	# lidar.start_all()

	t = threading.Thread(target=worker)
	t.start()

	ts_prev = None
	detect_target = True
	detect_target = False

	grid = get_grid()
	cnt = 0
	point_size = 2

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		# print(data)

		# calculate current calibration
		lidar_calib = {'x': offx, 'y': offy, 'z': offz, 'yaw': yaw, 'pitch': pitch, 'roll': roll}
		R_lidar = eulerAnglesToRotationMatrix([lidar_calib['pitch'], lidar_calib['yaw'], lidar_calib['roll']]) # TODO load this from yaml file
		T_lidar = np.array([lidar_calib['x'], lidar_calib['y'], lidar_calib['z']])

		# print(offx, offy, offz, roll, pitch, yaw)


		if R.image is not None and R.image_ok:
			# print(R.image.shape)
			ts = datetime.datetime.now()

			# R.image = cv2.undistort(R.image, M, D)
			im = R.image.copy()
			print(im.shape, im.dtype)
			# im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			R.image_ok = False

			# print(im.shape)
			if detect_target:
				corners = find_corners(im)
				# print(corners)
				if corners is not None:

					_, rvec, tvec, _ = cv2.solvePnPRansac(grid, corners, M, D, iterationsCount = n_iter, reprojectionError = repr_err)
					pts, _ = cv2.projectPoints(grid, rvec, tvec, M, distCoeffs=D)

					e = np.mean(np.abs(pts-corners))
					# print(e)

					if e>err_thr:
						timestamp = int(round(ts.timestamp()))

						cv2.imwrite(f"{out_dir}{timestamp}_{e:.3f}.jpg", im)

					
					cv2.drawChessboardCorners(im, patternsize, corners, True)
					
					
			# if pc is not None:
			# 	ldr = pc.copy()
			# 	# print(ldr)
			# 	# print(ldr.shape)
			# 	# if not ldr:
			# 	# 	continue
			# 	ldr[:,:3] = (C_lidar @ ldr[:,:3].T).T
			# 	# pts, colors, _ = project_lidar_points(ldr, im.shape, R_lidar, T_lidar, M, D, rn=3)
			# 	pts, colors, mask = project_lidar_points(ldr, im.shape, R_lidar, T_lidar, M, np.array([]), rn=3)
			# 	pts = pts[mask,:]
			# 	colors = colors[mask]

			# 	if pts.shape[0]!=0 and pts is not None:
			# 		# print(pts.shape)
			# 		for point, clr in zip(pts, colors):
			# 			# for point in pts:
			# 			point = (int(point[0]), int(point[1]))
			# 			# clr = (0,0,255)
			# 			# print(clr)
			# 			clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
			# 			# print(clr, type(clr[0]), type(clr))
			# 			try:
			# 				cv2.circle(im, point,point_size, clr, -1)
			# 			except:
			# 				print(point)
					

			# im = cv2.applyColorMap(im, cv2.COLORMAP_PLASMA)

			cv2.imshow(window_name, im)

			# calculate fps
			# if ts_prev is not None:
			# 	dt = ts-ts_prev
			# 	delay = dt.total_seconds() * 1000
			# 	print(delay)
			# 	if delay!=0:
			# 		fps = 1000/delay
			# 		print(f"FPS: {fps}")
			# ts_prev = ts

			# if corners is not None:
			# 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)
			
			key = cv2.waitKey(1)

			if key & 0xFF == ord("e"):
				offx-=dx
			elif key & 0xFF == ord("r"):
				offx+=dx
			elif key & 0xFF == ord("s"):
				offy-=dy
			elif key & 0xFF == ord("d"):
				offy+=dy
			elif key & 0xFF == ord("x"):
				offz-=dz
			elif key & 0xFF == ord("c"):
				offz+=dz

			elif key & 0xFF == ord("u"):
				roll-=dr
			elif key & 0xFF == ord("i"):
				roll+=dr
			elif key & 0xFF == ord("j"):
				pitch-=dp
			elif key & 0xFF == ord("k"):
				pitch+=dp
			elif key & 0xFF == ord("n"):
				yaw-=dw
			elif key & 0xFF == ord("m"):
				yaw+=dw
			elif key & 0xFF == ord('q'):
				thread_stop=True
				R.stop_all()
				t.join()
				break
			# break

	# R.stop_sending()

if __name__=='__main__':
	main()