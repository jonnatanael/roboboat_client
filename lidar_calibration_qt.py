from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt, QTimer, QObject, QThread, pyqtSignal, QDateTime
import cv2
import sys, time, signal, os
import numpy as np
import pyqtgraph.opengl as gl
import pyqtgraph as pg

from gui_utils import CompassWidget
from read_utils import RadarParser, ThermalParser, PolarizationParser, GPSParser, LidarParser

def eulerAnglesToRotationMatrix(theta):
	theta = [np.radians(x) for x in theta]
	
	R_x = np.array([[1, 0, 0],
					[0, np.cos(theta[0]), -np.sin(theta[0]) ],
					[0, np.sin(theta[0]), np.cos(theta[0]) ]
					])
		
		
					
	R_y = np.array([[np.cos(theta[1]), 0,np.sin(theta[1]) ],
					[0, 1,0],
					[-np.sin(theta[1]),0,np.cos(theta[1]) ]
					])
				
	R_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
					[np.sin(theta[2]), np.cos(theta[2]), 0],
					[0, 0,1]
					])
					
	R = np.dot(R_x, np.dot( R_y, R_z ))

	return R

def load_camera_calibration(filename):

	if os.path.isfile(filename):

		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		# M1 = fs.getNode("M1").mat()
		# M2 = fs.getNode("M2").mat()
		# D1 = fs.getNode("D1").mat()
		# D2 = fs.getNode("D2").mat()
		# R = fs.getNode("R").mat()
		# T = fs.getNode("T").mat()
		sz = fs.getNode("imageSize")
		# for a in sz:
		# 	print(a)
		M = fs.getNode("cameraMatrix").mat()
		D = fs.getNode("distCoeffs").mat()
		width = sz.at(0).real()
		height = sz.at(1).real()
		return (width, height, M,D)
		# return 1
	else:
		print("calibration file not found!")

def construct_flir_matrix():
	(width, height, M, D) = load_camera_calibration("calibration_flir.yml")

	T = np.zeros((3,1))
	T = np.array([[0],[0],[0]])
	r = np.array([[1,0,0],[0,1,0],[0,0,1]])
	C = np.vstack((np.hstack((r, T)),np.array([0,0,0,1])))
	M = np.hstack((M, np.zeros((3,1))))
	P = np.dot(M, C)

	return P

class LidarWorker(QObject):
	finished = pyqtSignal()
	data = pyqtSignal(object)

	def __init__(self):
		super(LidarWorker, self).__init__()
		self.parser = LidarParser()

	def run(self):
		# self.parser.set_parameters(azimuth_filter=2, elevation_filter=1, fov_start = 0, fov_stop = 180)
		self.parser.set_parameters(azimuth_filter=4, elevation_filter=1, fov_start = 0, fov_stop = 360)
		time.sleep(1)
		self.parser.start_all()

		while True:

			data, addr = self.parser.socket_in.recvfrom(1000)
			self.parser.parse_packet(data)
			# print(data)
			if self.parser.pc is not None and len(self.parser.pc)!=0:
				self.data.emit(self.parser.pc)

				# self.parser.project_to_image()

			# time.sleep(1)

class PolarizationWorker(QObject):
	finished = pyqtSignal()
	data = pyqtSignal(object)

	def __init__(self):
		super(PolarizationWorker, self).__init__()
		self.parser = PolarizationParser()

	def run(self):
		self.parser.set_parameters(num_images=0, frame_rate=10)
		time.sleep(1)
		self.parser.start_all()


		while True:
			data, addr = self.parser.socket_in.recvfrom(1000)
			self.parser.parse_packet(data)
			if self.parser.image is not None:
				self.data.emit(self.parser.image)
				self.parser.image = None

class PointCloudDisplay(gl.GLViewWidget):

	def __init__(self, parent=None):
		super().__init__(parent=parent)


	def keyPressEvent(self, event):
		self.parent().keyPressEvent(event)

class ClientWidget(QtWidgets.QWidget):
	def __init__(self, parent=None):
		super().__init__(parent=parent)

		self.resize(1280, 720)

		self.layout = QtWidgets.QHBoxLayout()
		self.setLayout(self.layout)

		# group sensor widgets
		hbox = QtWidgets.QHBoxLayout()
		groupBox = QtWidgets.QGroupBox(self)
		groupBox.setLayout(hbox)

		self.image_frame_polar = QtWidgets.QLabel()
		self.layout.addWidget(self.image_frame_polar)

		# flir
		self.thread_polar = QThread()
		self.worker_polar = PolarizationWorker()
		self.worker_polar.moveToThread(self.thread_polar)

		self.thread_polar.started.connect(self.worker_polar.run)
		self.worker_polar.data.connect(self.updatePolarization)
		self.thread_polar.start()

		# lidar
		self.thread_lidar = QThread()
		self.worker_lidar = LidarWorker()
		self.worker_lidar.moveToThread(self.thread_lidar)

		self.thread_lidar.started.connect(self.worker_lidar.run)
		self.worker_lidar.data.connect(self.updateLidar)
		self.thread_lidar.start()

		# lidar widgets
		# self.gl_lidar = gl.GLViewWidget()
		self.gl_lidar = PointCloudDisplay()
		self.layout.addWidget(self.gl_lidar)
		self.gl_lidar.show()
		self.grid = gl.GLGridItem()
		self.gl_lidar.addItem(self.grid)

		# hbox.addWidget(self.gl_lidar)

		# set size
		# self.gl_lidar.resize(700, 700)
		self.gl_lidar.show()

		self.sp1_lidar = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
		self.gl_lidar.addItem(self.sp1_lidar)
		self.sp2_lidar = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
		self.gl_lidar.addItem(self.sp2_lidar)


		self.layout.addWidget(groupBox)

		# add calibration buttons
		vbox = QtWidgets.QVBoxLayout()
		buttonBox = QtWidgets.QGroupBox(self)
		buttonBox.setLayout(vbox)
		parameters = ['x', 'y', 'z', 'roll','pitch','yaw']

		self.lidar_calib = {'x': 0.0, 'y': -1.6, 'z': 1.6, 'yaw': -23, 'pitch': 0, 'roll': 0} # with old calibration
		# self.lidar_calib = {'x': 0.0, 'y': 0, 'z': 0, 'yaw': 0, 'pitch': 0, 'roll': 0} # new calibration
		self.trans_delta = 0.05
		self.rot_delta = 2

		self.param_labels = {}
		for p in parameters:

			hb = QtWidgets.QHBoxLayout()
			paramBox = QtWidgets.QGroupBox(self)
			paramBox.setLayout(hb)

			paramBox.setObjectName(p+'_main')

			lbl1 = QtWidgets.QLabel(p)
			minus = QtWidgets.QPushButton("-")
			lbl2 = QtWidgets.QLabel("{:.2f}".format(self.lidar_calib[p]))

			plus = QtWidgets.QPushButton("+")

			minus.setObjectName(p+'_minus')
			plus.setObjectName(p+'_plus')
			# plus.setObjectName(p+'_label')
			# print(p+'_label')

			hb.addWidget(lbl1)
			hb.addWidget(minus)
			hb.addWidget(lbl2)
			hb.addWidget(plus)

			minus.clicked.connect(self.handle_button)
			plus.clicked.connect(self.handle_button)

			self.param_labels[p]=lbl2

			vbox.addWidget(paramBox)

		self.layout.addWidget(buttonBox)
		

		self.counter_thermal = 0
		self.counter_polar = 0

		self.timer=QTimer()
		# self.timer.timeout.connect(self.showTime)
		# self.timer.start(1000)

		self.signal = signal.signal(signal.SIGINT, self.signal_handler)

		# set internal data storage

		self.thermal_image = None
		self.flir_image = None
		self.lidar_scan = None

		self.flir_matrix = construct_flir_matrix()

		self.flir_image_projected = None

		# lidar_calib = {'df': 0.1, 'dv': -0.465, 'dh': 0.16, 'dy': 1.2, 'dp': 0, 'dr': 1.5}
		# lidar_calib = {'df': 0.1, 'dv': -0.465, 'dh': 0.16, 'dy': -25, 'dp': 0, 'dr': 1.5}
		# self.lidar_calib = {'x': 0.0, 'y': 0, 'z': 0, 'yaw': 0, 'pitch': 0, 'roll': 0}
		



	def handle_button(self, a):
		name = self.sender().objectName()
		clicked = pyqtSignal()

		print(name, clicked)

		nm = name.split('_')[0]

		direction = 1 if name.split('_')[1]=='plus' else -1

		print(nm, direction)

		if nm == 'x' or nm=='y' or nm=='z':
			self.lidar_calib[nm]+=direction*self.trans_delta
		else:
			self.lidar_calib[nm]+=direction*self.rot_delta

		self.param_labels[nm].setText("{:.2f}".format(self.lidar_calib[nm]))

		# print(self.lidar_calib)
		# print(self, f"{nm}_label", self.layout.itemAt(2).widget())
		# widget = self.layout.itemAt(2).widget().findChild(QtWidgets.QLabel, f"{nm}_main")
		# print(widget)

	def closeEvent(self, event):
		# self.worker_gps.parser.stop_all()
		# self.worker_thermal.parser.stop_all()
		self.worker_polar.parser.stop_all()
		# self.worker_radar.parser.stop_all()
		self.worker_lidar.parser.stop_all()
		event.accept()

	def signal_handler(self, sig, frame):
		p = self.worker_polar.parser
		# print(p)
		p.stop_all()

		sys.exit()

	def showTime(self):
		time=QDateTime.currentDateTime()
		print(time, self.counter_thermal, self.counter_polar)

		self.counter_thermal = 0
		self.counter_polar = 0

	def updatePolarization(self, image):
		self.counter_polar+=1

		self.flir_image = image

		if self.lidar_scan is not None:
			# print("projecting")
			image = self.project_lidar_to_flir()

		self.image_polar = QtGui.QImage(image.data, image.shape[1], image.shape[0], image.strides[0], QtGui.QImage.Format_RGB888 ).rgbSwapped()
		pixmap = QtGui.QPixmap.fromImage(self.image_polar)
		pixmap = pixmap.scaled(image.shape[1], image.shape[0], QtCore.Qt.KeepAspectRatio)

		self.image_frame_polar.setPixmap(pixmap)

		

	def project_lidar_to_flir(self):
		# print(self.lidar_calib)
		
		(width, height, M, D) = load_camera_calibration("calibration_flir_new.yml")
		

		R_velo = eulerAnglesToRotationMatrix([self.lidar_calib['pitch'], self.lidar_calib['yaw'], self.lidar_calib['roll']])
		T_velo = np.array([self.lidar_calib['x'], self.lidar_calib['y'], self.lidar_calib['z']])

		# print(self.lidar_scan.shape)

		skip = 1
		pc = self.lidar_scan[::skip, 0:3].T
		rnge = 3
		l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
		# l2im = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
		pc=pc[:,np.sqrt(pc[0,:]**2+pc[1,:]**2+pc[2,:]**2)<rnge]
		pc = pc[:,pc[1,:]>0] # remove points behind the camera
		pc = l2im.dot(pc)

		# print(pc[0,:].shape, np.mean(pc[0,:]))
		# print(pc[1,:].shape, np.mean(pc[1,:]))
		# print(pc[2,:].shape, np.mean(pc[2,:]))

		pts, _ = cv2.projectPoints(pc, R_velo, T_velo, M, distCoeffs=np.array([]))
		pts = pts[:,0,:]
		pts = pts[pts[:,0]>0]
		pts = pts[pts[:,1]>0]
		pts = pts[pts[:,0]<width]
		pts = pts[pts[:,1]<height]



		print(pts.shape)

		for point in pts:
			point = (int(point[0]), int(point[1]))
			self.flir_image = cv2.circle(self.flir_image,point,4,(0,0,255), -1)
			# self.updatePolarization(self.flir_image)

		return self.flir_image


	def updateLidar(self, data):
		# print(data.shape)
		# print(data[1:10,:])
		# data*=1e3
		# print(data[1:10,:])

		# self.sp1_lidar.setData(pos=pts)
		# color = np.zeros((pts.shape[0],3), dtype=np.float32)
		# color[:,0]=1
		# self.sp1_lidar.setData(color=color, size = 5)

		# self.sp1_lidar.setData(pos=np.array([[0,0,0]]))
		# color = np.zeros((1,3), dtype=np.float32)
		# color[:,0]=1
		# self.sp1_lidar.setData(color=color, size=20)

		skip = 1
		d = data[::skip,0:3]

		self.sp2_lidar.setData(pos=d)
		color = np.zeros((d.shape[0],3), dtype=np.float32)
		color[:,1]=1
		self.sp2_lidar.setData(color=color, size = 5)

		self.lidar_scan = data

		# if self.flir_image is not None:
		# 	# print("projecting")
		# 	self.project_lidar_to_flir()

	def keyPressEvent(self, e):

		if e.key() == QtCore.Qt.Key_Escape:
			self.close()

	def updateRadar(self, data):
		# circle
		n = np.arange(0,2*np.pi,2*np.pi/200)
		rn = 33
		x = np.sin(n)*rn
		y = np.cos(n)*rn
		z = np.zeros_like(n)
		pts = np.vstack((x,y,z)).T

		self.sp1_radar.setData(pos=pts)
		color = np.zeros((pts.shape[0],3), dtype=np.float32)
		color[:,0]=1
		self.sp1_radar.setData(color=color, size = 5)

		# self.sp1_radar.setData(pos=np.array([[0,0,0]]))
		# color = np.zeros((1,3), dtype=np.float32)
		# color[:,0]=1
		# self.sp1_radar.setData(color=color, size=20)

		self.sp2_radar.setData(pos=data[:,0:3])
		color = np.zeros((data.shape[0],3), dtype=np.float32)
		color[:,1]=1
		self.sp2_radar.setData(color=color, size = 5)

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	radar_widget = ClientWidget()
	radar_widget.show()

	timer = QTimer()
	timer.timeout.connect(lambda: None)
	timer.start(100)

	app.exec_()
