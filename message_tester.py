# sends different messages to the server, used for development purposes
# i.e. check if server gets and interprets the messages correctly

import socket, math
import numpy as np
import struct, zlib, binascii, time, datetime, signal, sys
from read_utils import ZEDParser
from socket import *

R = ZEDParser()

def sigint_handler(sig, frame):

	R.set_parameters(acquisition=int(False))

	R.stop_all()
	sys.exit()

def main():

	signal.signal(signal.SIGINT, sigint_handler)


	# R.set_parameters(fov_start=0, fov_stop=360, sending=1, acquisition=1, write=1)
	# R.set_parameters(fov_start=0, fov_stop=360, sending=1, acquisition=1, write=1)
	# R.set_parameters(sending=1, acquisition=1)
	# R.set_parameters(fov_start=0, fov_stop=360, acquisition=1)


	# R.set_parameters(azimuth_filter=4, elevation_filter=4, fov_start=30, fov_stop=40)
	# R.set_parameters(azimuth_filter=1, elevation_filter=1, fov_start=0, fov_stop=360)
	# return

	# R.start_all()
	# R.set_parameters(self, return_mode=None, rpm=None, azimuth_filter=None, elevation_filter=None, fov_start=None, fov_stop=None, acquisition=None, sending=None, reboot=None):


	# R.set_parameters(reboot=int(True))
	# R.set_parameters(shutdown=int(True))
	R.set_parameters(acquisition=1)
	# time.sleep(20)
	# R.set_parameters(acquisition=0)
	# R.set_parameters(sending_rgb=int(True))
	# R.set_parameters(sending_rgb=int(False))
	# R.set_parameters(sending_depth=int(True))
	# R.set_parameters(sending_depth=int(False))
	# R.set_parameters(set_resolution=int(True), set_height=100, set_width=100)
	# R.set_parameters(set_resolution=int(True), set_height=200, set_width=200)

	# R.stop_sending()
	# R.get_parameters(status=True)
	# R.get_parameters(diagnostics=True)


	# zed flags testing
	# R.set_parameters(acquisition=int(True))
	# time.sleep(2)

	# R.set_parameters(sending_rgb=int(True))
	# R.set_parameters(sending_depth=int(True))

	# time.sleep(2)

	# R.set_parameters(sending_rgb=int(False))
	# time.sleep(1)


	# R.set_parameters(acquisition=int(False))


	
	return

	while True:
		# try:
		data, addr = R.socket_in.recvfrom(63000)
		# print(addr)
		R.parse_packet(data)
		# print("waiting")
		# except:
		# 	print("timeout")
		if R.image_ok:
			print(R.image.shape)
			R.image_ok = False
		# if R.pc is not None:
			# print(len(R.pc))


if __name__=='__main__':
	main()
