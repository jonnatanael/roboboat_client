from socket import *
import time, datetime, math
import numpy as np
# import struct, zlib, binascii, sys
# import pptk
from read_utils import ThermalParser
import cv2
# from PIL import Image

def main():

	window_name = "thermal"
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

	# R = ThermalParser(IP='192.168.90.72', in_port=27132)
	# R = ThermalParser(in_port=27132)
	R = ThermalParser()

	# R.set_parameters(num_images=5, shutterless=1, auto_gain_control=2)
	#R.set_parameters(num_images=0, frame_rate=10)
	# R.set_parameters(num_images=0, frame_rate=100, acquisition=True, sending=False)
	# R.set_parameters(num_images=0, frame_rate=10, auto_gain_control=1)
	# time.sleep(1)

	# return
	# R.get_parameters()
	# R.start_all()
	# R.start_sending()
	# R.stop_acquisition()


	# R.start_acquisition()
	R.stop_all()


	return

	ts_prev = None

	while True:
		# print("waiting")
		
		# data, addr = R.socket_in.recvfrom(1000)
		data, addr = R.socket_in.recvfrom(65000)
		# print(data)

		print(R.parameters)
		R.get_parameters()


		R.parse_packet(data)
		# print(data)
		# if R.parameters is not None:
		# 	print(R.parameters)
		# R.display()
		if R.image is not None and R.image_ok:
			ts = datetime.datetime.now()
			# print(ts)
			# print(R.image.shape)

			im = R.image.copy()
			print(im.shape)

			cv2.imshow(window_name, im)

			if ts_prev is not None:
				dt = ts-ts_prev
				delay = dt.total_seconds() * 1000
				# print(delay)
				fps = 1000/delay
				print(f"FPS: {fps}")
			ts_prev = ts

			# if corners is not None:
			# 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)
			R.image_ok = False

			if cv2.waitKey(1) & 0xFF == ord('q'):
				# thread_stop=True
				R.stop_all()
				# t.join()
				break
			# break

if __name__=='__main__':
	main()