from socket import *
# import time, datetime, math
import numpy as np
# import datetime
import time
import cv2, os
import datetime


from read_utils import PolarizationParser
# from live_client import load_camera_calibration

from calibration_utils import *


calib_fn = '/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_polarization_camera.yaml'

(width, height, M, D) = load_camera_calibration(calib_fn)
# R_lidar, T_lidar = load_lidar_calibration(f"helper_calibrations/calibration_lidar_polarization_camera.yaml")

patternsize = (3,5)

n_iter = 1000
repr_err = 0.001
err_thr = 0.5
err_thr = 1.0

out_dir = 'polarization_extra/'

try:
	os.mkdir(out_dir)
except:
	pass

start_time = time.time()
def start_timing():
	global start_time

	start_time = time.time()

def end_timing(s=''):


	print(f'{s} took: {(time.time() - start_time)} seconds')

	# print("--- %s seconds ---" % (time.time() - start_time))

def simple():

	window_name = "FLIR"
	# cv2.namedWindow(window_name, cv2.WINDOW_NORMAL) 

	R = PolarizationParser()
	ts_prev = None
	R.start_sending()
	# R.start_all()
	# R.stop_all()

	# return

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(65000)
		R.parse_packet(data)
		# print(data)

		if R.image is not None and R.image_ok:		

			
			im = R.image.copy()


			ts = datetime.datetime.now()
			if ts_prev is not None:
				dt = ts-ts_prev
				fps = 1/dt.total_seconds()
				print(f"FPS FLIR: {fps}")
			ts_prev = ts


			# print(f'{im.shape=}')
			# print(D)
			cv2.imshow(window_name, im)

			R.image_ok = False

			if cv2.waitKey(1) & 0xFF == ord('q'):
				# thread_stop=True
				R.stop_sending()
				# t.join()
				break
			# break

	R.stop_sending()
	

def main():

	window_name = "FLIR"
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL) 

	R = PolarizationParser()
	# R.start_sending()
	R.start_all()

	ts_prev = None
	detect_target = True
	detect_target = False

	grid = get_grid()
	cnt = 0

	k1_bins = 100
	k2_bins = 100
	k3_bins = 100

	cv2.createTrackbar('k1', window_name, 0, k1_bins, lambda x: x)
	cv2.createTrackbar('k2', window_name, 0, k2_bins, lambda x: x)
	cv2.createTrackbar('k3', window_name, 0, k3_bins, lambda x: x)

	cv2.setTrackbarPos('k1', window_name, k1_bins//2)
	cv2.setTrackbarPos('k2', window_name, k2_bins//2)
	cv2.setTrackbarPos('k3', window_name, k3_bins//2)

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(65000)
		R.parse_packet(data)
		print(data)

		k1 = cv2.getTrackbarPos('k1', window_name)
		k1 = (k1/k1_bins)-0.5
		k2 = cv2.getTrackbarPos('k2', window_name)
		k2 = (k2/k2_bins)-0.5
		k3 = cv2.getTrackbarPos('k3', window_name)
		k3 = (k3/k3_bins)-0.5
		# print(k1)

		if R.image is not None and R.image_ok:
			# print(R.image.shape)
			ts = datetime.datetime.now()

			# R.image = cv2.undistort(R.image, M, D)
			im = R.image.copy()
			# print(D)
			D_ = D.copy()
			print(D_.shape)
			D_[0,0]=k1
			D_[0,1]=k2
			D_[0,2]=k3
			im = cv2.undistort(im, M, D_)

			# print(im.shape)
			if detect_target:
				corners = find_corners(im)
				# print(corners)
				if corners is not None:

					_, rvec, tvec, _ = cv2.solvePnPRansac(grid, corners, M, D, iterationsCount = n_iter, reprojectionError = repr_err)
					pts, _ = cv2.projectPoints(grid, rvec, tvec, M, distCoeffs=D)

					e = np.mean(np.abs(pts-corners))
					print(e)

					if e>err_thr:
						timestamp = int(round(ts.timestamp()))

						cv2.imwrite(f"{out_dir}{timestamp}_{e:.3f}.jpg", im)

					im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
					cv2.drawChessboardCorners(im, patternsize, corners, True)
			

			cv2.imshow(window_name, im)

			R.image_ok = False

			if cv2.waitKey(1) & 0xFF == ord('q'):
				# thread_stop=True
				# R.stop_all()
				# t.join()
				break
			# break

	# R.stop_sending()

if __name__=='__main__':
	# main()
	simple()