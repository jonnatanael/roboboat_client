from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt, QTimer, QObject, QThread, pyqtSignal
import cv2
import sys, time
import numpy as np
import pyqtgraph.opengl as gl
import pyqtgraph as pg

from gui_utils import CompassWidget
from read_utils import RadarParser, LidarParser

class Worker(QObject):
	finished = pyqtSignal()
	data = pyqtSignal(object)

	def __init__(self):
		super(Worker, self).__init__()
		self.parser = LidarParser()

	def run(self):
		self.parser.set_parameters(azimuth_filter=4, elevation_filter=1, fov_start=0, fov_stop=360)
		time.sleep(1)
		self.parser.start_all()

		while True:
			print("worker loop")
			data, addr = self.parser.socket_in.recvfrom(1000)
			self.parser.parse_packet(data)
			if self.parser.pc is not None and len(self.parser.pc)!=0:
				self.data.emit(self.parser.pc)
			# time.sleep(0.5)

class LidarWidget(pg.GraphicsWindow):
	def __init__(self, parent=None):
		super().__init__(parent=parent)

		self.layout = QtWidgets.QVBoxLayout()		
		self.setLayout(self.layout)

		self.gl = gl.GLViewWidget()
		self.layout.addWidget(self.gl)
		self.gl.show()
		self.grid = gl.GLGridItem()
		self.gl.addItem(self.grid)

		self.sp2 = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
		self.gl.addItem(self.sp2)		

		# thread
		self.thread = QThread()
		self.worker = Worker()
		self.worker.moveToThread(self.thread)

		self.thread.started.connect(self.worker.run)
		self.worker.data.connect(self.update)
		self.thread.start()

	def closeEvent(self, event):
		# self.worker.parser.stop_all()
		# event.accept()
		pass

	def update(self, data):
		# print(len(data), type(data))
		# print(data.shape)
		self.sp2.setData(pos=data[:,0:3])
		color = np.zeros((data.shape[0],3), dtype=np.float32)
		color[:,1]=1
		self.sp2.setData(color=color)

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	radar_widget = LidarWidget()
	radar_widget.show()

	timer = QTimer()
	timer.timeout.connect(lambda: None)
	timer.start(100)

	app.exec_()
