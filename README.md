This is the toolbox for communicating with the Davimar sensor system.
Its main purpose is to facilitate data capture and to enable live data viewing.

To install from requirements run: pip install -r requirements.txt

---

# Main scripts:

#### control_panel.py
Probably the most important part of the toolbox, the control panel continuously pings all the sensor boxes to check if they are online. It also parses telemetry, system and sync data so one can easily see if everything is in working order. It contains buttons to remotely reboot or shutdown each of the boxes as well as toggle buttons to start/stop capturing data on each of the sensor boxes.

When run, it starts data_splitter.py in the background.

#### data_splitter.py
This utility script captures data sent to the current local IP and forwards it to different ports on the local IP. This data can then be read by other programs, like live_viewer.py

#### set_parameters.py
This script is used to set parameters on sensor boxes. It can be used to synchronize local copies of configuration files with the sensor boxes. Its main purpose is to write the local IP address to each of the config files, upload them to corresponding boxes and restart the daemons. This is in order to get the sensor boxes to send the data straight to the client computer and reduce load on the network.

#### live_viewer.py
This program is used to view the data that is being streamed from the sensor boxes during data capture. By default it displays the image data and only the number of points for point cloud data.
GPS data is not visualized.

#### gps_display_demo.py
This is used to display GPS and IMU data. It is a separate application because the map is also displayed and GPS points are plotted on it.

#### stream_zed_lidar.py
This runs a stream from both zed and lidar, reads calibration data from .yaml file and projects lidar points onto the zed image. The scripts also contains functionalites to manually adjust the camera-lidar calibration if needed.

#### utils.py
This at the moment performs two utility functions. The first is to compress and download data from the selected sensor boxes. The data downloaded is limited to the current day by default but an arbitrary date can be set. This function is performed by running the script with the parameter 'get'.
The path to which the data is saved is hardcoded in the script. All the relevant data on one sensor is combined (not compressed) into .zip files and sent over network using scp. After successful transfer, the zip file on the sensor box is deleted, the file is extracted and .mjpg recordings from stereo boxes are converted into .mkv format.

The other utility function is to delete ALL the data for the selected date from ALL the sensor boxes. This should be used with caution. But it can be useful to keep the disks clean after the relevant data has been downloaded and stored elsewhere. This function is performed by running the script with the parameter 'delete'.
