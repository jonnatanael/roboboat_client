import time, datetime, math, glob, cv2, os, struct, binascii
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
import zlib
from termcolor import colored

from ltf_parser import ltf_parser

from read_utils import gps_params

def check_crc(crc, data): # TODO switch to little endian on server side
	a = binascii.crc32(data).to_bytes(4,'big')
	# a = binascii.crc32(data).to_bytes(4,'little')
	# print(a, crc)
	
	return crc==a

def check_crc2(crc, data): # TODO switch to little endian on server side
	# for gps
	# a = binascii.crc32(data).to_bytes(4,'big')
	a = binascii.crc32(data).to_bytes(4,'little')
	# print(a, crc)
	
	return crc==a

def parse_gps_data(data):
	data_len = data[0:2].hex()
	crc = data[-4:]
	data = data[2:-4]

	if not check_crc2(crc, data):
		print("CRC failed")

	res = {}
	for k,v in gps_params.items():

		o = v[0]
		ln = v[1]
		# print(k, data[o:o+ln].hex())
		res[k]=data[o:o+ln]

	parse_fields(gps_params, res)
	return res

def parse_fields(info, data):
	# info is a dict of response information such as offset and type, data is a byte string
	for k,v in data.items():
		c = info[k][-1]
		if c=='bits' or c=='char' or c=='bool' or c=='uint8':
			data[k]=int.from_bytes(v, 'big')
		elif c=='float':
			data[k]=struct.unpack('>f', v)[0]
		elif c=='double':
			data[k]=struct.unpack('d', v)[0]
		elif c=='uint64':
			data[k]=struct.unpack('q', v)[0]
		elif c=='uint32':
			data[k]=struct.unpack('>I', v)[0]
		elif c=='int32':
			data[k]=struct.unpack('i', v)[0]
		elif c=='uint16':
			data[k]=struct.unpack('>H', v)[0]
		elif c=='int16':
			data[k]=struct.unpack('h', v)[0]
		elif c=='string':
			data[k]=v.decode("utf-8")

def check_radar():
	pth = 'radar_data/'

	radar_log = sorted(glob.glob(pth+'RadarLOG*.txt'))
	radar_raw = sorted(glob.glob(pth+'RadarRAW*.txt'))
	radar_ply = sorted(glob.glob(pth+'*.ply'))

	display=False
	display=True

	n = 20
	x_range = n
	y_range = n
	z_range = n

	if display:
		fig = plt.figure()

	for lg, raw, ply in zip(radar_log, radar_raw, radar_ply):
		# print(lg, raw, ply)

		with open(lg) as f:
			lines = f.readlines()
			res = [l.split(',') for l in lines[1:]]
			res = [[int(l[0]),int(l[1]),int(l[2])-1] for l in res]
			meta = np.array(res)

		with open(raw) as f:
			lines = f.readlines()
			res = [l.split(',') for l in lines[2:]]
			res = [[float(l[0]),float(l[1]),float(l[2]),float(l[3])] for l in res]
			data = np.array(res)

		# calculate timings and FPS
		timestamps = meta[:,0]
		d = np.diff(timestamps)*1e-6
		fps = 1/d

		plt.clf()
		plt.subplot(1,2,1)
		plt.title("difference")
		plt.plot(d)
		plt.subplot(1,2,2)
		plt.title("FPS")
		plt.plot(fps)
		# plt.show()
		plt.draw()
		plt.waitforbuttonpress()

		if display:		

			for p in meta:
				plt.clf()
				# print(p)
				ts = p[0]*1e-6
				ln = p[1]
				offset = p[2]
				cur = data[offset:offset+ln]

				# print(datetime.datetime.fromtimestamp(ts))
				# print(cur)				

				ax = fig.add_subplot(111, projection='3d')

				ax.set_xlim3d(-x_range, x_range)
				ax.set_ylim3d(-y_range, y_range)
				ax.set_zlim3d(-z_range, z_range)
				plt.plot(cur[:,0],cur[:,1],cur[:,2], 'r.')
				plt.plot([0,0],[0,0],[0,0], 'k*')
				# plt.show()
				plt.pause(0.01)
				plt.waitforbuttonpress()

def check_thermal(pth, display=False):
	# pth = 'thermal_data/'
	# pth = '/media/jon/disk1/davimar/2021-06-18/thermal_camera/'
	# pth = '/media/jon/disk1/davimar/2021-06-18/thermal_camera/'
	# pth = '/media/jon/dusko/davimar_data/2022-07-14-ljubljanica3/'
	# pth = '/media/jon/dusko/davimar_data/2023-01-13-ljubljanica4/'
	# pth = '/media/jon/disk1/davimar/2023-01-27/'


	pth+='thermal_camera/'

	# images = sorted(glob.glob(pth+'Images*'))
	indices = sorted(glob.glob(pth+'*.txt'))

	im_size = (288, 384)
	start = 5

	display = False
	# display = True

	D = []

	for i, idx_fn in enumerate(indices):
		# print(idx_fn)

		with open(idx_fn) as f:
			lines = f.readlines()
			res = [l.split(',') for l in lines]
			res = [[int(l[0]),int(l[1]),int(l[2])] for l in res]
			data = np.array(res)

		indices = data[start:,0]
		timestamps = data[start:,1]

		# print(timestamps)

		# d = np.diff(timestamps)*1e-6
		# fps = 1/d

		d = (np.diff(timestamps)*1e-6)+1e-10
		

		D.extend(d)

		if display:
			fps = 1/d

			# print(np.min(d), np.max(d))
			# print(1/np.min(d), 1/np.max(d))

			# print(1/np.median(d))
			# print(1/np.mean(d))

			print("mean diff:", np.mean(d))
			print("median diff:", np.median(d))
			print("mean fps:", 1/np.mean(d))
			print("median fps:", 1/np.median(d))

			# continue

			# dt_object1 = datetime.datetime.fromtimestamp(timestamps[0])
			# print(dt_object1)

			plt.clf()
			plt.subplot(1,2,1)
			plt.title("difference")
			plt.plot(d)
			plt.subplot(1,2,2)
			plt.title("FPS")
			plt.plot(fps)
			plt.show()
			# plt.draw()
			# plt.waitforbuttonpress()

	if len(D)>0:

		return 1/np.mean(D), 1/np.median(D), np.max(D)
	else:
		return None, None, None

def check_flir_deprecated():
	pth = 'flir_data/'

	images = sorted(glob.glob(pth+'Images*'))
	indices = sorted(glob.glob(pth+'Index*'))

	display = False
	display = True
	start = 1

	for i, (im_fn, idx_fn) in enumerate(zip(images, indices)):
		# if i==0:
		# 	continue
		# res = []
		# plt.clf()
		# print(im_fn, idx_fn)

		with open(idx_fn) as f:
			lines = f.readlines()
			res = [l.split(',') for l in lines]
			res = [[int(l[0]),int(l[1]),int(l[2]),int(l[3])] for l in res]
			data = np.array(res)
		# print(data)
		

		indices = data[start:,0]
		timestamps = data[start:,1]
		offsets = data[start:,2]
		lengths = data[start:,3]

		# print(timestamps)

		d = np.diff(timestamps)*1e-6
		fps = 1/d

		# dt_object1 = datetime.datetime.fromtimestamp(timestamps[0])
		# print(dt_object1)

		plt.clf()
		plt.subplot(1,2,1)
		plt.title("difference")
		plt.plot(d)
		plt.subplot(1,2,2)
		plt.title("FPS")
		plt.plot(fps)
		# plt.show()
		plt.draw()
		plt.waitforbuttonpress()

		if display:
			plt.clf()

			for idx in range(0,data.shape[0]-1):
				print(idx, data.shape[0])
				# print(im_fn, idx_fn)				

				with open(im_fn, "rb") as f:
					index = data[idx,0]
					ts = data[idx,1]
					# offset = data[idx,2]
					offset = offsets[idx]
					length = lengths[idx]					

					print("ts", ts)
					image_bytes = np.fromfile(f, dtype=np.uint8,count=length, offset=offset)
				# print(len(image_bytes))

				nparr = np.frombuffer(bytes(image_bytes), np.uint8)
				im = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
				
				plt.imshow(im)
				# plt.show()
				plt.draw()
				plt.pause(0.01)
				# plt.waitforbuttonpress()

def check_flir(pth, display=False):
	# pth = '/media/jon/dusko/davimar_data/2023-01-13-ljubljanica4/'
	# pth = '/media/jon/disk1/davimar/2023-01-27/'

	pth+='polarization_camera/'

	# images = sorted(glob.glob(pth+'Images*'))
	# indices = sorted(glob.glob(pth+'Index*'))
	indices = sorted(glob.glob(pth+'*.txt'))
	# im_size = (2432, 2048)

	# display = False
	# m = 5
	# start = m
	# end = len(indices)-m

	D = []

	for i, idx_fn in enumerate(indices):
		# if i==0:
		# 	continue
		# res = []
		# plt.clf()
		# print(im_fn, idx_fn)

		with open(idx_fn) as f:
			lines = f.readlines()
			res = [l.split(',') for l in lines]
			res = [[int(l[0]),int(l[1]),int(l[2])] for l in res]
			data = np.array(res)


		indices = data[:,0]
		timestamps = data[:,1]

		# print(np.diff(timestamps))

		d = (np.diff(timestamps)*1e-6)+1e-10
		d = d[:-1]

		D.extend(d)

		if display:

			fps = 1/d

			# print(fps)

			print("mean diff:", np.mean(d))
			print("median diff:", np.median(d))
			print("mean fps:", 1/np.mean(d))
			print("median fps:", 1/np.median(d))

			# dt_object1 = datetime.datetime.fromtimestamp(timestamps[0])
			# print(dt_object1)

			# continue

			plt.clf()
			plt.subplot(1,2,1)
			plt.title("difference")
			plt.plot(d)
			plt.subplot(1,2,2)
			plt.title("FPS")
			plt.plot(fps)
			# plt.show()
			plt.draw()
			# plt.waitforbuttonpress()
			plt.show()

	if len(D)>0:
		return 1/np.mean(D), 1/np.median(D), np.max(D)
	else:
		return None, None, None

def check_gps():
	# pth = 'gps_data/'
	pth = '/media/jon/disk1/davimar/2021-06-18/GPSIMU/'

	files = sorted(glob.glob(pth+'*.xyzrpy'))

	plt.figure()

	for fn in files[-2:]:
		plt.clf()

		with open(fn, 'rb') as f:
			data = f.read()

		idx = 0
		ln = 69

		yaw = []
		pitch = []
		roll = []
		timestamps = []

		for idx in range(len(data)//ln):

			d = data[idx*ln:idx*ln+ln]
		
			
			res = parse_gps_data(d)
			# print(res)
			yaw.append(res['yaw'])
			pitch.append(res['pitch'])
			roll.append(res['roll'])
			# ts = res['timestamp1']
			ts = res['timestamp1']+res['timestamp2']*1e-6
			# ts = res['timestamp1']
			timestamps.append(ts)

		d = np.diff(timestamps)
		# print(np.median(d))
		# print(timestamps)
		# for ts in timestamps:
		# 	dt = datetime.datetime.fromtimestamp(ts)
		# 	print(dt)

		d = (np.diff(timestamps))+1e-10
		fps = 1/d

		# print(fps)

		print(1/np.median(d))

		continue


		plt.subplot(2,3,1)
		plt.plot(timestamps, 'b.')
		plt.title("timestamps")
		plt.subplot(2,3,2)
		plt.plot(d, 'b.')
		plt.title("d")
		plt.subplot(2,3,3)
		plt.plot(roll)
		plt.title("roll")
		plt.subplot(2,3,4)
		plt.plot(pitch)
		plt.title("pitch")
		plt.subplot(2,3,5)
		plt.plot(yaw)
		plt.title("yaw")
		# plt.show()
		plt.draw()
		# plt.waitforbuttonpress()
		plt.show()

def check_lidar():
	pth = 'lidar_data/'

	files = sorted(glob.glob(pth+'*.ltf'))

	for fn in files:

		with open(fn, 'rb') as f:
			data = f.read()
		ltf_parser(data)

def check_stereo():
	pth = '/media/jon/disk1/davimar/2021-10-27/'

	pth+='stereo_left/'

	# images = sorted(glob.glob(pth+'Images*'))
	# indices = sorted(glob.glob(pth+'Index*'))
	# indices = sorted(glob.glob(pth+'*.txt'))
	indices = sorted(glob.glob(pth+'*.txtx'))

	print(indices)

	for i, idx_fn in enumerate(indices):
		with open(idx_fn) as f:
			lines = f.readlines()

		lines = [float(l)*1e-3 for l in lines[1:]]
		# print(lines)

		# timestamps = []

		for l in lines:
			print(l)
			dt = datetime.datetime.fromtimestamp(l)
			print(dt)

		timestamps = np.array(lines)

		d = (np.diff(timestamps))+1e-10
		fps = 1/d

		# print(fps)

		print("mean diff:", np.mean(d))
		print("median diff:", np.median(d))
		print("mean fps:", 1/np.mean(d))
		print("median fps:", 1/np.median(d))

def check_zed():
	# pth = '/media/jon/disk1/davimar/2021-10-27/'
	pth = '/media/jon/disk1/davimar/2022-11-21-obala1/'
	pth = '/media/jon/dusko/davimar_data/2023-01-13-ljubljanica4/'

	pth+='zed/'

	# images = sorted(glob.glob(pth+'Images*'))
	# indices = sorted(glob.glob(pth+'Index*'))
	indices = sorted(glob.glob(pth+'*.txt'))
	# indices = sorted(glob.glob(pth+'*.txtx'))

	print(indices)

	for i, idx_fn in enumerate(indices):
		with open(idx_fn) as f:
			lines = f.readlines()

		lines = [float(l) for l in lines[1:]]
		# print(lines)

		# timestamps = []

		for l in lines:
			# print(l)
			dt = datetime.datetime.fromtimestamp(l)
			# print(dt)

		timestamps = np.array(lines)

		d = (np.diff(timestamps))+1e-10
		fps = 1/d

		# print(fps)

		print("mean diff:", np.mean(d))
		print("median diff:", np.median(d))
		print("mean fps:", 1/np.mean(d))
		print("median fps:", 1/np.median(d))

def get_zed_info(pth, params):
	pth+='zed/'
	indices = sorted(glob.glob(pth+'*.txt'))

	result = []

	for i, idx_fn in enumerate(indices):
		with open(idx_fn) as f:
			lines = f.readlines()

		lines = [float(l) for l in lines[1:]]

		for l in lines:
			dt = datetime.datetime.fromtimestamp(l)

		timestamps = np.array(lines)

		d = (np.diff(timestamps))+1e-10
		result.append(1/np.mean(d))

	return np.mean(result)

def get_polar_info(pth, params):
	pth+='polarization_camera/'

	indices = sorted(glob.glob(pth+'*.txt'))

	result = []

	for i, idx_fn in enumerate(indices):

		with open(idx_fn) as f:
			lines = f.readlines()
			res = [l.split(',') for l in lines]
			res = [[int(l[0]),int(l[1]),int(l[2])] for l in res]
			data = np.array(res)

		timestamps = data[:,1]

		d = (np.diff(timestamps)*1e-6)+1e-10
		result.append(1/np.mean(d))

	return np.mean(result)

def get_thermal_info(pth, params):

	pth = '/media/jon/disk1/davimar/2021-09-17/thermal_camera/'

	indices = sorted(glob.glob(pth+'*.txt'))

	result = []

	for i, idx_fn in enumerate(indices):

		with open(idx_fn) as f:
			lines = f.readlines()
			res = [l.split(',') for l in lines]
			res = [[int(l[0]),int(l[1]),int(l[2])] for l in res]
			data = np.array(res)

		d = (np.diff(data[:,1])*1e-6)+1e-10
		result.append(1/np.mean(d))

	return np.mean(result)

def get_stereo_info(pth, params):

	pth+=f'stereo_{params}/'

	# images = sorted(glob.glob(pth+'Images*'))
	# indices = sorted(glob.glob(pth+'Index*'))
	indices = sorted(glob.glob(pth+'*.txt'))
	# indices = sorted(glob.glob(pth+'*.txtx'))

	result = []

	for i, idx_fn in enumerate(indices):
		with open(idx_fn) as f:
			lines = f.readlines()

		lines = [float(l)*1e-3 for l in lines[1:]]
		# print(lines)

		# timestamps = []

		for l in lines:
			# print(l)
			dt = datetime.datetime.fromtimestamp(l)
			# print(dt)

		timestamps = np.array(lines)

		d = (np.diff(timestamps))+1e-10

		result.append(1/np.mean(d))

	return np.mean(result)

def get_radar_info(pth, params):
	pth+=f'radar/'

	radar_log = sorted(glob.glob(pth+'RadarLOG*.txt'))

	result = []
	
	for lg in radar_log:
		# print(lg, raw, ply)

		with open(lg) as f:
			lines = f.readlines()
			res = [l.split(',') for l in lines[1:]]
			res = [[int(l[0]),int(l[1]),int(l[2])-1] for l in res]
			meta = np.array(res)

		# calculate timings and FPS
		timestamps = meta[:,0]
		d = np.diff(timestamps)*1e-6
		# fps = 1/d

		result.append(1/np.mean(d))

	# print(result)

	return np.mean(result)

def get_gps_info(pth, params):
	pth+= 'GPSIMU/'
	files = sorted(glob.glob(pth+'*.xyzrpy'))
	result = []

	for fn in files:

		with open(fn, 'rb') as f:
			data = f.read()

		ln = 69

		timestamps = []

		for idx in range(100, len(data)//ln):
			d = data[idx*ln:idx*ln+ln]
			res = parse_gps_data(d)
			ts = res['timestamp1']+res['timestamp2']*1e-6
			timestamps.append(ts)

		d = np.diff(timestamps)
		d+=1e-10

		# result.append(1/np.mean(d))
		result.append(1/np.median(d))

	return np.mean(result)

def get_lidar_info(pth, params):
	from LidarDataReader import LidarDataReader
	# TODO get timestamps properly, try to measure the time for 150 packets, should be about 100ms
	pth+= 'lidar/'

	# files = sorted(glob.glob(pth+'*.ltf'))

	result = []

	lidar = LidarDataReader(pth)
	# print(lidar.files)

	# NOTE timestamps are timestamps of chunks
	# each chunk has 1024 packets
	# each packet has 100 blocks
	# each block has 32 points

	for i in range(len(lidar.files)):
		lidar.open(i)

		# print(len(lidar.timestamps))

		d = np.diff(lidar.timestamps)
		# print(d)
		# result.append(1/np.median(d))
		result.append(np.median(d))
		# result.append(np.mean(d))

		for ts in lidar.timestamps:
			dt = datetime.datetime.fromtimestamp(ts)
			print(dt)

	# return np.mean(result)
	return np.mean(result)

	# for fn in files:

		# with open(fn, 'rb') as f:
			# data = f.read()
		# ltf_parser(data)



	# return len(data)

def check_all():
	# pth = '/media/jon/disk1/davimar/2021-12-24/'
	# pth = '/media/jon/disk1/davimar/2021-12-06/'
	# pth = '/media/jon/disk1/davimar/2022-01-05/'
	# pth = '/media/jon/disk1/davimar/2022-03-07/'
	# pth = '/media/jon/disk1/davimar/2021-09-17/'
	pth = '/media/jon/disk1/davimar/2023-01-27/'

	sensor_info = {
		'zed': {'fn': get_zed_info, 'params': ''},
		# 'stereo_front': {'fn': get_stereo_info, 'params': 'front'},
		# 'stereo_side': {'fn': get_stereo_info, 'params': 'side'},
		'thermal_camera': {'fn': get_thermal_info, 'params': ''},
		'polarization_camera': {'fn': get_polar_info, 'params': ''},
		'lidar': {'fn': get_lidar_info, 'params': ''},
		# 'radar': {'fn': get_radar_info, 'params': ''},
		# 'GPSIMU': {'fn': get_gps_info, 'params': ''},
	}

	for s,v in sensor_info.items():

		files = glob.glob(f"{pth}{s}/*")

		if not files:
			print(f"files empty for {s}".upper())
		else:
			# print(files)
			fps = v['fn'](pth, v['params'])

			if s=='lidar':
				print(f"mean chunk delay for {s}: {fps}")
			else:
				print(f"mean fps for {s}: {fps}")

			# check each file for proper recording

			# for f in files:


		# print(files)

def check_recording():

	data_dir = '/media/jon/dusko/davimar_data/'

	dirs = sorted(glob.glob(data_dir+'*'))

	print(dirs)

	sensors = {
		'thermal_camera': {'function': check_thermal, 'expected': 10, 'means': [], 'medians': []},
		'polarization_camera': {'function': check_flir, 'expected': 8, 'means': [], 'medians': []},
	}

	for pth in dirs:

		if '2021-08-20' in pth:
			continue

		if '2021-09-06' in pth:
			continue

		print(colored(pth, 'green'))		

		for k,v in sensors.items():
			mean, median, max_delay = v['function'](pth+'/', display=False)
			if mean is not None:
				print(f"{k}:, mean: {mean:.2f}, median: {median:.2f}, max delay: {max_delay:.2f}s")
				sensors[k]['means'].append(mean)
				sensors[k]['medians'].append(median)

			# print()

		# polar_mean, polar_median = check_flir(pth)
		# print(polar_mean, polar_median)

		# thermal_mean, thermal_median = check_thermal(pth)
		# print(thermal_mean, thermal_median)

	for k,v in sensors.items():
		plt.figure(k)
		# plt.subplot(1,2,1)
		plt.plot(v['means'], label='means')
		plt.plot(v['medians'], label='medians')
		plt.legend()

	plt.show()

def main():
	# pass
	# check_all()
	# check_gps()
	# check_lidar()
	# check_radar()
	# check_thermal()
	# check_flir()
	# check_stereo()
	# check_zed()

	check_recording()

if __name__=='__main__':
	main()