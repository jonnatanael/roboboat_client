from socket import *
# import time, datetime, math
import numpy as np
# import struct, zlib, binascii
# import pptk
import time
import cv2, os, signal, sys
from read_utils import PolarizationParser, ThermalParser, ZEDParser
import threading
from numpy.linalg import inv

thread_stop = False
im_polar = None
polar = None

from projection_utils import *


# def worker():
# 	global thread_stop, im_thermal, thermal

# 	while True:
# 		# print("lidar thread")
# 		data, addr = thermal.socket_in.recvfrom(1000)
# 		# print(addr)
# 		thermal.parse_packet(data)
# 		if thermal.image is not None and thermal.image_ok:
# 			# print(len(thermal.pc))
# 			im_thermal = thermal.image

# 		# print(thread_stop)

# 		if thread_stop:
# 			break
# 	return

def worker():
	global thread_stop, im_polar, polar

	while True:
		# print("lidar thread")
		data, addr = polar.socket_in.recvfrom(1000)
		# print(addr)
		polar.parse_packet(data)
		if polar.image is not None and polar.image_ok:
			# print(len(polar.pc))
			im_polar = polar.image
			im_polar = cv2.resize(im_polar, None, fx=2, fy=2)

			polar.image_ok = False

		# print(thread_stop)

		if thread_stop:
			print("stopping thread")
			break
	return

# TODO get mapping between the images from calibration (transitive, i.e. from polar to zed and from zed to thermal or vice versa)




zed = None

def signal_handler(sig, frame):
	print('You pressed Ctrl+C!')
	global zed, polar, thread_stop
	thread_stop=True
	print("signal stopping")
	# zed.stop_acquisition()
	# polar.stop_all()
	# sys.exit()

# exit()

def main():

	global zed, thread_stop, im_polar, polar

	signal_a = signal.signal(signal.SIGINT, signal_handler)

	# while True:
	# 	# print("loop")
	# 	pass

	zed = ZEDParser()
	# thermal = ThermalParser()
	polar = PolarizationParser()

	zed.start_acquisition()
	polar.start_all()

	# start thread for thermal
	t = threading.Thread(target=worker)
	t.start()


	alpha=0.5
	calculate_map = True
	calculate_map = False

	# parameters
	offx= 0.0; offy= 0.0; offz= 0.0; roll = 0.0; pitch = 0.0; yaw = 0.0
	D = 2
	# D = 200

	dt = 0.05
	da = 0.5
	dd = 0.5

	dx = dy = dz = dt
	dr = dp = dw = da

	(M1, M2, D1, D2, R1, t1, R2, t2) = load_calibration_data('calibration_zed_polarization_camera.yaml')
	P_zed = rt_to_projection_matrix(R1,t1)
	P_polar = rt_to_projection_matrix(R2,t2)

	P_polar_zed = np.dot(P_polar, inv(P_zed)) # zed -> polar

	print(P_polar_zed)

	(width1, height1, M1, D1) = load_camera_calibration('calibration_zed.yaml')
	(width2, height2, M2, D2) = load_camera_calibration('calibration_polarization_camera.yaml')
	# (width2, height2, M2, D2) = load_camera_calibration('calibration_thermal_camera.yaml')

	# print(width1, height1)
	# print(width2, height2)

	im1 = np.ones((int(height1), int(width1)))
	im2 = np.zeros((int(height2), int(width2)))

	# resize
	M1[0,:]*=0.5
	M1[1,:]*=0.5
	M2[0,:]*=0.5
	M2[1,:]*=0.5

	im1 = np.ones((int(height1//2), int(width1//2)))
	im2 = np.zeros((int(height2//2), int(width2//2)))


	# mp_stereo_polar = project_image_final(im1, im2, P_thermal_polar, M1, M2, t = t1[0,2])
	# mp_stereo_polar = project_image_final(im1, im2, P_thermal_polar, M1, M2, t = t2[0,-1])
	# mp_stereo_polar = project_image_final(im1, im2, P_thermal_polar, M1, M2, t = 2)
	mp_polar_zed = project_image_final(im1, im2, P_polar_zed, M1, M2, t = D)

	mp_polar_zed = cv2.resize(mp_polar_zed, (int(width1), int(height1)))

	print(mp_polar_zed.shape)

	key = -1
	while key & 0xFF != ord("q"):
		# continue
		# zed.get_parameters()
		data, addr = zed.socket_in.recvfrom(1000)
		zed.parse_packet(data)
		# print(data)

		# if zed.image is not None and zed.image_ok and im_thermal is not None:
		if zed.image is not None and zed.image_ok:

			if calculate_map:

				# P_polar_zed = np.dot(P_polar, inv(P_zed))

				R = eulerAnglesToRotationMatrix([pitch, yaw, roll]) # TODO load this from yaml file
				T = np.array([[offx, offy, offz]])
				# print(R,t,R.shape, t.shape)
				P_polar_zed = rt_to_projection_matrix(R,T)


				mp_polar_zed = project_image_final(im1, im2, P_polar_zed, M1, M2, t = D)*2
				mp_polar_zed = cv2.resize(mp_polar_zed, (int(width1), int(height1)))

			im_zed = zed.image.copy()
			im_zed = cv2.cvtColor(im_zed, cv2.COLOR_RGB2BGR)
			zed.image_ok = False
			im_zed = cv2.resize(im_zed, (int(width1), int(height1)))

			# print(zed.image.shape)
			if im_polar is not None:
				# print(im_polar.shape)
				polar_to_zed = cv2.remap(im_polar, mp_polar_zed, None, cv2.INTER_LINEAR)
				final = cv2.addWeighted(im_zed, alpha, polar_to_zed, (1-alpha), 0.0)
				cv2.imshow("image",final)
			else:
				cv2.imshow("image",im_zed)
				

			# im_zed = cv2.resize(im_zed, None, fx=4, fy=4)
			
			# print(im.shape)

			

			# print(im_zed.shape)
			# print(polar_to_zed.shape)
			

			# 
			# cv2.imshow("polar",im_polar)
			# cv2.imshow("map",polar_to_zed)
			

			# print("polar", im.shape)
			# print("mapped", thermal_to_polazed.shape)

			# # if corners is not None:
			# # 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)		

			# if cv2.waitKey(1) & 0xFF == ord('q'):
				# thread_stop=True
				# zed.stop_all()
				# thermal.stop_all()
				# t.join()
				# break
			# break

			key = cv2.waitKey(1)
			
			if key & 0xFF == ord("e"):
				offx-=dx
				calculate_map = True
			elif key & 0xFF == ord("r"):
				offx+=dx
				calculate_map = True
			elif key & 0xFF == ord("s"):
				offy-=dy
				calculate_map = True
			elif key & 0xFF == ord("d"):
				offy+=dy
				calculate_map = True
			elif key & 0xFF == ord("x"):
				offz-=dz
				calculate_map = True
			elif key & 0xFF == ord("c"):
				offz+=dz
				calculate_map = True

			elif key & 0xFF == ord("u"):
				roll-=dr
				calculate_map = True
			elif key & 0xFF == ord("i"):
				roll+=dr
				calculate_map = True
			elif key & 0xFF == ord("j"):
				pitch-=dp
				calculate_map = True
			elif key & 0xFF == ord("k"):
				pitch+=dp
				calculate_map = True
			elif key & 0xFF == ord("n"):
				yaw-=dw
				calculate_map = True
			elif key & 0xFF == ord("m"):
				yaw+=dw
				calculate_map = True
			elif key & 0xFF == ord("+"):
				# print("+")
				D+=dd
				calculate_map = True
				# azi_filter = max(1,azi_filter-1)
				# print(azi_filter)
				# lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("-"):
				# print("-")
				D-=dd
				calculate_map = True

			print(f"D: {D:.2f}, x: {offx:.2f}, y: {offy:.2f}, z: {offz:.2f}, roll: {roll:.2f}, pitch: {pitch:.2f}, yaw: {yaw:.2f}", end='\r')

		if thread_stop:
			break

			

	thread_stop=True
	zed.stop_acquisition()
	polar.stop_all()
	
	t.join()

if __name__=='__main__':
	main()