import socket, math
import numpy as np
import struct, zlib, binascii, time, datetime, signal, sys

from read_utils import TelemetryParser

def main():


	# d = {}
	# for x in range(1,10):
	# 	d["string{0}".format(x)] = 'Hello'

	# print(d)

	# return
	# R = TelemetryParser('imu_gps')
	# R = TelemetryParser('lidar')
	# R = TelemetryParser('radar')
	# R = TelemetryParser('rgb_camera', sensor_number=1)
	# R = TelemetryParser('rgb_camera', sensor_number=2)
	R = TelemetryParser('rgb_camera', sensor_number=3) # ZED/jetson
	# R = TelemetryParser('polarization_camera')
	# R = TelemetryParser('thermal_camera')

	while True:
		data, _ = R.socket_in.recvfrom(1000)
		R.parse_packet(data)

		print("system_data", R.system_data)
		print("sync_data", R.sync_data)
		print("electrical_data", R.electrical_data)

if __name__=='__main__':
	main()
