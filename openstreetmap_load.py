# from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np

import math
# import urllib2
import urllib.request as urllib2
# import 
import io, os, glob
from PIL import Image

from recording_stats import parse_gps_data

def deg2num(lat_deg, lon_deg, zoom):
	lat_rad = math.radians(lat_deg)
	n = 2.0 ** zoom
	xtile = int((lon_deg + 180.0) / 360.0 * n)
	ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
	return (xtile, ytile)

def num2deg(xtile, ytile, zoom):
	"""
	http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
	This returns the NW-corner of the square. 
	Use the function with xtile+1 and/or ytile+1 to get the other corners. 
	With xtile+0.5 & ytile+0.5 it will return the center of the tile.
	"""
	n = 2.0 ** zoom
	lon_deg = xtile / n * 360.0 - 180.0
	lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
	lat_deg = math.degrees(lat_rad)
	return (lat_deg, lon_deg)

def getImageCluster(lat_deg, lon_deg, delta_lat,  delta_long, zoom):

	hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
	   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	   'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
	   'Accept-Encoding': 'none',
	   'Accept-Language': 'en-US,en;q=0.8',
	   'Connection': 'keep-alive'}

	smurl = r"http://a.tile.openstreetmap.org/{0}/{1}/{2}.png"
	xmin, ymax = deg2num(lat_deg, lon_deg, zoom)
	xmax, ymin = deg2num(lat_deg + delta_lat, lon_deg + delta_long, zoom)

	bbox_ul = num2deg(xmin, ymin, zoom)
	bbox_ll = num2deg(xmin, ymax + 1, zoom)
	print (bbox_ul, bbox_ll)

	bbox_ur = num2deg(xmax + 1, ymin, zoom)
	bbox_lr = num2deg(xmax + 1, ymax +1, zoom)
	print(bbox_ur, bbox_lr)

	Cluster = Image.new('RGB',((xmax-xmin+1)*256-1,(ymax-ymin+1)*256-1) )
	for xtile in range(xmin, xmax+1):
		for ytile in range(ymin,  ymax+1):
			# try:			

			imgurl=smurl.format(zoom, xtile, ytile)
			fn = 'openstreetmap_cache/{}_{}_{}.png'.format(zoom, xtile, ytile)
			# 
			# print("Saving: " + fn)

			if not os.path.exists(fn):
				print("Opening: " + imgurl)

				req = urllib2.Request(imgurl, headers=hdr)
				# 
				# print(imgstr.read())
				# tile = Image.open(imgstr.raw)
				imgstr = urllib2.urlopen(req)
				# print(imgstr)
				tile = Image.open(imgstr)
				# print(np.asarray(tile).shape)
				tile.save(fn)
			else:
				tile = Image.open(fn)
				# print(np.asarray(tile).shape)

			Cluster.paste(tile, box=((xtile-xmin)*255 ,  (ytile-ymin)*255))
			# except: 
			#     print("Couldn't download image")
			#     tile = None

	return np.asarray(Cluster), [bbox_ll[1], bbox_ll[0], bbox_ur[1], bbox_ur[0]]

def read_from_file():
	# pth = 'gps_data/'
	# pth = "/home/jon/Desktop/6.5.2021 davimar calibration recording/GPS/"
	# pth = "/home/jon/Desktop/5.5.2021 recording/gps/"
	pth = "/home/jon/Desktop/21.5.2021/gps/"

	files = sorted(glob.glob(pth+'*.xyzrpy'))
	print(files)

	# plt.figure()

	# for fn in files:
	# 	plt.clf()
	file_idx = 3
	fn = files[file_idx]
	ln = 69

	latitudes = []
	longitudes = []
	modes = []
	modes_set = set()

	with open(fn, 'rb') as f:
		data = f.read()

		for idx in range(len(data)//ln):

			d = data[idx*ln:idx*ln+ln]
			res = parse_gps_data(d)

			# print(res['mode'])
			# if res["mode"] == 1024:
			# if res["mode"] ==256:

			latitudes.append(np.degrees(res['latitude']))
			longitudes.append(np.degrees(res['longitude']))
			modes.append(res['mode'])
			modes_set.add(res["mode"])


	print(modes_set)

	delta_lat = 0.08
	delta_long = 0.16
	zoom = 15
	skip = 10

	im, bbox = getImageCluster(latitudes[0]-delta_lat/2, longitudes[0]-delta_long/2, delta_lat,  delta_long, zoom)
	lon_min = bbox[0]
	lat_min = bbox[1]
	lon_max = bbox[2]
	lat_max = bbox[3]
	w = im.shape[1]
	h = im.shape[0]

	plt.clf()
	plt.imshow(im)

	clrs = {
		1024: (0,1,0),
		0: (1,0,0),
		512: (1,1,0),
		768: (0,1,1),
		256: (0,0,1)
	}

	for lat, lon, mode in zip(latitudes[::skip], longitudes[::skip], modes[::skip]):

		x = (lon-lon_min)/(lon_max-lon_min)
		y = (lat-lat_min)/(lat_max-lat_min)

		res_x = x*w
		res_y = h-(y*h)

		# print(res_x, res_y)
		# plt.plot(res_x, res_y,'r.')
		plt.plot(res_x, res_y,marker='.', color=clrs[mode])

		# plt.draw()
		# plt.pause(0.01)

	plt.show()

		# idx = 0
	# 	ln = 69

	# 	yaw = []
	# 	pitch = []
	# 	roll = []
	# 	timestamps = []

	# 	for idx in range(len(data)//ln):

	# 		d = data[idx*ln:idx*ln+ln]
		
			
	# 		res = parse_gps_data(d)
	# 		# print(res)
	# 		yaw.append(res['yaw'])
	# 		pitch.append(res['pitch'])
	# 		roll.append(res['roll'])
	# 		# ts = res['timestamp1']
	# 		ts = res['timestamp1']*1e6+res['timestamp2']
	# 		timestamps.append(ts)


if __name__ == '__main__':

	read_from_file()

	exit()

	lat = 46.04463
	lon = 14.48933

	delta_lat = 0.02
	delta_long = 0.06
	# lat_deg, lon_deg, delta_lat,  delta_long, zoom = 45.720-0.04/2, 4.210-0.08/2, 0.04,  0.08, 14
	lat_deg, lon_deg, delta_lat,  delta_long, zoom = 46.04463-delta_lat/2, 14.48933-delta_long/2, delta_lat,  delta_long, 18
	a, bbox = getImageCluster(lat_deg, lon_deg, delta_lat,  delta_long, zoom)

	print(a,bbox)

	lon_min = bbox[0]
	lat_min = bbox[1]
	lon_max = bbox[2]
	lat_max = bbox[3]

	a = np.asarray(a)
	# print(a)
	print(a.shape)
	w = a.shape[1]
	h = a.shape[0]

	x = (lon-lon_min)/(lon_max-lon_min)
	y = (lat-lat_min)/(lat_max-lat_min)

	res_x = x*w
	res_y = h-(y*h)

	plt.clf()
	plt.imshow(a)
	plt.plot(res_x, res_y,'r.')

	plt.show()

	# fig = plt.figure(figsize=(10, 10))
	# ax = plt.subplot(111)
	# m = Basemap(
	#     llcrnrlon=bbox[0], llcrnrlat=bbox[1],
	#     urcrnrlon=bbox[2], urcrnrlat=bbox[3],
	#     projection='merc', ax=ax
	# )
	# # list of points to display (long, lat)
	# ls_points = [m(x,y) for x,y in [(4.228, 45.722), (4.219, 45.742), (4.221, 45.737)]]
	# m.imshow(a, interpolation='lanczos', origin='upper')
	# ax.scatter([point[0] for point in ls_points],
	#            [point[1] for point in ls_points],
	#            alpha = 0.9)
	# plt.show()