import numpy as np
import socket
import cv2, struct, io, os, sys
from PIL import Image
import subprocess
from datetime import datetime
from utils import get_local_IP


from calibration_utils import *
from camera_lidar_calibration_helper import get_edges

patternsize = (3,5)

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

def main_deprecated():

	print(sys.argv)

	# camera = 0 # left
	camera = 1 # right

	if len(sys.argv)>1:
		camera = sys.argv[1]

	# local_IP = '192.168.90.14' # PC
	local_IP = get_local_IP()
	# local_IP = '192.168.90.79' # laptop
	remote_IP = '192.168.90.74'


	# stolen from: https://stackoverflow.com/questions/45924220/netcat-h-264-video-from-raspivid-into-opencv

	server_socket = socket.socket()
	server_socket.bind(('0.0.0.0', 1200))
	server_socket.listen(0)
	 
	start_time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
	print("date and time: ", start_time)

	out_dir = 'stereo_out/'+start_time+'/'

	cnt=0
	write = False
	project_lidar = True
	ts_prev = None

	if write:
		try:
			os.makedirs(out_dir)
		except OSError as error:
			print(error)
			print("Directory '%s' can not be created")

	# start raspivid on RPi

	shutter = 500 # to je reasonable za desno kamero
	# shutter = 6000000
	
	command = f"python camera_stream.py {camera} {local_IP} {shutter}"
	# command = f"python camera_stream.py {camera} {local_IP}"
	print(command)
	subprocess.call(['ssh', '-t', '-f', "pi@"+remote_IP, "nohup", command], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

	connection = server_socket.accept()[0].makefile('rb')

	# blob detector
	params = cv2.SimpleBlobDetector_Params()

	# Change thresholds
	params.minThreshold = 10
	params.maxThreshold = 1000

	# Filter by Area.
	params.filterByArea = True
	params.minArea = 20

	# Filter by Circularity
	params.filterByCircularity = True
	params.minCircularity = 0.1

	# Filter by Convexity
	params.filterByConvexity = False
	params.minConvexity = 0.87

	# Filter by Inertia
	params.filterByInertia = False
	params.minInertiaRatio = 0.01

	# Create a detector with the parameters
	detector = cv2.SimpleBlobDetector_create(params)

	thr = 200
	thr_step = 5

	# TODO define mask

	mask = None
	cx = 648
	cy = 486
	radius = 100
	radius_step = 20
	center_step = 10

	cx = 588
	cy = 346
	radius = 560

	thickness = 4
	
	try:

		# R = LidarParser()
		# R.set_parameters(fov_start=0, fov_stop=180, azimuth_filter=1, elevation_filter=1)

	
		# R.start_all()

		

		while True:
			
			x = connection.read(struct.calcsize('<L'))
			# print(x)
			image_len = struct.unpack('<L', x)[0]
			# print(image_len)
			if not image_len:
				break
			
			image_stream = io.BytesIO()
			image_stream.write(connection.read(image_len))
			image_stream.seek(0)
			image = Image.open(image_stream)

			data = np.fromstring(image_stream.getvalue(), dtype=np.uint8)
			im = cv2.imdecode(data, 1)


			# if mask is None:
			# 	mask = np.zeros((im.shape[0], im.shape[1]), dtype=np.uint8)
			# 	# mask = np.zeros_like(im)
			# 	cv2.circle(mask, (cx, cy), radius=radius, color=1, thickness=-1)
			# 	# cv2.circle(mask, (cx, cy), radius=radius, color=(1,1,1), thickness=-1)

			# print(im.shape)
			# print(mask.shape)

			im2 = im.copy()

			im2 = cv2.cvtColor(im2, cv2.COLOR_RGB2GRAY)


			idx = im2<thr
			# print(idx)
			im2 = np.where(im2>thr, 255, 0).astype(np.uint8)
			im2[idx]=0
			im2[~idx]=255

			# im2 = cv2.adaptiveThreshold(im2, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 21, 10)

			if write:
				cv2.imwrite(f'{out_dir}{str(cnt)}.jpg',im)

			# keypoints = detector.detect(~im2)
			# for p in keypoints:
			# 	# print(p.pt, p.size)
			# 	x = int(p.pt[0])
			# 	y = int(p.pt[1])
			# 	s = int(p.size//2)
			# 	cv2.circle(im, (x, y), radius=s, color=(0, 255, 0), thickness=thickness)

			keypoints = detector.detect(im2)
			for p in keypoints:
				# print(p.pt, p.size)
				x = int(p.pt[0])
				y = int(p.pt[1])
				s = int(p.size//2)
				s = 5
				# cv2.circle(im, (x, y), radius=s, color=(0, 0, 255), thickness=thickness)
				cv2.circle(im, (x, y), radius=s, color=(0, 0, 255), thickness=-1)


			# im*=np.expand_dims(mask, -1)
			# im*=mask
			# im[...,1]*=mask
			# im[...,2]*=mask

			# canny1 = cv2.Canny(im, 75, 200)
			# canny2 = cv2.Canny(im2, 75, 200)

			cv2.imshow("Frame",im)
			cv2.imshow("raw",im2)
			# cv2.imshow("canny1",canny1)
			# cv2.imshow("canny2",canny2)
			# cv2.imshow("mask",mask)

			key = cv2.waitKey(1) & 0xFF

			if key == ord('q'):
				break

			elif key== ord("+"):

				print("+")

				thr = min(255,thr+thr_step)
				print(thr)
			elif key== ord("-"):
				print('-')
				thr = max(0,thr-thr_step)
				print(thr)

			# elif key== ord("u"):
			# 	radius-=radius_step
			# elif key==ord("i"):
			# 	radius+=radius_step
			# elif key==ord("a"): # left
			# 	cx-=center_step
			# elif key==ord("d"): # right
			# 	cx+=center_step
			# elif key==ord("w"): # up
			# 	cy-=center_step
			# elif key==ord("s"): # down
			# 	cy+=center_step

			# print(key)


			# print(cx,cy, radius)

			cnt+=1

	finally:
		cv2.destroyAllWindows() #cleanup windows 
		connection.close()
		server_socket.close()
		# R.stop_all()

def main():

	grid = get_grid()

	camera = 1 # right

	# local_IP = '192.168.90.14' # PC
	local_IP = get_local_IP()
	# local_IP = '192.168.90.79' # laptop
	remote_IP = '192.168.90.74'

	# stolen from: https://stackoverflow.com/questions/45924220/netcat-h-264-video-from-raspivid-into-opencv

	server_socket = socket.socket()
	server_socket.bind(('0.0.0.0', 1200))
	server_socket.listen(0)

	cnt=0
	write = False
	project_lidar = True
	ts_prev = None

	# start raspivid on RPi

	shutter = 500 # to je reasonable za desno kamero
	
	command = f"python camera_stream.py {camera} {local_IP} {shutter}"
	# command = f"python camera_stream.py {camera} {local_IP}"
	print(command)
	subprocess.call(['ssh', '-t', '-f', "pi@"+remote_IP, "nohup", command], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

	connection = server_socket.accept()[0].makefile('rb')

	thickness = 4

	sensor = 'stereo_front_right'
	calib_fn = f'helper_calibrations/calibration_{sensor}_helper.yaml'
	(width, height, M, D) = load_camera_calibration(calib_fn)
	
	try:
		while True:
			
			x = connection.read(struct.calcsize('<L'))
			image_len = struct.unpack('<L', x)[0]
			if not image_len:
				break
			
			image_stream = io.BytesIO()
			image_stream.write(connection.read(image_len))
			image_stream.seek(0)
			image = Image.open(image_stream)

			data = np.fromstring(image_stream.getvalue(), dtype=np.uint8)
			im = cv2.imdecode(data, 1)

			corners = None
			corners = find_corners(255-im)

			if corners is not None:
				# cv2.drawChessboardCorners(im, patternsize, corners, True)
				_, rvec, tvec = cv2.solvePnP(grid, corners, M, D, flags=cv2.SOLVEPNP_IPPE) # coplanar points
			
				edges = get_edges(im, M, corners, c=11)
				edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2RGB)
				edges = edges.astype(np.float32)
				im = im.astype(np.float32)/255
				x = edges
				im = im*(1-x)+edges
			

			cv2.imshow("im",im)
			# cv2.imshow("raw",im2)
			# cv2.imshow("canny1",canny1)
			# cv2.imshow("canny2",canny2)
			# cv2.imshow("mask",mask)

			key = cv2.waitKey(1) & 0xFF

			if key == ord('q'):
				break

			# elif key== ord("+"):

			# 	print("+")

			# 	thr = min(255,thr+thr_step)
			# 	print(thr)
			# elif key== ord("-"):
			# 	print('-')
			# 	thr = max(0,thr-thr_step)
			# 	print(thr)

	
			# cnt+=1

	finally:
		cv2.destroyAllWindows() #cleanup windows 
		connection.close()
		server_socket.close()
		# R.stop_all()

if __name__=='__main__':
	main()
