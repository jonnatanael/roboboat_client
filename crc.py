import binascii
from array import array

poly = 0xEDB88320

table = array('L')
for byte in range(256):
	crc = 0
	for bit in range(8):
		if (byte ^ crc) & 1:
			crc = (crc >> 1) ^ poly
		else:
			crc >>= 1
		byte >>= 1
	table.append(crc)
	# print(hex(crc))

# print(table)

def crc32(string):
	value = 0xffffffff
	print("value: ", int(value))
	for ch in string:
		print(chr(ch) )
		print(int(value))
		print((ch ^ value))
		value = table[(ch ^ value) & 0xff] ^ (value >> 8)

	print("end value: ", value)
	print(1-value)
	return -1 - value

# test

# data = (
#     b'',
#     b'test',
#     b'hello-world',
#     b'1234',
#     b'A long string to test CRC32 functions',
# )

data = (
	# b'test',
	b'hello-world',
)

for s in data:
	print( repr(s))
	a = binascii.crc32(s)
	print('%08x' % (a & 0xffffffff))
	b = crc32(s)
	print( '%08x' % (b & 0xffffffff))
	print("")