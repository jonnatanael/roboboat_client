from socket import *
# import time, datetime, math
import numpy as np
# import datetime
import time, threading
import cv2, os
import datetime


from read_utils import RGBParser, LidarParser
# from live_client import load_camera_calibration

from calibration_utils import *

thread_stop = False
lidar = None
pc = None

side = 'left'
side = 'right'

calib_fn = f'/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_stereo_front_{side}_helper.yaml'

(width, height, M, D) = load_camera_calibration(calib_fn)
R_lidar, T_lidar, C_lidar = load_lidar_calibration(f"/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_lidar_stereo_front_{side}.yaml")

# modify calibration matrix


# print(M)

if side=='left':
	# M[0,0]*=0.69
	# M[1,1]*=1.1
	# M[0,2] = width/2
	# M[1,2] = height/2
	# # M[0,2]*=0.69
	# # M[1,2]*=1.1

	# convert to 1440x1080
	# f = 1.11111
	# M*=f
	# M[-1,-1]=1

	# fx = 0.62222
	# M[0,0]*=fx
	# M[0,2]-=272
	# M[0,2]-=200
	# M[1,2]+=54
	# M[0,2]=896/2
	# M[1,2]=1080/2
	# M[0,2]=1080/2
	# M[1,2]=896/2

	# T = np.array([[],[],[]])

	# scale for network image
	# f = 0.5
	# M*=f
	# M[-1,-1]=1

	# f = 1.111111
	# f = 0.8

	offx = -272
	M[0,2]+=int(offx)
	# # M[1,2]+=int(offy)

	# M*=f
	# M[-1,-1]=1

	# pp = [448, 540]

	# M[0,-1]=pp[0]
	# M[1,-1]=pp[1]

	# print(M)
	pass
else:
	# f1 = 0.8
	# M[0,0]*=f1
	# M[1,1]*=f1
	# pp = [448, 540]

	# M[0,-1]=pp[0]
	# M[1,-1]=pp[1]

	# f = 1.388888875
	# M[0,0]*=f
	# M[1,1]*=f
	# M[-1,-1]=1

	offx = -272
	M[0,2]+=int(offx)

	# f = 0.9
	# M[0,0]*=f
	# M[1,1]*=f

patternsize = (3,5)

n_iter = 1000
repr_err = 0.001
err_thr = 0.5
err_thr = 1.0

def worker():
	global thread_stop, pc, lidar

	while True:
		# print("lidar thread")
		data, addr = lidar.socket_in.recvfrom(1000)
		# print(addr)
		lidar.parse_packet(data)
		if lidar.pc is not None:
			# print(len(R.pc))
			pc = lidar.pc
			# pc = pc[:,0:3]
			# print(pc.shape)

		# print(thread_stop)

		if thread_stop:
			lidar.stop_all()
			break

def main():
	global thread_stop, pc, lidar

	window_name = f"stereo front {side}"
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

	R = RGBParser('front')
	# R.start_sending()
	R.start_all()

	lidar = LidarParser()
	# lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
	lidar.start_all()

	t = threading.Thread(target=worker)
	t.start()

	ts_prev = None
	detect_target = True
	detect_target = False

	display_lidar = False
	display_lidar = True

	grid = get_grid()
	cnt = 0
	rnge = 10
	radius = 1

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		# print(data)

		if R.image is not None and R.image_ok:
			# print(R.image.shape)
			ts = datetime.datetime.now()


			# R.image = cv2.undistort(R.image, M, D)
			im = R.image.copy()
			# print(im.shape)
			im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			R.image_ok = False

			if side=='left':
				im = im[:, :im.shape[1]//2]				
			else:				
				im = im[:, im.shape[1]//2:]

			print(im.shape)
			im = cv2.undistort(im, M, D)


			# print(im.shape)
			if detect_target:
				corners = find_corners(im)
				# print(corners)
				if corners is not None:

					_, rvec, tvec, _ = cv2.solvePnPRansac(grid, corners, M, D, iterationsCount = n_iter, reprojectionError = repr_err)
					pts, _ = cv2.projectPoints(grid, rvec, tvec, M, distCoeffs=D)

					e = np.mean(np.abs(pts-corners))
					# print(e)

					if e>err_thr:
						timestamp = int(round(ts.timestamp()))

						cv2.imwrite(f"{out_dir}{timestamp}_{e:.3f}.jpg", im)

					
					cv2.drawChessboardCorners(im, patternsize, corners, True)
					
					
			if pc is not None and display_lidar:
				ldr = pc.copy()
				# print(ldr.shape)

				ldr[:,:3] = (C_lidar @ ldr[:,:3].T).T
				ldr = ldr[~np.all(ldr[:,0:3]==0, axis=1),:] # remove zero points

				pts, colors, _ = project_lidar_points(ldr, im.shape, R_lidar, T_lidar, M, np.array([]), rn=rnge)
				# pts, colors, _ = project_lidar_points(ldr, im.shape, R_lidar, T_lidar, M, D, rn=rnge)

				if pts is not None and pts.shape[1]!=0:
					for pt, color in zip(pts,colors):
						c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)][::-1])
						
						try:
							cv2.circle(im, (int(pt[0]),int(pt[1])), radius=radius, color=c, thickness=-1)
						except Exception as e:
							print(e)

				# print(ldr.shape)
				# pts, colors, _ = project_lidar_points(ldr, im, R_lidar, T_lidar, M, D)

				# if pts.shape[0]!=0 and pts is not None:
				# 	# print(pts.shape)
				# 	for point, clr in zip(pts, colors):
				# 		# for point in pts:
				# 		point = (int(point[0]), int(point[1]))
				# 		# clr = (0,0,255)
				# 		# print(clr)
				# 		clr = (int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
				# 		# print(clr, type(clr[0]), type(clr))
				# 		im = cv2.circle(im, point,4, clr, -1)
					

			cv2.circle(im, (int(M[0,-1]),int(M[1,-1])), radius=5, color=(0,0,255), thickness=-1)

			cv2.imshow(window_name, im)

			# calculate fps
			# if ts_prev is not None:
			# 	dt = ts-ts_prev
			# 	delay = dt.total_seconds() * 1000
			# 	print(delay)
			# 	if delay!=0:
			# 		fps = 1000/delay
			# 		print(f"FPS: {fps}")
			# ts_prev = ts

			# if corners is not None:
			# 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)
			

			if cv2.waitKey(1) & 0xFF == ord('q'):
				thread_stop=True
				R.stop_all()
				t.join()
				break
			# break

	R.stop_all()

if __name__=='__main__':
	main()