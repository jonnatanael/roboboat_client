from openstreetmap_load import deg2num, num2deg
import numpy as np
from PIL import Image
import os
from urllib.request import urlopen, Request

def gps_get_image(lat, lon, delta_lat, delta_lon, zoom):

	hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
	   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	   'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
	   'Accept-Encoding': 'none',
	   'Accept-Language': 'en-US,en;q=0.8',
	   'Connection': 'keep-alive'}

	smurl = r"http://a.tile.openstreetmap.org/{0}/{1}/{2}.png"
	xmin, ymax = deg2num(lat, lon, zoom)
	xmax, ymin = deg2num(lat + delta_lat, lon + delta_lon, zoom)

	bbox_ul = num2deg(xmin, ymin, zoom)
	bbox_ll = num2deg(xmin, ymax + 1, zoom)
	# print (bbox_ul, bbox_ll)

	bbox_ur = num2deg(xmax + 1, ymin, zoom)
	bbox_lr = num2deg(xmax + 1, ymax +1, zoom)
	# print(bbox_ur, bbox_lr)

	Cluster = Image.new('RGB',((xmax-xmin+1)*256-1,(ymax-ymin+1)*256-1) )
	for xtile in range(xmin, xmax+1):
		for ytile in range(ymin,  ymax+1):
			# try:			

			imgurl=smurl.format(zoom, xtile, ytile)
			fn = 'openstreetmap_cache/{}_{}_{}.png'.format(zoom, xtile, ytile)
			# 
			# print("Saving: " + fn)

			if not os.path.exists(fn):
				# print("Opening: " + imgurl)

				req = Request(imgurl, headers=hdr)
				# 
				# print(imgstr.read())
				# tile = Image.open(imgstr.raw)
				imgstr = urlopen(req)
				# print(imgstr)
				tile = Image.open(imgstr)
				# print(np.asarray(tile).shape)
				tile.save(fn)
			else:
				tile = Image.open(fn)
				# print(np.asarray(tile).shape)

			Cluster.paste(tile, box=((xtile-xmin)*255 ,  (ytile-ymin)*255))
			# except: 
			#     print("Couldn't download image")
			#     tile = None

	return np.asarray(Cluster), [bbox_ll[1], bbox_ll[0], bbox_ur[1], bbox_ur[0]]