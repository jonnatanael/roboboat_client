import cv2
import matplotlib.pyplot as plt
import numpy as np
from read_utils import GPSParser

# 'latitude': 46.044596723522496, 'longitude': 14.48975455056181
# https://www.openstreetmap.org/#map=9/46.1665/14.6283

# lat_min = 45.3290
# lat_max = 46.9915
# lon_min = 12.8128
# lon_max = 16.4438

lat_min = 46.04352
lat_max = 46.04572
lon_min = 14.48621
lon_max = 14.49242

# lat = 46.04437132727988
# lon = 14.489913352460013

# FE center
lat = 46.04463
lon = 14.48933

# ELES
lat = 46.04519
lon = 14.49072

fn = 'map3.png'

def map_matplotlib():

	BBox = (lon_min, lon_max, lat_min, lat_max)

	ruh_m = plt.imread(fn)

	fig, ax = plt.subplots(figsize = (8,7))
	ax.scatter([lon], [lat], zorder=1, alpha= 1, c='b', s=50)
	ax.set_title('Plotting Spatial Data on Riyadh Map')
	ax.set_xlim(BBox[0],BBox[1])
	ax.set_ylim(BBox[2],BBox[3])
	ax.imshow(ruh_m, zorder=0, extent = BBox, aspect= 'equal')

	cs = ax.collections[0]
	cs.set_offset_position('data')
	print(cs.get_offsets())

	plt.show()

	# exit()

def map_cv2():

	img = cv2.imread(fn)

	(h,w,c) = img.shape
	print(w,h)

	# hack, ker nisem opazil, da je offset po višini h-x
	# a = np.linspace(lon_min,lon_max, w)
	# min_d_lon = 1e6
	# for i,v in enumerate(a):
	# 	d = np.linalg.norm(v-lon)
	# 	if d<min_d_lon:
	# 		min_d_lon = d
	# 		min_idx_lon = i
	# print(min_d_lon, min_idx_lon, a[min_idx_lon])

	# a = np.linspace(lat_min,lat_max, h)
	# min_d_lat = 1e6
	# for i,v in enumerate(a):
	# 	d = np.linalg.norm(v-lat)
	# 	if d<min_d_lat:
	# 		min_d_lat = d
	# 		min_idx_lat = i
	# print(min_d_lat, min_idx_lat, a[min_idx_lat])
	# img = cv2.circle(img, (min_idx_lon,h-min_idx_lat), 5, (255,0,0), -1)
	# print((h-min_idx_lat)/h, min_idx_lon/w)

	x = (lon-lon_min)/(lon_max-lon_min)
	y = (lat-lat_min)/(lat_max-lat_min)

	print((lon-lon_min))
	print((lon_max-lon_min))
	print((lat-lat_min))
	print((lat_max-lat_min))
	print(x,y)
	print((int(x*w),h-int(y*h)))

	# img = cv2.circle(img, (int(x*h),int(y*w)), 5, (0,0,255))
	img = cv2.circle(img, (int(x*w),h-int(y*h)), 5, (0,0,255),-1)
	# img = cv2.circle(img, (int(y*h),int(x*w)), 5, (0,0,255))
	# img = cv2.circle(img, (int(y*w),int(x*h)), 5, (0,0,255))
	# img = cv2.circle(img, (int(0.46*w),int(0.56*h)), 5, (255,0,255), -1)
	

	cv2.namedWindow('', cv2.WINDOW_NORMAL)
	cv2.imshow("", img)
	cv2.waitKey(0)

def map_cv2_2():

	img = cv2.imread('map2.png')

	lat_min = 45.9972
	lat_max = 46.1013
	lon_min = 14.4091
	lon_max = 14.6360

	(h,w,c) = img.shape
	x = (lon-lon_min)/(lon_max-lon_min)
	y = (lat-lat_min)/(lat_max-lat_min)
	img = cv2.circle(img, (int(x*w),h-int(y*h)), 5, (0,0,255),-1)

	cv2.namedWindow('', cv2.WINDOW_NORMAL)
	cv2.imshow("", img)
	cv2.waitKey(0)

def live_map():
	R = GPSParser()
	R.start_all()

	img = plt.imread(fn)
	h = img.shape[0]
	w = img.shape[1]
	print(w,h)
	
	while True:		
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		if R.data is not None:
			print(R.data)		

		if R.data is not None:

			lat = R.data['latitude']
			lon = R.data['longitude']

			# calculate
			x = (lon-lon_min)/(lon_max-lon_min)
			y = (lat-lat_min)/(lat_max-lat_min)

			res_x = x*w
			res_y = h-(y*h)

			plt.clf()
			plt.imshow(img)
			plt.plot(res_x, res_y,'r.')

			plt.title(R.data['timestamp1'])

			# plt.gca().set_xlim(1000,1800)
			# plt.gca().set_ylim(400,1000)	
			# plt.gca().invert_yaxis()

			# plt.waitforbuttonpress()
			plt.draw()
			plt.pause(0.5)

if __name__ == '__main__':
	# map_matplotlib()
	# map_cv2()
	# map_cv2_2()
	live_map()