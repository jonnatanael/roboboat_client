import socket, time, datetime, math
import numpy as np
# import pptk
from read_utils import GPSParser

def main():
	R = GPSParser()
	# R.set_parameters(acquisition=True, sending=1, calibration=0, reboot=0, shutdown=0)
	R.start_all()
	# R.start_acquisition()

	# return
	# R.start_sending()
	
	while True:		
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		if R.data is not None:
			print(R.data)
			print(R.data['latitude'], R.data['longitude'], R.data['mode'], R.data['accuracy'])
			# R.print_data()

if __name__=='__main__':
	main()
