from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt, QTimer, QObject, QThread, pyqtSignal
import cv2
import sys, time
import numpy as np
import pyqtgraph.opengl as gl
import pyqtgraph as pg

from gui_utils import CompassWidget
from read_utils import RadarParser

class Worker(QObject):
	finished = pyqtSignal()
	data = pyqtSignal(object)

	def __init__(self):
		super(Worker, self).__init__()
		self.parser = RadarParser()

	def run(self):
		# self.parser.start_all()
		self.parser.start_sending()

		while True:
			data, addr = self.parser.socket_in.recvfrom(1000)
			self.parser.parse_packet(data)
			if self.parser.pc is not None and len(self.parser.pc)!=0:
				self.data.emit(self.parser.pc)

class RadarWidget(pg.GraphicsWindow):
	def __init__(self, parent=None):
		super().__init__(parent=parent)

		self.layout = QtWidgets.QVBoxLayout()		
		self.setLayout(self.layout)

		self.gl = gl.GLViewWidget()
		self.gl.setCameraPosition(pos=QtGui.QVector3D(-0.6320943236351013, 12.38536262512207, 14.325300216674805), distance=97.87839624836319, elevation=np.degrees(0.6108652381980153), azimuth=np.degrees(-1.5882496193148399))

		self.layout.addWidget(self.gl)
		self.gl.show()
		self.grid = gl.GLGridItem()
		self.gl.addItem(self.grid)

		# self.sp1 = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
		# self.gl.addItem(self.sp1)

		self.sp2 = gl.GLScatterPlotItem(pos=np.random.randint(-1,1,size=(1,3)))
		self.gl.addItem(self.sp2)

		self.draw_frustum()
		# self.draw_circle(100, 100)

		# thread
		self.thread = QThread()
		self.worker = Worker()
		self.worker.moveToThread(self.thread)

		self.thread.started.connect(self.worker.run)
		self.worker.data.connect(self.update)
		self.thread.start()


	def draw_frustum(self):

		fovx = np.radians(50)
		fovy = np.radians(15)
		rn = 100

		p0 = (0,0,0)
		p1 = (rn*np.sin(fovx), rn*np.cos(fovx), rn*np.sin(fovy/2))
		p2 = (-rn*np.sin(fovx), rn*np.cos(fovx), rn*np.sin(fovy/2))
		# ln1 = gl.GLLinePlotItem(pos=np.array([p0, p1]), width=1, antialias=False)
		ln1 = gl.GLLinePlotItem(pos=np.array([p0, p1, p2, p0]), width=1, antialias=False)
		self.gl.addItem(ln1)

		p3 = (rn*np.sin(fovx), rn*np.cos(fovx), -rn*np.sin(fovy/2))
		p4 = (-rn*np.sin(fovx), rn*np.cos(fovx), -rn*np.sin(fovy/2))
		ln2 = gl.GLLinePlotItem(pos=np.array([p0, p3, p4, p0]), width=1, antialias=False)
		self.gl.addItem(ln2)

		self.gl.addItem(gl.GLLinePlotItem(pos=np.array([p1,p3]), width=1, antialias=False))
		self.gl.addItem(gl.GLLinePlotItem(pos=np.array([p2,p4]), width=1, antialias=False))



	def closeEvent(self, event):
		# self.worker.parser.stop_all()
		# self.worker.parser.stop_sending()
		# event.accept()
		pass

	def draw_circle(self, n=200, rn = 33):

		# circle
		N = np.arange(0,2*np.pi,2*np.pi/float(n))
		# rn = 33
		x = np.sin(N)*rn
		y = np.cos(N)*rn
		z = np.zeros_like(N)
		pts = np.vstack((x,y,z)).T

		pts = np.append(pts, np.array([pts[0,:]]), axis=0)


		ln = gl.GLLinePlotItem(pos=pts, width=1, antialias=False, color=(0,0,1,1))
		self.gl.addItem(ln)

	def update(self, data):

		

		# self.sp1.setData(pos=np.array([[0,0,0]]))
		# color = np.zeros((1,3), dtype=np.float32)
		# color[:,0]=1
		# self.sp1.setData(color=color, size=20)

		self.sp2.setData(pos=data[:,0:3])
		color = np.zeros((data.shape[0],4), dtype=np.float32)
		color[:,1]=1

		speed = data[:,-2]

		color[:,-1]=1

		# color non-static points with red
		for i,x in enumerate(speed):

			if x!=0.0:
				color[i,:]=(1,0,0,1)

		sizes = data[:,-1]
		# size = np.random.randint(0, 20, sizes.shape)

		# self.sp2.setData(color=color, size = 10)
		self.sp2.setData(color=color, size = sizes)


		# center = self.gl.opts['center']
		# dist = self.gl.opts['distance']
		# elev = self.gl.opts['elevation'] * np.pi/180.
		# azim = self.gl.opts['azimuth'] * np.pi/180.
		# print(center,dist,elev,azim)


if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	radar_widget = RadarWidget()
	radar_widget.show()

	timer = QTimer()
	timer.timeout.connect(lambda: None)
	timer.start(100)

	app.exec_()
