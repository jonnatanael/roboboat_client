from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt, QTimer, QObject, QThread, pyqtSignal
import cv2
import sys, time, signal
import numpy as np
from collections import deque

from gui_utils import CompassWidget
from read_utils import GPSParser
from gps_utils import gps_get_image

# map: https://towardsdatascience.com/easy-steps-to-plot-geographic-data-on-a-map-python-11217859a2db

# N 46.8827
# S 45.4119
# E 16.5921
# W 13.3759
# Z 9

class GPSWorker(QObject):
	finished = pyqtSignal()
	progress = pyqtSignal(object)

	def __init__(self):
		super(GPSWorker, self).__init__()
		self.parser = GPSParser()

		self.data = {}

	def run(self):
		# self.parser.start_all()
		self.parser.start_sending()

		while True:
			# print("a")
			data, addr = self.parser.socket_in.recvfrom(1000)
			self.parser.parse_packet(data)

			# print(self.parser.data)

			if self.parser.data is not None:

				self.data['pitch'] = self.parser.data['pitch']
				self.data['yaw'] = self.parser.data['yaw']			
				self.data['roll'] = self.parser.data['roll']
				self.data['latitude'] = self.parser.data['latitude']
				self.data['longitude'] = self.parser.data['longitude']
				# print(self.parser.data)
				self.data['mode'] = self.parser.data['mode']
				self.data['accuracy'] = self.parser.data['accuracy']

				# if self.data['yaw'] is not None:
				self.progress.emit(self.data)

class GPSWidget(QtWidgets.QWidget):
	def __init__(self, parent=None):
		super(GPSWidget, self).__init__(parent)

		# return

		self.compass = CompassWidget()
		self.layout = QtWidgets.QHBoxLayout()

		self.setLayout(self.layout)

		gps_layout = QtWidgets.QVBoxLayout()
		groupBoxGps = QtWidgets.QGroupBox()
		groupBoxGps.setMaximumWidth(300)
		groupBoxGps.setMaximumHeight(500)
		groupBoxGps.setLayout(gps_layout)

		gps_layout.addWidget(self.compass)
		# self.layout.addWidget(self.image_frame)

		# set labels
		# pitch
		self.pitchLabel = QtWidgets.QLabel()
		gps_layout.addWidget(self.pitchLabel)
		# roll
		self.rollLabel = QtWidgets.QLabel()
		gps_layout.addWidget(self.rollLabel)
		# yaw
		self.yawLabel = QtWidgets.QLabel()
		gps_layout.addWidget(self.yawLabel)
		# latitude
		self.latitudeLabel = QtWidgets.QLabel()
		gps_layout.addWidget(self.latitudeLabel)
		# longitude
		self.longitudeLabel = QtWidgets.QLabel()
		gps_layout.addWidget(self.longitudeLabel)
		# mode
		self.modeLabel = QtWidgets.QLabel()
		gps_layout.addWidget(self.modeLabel)
		# accuracy
		self.accuracyLabel = QtWidgets.QLabel()
		gps_layout.addWidget(self.accuracyLabel)

		# add groupbox for image
		image_layout = QtWidgets.QVBoxLayout()
		groupBoxImage = QtWidgets.QGroupBox()
		groupBoxImage.setMinimumHeight(100)
		groupBoxImage.setMinimumWidth(100)
		# groupBox.setMaximumHeight(1000)
		groupBoxImage.setLayout(image_layout)
		self.image_frame_gps = QtWidgets.QLabel("image placeholder")
		image_layout.addWidget(self.image_frame_gps)

		self.layout.addWidget(groupBoxGps)
		self.layout.addWidget(groupBoxImage)

		self.gps_image = None

		# set up parameters for image
		self.delta_lat = 0.004
		self.delta_lon = 0.01
		self.zoom = 18
		self.zoom = 17

		# self.history = True
		self.history = False
		self.history_size= 20

		self.point_size = 5

		self.past_locations = deque([], self.history_size) 

		# self.parser = GPSParser()
		# self.parser.start_sending()

		# self.show_image()

		# thread
		self.thread = QThread()
		self.worker = GPSWorker()
		self.worker.moveToThread(self.thread)

		self.thread.started.connect(self.worker.run)
		self.worker.progress.connect(self.updateData)
		self.thread.start()

		self.signal = signal.signal(signal.SIGINT, self.signal_handler)

		# start sending timer
		self.timer2=QTimer()
		self.timer2.timeout.connect(self.start_sending)
		self.timer2.start(5000)

	def start_sending(self):
		# print("sending signal to stream data over network")
		self.worker.parser.start_sending()

	def closeEvent(self, event):
		# self.worker.parser.stop_all()
		self.worker.parser.stop_sending()
		event.accept()

	def signal_handler(self, sig, frame):
		# p = self.worker.parser
		# p.stop_all()
		self.worker.parser.stop_sending()

		sys.exit()

	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_H:
			# print("show history")
			self.history = not self.history

	def wheelEvent(self, event):

		MOD_MASK = (Qt.CTRL | Qt.ALT | Qt.SHIFT | Qt.META)

		direction = np.sign(event.angleDelta().y())
		modifiers = event.modifiers()==Qt.ControlModifier

		if modifiers:
			self.point_size = min(max(self.point_size+direction, 1), 20)

		else:
			new_zoom = min(max(self.zoom+direction, 1), 19)

			if new_zoom!=self.zoom:
				if direction==1:
					f = 0.5			
				else:
					f = 2

				self.delta_lat *= f
				self.delta_lon *= f

				self.gps_image = None

				self.zoom = new_zoom		

				# print(self.zoom, self.delta_lat, self.delta_lon)

	def draw_points_cv2(self, im, lat, lon):
		# im = self.gps_image.copy()

		# cv2.circle(im, (100,100),5, (255,0,0), -1)

		new_image_margin = 0.1

		if im is None:
			return

		lon_min = self.gps_bbox[0]
		lat_min = self.gps_bbox[1]
		lon_max = self.gps_bbox[2]
		lat_max = self.gps_bbox[3]

		h = im.shape[0]
		w = im.shape[1]

		x = (lon-lon_min)/(lon_max-lon_min)
		y = (lat-lat_min)/(lat_max-lat_min)

		if x<new_image_margin or x>(1-new_image_margin) or y<new_image_margin or y>(1-new_image_margin):
			self.gps_image = None


		# res_x = int(w-(x*w))
		res_x = int(x*w)
		res_y = int(h-(y*h))

		cv2.circle(im, (res_x,res_y), self.point_size, (255,0,0), -1)

		return im

	def draw_heading(self, im, lat, lon, yaw):

		line_thickness = max(1, self.point_size-2)

		lon_min = self.gps_bbox[0]
		lat_min = self.gps_bbox[1]
		lon_max = self.gps_bbox[2]
		lat_max = self.gps_bbox[3]

		h = im.shape[0]
		w = im.shape[1]

		x = (lon-lon_min)/(lon_max-lon_min)
		y = (lat-lat_min)/(lat_max-lat_min)

		res_x = int(x*w)
		res_y = int(h-(y*h))

		yaw = np.radians(yaw-90)

		dx = np.cos(yaw)
		dy = np.sin(yaw)
		f = 50

		im = cv2.line(im, (res_x,res_y), (int(res_x+dx*f), int(res_y+dy*f)), (0, 0, 0), thickness=line_thickness)

		return im

	def updateData(self, data):
		self.compass.setAngle(float(data['yaw']))
		self.pitchLabel.setText("pitch: {:.3f}".format(data['pitch']))
		self.rollLabel.setText("roll: {:.3f}".format(data['roll']))
		self.yawLabel.setText("yaw: {:.3f}".format(data['yaw']))
		self.latitudeLabel.setText("latitude: {:.3f}".format(data['latitude']))
		self.longitudeLabel.setText("longitude: {:.3f}".format(data['longitude']))
		self.modeLabel.setText("mode: {:.3f}".format(data['mode']))
		self.accuracyLabel.setText("accuracy: {:.3f}".format(data['accuracy']))
		# print(data['latitude'], data['longitude'])

		lat = data['latitude']
		lon = data['longitude']
		yaw = data['yaw']

		if self.gps_image is None:
			self.gps_image, self.gps_bbox = gps_get_image(lat-self.delta_lat/2, lon-self.delta_lon/2, self.delta_lat, self.delta_lon, self.zoom)
			return
		# else:
			# TODO check if current location is near the edge and correct accordingly

			# print(self.gps_image.shape)
			# return

		im = self.gps_image.copy()

		# overlay = np.zeros_like(self.gps_image)
		overlay = np.ones_like(self.gps_image)
		overlay = im.copy()
		# print("overlay", overlay.shape, overlay.dtype)

		self.past_locations.appendleft((lat, lon))

		if self.history:			
			for i, pt in enumerate(self.past_locations):
				# print(pt)
				lat_, lon_ = pt
				# print(lat_, lon_)
				x = overlay.copy()
				x = self.draw_points_cv2(x, lat_, lon_)
				alpha = i/self.history_size
				overlay = cv2.addWeighted(overlay, alpha, x, 1 - alpha, 0)

			# self.past_locations.popleft()

			# draw heading
			overlay = self.draw_heading(overlay, lat, lon, yaw)

			alpha = 1
			image_final = cv2.addWeighted(overlay, alpha, im, 1 - alpha, 0)


		else:
			# draw point on image
			# im = self.draw_points_cv2(im, lat, lon)
			# overlay = im.copy()
			print(lat, lon)

			# x = overlay.copy()
			# x = self.draw_points_cv2(x, lat, lon)
			# alpha = 1
			# overlay = cv2.addWeighted(overlay, alpha, x, 1 - alpha, 0)

			image_final = self.draw_points_cv2(im, lat, lon)
			overlayimage_final = self.draw_heading(image_final, lat, lon, yaw)
			


		# self.past_locations.append((lat, lon))
		
		# print(self.past_locations)

		# if len(self.past_locations)>100:
		# 	self.past_locations = self.past_locations[100:] 

		# gps_image = QtGui.QImage(self.gps_image.data, self.gps_image.shape[1], self.gps_image.shape[0], self.gps_image.strides[0], QtGui.QImage.Format_RGB888 )
		# gps_image = QtGui.QImage(im.data, im.shape[1], im.shape[0], im.strides[0], QtGui.QImage.Format_RGB888 )
		gps_image = QtGui.QImage(image_final.data, image_final.shape[1], image_final.shape[0], image_final.strides[0], QtGui.QImage.Format_RGB888 )
		pixmap = QtGui.QPixmap.fromImage(gps_image)
		pixmap = pixmap.scaled(self.image_frame_gps.size(), Qt.KeepAspectRatio)
		self.image_frame_gps.setPixmap(pixmap)
		# self.draw_gps_point(lat, lon)

	def draw_gps_point(self, lat, lon):
		lon_min = self.gps_bbox[0]
		lat_min = self.gps_bbox[1]
		lon_max = self.gps_bbox[2]
		lat_max = self.gps_bbox[3]

		# print(lon_min, lon_max)
		h = self.gps_image.shape[0]
		w = self.gps_image.shape[1]

		a = self.image_frame_gps.size()
		print(a)

		# w, h = a

		h = self.image_frame_gps.size().height()
		w = self.image_frame_gps.size().width()

		# w = self.gps_size[0]
		# h = self.gps_size[1]
		# print(w,h)


		draw_all = False
		# draw_all = True

		qp = QtGui.QPainter(self.image_frame_gps.pixmap())
		pen = QtGui.QPen(QtCore.Qt.red, 1)
		brush = QtGui.QBrush(QtCore.Qt.red)
		qp.setPen(pen)
		qp.setBrush(brush)

		# if draw_all:

		# 	# pen = QtGui.QPen(QtCore.Qt.red, 0.1)

		# 	# print(self.gps_memory[-1][0], self.gps_memory[-1][1], self.mode)

		# 	for lat, lon in self.gps_memory:
		# 		# print(lat, lon)
		# 		x = (lon-lon_min)/(lon_max-lon_min)
		# 		y = (lat-lat_min)/(lat_max-lat_min)
		# 		res_x = w-(x*w)
		# 		res_y = h-(y*h)
		# 		qp.drawEllipse(QtCore.QPoint(res_x, res_y), 5, 5)
		# else:
		# draw last value

		x = (lon-lon_min)/(lon_max-lon_min)
		y = (lat-lat_min)/(lat_max-lat_min)
		print("x", x, y)

		if x<0.1 or x>0.9 or y<0.1 or y>0.9:
			self.gps_image = None

		# print(lat, lon, self.mode)

		# res_x = x*w
		res_x = w-(x*w)
		# res_y = h-(y*h)
		res_y = (y*h)
		# print(res_x, res_y)
		
		# for i in range(self.points.count()):
		qp.drawEllipse(QtCore.QPoint(res_x, res_y), 5, 5)

if __name__ == '__main__':
	app = QtWidgets.QApplication(sys.argv)
	display_image_widget = GPSWidget()
	display_image_widget.show()

	timer = QTimer()
	timer.timeout.connect(lambda: None)
	timer.start(100)

	sys.exit(app.exec_())
