# listens to one camera and lidar
# extracts features from both
# if available, calculates error between features using pre-calculated calibration

from socket import *
# import time, datetime, math
import numpy as np
# import datetime
import time
import cv2, os, threading
import datetime
from numpy.linalg import norm


from read_utils import *
# from live_client import load_camera_calibration

from calibration_utils import *
from projection_utils import *

# lidar stream process
from multiprocessing import Process, Queue
from lidar_stream_shm import lidar_reader, create_shm_lidar

from calibrate_lidar_camera import calibrate

patternsize = (3,5)
grid = get_grid()

def get_lidar_features(pc, thr=0.05, n_beams=16):

	# TODO calculate azimuth
	# then, beam by beam, calculate the reasonable edge points

	beams = pc[:,-1]

	res = np.zeros_like(beams)

	for n in range(n_beams):
		idx = beams==n

		p = pc[idx, :]
		dst = np.zeros_like(p[:,0])

		for i, x in enumerate(p[:-1]):
			x_ = p[i+1]
			dt = np.sqrt((x[0]-x_[0])**2+(x[1]-x_[1])**2+(x[2]-x_[2])**2)

			if np.abs(dt)>thr:

				dst[i]=1
				dst[i+1]=1

		if dst.shape[0]!=0:
			dst[0]=1
			dst[-1]=1

		res[idx]=dst

	pc = pc[res==1, :]
	res = res[res==1]

	return pc, res

def is_collinear(a,b,c, thr = 0.01):

	# x1 = a[0]; y1 = a[1]
	# x2 = b[0]; y2 = b[1]
	# x3 = c[0]; y3 = c[1]

	# print(x1,y1,x2,y2,x3, y3)
	# print((y2-y3), (y3-y1), (y1-y2))

	# a = 1/2*(x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))

	# print(a)

	AB = a-b
	AC = a-c

	a = 0.5*np.sqrt(norm(AB)**2*norm(AC)**2-(np.dot(AB,AC))**2)
	# print(a)

	return a<thr

def process_lidar(pc, thr=0.05, n_beams=16, line_length=500):

	beams = pc[:,-1]

	res = np.zeros_like(beams)

	for n in range(n_beams):
		idx = beams==n

		p = pc[idx, :]
		dst = np.zeros_like(p[:,0])

		for i, x in enumerate(p[:-1]):
			x_ = p[i+1]
			dt = np.sqrt((x[0]-x_[0])**2+(x[1]-x_[1])**2+(x[2]-x_[2])**2)

			if np.abs(dt)>thr:

				dst[i]=1
				dst[i+1]=1

		if dst.shape[0]!=0:
			dst[0]=1
			dst[-1]=1

		res[idx]=dst

	edges = pc[res==1, :]
	# res = res[res==1]

	# print(edges.shape)

	out = np.zeros_like(beams)

	for n in range(n_beams):

		beam_idx = beams==n
		beam = pc[idx, :] 

		# edges_id = 
		beam_edges_idx = (beams==n) & (res==1)
		# print(len(beam), np.sum(res[beam_edges_idx]))
		# print(beam_edges.shape)

		indices = np.argwhere(beam_edges_idx)
		# print(indices)

		for i,x in enumerate(indices[:-1]):

			pt1 = pc[x]
			pt2 = pc[indices[i+1]]
			# print(x, indices[i+1], pt1,pt2)
			# idx2 = indices[i+1]

			start_idx = x[0]
			end_idx = indices[i+1][0]

			


			potential_line = pc[start_idx:end_idx,:]

			# print(x, indices[i+1])
			# print(potential_line.shape)

			if potential_line.shape[0]>line_length:

				# print(np.var(potential_line))

				out[start_idx]=1
				out[end_idx]=1

			continue


			s = 0

			if len(potential_line)<5:
				continue

			for pt in potential_line:
				ok = is_collinear(pc[start_idx,:], pt, pc[end_idx,:])
				if ok:
					s+=1

			perc = (s/len(potential_line))*100

			if perc>80:
				out[start_idx]=1
				out[end_idx]=1

			# print(f"line percent: {perc}")
			# print(f"line length: {len(potential_line)}")



		# TODO iterate through raw points, check if line exists between edges
		# for i,x in enumerate(res[beam_edges_idx]):
		# 	print(i,x)

	final = pc[out==1, :]

	return final
	# return edges

def main():

	# sensors = ['zed', 'polarization_camera', 'stereo_front_left', 'stereo_front_right', 'stereo_side_left', 'stereo_side_right', 'thermal_camera']
	# sensors_parsers = [ZEDParser, PolarizationParser, RGBParser, RGBParser, RGBParser, RGBParser, ThermalParser]

	sensor_info = {'zed':ZEDParser, 'polarization_camera':PolarizationParser, 'stereo_side_left':RGBParser, 'stereo_side_right':RGBParser, 'thermal_camera':ThermalParser }

	sensor = 'thermal_camera'
	# sensor = 'polarization_camera'
	# sensor = 'zed'
	# sensor = 'stereo_side_left'
	# sensor = 'stereo_side_right'

	if 'stereo' in sensor:
		if 'left' in sensor:
			parser = sensor_info[sensor]('left')
		else:
			parser = sensor_info[sensor]('right')

	else:
		parser = sensor_info[sensor]()

	# load calibrations
	calib_fn = f'helper_calibrations/calibration_{sensor}_helper.yaml'

	(width, height, M, D) = load_camera_calibration(calib_fn)

	if sensor=='zed':
		M*=0.5
		M[-1,-1]=1

	R_lidar, T_lidar, C_lidar = load_lidar_calibration(f"/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_lidar_{sensor}.yaml")

	# save timestamp and set up out dir
	start_ts = datetime.datetime.now()
	start_timestamp = int(round(start_ts.timestamp()))

	out_dir = f'calibration_data/lidar_calibration_data_{sensor}_{start_timestamp}/'

	# setup lidar process
	lidar_height = 60000
	shr_lidar, pc = create_shm_lidar(lidar_height)
	p1 = Process(target=lidar_reader, args=(shr_lidar.name,))
	p1.start()

	parser.start_all()

	rnge = 10

	if sensor=='thermal_camera':
		radius = 1
	else:
		radius = 3
	edge_constant = 11
	edge_constant = 31
	# edge_constant = 51

	# toggles
	display_lidar = True
	# display_lidar = False
	detect_target = True
	display_corners = True
	# display_corners = False
	display_edges = True
	undistort = False
	undistort = True
	calculate_lidar_edges = True
	calculate_lidar_edges = False

	# line_length=500

	cv2.namedWindow(sensor, cv2.WINDOW_NORMAL)
	cv2.createTrackbar('lidar', sensor, 0, 1, lambda *args: None)
	cv2.createTrackbar('undistort', sensor, 0, 1, lambda *args: None)
	cv2.createTrackbar('corners', sensor, 0, 1, lambda *args: None)
	cv2.createTrackbar('edges', sensor, 0, 1, lambda *args: None)
	# cv2.createTrackbar('lidar edges', sensor, 0, 1, lambda *args: None)
	# cv2.createTrackbar('line_length', sensor, 2, 1000, lambda *args: None)
	cv2.createTrackbar('range', sensor, 10, 100, lambda *args: None)

	cv2.setTrackbarPos('lidar', sensor, 1 if display_lidar else 0)
	cv2.setTrackbarPos('undistort', sensor, 1 if undistort else 0)
	cv2.setTrackbarPos('corners', sensor, 1 if display_corners else 0)
	cv2.setTrackbarPos('edges', sensor, 1 if display_edges else 0)
	# cv2.setTrackbarPos('lidar edges', sensor, 1 if calculate_lidar_edges else 0)
	# cv2.setTrackbarPos('line_lengths', sensor, line_length)

	# stability
	corners_thr = 1.0
	stability_count = 0
	stability_threshold = 10
	stability_threshold = 1
	stable = False
	corners_prev = None

	# positions list
	positions = []
	images = []
	scans = []
	edges_list = []
	lidar_edges_list = []
	position_threshold = 5

	while True:

		# check queue
		try:
			# x = q.get_nowait()
			(R_lidar, T_lidar) = q.get_nowait()
			# print(x)
			calibration_running = False
		except:
			pass

		data, addr = parser.socket_in.recvfrom(1000)
		parser.parse_packet(data)

		edges = None

		# query trackbars
		display_lidar = True if cv2.getTrackbarPos('lidar', sensor)==1 else False
		undistort = True if cv2.getTrackbarPos('undistort', sensor)==1 else False
		display_corners = True if cv2.getTrackbarPos('corners', sensor)==1 else False
		display_edges = True if cv2.getTrackbarPos('edges', sensor)==1 else False
		# calculate_lidar_edges = True if cv2.getTrackbarPos('lidar edges', sensor)==1 else False
		# line_length = cv2.getTrackbarPos('line_length', sensor)

		rnge = cv2.getTrackbarPos('range', sensor)

		if parser.image is not None and parser.image_ok:

			ts = datetime.datetime.now()
			ts = int(round(ts.timestamp()))

			im = parser.image.copy()
			im_raw = im.copy()
			parser.image_ok = False

			if sensor=='polarization_camera' or sensor=='thermal_camera':
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			elif 'stereo' in sensor:
				if 'left' in sensor:
					im = im[:,:im.shape[1]//2]
				else:
					im = im[:,im.shape[1]//2:]
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
				im_raw = im.copy()

			if undistort:
				im = cv2.undistort(im, M, D)

			corners = None
			if detect_target:

				corners = find_corners(im)

				if corners is None:
					corners = find_corners(255-im)

				if corners is not None:
					if display_corners:
						cv2.drawChessboardCorners(im, patternsize, corners, True)

					edges = get_edges(im, M, corners, c=edge_constant)
					edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2RGB)
					edges = edges.astype(np.float32)

					# check stability
					if corners_prev is not None:
						# print(stability_count)
						diff = np.mean(np.abs(corners-corners_prev))
						if diff>corners_thr:
							stability_count = 0
							stable = False
						else:
							stability_count+=1
							if stability_count>stability_threshold:
								stable=True
					corners_prev = corners

					# check if position was seen before
					new_position = True
					for c in positions:
						diff = np.mean(np.abs(c-corners))
						if diff < position_threshold:
							new_position = False

			if display_lidar:
				ldr = pc.copy()


				ldr[:,:3] = (C_lidar @ ldr[:,:3].T).T
				ldr = ldr[~np.all(ldr[:,0:3]==0, axis=1),:] # remove zero points
				# print(ldr.shape)
				ldr = ldr[ldr[:,2]>0,:] # remove points behind camera
				if undistort:		
					pts, colors, _ = project_lidar_points(ldr, im.shape, R_lidar, T_lidar, M, np.array([]), rn=rnge, nonlinear_factor=0.6)
				else:
					pts, colors, _ = project_lidar_points(ldr, im.shape, R_lidar, T_lidar, M, D, rn=rnge, nonlinear_factor=0.6)


				if pts is not None and pts.shape[1]!=0:
					for pt, color in zip(pts,colors):
						# c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)][::-1])					
						c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)])					
						try:
							cv2.circle(im, (int(pt[0]),int(pt[1])), radius=radius, color=c, thickness=-1)
						except:
							pass


			if edges is not None:
				if display_edges:

					if not new_position:
						edges[...,0]=0
						edges[...,2]=0
					else:
						edges[...,0]=0
						edges[...,1]=0

					im = (im/255).astype(float)
					im = im*(1-edges)+edges

			draw_border(im, clr=(0,255,0) if stable else (0,0,255), thickness=5)

			cv2.imshow(sensor, im)

			key = cv2.waitKey(1) & 0xFF

			if key == ord('q'):
				break
			elif key ==ord('s'):
				
				if corners is not None and stable:	

					if new_position:
						ldr_raw = pc.copy()

						positions.append(corners)

						print("saving data")

						try:
							os.mkdir(out_dir)
						except:
							pass

						np.save(f"{out_dir}{ts}.npy", {'pc': ldr_raw, 'corners': corners})
						cv2.imwrite(f"{out_dir}{ts}.png", im_raw)

					else:
						print("this position was aleady saved")
				else:
					print("image not stable or target not detected")

	parser.stop_all()
	p1.terminate()
	p1.join()
	shr_lidar.close()
	shr_lidar.unlink()
	cv2.destroyAllWindows() #cleanup windows 

if __name__=='__main__':
	main()