from socket import *
import time, datetime, math
import numpy as np
# import struct, zlib, binascii, sys
# import pptk
from read_utils import ThermalParser
import cv2
# from PIL import Image

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

def main():
	patternsize = (3,5)

	window_name = "thermal"
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
	R = ThermalParser()

	R.start_all()

	while True:
		
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		if R.image is not None and R.image_ok:
			im = R.image.copy()
			R.image_ok = False

			im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)

			corners = find_corners(255-im)

			if corners is not None:

				# if draw_target:
				cv2.drawChessboardCorners(im, patternsize, corners, True)

			cv2.imshow(window_name, im)

			key = cv2.waitKey(1) & 0xFF
			if key == ord('q'):
				# thread_stop=True
				R.stop_all()
				# t.join()
				break
			# break

			# elif key & 0xFF == ord("+"):

			# 	print("+")

			# 	thr = min(255,thr+thr_step)
			# # 	print(azi_filter)
			# # 	lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			# elif key & 0xFF == ord("-"):
			# 	print('-')
			# 	thr = max(0,thr-thr_step)

def main_deprecated():

	window_name = "thermal"
	# cv2.namedWindow("raw", cv2.WINDOW_NORMAL)
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
	# cv2.namedWindow("thermal2", cv2.WINDOW_NORMAL)

	# R = ThermalParser(IP='192.168.90.72', in_port=27132)
	# R = ThermalParser(in_port=27132)
	R = ThermalParser()

	# R.set_parameters(num_images=5, shutterless=1, auto_gain_control=2)
	#R.set_parameters(num_images=0, frame_rate=10)
	# R.set_parameters(num_images=0, frame_rate=100, acquisition=True, sending=False)
	# R.set_parameters(num_images=0, frame_rate=10, auto_gain_control=1)
	# time.sleep(1)

	# return
	# R.get_parameters()
	R.start_all()
	# R.start_sending()
	# R.start_acquisition()
	# return

	ts_prev = None

	thr = 80
	thr_step = 5
	blob_area_thresh = 200

	params = cv2.SimpleBlobDetector_Params()

	# Change thresholds
	params.minThreshold = 200
	params.maxThreshold = 1000

	# Filter by Area.
	params.filterByArea = True
	params.minArea = 50

	# Filter by Circularity
	params.filterByCircularity = True
	params.minCircularity = 0.1

	# Filter by Convexity
	params.filterByConvexity = True
	params.minConvexity = 0.87

	# Filter by Inertia
	params.filterByInertia = False
	params.minInertiaRatio = 0.01

	# Create a detector with the parameters
	detector = cv2.SimpleBlobDetector_create(params)

	while True:
		# print("waiting")
		
		data, addr = R.socket_in.recvfrom(1000)
		# print(data)

		R.parse_packet(data)
		# print(data)
		# if R.parameters is not None:
		# 	print(R.parameters)
		# R.display()
		if R.image is not None and R.image_ok:
			ts = datetime.datetime.now()
			# print(ts)
			# print(R.image.shape)

			im = R.image.copy()
			R.image_ok = False
			im2 = im.copy()
			
			idx = im2<thr
			# print(idx)
			im2 = np.where(im2>thr, 255, 0).astype(np.uint8)
			im2[idx]=0
			im2[~idx]=255
			# print(im.shape, im.dtype)
			# print(im2.shape, im2.dtype)

			im3 = cv2.normalize(im, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)

			# x = np.argmax(im3)
			# y = np.unravel_index(im3.argmax(), im3.shape)
			# print(x, y, im[y[0], y[1]])

			# im3 = cv2.applyColorMap(im, cv2.COLORMAP_PLASMA)

			# cv2.circle(im3, (y[1], y[0]), radius=3, color=(255,255,0), thickness=-1)

			

			# contours
			# cnts = cv2.findContours(im2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			# cnts = cnts[0] if len(cnts) == 2 else cnts[1]
			# # print(cnts)

			# im2 = cv2.cvtColor(im2, cv2.COLOR_GRAY2RGB)

			# for c in cnts:
			# 	blob_area = cv2.contourArea(c)
			# 	if blob_area > blob_area_thresh:
			# 		cv2.drawContours(im2, [c], -1, (0,0,255), 1)

			# blobs
			
			# Detect blobs.
			im3 = cv2.cvtColor(im3, cv2.COLOR_GRAY2RGB)

			keypoints = detector.detect(~im2)

			im3 = cv2.applyColorMap(im3, cv2.COLORMAP_PLASMA)

			# print(keypoints)
			# im2 = cv2.cvtColor(im2, cv2.COLOR_GRAY2RGB)
			# # Draw detected blobs as red circles.
			# # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
			# im3 = cv2.drawKeypoints(im3, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
			m = 5
			for p in keypoints:
				# print(p.pt, p.size)
				x = int(p.pt[0])
				y = int(p.pt[1])
				s = int(p.size//2)
				cv2.circle(im3, (x, y), radius=s, color=(0, 255, 0), thickness=1)
				# area = im[x-s:x+s, y-s:y+s]
				area = im[y-s:y+s, x-s:x+s]
				# print(area)
				try:
					value = int(np.mean(area))
					cv2.putText(im3 , str(value), (x-s-m,y-s-m), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
				except:
					pass


			# cv2.imshow("raw", im2)
			cv2.imshow(window_name, im3)
			# cv2.imshow("thermal2", im2)

			key = cv2.waitKey(1) & 0xFF

			print(f"threshold: {thr}")

			if key == ord('q'):
				# thread_stop=True
				R.stop_all()
				# t.join()
				break
			# break

			elif key & 0xFF == ord("+"):

				print("+")

				thr = min(255,thr+thr_step)
			# 	print(azi_filter)
			# 	lidar.set_parameters(fov_start=0, fov_stop=360, azimuth_filter=azi_filter, elevation_filter=elev_filter)
			elif key & 0xFF == ord("-"):
				print('-')
				thr = max(0,thr-thr_step)

if __name__=='__main__':
	main()