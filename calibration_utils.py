import numpy as np
import cv2, os
from matplotlib import pyplot as plt

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

def eulerAnglesToRotationMatrix(theta):
	# Calculates Rotation Matrix given euler angles.
	theta = [np.radians(x) for x in theta]
	
	R_x = np.array([[1, 0, 0],
					[0, np.cos(theta[0]), -np.sin(theta[0]) ],
					[0, np.sin(theta[0]), np.cos(theta[0]) ]
					])
		
		
					
	R_y = np.array([[np.cos(theta[1]), 0,np.sin(theta[1]) ],
					[0, 1,0],
					[-np.sin(theta[1]),0,np.cos(theta[1]) ]
					])
				
	R_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
					[np.sin(theta[2]), np.cos(theta[2]), 0],
					[0, 0,1]
					])
					
	R = np.dot(R_x, np.dot( R_y, R_z ))

	return R

# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R) :
	Rt = np.transpose(R)
	shouldBeIdentity = np.dot(Rt, R)
	I = np.identity(3, dtype = R.dtype)
	n = np.linalg.norm(I - shouldBeIdentity)
	return n < 1e-6

def rotationMatrixToEulerAngles(R) :

	assert(isRotationMatrix(R))

	sy = np.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])

	singular = sy < 1e-6

	if not singular :
		x = np.arctan2(R[2,1] , R[2,2])
		y = np.arctan2(-R[2,0], sy)
		z = np.arctan2(R[1,0], R[0,0])
	else :
		x = np.arctan2(-R[1,2], R[1,1])
		y = np.arctan2(-R[2,0], sy)
		z = 0

	return np.array([x, y, z])

def adjust_contrast_brightness(img, contrast:float=1.0, brightness:int=0):
	"""
	Adjusts contrast and brightness of an uint8 image.
	contrast:   (0.0,  inf) with 1.0 leaving the contrast as is
	brightness: [-255, 255] with 0 leaving the brightness as is
	"""
	brightness += int(round(255*(1-contrast)/2))
	return cv2.addWeighted(img, contrast, img, 0, brightness)

def get_grid():
	# create grid

	objectPoints= []
	grid_size = 0.3
	rows, cols = 3, 6

	z = 0
	off = grid_size/2

	for i in range(rows):
		row = []
		for j in range(cols):
			
			if j%2==0:
				p = (-j*off,i*(-grid_size),z)
				row.append(p)
			else:
				if i<rows-1:
					p = (-j*(off),-(i*(off*2)+off),z)
					row.append(p)

		if i<rows-1:
			row = row[::2]+row[1::2]

		objectPoints.extend(row)

	objectPoints= np.array(objectPoints).astype('float32')

	objectPoints[:,0]*=-1
	objectPoints[:,1]*=-1

	return objectPoints

def find_corners(im, patternsize = (3,5)):

	ret, corners = cv2.findCirclesGrid(im, patternsize, flags=cv2.CALIB_CB_ASYMMETRIC_GRID+cv2.CALIB_CB_CLUSTERING)

	return corners if ret else None

def load_camera_calibration(filename):

	if os.path.isfile(filename):

		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		sz = fs.getNode("imageSize")
		M = fs.getNode("cameraMatrix").mat()
		D = fs.getNode("distCoeffs").mat()
		if sz.isSeq(): # za Rokov format, ki lahko vsebuje sezname
			width = sz.at(0).real()
			height = sz.at(1).real()
		else:
			sz = sz.mat()
			height = sz[0][0]
			width = sz[1][0]
		return (width, height, M, D)
	else:
		print("calibration file not found!")

def load_lidar_calibration(filename):
	if os.path.isfile(filename):
		fs = cv2.FileStorage(filename, cv2.FILE_STORAGE_READ)
		R = fs.getNode("R").mat()
		T = fs.getNode("T").mat()
		C = fs.getNode("C").mat()
		
		return (R, T, C)
	else:
		print("calibration file not found!")

def get_colors(points, rnge=100, relative=False, cmap_name='Wistia', cmap_len=256, nonlinear_factor=1):
	colors = []
	dist = np.sqrt(points[0,:]**2+points[1,:]**2+points[2,:]**2)

	if relative:
		rnge = np.max(dist)

	f = dist/rnge
	f = np.clip(f, 0, 1)

	if nonlinear_factor!=1:
		f = np.power(f, nonlinear_factor)
	
	cmap = plt.get_cmap(cmap_name)
	sm = plt.cm.ScalarMappable(cmap=cmap)
	color_range = sm.to_rgba(np.linspace(0, 1, cmap_len))[:,0:3]

	idx = f*(cmap_len-1)

	for i in idx:
		clr = color_range[int(i),::-1]
		colors.append(clr)

	return np.array(colors)

def project_lidar_points(point_cloud, sz, R, t, M, D, rn=1e6, cmap_name='turbo_r', cmap_len=256, nonlinear_factor=1):
	# pc = point_cloud[:,:3].T
	# idx = (pc[2,:]>0) # remove points behind camera
	# pc = pc[:,idx]

	if point_cloud.shape[1]==0:
		return point_cloud, None, None

	colors = get_colors(point_cloud, rn, cmap_name=cmap_name, cmap_len=cmap_len, nonlinear_factor=nonlinear_factor)

	pts, _ = cv2.projectPoints(point_cloud, R, t, M, distCoeffs=D)
	pts = pts[:,0,:]
	mask = (pts[:,0]>0) & (pts[:,1]>0) & (pts[:,0]<sz[1]-1) & (pts[:,1]<sz[0]-1) # create mask for valid pixels

	return pts, colors, mask


def get_mask(shape, rvec, tvec, M):

	# define target plane
	# plane_w = -1.48
	# plane_h = -1.0502
	plane_w = 1.48
	plane_h = 1.0502
	points = np.array([[0,0,0],[0,plane_h,0],[plane_w,plane_h,0],[plane_w,0,0]])
	points[:,0]-=0.365
	points[:,1]-=0.215

	corner_pts, _ = cv2.projectPoints(points, rvec, tvec, M, distCoeffs=None)
	corner_pts = corner_pts[:,0,:]

	mask = np.zeros(shape,dtype=np.uint8)
	# print(mask.shape)
	cv2.fillPoly(mask, pts = [corner_pts.astype(np.int32)], color=(255,255,255))

	return mask

def get_edges(im, M, corners, c=1):

	grid = get_grid()

	_, rvec, tvec = cv2.solvePnP(grid, corners, M, None)
	mask = get_mask(im.shape[:-1], rvec, tvec, M)

	dist = tvec[-1,0]

	# n_blur = 255
	# n_blur = np.sqrt(im.shape[0]**2+im.shape[1]**2)*0.1
	# n_blur = np.sqrt(im.shape[0]**2+im.shape[1]**2)/((dist**2)*0.3)
	# diag = np.sqrt(im.shape[0]**2+im.shape[1]**2)

	# n_blur = (dist**2)*5+diag*0.1
	# n_blur = dist*50
	# n_blur = np.sqrt(dist)*50
	# n_blur = (np.sqrt(dist)+np.sqrt(diag))*5 # to dela kar ok, do razdalje cca 6m
	# n_blur = ((M[0,0])/np.sqrt(dist))/5

	f = M[0,0]
	# c = 2e4*1
	# c = 4e4
	# c = 1.5e4
	# c = 1e5
	# a = 0.9
	# n_blur = (c*dist)/f
	n_blur = (c*10)/(f*dist)
	# n_blur = c/dist
	n_blur = f/(dist*c)

	# print(n_blur)
 
	# n_blur/=np.sqrt(dist)
	# n_blur/=dist**2
	# n_blur/=dist/5

	n_blur = int(n_blur)
	n_blur = c
	n_blur+=1 if n_blur%2==0 else 0

	# print(diag, dist, n_blur)
	# n_blur = 355


	# sigma = 20
	# sigma = n_blur//20
	# sigma = n_blur//10
	sigma = n_blur//5

	edges = cv2.Canny(mask, threshold1=0, threshold2=100).astype(np.float32)
	# edges = cv2.GaussianBlur(edges,(n_blur,n_blur),0)
	edges = cv2.GaussianBlur(edges,(n_blur,n_blur),sigma, sigma)
	# print("edges", edges.shape, edges.dtype)

	# print(np.unique(edges))

	# give larger weights to smaller targets
	# edges*=dist*5

	return edges/np.max(edges)


def draw_border(im, clr=(0,255,0), thickness=5):

	# draw stability indicator
	frame_width = 5
	frame_margin = thickness//2
	# frame_clr = (0,255,0) if stable else (0,0,255)
	cv2.rectangle(im, (frame_margin, frame_margin), (im.shape[1]-frame_margin, im.shape[0]-frame_margin), clr, thickness)

	return im