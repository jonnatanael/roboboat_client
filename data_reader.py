import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from PIL import Image
import cv2, glob, datetime

from ltf_parser import ltf_parser


data_dir = "F:\\Jon\\Lab\\davimar\\paddys_davimar_test\\"

def read_thermal_data():
	pth = data_dir+"zunanji_test_thermal\\"

	images = sorted(glob.glob(pth+'Images*'))
	indices = sorted(glob.glob(pth+'Index*'))

	im_size = (288, 384)
	start = 5

	# display = False
	display = True
	idx = 2

	im_fn = images[idx]
	idx_fn = indices[idx]

	# for i, (im_fn, idx_fn) in enumerate(zip(images, indices)):

	with open(idx_fn) as f:
		lines = f.readlines()
		res = [l.split(',') for l in lines]
		res = [[int(l[0]),int(l[1]),int(l[2])] for l in res]
		data = np.array(res)

	indices = data[start:,0]
	timestamps = data[start:,1]


	if display:
		# plt.clf()

		for idx in range(0,data.shape[0]):
			print(idx, data.shape[0])
			print(im_fn, idx_fn)

			with open(im_fn, "rb") as f:
				# index = data[idx,0]
				offset = data[idx,2]

				image_bytes = np.fromfile(f, dtype=np.uint16,count=im_size[0]*im_size[1], offset=offset)
			# print(len(image_bytes))

			im = image_bytes.reshape(im_size) #notice row, column format
			plt.clf()
			# im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			# im = cv2.merge([(im/(2**16))*255,(im/(2**16))*255,(im/(2**16))*255])
			im = (im/256).astype('uint8')
			# im = cv2.merge([im,im,im])
			plt.imshow(im)
			# plt.show()
			plt.draw()
			plt.pause(0.01)
			# plt.waitforbuttonpress()

def read_polar_data():

	# data_dir = "C:\\Users\\Jon\\Desktop\\paddys_davimar_test\\zunanji_test_polar\\"
	pth = data_dir+"zunanji_test_polar\\"

	images = sorted(glob.glob(pth+'Images*.raw'))
	indices = sorted(glob.glob(pth+'Index*.txt'))
	print(images)
	# im_size = (2432, 2048)
	im_size = (1216, 1024)

	# display = False
	display = True
	start = 5
	idx = 0

	im_fn = images[idx]
	idx_fn = indices[idx]

	# for i, (im_fn, idx_fn) in enumerate(zip(images, indices)):
		# print(i, idx_fn)
		# if i==0:
		# 	continue
		# res = []
		# plt.clf()
		# print(im_fn, idx_fn)

	with open(idx_fn) as f:
		lines = f.readlines()
		res = [l.split(',') for l in lines]
		res = [[int(l[0]),int(l[1]),int(l[2]),int(l[3])] for l in res]
		data = np.array(res)

		indices = data[start:,0]
		timestamps = data[start:,1]
		offsets = data[start:,2]
		lengths = data[start:,3]


	if display:
		# plt.clf()

		for idx in range(0,data.shape[0]):
			print(idx, data.shape[0])
			# print(im_fn, idx_fn)				

			with open(im_fn, "rb") as f:
				index = data[idx,0]
				ts = data[idx,1]
				# offset = data[idx,2]
				offset = offsets[idx]
				length = lengths[idx]					

				print("ts", ts)
				#image_bytes = np.fromfile(f, dtype=np.uint8,count=im_size[0]*im_size[1], offset=offset)
				#image_bytes = np.fromfile(f, dtype=np.uint8,count=offset2-offset, offset=offset)
				image_bytes = np.fromfile(f, dtype=np.uint8,count=length, offset=offset)
				# print(len(image_bytes))

				nparr = np.frombuffer(bytes(image_bytes), np.uint8)
				im = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
				print(im.size)
				
				plt.clf()
				plt.imshow(im)
				# plt.show()
				plt.draw()
				plt.pause(0.01)
				# plt.waitforbuttonpress()

def read_radar_data():
	# data_dir = "C:\\Users\\Jon\\Desktop\\paddys_davimar_test\\zunanji_test_radar\\"
	data_dir = '/home/jon/Desktop/6.5.2021 davimar calibration recording/radar/'

	radar_log = sorted(glob.glob(data_dir+'RadarLOG*.txt'))
	radar_raw = sorted(glob.glob(data_dir+'RadarRAW*.txt'))
	radar_ply = sorted(glob.glob(data_dir+'*.ply'))

	display=False
	display=True

	n = 20
	x_range = n
	y_range = n
	z_range = n

	idx = 1
	lg = radar_log[idx]
	raw = radar_raw[idx]
	ply = radar_ply[idx]

	with open(lg) as f:
		lines = f.readlines()
		res = [l.split(',') for l in lines[1:-1]]
		# print(res[-1])
		res = [[int(l[0]),int(l[1]),int(l[2])-1] for l in res]
		meta = np.array(res)

	with open(raw) as f:
		lines = f.readlines()
		res = [l.split(',') for l in lines[2:]]
		res = [[float(l[0]),float(l[1]),float(l[2]),float(l[3])] for l in res]
		data = np.array(res)

	fig = plt.figure()
	if display:		

		for p in meta:
			plt.clf()
			# print(p)
			ts = p[0]*1e-6
			ln = p[1]
			offset = p[2]
			cur = data[offset:offset+ln]
			# print(ts, p[0], datetime.datetime.fromtimestamp(p[0]))
			# print(date)

			x = cur[:,0] * np.cos(cur[:,2]*np.pi/180) * np.sin(cur[:,1]*np.pi/180)
			y = cur[:,0] * np.cos(cur[:,2]*np.pi/180) * np.cos(cur[:,1] * np.pi / 180)		
			z = cur[:,0] * np.sin(cur[:,2]*np.pi/180)

			cur = np.vstack([x,y,z]).T

			date = datetime.datetime.fromtimestamp(ts)
			# print(cur)				

			ax = fig.add_subplot(111, projection='3d')

			ax.set_xlim3d(-x_range, x_range)
			ax.set_ylim3d(-y_range, y_range)
			ax.set_zlim3d(-z_range, z_range)
			plt.plot(cur[:,0],cur[:,1],cur[:,2], 'r.')
			plt.plot([0,0],[0,0],[0,0], 'k*')
			plt.title(date)
			# plt.show()
			plt.pause(0.01)
			# plt.waitforbuttonpress()

def read_lidar_data():

	# data_dir = "C:\\Users\\Jon\\Desktop\\paddys_davimar_test\\zunanji_test_lidar\\"

	# data_dir = "/home/jon/Desktop/6.5.2021 davimar calibration recording/lidar/"
	data_dir = '/home/jon/Desktop/rb_client/lidar_data/'

	files = sorted(glob.glob(data_dir+'*.ltf'))

	# for fn in files:
	idx = 0
	fn = files[idx]

	with open(fn, 'rb') as f:
		data = f.read()
	ltf_parser(data)

def main():
	# read_polar_data()
	# read_thermal_data()
	# read_radar_data()
	read_lidar_data()

if __name__=='__main__':
	main()