from socket import *
import numpy as np
import time
import cv2, os
from datetime import datetime


from read_utils import *
# from live_client import load_camera_calibration

from calibration_utils import *
from camera_lidar_calibration_helper import get_edges

def rectContains(rect,pt):
	logic = rect[0] < pt[0] < rect[0]+rect[2] and rect[1] < pt[1] < rect[1]+rect[3]
	return logic

sensor_info = {'polarization_camera': PolarizationParser, 'stereo_side_left': RGBParser, 'stereo_side_right': RGBParser, 'thermal_camera': ThermalParser, 'stereo_front_left': RGBParser, 'stereo_front_right': RGBParser}

# sensor = 'polarization_camera'
# sensor = 'thermal_camera'
# sensor = 'stereo_side_left'
# sensor = 'stereo_front_left'
sensor = 'stereo_front_right'

def create_patches(width, height, patches_x=4, patches_y=4):

	patches = []

	patch_w = width//patches_x
	patch_h = height//patches_y

	for i in range(patches_x):
		for j in range(patches_y):
			# roi = (i,j)
			x = (int(i*patch_w), int(j*patch_h), patch_w, patch_h)
			patches.append(x)

	return patches

def main():

	window_name = sensor
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

	if not 'stereo' in sensor:
		parser = sensor_info[sensor]()
	else:
		if 'front' in sensor:
			parser = sensor_info[sensor]('front')
		else:
			parser = sensor_info[sensor]('side')

	parser.start_all()

	grid = get_grid()
	patternsize = (3,5)

	# parameters
	contrast = 1.0
	brightness = 0

	contrast_dx = 0.25
	brightness_dx = 10

	# stability parameters
	pos_prev = None
	c_prev = None
	stable = False
	stability_threshold = 0.5 # difference between poses when camera and target are stationary
	stability_timeout = 5 # number of frames to determine stability
	stability_count = 0

	positions = []
	position_threshold = 5

	start_ts = datetime.datetime.now()
	start_timestamp = int(round(start_ts.timestamp()))
	out_dir = f'calibration_data/{sensor}_calibration_set_{start_timestamp}/'
	ts_prev = None

	while True:
		data, addr = parser.socket_in.recvfrom(1000)
		parser.parse_packet(data)

		if parser.image is not None and parser.image_ok:
			# print(parser.image.shape)
			ts = datetime.datetime.now()

			if ts_prev is not None:
				dt = ts-ts_prev
				fps = 1/dt.total_seconds()
				print("fps", fps)
			ts_prev = ts
			

			# parser.image = cv2.undistort(parser.image, M, D)
			im = parser.image.copy()

			if 'stereo' in sensor and 'left' in sensor:
				im = im[:,0:im.shape[1]//2]
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			elif 'stereo' in sensor and 'right' in sensor:
				im = im[:,im.shape[1]//2:]
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			elif 'thermal' in sensor:
				im = 255-im
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			else:
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			im = adjust_contrast_brightness(im, contrast = contrast, brightness=brightness)

			ts = datetime.datetime.now()
			im_raw = im.copy()

			corners = find_corners(im)
			if corners is not None:
				cv2.drawChessboardCorners(im, patternsize, corners, True)

				if c_prev is not None:
					diff = np.mean(np.abs(corners-c_prev))
					# print(diff)
					if diff>stability_threshold:
						stable=False
						stability_count = 0
					else:
						stability_count+=1
						if stability_count>stability_timeout:
							stable=True
				c_prev = corners.copy()

				# check if adding new position
				new_position = True
				if stable:					
					for c in positions:
						diff = np.mean(np.abs(c-corners))
						if diff < position_threshold:
							new_position = False

				clr = (0,0,255) if new_position else (0,255,0)
				for pt in corners:
					pt = pt[0]
					if sensor=='thermal_camera':
						cv2.circle(im, (int(pt[0]),int(pt[1])), radius=1, color=clr, thickness=-1)
					else:
						cv2.circle(im, (int(pt[0]),int(pt[1])), radius=5, color=clr, thickness=-1)


			draw_border(im, clr=(0,255,0) if stable else (0,0,255), thickness=5)

			cv2.imshow(window_name, im)

			# if corners is not None:
			# 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)
			parser.image_ok = False

			key = cv2.waitKey(1) & 0xFF

			if key == ord('q'):
				# thread_stop=True
				# R.stop_all()
				# t.join()
				break
			elif key==ord('e'):
				contrast=max(0, contrast-contrast_dx)
				# print(contrast)
			elif key==ord('r'):
				contrast=contrast+contrast_dx
				# print(contrast)
			elif key==ord('u'):
				brightness=max(-255, brightness-brightness_dx)
				# print(brightness)
			elif key==ord('i'):
				brightness=min(255, brightness+brightness_dx)
				# print(brightness)
			elif key == ord('s'):
				if corners is not None and stable:
					if new_position:

						if not positions:
							# create output dir							
							try:
								os.mkdir(out_dir)
							except:
								pass

						positions.append(corners)

						# TODO save data
						print("saving data")
						timestamp = int(round(ts.timestamp()))

						cv2.imwrite(f"{out_dir}{timestamp}.jpg", im_raw)
						np.save(f"{out_dir}{timestamp}.npy", corners)

					else:
						print("this position was aleady saved")

				else:
					print("image not stable or target not detected")

	parser.stop_all()

	return

	contrast = 1.0
	brightness = 0

	contrast_dx = 0.25
	brightness_dx = 10

	# existing_thr = 50

	# variables for stability
	x_prev = None
	stability_threshold = 0.01 # difference between poses when camera and target are stationary
	stability_timeout = 5 # number of frames to determine stability
	stability_count = 0
	stable = False

	# variables for unique position
	position_threshold = 0.1 # idk about the proper value
	position_threshold = 0.05

	draw_edges = True
	# draw_edges = False
	corners = None

	target_error_ok = True

	# calib_fn = 'calibration_polar_test.yaml'
	# calib_fn = f'/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_{sensor}_helper.yaml'
	calib_fn = f'helper_calibrations/calibration_{sensor}_helper.yaml'

	patches = None
	patches_x = 5
	patches_y = 5

	# switch this off if you want image quadrants
	calib_exist = False
	# calib_exist = True

	start_ts = datetime.datetime.now()
	start_timestamp = int(round(start_ts.timestamp()))

	if calib_exist:
		try:
			(width, height, M, D) = load_camera_calibration(calib_fn)
			# calib_exist = True

			out_dir = f'calibration_data/{sensor}_extra_{start_timestamp}/'

			try:
				os.mkdir(out_dir)
			except:
				pass
			corners_list = []

		except:
			pass

		# print("calibration", calib_exist)
	else:
		width = None
		height = None
		

	window_name = sensor
	cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

	if not 'stereo' in sensor:
		R = sensor_info[sensor]()
	else:
		if 'front' in sensor:
			R = sensor_info[sensor]('front')
		else:
			R = sensor_info[sensor]('side')

	# R = PolarizationParser()
	# R.start_sending()
	R.start_all()

	ts_prev = None
	detect_target = True
	# detect_target = False

	grid = get_grid()
	cnt = 0

	# if not calib_exist:
	# 	patches = create_patches(width, height)
	# 	corners_list = [None]*len(patches)
	# 	images_list = [None]*len(patches)
	# else:
	

	# if patches is None and calib_exist:
	# 	patches = create_patches(width, height, patches_x=patches_x, patches_y=patches_y)
	# 	corners_list = [None]*len(patches)
	# 	images_list = [None]*len(patches)
	# print(corners_list)
	add = False

	display_previous_positions = True

	ts_prev = None

	while True:
		# R.get_parameters()
		data, addr = R.socket_in.recvfrom(1000)
		R.parse_packet(data)
		# print(data)

		if R.image is not None and R.image_ok:
			# print(R.image.shape)
			ts = datetime.datetime.now()

			# R.image = cv2.undistort(R.image, M, D)
			im = R.image.copy()

			if ts_prev is not None:
				dt = ts-ts_prev
				fps = 1/dt.total_seconds()
			ts_prev = ts
			print("fps", fps)

			if 'stereo' in sensor and 'left' in sensor:
				im = im[:,0:im.shape[1]//2]
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			elif 'stereo' in sensor and 'right' in sensor:
				im = im[:,im.shape[1]//2:]
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			elif 'thermal' in sensor:
				im = 255-im
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			else:
				im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
			im = adjust_contrast_brightness(im, contrast = contrast, brightness=brightness)

			raw_im = im.copy()

			height, width, _ = im.shape

			if patches is None and not calib_exist:
				patches = create_patches(width, height, patches_x=patches_x, patches_y=patches_y)
				corners_list = [None]*len(patches)
				images_list = [None]*len(patches)

			# print(im.shape)
			if detect_target:
				corners = find_corners(im)
				# print(corners)
				if corners is not None:

					# get target position
					if calib_exist:
						# _, rvec, tvec, _ = cv2.solvePnPRansac(grid, corners, M, D, iterationsCount = n_iter, reprojectionError = repr_err)
						# _, rvec, tvec = cv2.solvePnP(grid, corners, M, D, flags=cv2.SOLVEPNP_ITERATIVE)
						_, rvec, tvec = cv2.solvePnP(grid, corners, M, D, flags=cv2.SOLVEPNP_IPPE) # coplanar points
						# _, rvec, tvec = cv2.solvePnP(grid, corners, M, D, flags=cv2.SOLVEPNP_SQPNP) 

					else:
						f = 100.0
						fake_M = np.array([[f, 0, width/2],[0, f, height/2],[0,0,1]])
						# _, rvec, tvec, _ = cv2.solvePnPRansac(grid, corners, fake_M, np.array([]), iterationsCount = n_iter, reprojectionError = repr_err)
						_, rvec, tvec = cv2.solvePnP(grid, corners, fake_M, np.array([]))

					# calculate stability
					x = np.vstack((rvec, tvec))
					if x_prev is not None:
						diff = np.mean(np.abs(x-x_prev))
						# print(diff)
						if diff>stability_threshold:
							stable=False
							stability_count = 0
						else:
							stability_count+=1
							if stability_count>stability_timeout:
								stable=True
					x_prev = x.copy()

					e = 0

					# calculate position error if previous calibration exists
					if calib_exist:
						
						pts, _ = cv2.projectPoints(grid, rvec, tvec, M, distCoeffs=D)

						edges = get_edges(im, M, corners, c=11)
						edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2RGB)
						edges = edges.astype(np.float32)

						e = np.mean(np.abs(pts-corners))

						print("target error", e)

						target_error_ok = e<err_thr

						if not target_error_ok and stable: # if error is large despite the image being stable, save extra data
							timestamp = int(round(ts.timestamp()))

							if not corners_list:
								add = True
								# corners_list.append((x,corners))
							else:

								add = True
								for x_, c_ in corners_list:
									# print(pts)
									# print(c)
									# diff = np.mean(np.abs(pts-c))
									diff = np.mean(np.abs(x-x_)) # calculate difference between saved positions and current position
									# print(diff)

									if diff < position_threshold:
										add = False
										break

							if add:
								print("saving new image")
								corners_list.append((x, corners))

								# cv2.imwrite(f"{out_dir}{timestamp}_{e:.3f}.jpg", im)
								# np.save(f"{out_dir}{timestamp}_{e:.3f}.npy", corners)

							print(len(corners_list))
							# print(corners_list)

						# for p in corners:
						# 	p = p[0]
						# 	pt = (int(p[0]), int(p[1]))

						# 	clr = (0,255,0) if e<err_thr else (0,0,255)
						# 	im = cv2.circle(im, pt, radius=5, color=clr, thickness=-1)

					else:
						
					
						cv2.drawChessboardCorners(im, patternsize, corners, True)
					
						m = np.mean(corners, axis=0)[0]
						# print(m)

						im = cv2.circle(im, (int(m[0]),int(m[1])), radius=5, color=(0,255,0), thickness=-1)

					# add new patch
					if not calib_exist:
						for patch_idx, p in enumerate(patches):
							# print(p[0],p[1])
							p1 = (p[0]+1,p[1]+1)
							p2 = (int(p1[0]+p[2]-1), int(p1[1]+p[3]-1))
							# print(p1,p2)

							if corners_list[patch_idx] is None and rectContains(p, m) and stable: # only add new corners/image if patch contains center, the image is stable and previous data does not exist
								corners_list[patch_idx] = corners
								images_list[patch_idx] = raw_im.copy()
								break

							# print(rectContains(p, m))

				# draw patches
				if not calib_exist:
					for patch_idx, p in enumerate(patches):
						# print(p[0],p[1])
						p1 = (p[0]+1,p[1]+1)
						p2 = (int(p1[0]+p[2]-1), int(p1[1]+p[3]-1))

						if corners_list[patch_idx] is None:
							clr = (0,0,255)
						else:
							clr = (0,255,0)
						im = cv2.rectangle(im, p1, p2, color=clr, thickness=1)

			# draw stability indicator			
			frame_clr = (0,255,0) if stable else (0,0,255)
			cv2.rectangle(im, (frame_margin, frame_margin), (im.shape[1]-frame_margin, im.shape[0]-frame_margin), frame_clr, frame_width)

			# draw edges
			if calib_exist and corners is not None and draw_edges:
				im = im.astype(np.float32)/255

				# color target edges in red if error with current calibration is too large and current position has not been saved
				# if target_error_ok:
				if not add:
					edges[...,0]=0
					edges[...,2]=0
				else:
					edges[...,0]=0
					edges[...,1]=0

				mask = edges
				im = im*(1-mask)+edges

			if display_previous_positions and calib_exist:
				im_ = im.copy()
				for _, c in corners_list:
					for p in c:
						p = p[0]
						pt = (int(p[0]), int(p[1]))
						clr = (1,0,0)
						im_ = cv2.circle(im_, pt, radius=5, color=clr, thickness=-1)

				alpha=0.5
				im = cv2.addWeighted(im, alpha, im_, (1-alpha), 0.0)

			if corners is not None and e:
				clr = (0,0,255) if e>err_thr else (0,255,0)
				cv2.putText(im, f"{e:.2f}", (50,70), cv2.FONT_HERSHEY_SIMPLEX, 2, clr, 3)

			cv2.imshow(window_name, im)

			# calculate fps
			# if ts_prev is not None:
			# 	dt = ts-ts_prev
			# 	delay = dt.total_seconds() * 1000
			# 	print(delay)
			# 	if delay!=0:
			# 		fps = 1000/delay
			# 		print(f"FPS: {fps}")
			# ts_prev = ts

			# if corners is not None:
			# 	im = cv2.drawChessboardCorners(im, patternsize, corners, True)
			R.image_ok = False

			key = cv2.waitKey(1) & 0xFF

			if key == ord('q'):
				# thread_stop=True
				# R.stop_all()
				# t.join()
				break
			elif key==ord('e'):
				contrast=max(0, contrast-contrast_dx)
				# print(contrast)
			elif key==ord('r'):
				contrast=contrast+contrast_dx
				# print(contrast)
			elif key==ord('u'):
				brightness=max(-255, brightness-brightness_dx)
				# print(brightness)
			elif key==ord('i'):
				brightness=min(255, brightness+brightness_dx)
				# print(brightness)

	# break
	# if not any(i is None for i in corners_list):

	# save all images and corners

	# if not calib_exist:
	# 	out_dir = f'calibration_data/{sensor}_calibration_set_{start_timestamp}/'

	# 	try:
	# 		os.mkdir(out_dir)
	# 	except:
	# 		pass

	# 	for idx, (im, c) in enumerate(zip(images_list, corners_list)):
	# 		if im is not None:
	# 			cv2.imwrite(f"{out_dir}{idx}.jpg", im)
	# 			np.save(f"{out_dir}{idx}.npy", c)


	R.stop_all()

if __name__=='__main__':
	main()