import sys
import os, errno, glob
# from matplotlib import pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
# import pptk
import numpy as np
from scipy.spatial.transform import Rotation as R
import time

# np.random.seed(1234)

from mayavi import mlab

pi = np.pi
cos = np.cos
sin = np.sin
red = (1,0,0)
green = (0,1,0)
blue = (0,0,1)
white = (1,1,1)
black = (0,0,0)

# angles in degrees
# length in centimeters

jetson_baseline = 15
stereo_baseline=33.5

sensors = [
	{'name': 'IMU', 'x': 35, 'y': 0, 'z': 70, 'roll':0, 'pitch': 0, 'yaw': 0},
	{'name': 'LIDAR', 'x': 23, 'y': 15, 'z': 70, 'roll': 0, 'pitch': 0, 'yaw': 0},
	{'name': 'jetson_left', 'x': 32, 'y': 5, 'z': 7.5, 'roll': 0, 'pitch': 0, 'yaw': 0},
	{'name': 'jetson_right', 'x': 32-jetson_baseline, 'y': 5, 'z': 7.5, 'roll': 0, 'pitch': 0, 'yaw': 0},
	{'name': 'thermal', 'x': 17, 'y': 5, 'z': 23.5, 'roll': 0, 'pitch': 0, 'yaw': 0},
	{'name': 'polar', 'x': 39, 'y': 5, 'z': 23.5, 'roll': 0, 'pitch': 0, 'yaw': 0},
	{'name': 'radar', 'x': 25, 'y': 5, 'z': 53.5, 'roll': 0, 'pitch': 0, 'yaw': 0},
	{'name': 'stereo_front_left', 'x': 43, 'y': 0, 'z': 37, 'roll': 0, 'pitch': 0, 'yaw': 0},
	{'name': 'stereo_front_right', 'x': 43-stereo_baseline, 'y': 0, 'z': 37, 'roll': 0, 'pitch': 0, 'yaw': 0},
	{'name': 'stereo_side_left', 'x': 46, 'y': 20, 'z': 37, 'roll': 0, 'pitch': 0, 'yaw': -90},
	{'name': 'stereo_side_right', 'x': 7.5, 'y': 20, 'z': 37, 'roll': 0, 'pitch': 0, 'yaw': 90},
]

class sensor():

	def __init__(self, name, x=0,y=0,z=0,roll=0,pitch=0,yaw=0):

		self.x = x
		self.y = y
		self.z = z
		self.roll = roll
		self.pitch = pitch
		self.yaw = yaw
		self.name = name

	def print_info(self):
		print(self.name)
		print("translation:", self.x, self.y, self.z)
		print("rotation:", self.roll, self.pitch, self.yaw)

	def draw_sensor_position(self, ln = 10.0, lw = 0.2, scale=1.0):

		# pts = np.array([[],[],[]])
		pt_x = [self.x+ln, self.y, self.z]
		pt_y = [self.x,self.y+ln, self.z]
		pt_z = [self.x, self.y, self.z+ln]

		r = R.from_euler('xyz', [self.pitch, self.roll, self.yaw], degrees=True)
		r = r.as_matrix()
		print(r)

		r2 = np.array([[0,0,1],[-1,0,0],[0,-1,0]])
		r = np.matmul(r2,r)

		pt = np.array([self.x,self.y,self.z])

		pt_x = np.matmul((pt_x-pt), r)+pt
		pt_y = np.matmul((pt_y-pt), r)+pt
		pt_z = np.matmul((pt_z-pt), r)+pt

		mlab.plot3d([self.x, pt_x[0]], [self.y, pt_x[1]], [self.z, pt_x[2]], color=red, tube_radius=lw)
		mlab.plot3d([self.x, pt_y[0]], [self.y, pt_y[1]], [self.z, pt_y[2]], color=green, tube_radius=lw)
		mlab.plot3d([self.x, pt_z[0]], [self.y, pt_z[1]], [self.z, pt_z[2]], color=blue, tube_radius=lw)

		mlab.text3d(self.x, self.y, self.z, self.name, color=white, scale = scale)

def draw_cs(ln = 10.0, lw = 0.2, scale=1.0):

	mlab.plot3d([0.0, ln], [0, 0], [0, 0], color=black, tube_radius=lw)
	mlab.plot3d([0.0, 0], [0, ln], [0, 0], color=black, tube_radius=lw)
	mlab.plot3d([0.0, 0], [0, 0], [0, ln], color=black, tube_radius=lw)

	# mlab.points3d(0.0,0,0,color=black, scale_factor = scale)

	mlab.text3d(0, 0, 0, "origin", color=white, scale = scale)

def draw_box(lw = 0.2):
	w = 50
	h = 80
	d = 40

	# bottom
	bottom = np.array([[0.0,w,w,0,0],[0,0,d,d,0],[0,0,0,0,0]]).T
	# print(bottom)
	mlab.plot3d(bottom[:,0], bottom[:,1], bottom[:,2], color=black, tube_radius=lw)
	mlab.plot3d(bottom[:,0], bottom[:,1], bottom[:,2]+h, color=black, tube_radius=lw)

	# print(bottom[:,0])
	# print(np.concatenate((bottom[:,0],bottom[:,0])))
	# print(np.concatenate((bottom[:,1],bottom[:,1])))
	# print(np.concatenate((bottom[:,2],bottom[:,2]+h)))

	print(bottom[0,0])
	print(bottom[0,1])

	mlab.plot3d(np.array((bottom[0,0],bottom[0,0])), np.array((bottom[0,1],bottom[0,1])), np.array((bottom[0,2],bottom[0,2]+h)), color=black, tube_radius=lw)
	mlab.plot3d(np.array((bottom[1,0],bottom[1,0])), np.array((bottom[1,1],bottom[1,1])), np.array((bottom[1,2],bottom[1,2]+h)), color=black, tube_radius=lw)
	mlab.plot3d(np.array((bottom[2,0],bottom[2,0])), np.array((bottom[2,1],bottom[2,1])), np.array((bottom[2,2],bottom[2,2]+h)), color=black, tube_radius=lw)
	mlab.plot3d(np.array((bottom[3,0],bottom[3,0])), np.array((bottom[3,1],bottom[3,1])), np.array((bottom[3,2],bottom[3,2]+h)), color=black, tube_radius=lw)

def draw_frustum():
	dist_thr = 5
	

	fovx = np.deg2rad(104)
	fovy = np.deg2rad(72)
	print(fovx,fovy)
	alpha = (pi-fovx)/2
	beta = (pi-fovy)/2

	# draw coordinate system
	l1 = mlab.plot3d([0, 0.5], [0, 0], [0, 0], color=black)
	l2 = mlab.plot3d([0, 0], [0, .5], [0, 0], color=black, line_width=5)
	l3 = mlab.plot3d([0, 0], [0, 0], [0, .5], color=black, line_width=5)
	

	mlab.plot3d([0, 5], [0, np.tan(fovx/2)*dist_thr], [0, np.tan(fovy/2)*dist_thr], color=black)
	mlab.plot3d([0, 5], [0, -np.tan(fovx/2)*dist_thr], [0, np.tan(fovy/2)*dist_thr], color=black)
	mlab.plot3d([0, 5], [0, -np.tan(fovx/2)*dist_thr], [0, -np.tan(fovy/2)*dist_thr], color=black)
	mlab.plot3d([0, 5], [0, np.tan(fovx/2)*dist_thr], [0, -np.tan(fovy/2)*dist_thr], color=black)

def main():

	sensor_list = []

	for s in sensors:
		# print(s)
		S = sensor(s['name'], s['x'],s['y'],s['z'],s['roll'],s['pitch'],s['yaw'])
		# sensor_list.append(S)

		S.print_info()
		S.draw_sensor_position()


	# return

	# IMU = sensor("IMU")

	# IMU.print_info()

	draw_cs()
	draw_box()
	# # draw_frustum()
	# IMU.draw_sensor_position()

	mlab.show()

if __name__ == "__main__":
	main()