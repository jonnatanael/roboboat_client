# set up the UDP reader for PC's IP and redirect all data to different ports

from socket import *
import sys, time, signal, os, threading
from read_utils import ThermalParser
from utils import get_local_IP

hdr = {
	'magic': [0, 8],
	'sensor_type': [8, 1],
	'sensor_subtype': [9, 1],
	'sensor_number':[10, 1],
	'direction':[11, 1],
	'payload_type':[12, 1],
	'payload': [13, -1]
}

def get_header_info(data):
	res = {}

	for k,v in hdr.items():
		o = v[0]
		ln = v[1]		
		if k =='payload':
			res[k]=data[o:o+(len(data)-13)]
		else:
			res[k]=int.from_bytes(data[o:o+ln], 'little')
	return res

stop_thread=False

def TelemetryListener(broadcast_IP='', local_IP='', port=4200):

	socket_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
	socket_in.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	socket_in.bind((broadcast_IP, port))

	socket_out = socket(AF_INET, SOCK_DGRAM)		
	socket_out.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

	port_out = 4200

	while not stop_thread:
		data, addr = socket_in.recvfrom(1000)
		hdr_info = get_header_info(data)
		# print(hdr_info)
		# if hdr_info['sensor_type']==6:
			# print(hdr_info)

		sensor_type = hdr_info['sensor_type']
		sensor_subtype = hdr_info['sensor_subtype']
		payload_type = hdr_info['payload_type']

		if sensor_subtype==5:
			socket_out.sendto(data, (local_IP, port_out))

def signal_handler(self, sig):
	global stop_thread, thread

	stop_thread=True
	thread.join()
	sys.exit()

def main():

	global stop_thread, thread

	a_signal = signal.signal(signal.SIGINT, signal_handler)

	port = 4200
	local_IP = get_local_IP()
	# local_IP = '192.168.90.255'
	# local_IP = '192.168.91.255' # 5.7.24
	# local_IP = '127.0.0.1'
	broadcast_IP = '192.168.91.255'
	# broadcast_IP = '255.255.255.255'

	socket_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
	socket_in.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	socket_in.bind((local_IP, port))
	# socket_in.bind((broadcast_IP, port))

	socket_out = socket(AF_INET, SOCK_DGRAM)		
	socket_out.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

	thread = threading.Thread(target=TelemetryListener, kwargs={'broadcast_IP':broadcast_IP, 'local_IP':local_IP})
	thread.start()

	# thread_gps = threading.Thread(target=TelemetryListener, kwargs={'broadcast_IP':'192.168.91.255', 'local_IP':local_IP})
	# thread_gps.start()

	while True:
		
		data, addr = socket_in.recvfrom(1000)
		hdr_info = get_header_info(data)
		# print(hdr_info)

		sensor_type = hdr_info['sensor_type']
		sensor_subtype = hdr_info['sensor_subtype']
		payload_type = hdr_info['payload_type']
		sensor_number = hdr_info['sensor_number']

		# print(sensor_type, sensor_subtype, payload_type, sensor_number)

		if sensor_subtype==1:
			# print("data")
			# port_out = int(f"27{sensor_subtype}{sensor_type}{sensor_number}")
			# socket_out.sendto(data, (local_IP, port_out))
			pass
		elif sensor_subtype==4:
			port_out = int(f"27500")
			# socket_out.sendto(data, (local_IP, port_out))
			# socket_out.sendto(data, (local_IP, port_out))
			socket_out.sendto(data, (local_IP, port))
		elif sensor_subtype==5:
			pass
			# port_out = int(27500)
			# socket_out.sendto(data, (local_IP, port_out))
			# socket_out.sendto(data, (broadcast_IP, port_out))
		else:
			pass
			# port_out = int(f"27{sensor_subtype}{sensor_type}{sensor_number}")
			# socket_out.sendto(data, (local_IP, port_out))

		# socket_out.sendto(data, (local_IP, port_out))

if __name__ == '__main__':
	main()