import numpy as np
import socket
import cv2, struct, io, os, sys, threading, time
from PIL import Image
import subprocess
from datetime import datetime
from matplotlib import pyplot as plt
from numpy.linalg import norm


from calibration_utils import *
from utils import get_local_IP
from read_utils import LidarParser
# from camera_lidar_calibration_helper import get_edges

from calibration_utils import *


from multiprocessing import Process, Queue
from lidar_stream_shm import lidar_reader, create_shm_lidar

from calibrate_lidar_camera import calibrate

# for image stream worker
I = None
thread_stop = False

def is_collinear(a,b,c, thr = 0.01):

	

	# x1 = a[0]; y1 = a[1]
	# x2 = b[0]; y2 = b[1]
	# x3 = c[0]; y3 = c[1]

	# print(x1,y1,x2,y2,x3, y3)
	# print((y2-y3), (y3-y1), (y1-y2))

	# a = 1/2*(x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))

	# print(a)

	AB = a-b
	AC = a-c

	a = 0.5*np.sqrt(norm(AB)**2*norm(AC)**2-(np.dot(AB,AC))**2)
	# print(a)

	return a<thr

	# a = b-a
	# b = c-a

	# x = np.cross(a,b)

	# print(norm(x), np.sum(np.abs(x))<thr)

	# return np.sum(np.abs(x))<thr

def fit_lines(pc):

	lines = []

	len_thr = 20

	pc = pc[pc[:,1]>0,:] # remove points behind

	idx = pc[:,:3].any(axis=1) # remove points at 0
	pc = pc[idx,:]


	# lines.append((0, pc[:,0], pc[:,1]))

	for beam_idx in range(16):

		beam = pc[pc[:,-1]==beam_idx,:] # extract beam
		beam = beam[:,:3]
		beam_ = beam.copy()

		p0 = beam[0,:]
		p1 = beam[1,:]
		# print(p0, p1)

		start = p0
		end = p1
		ref = p1

		# print(beam)

		cur_len = 0

		for pt in beam[2:,:]:

			ok = is_collinear(start, end, pt, thr=0.01)

			# if norm(pt-end)>0.5:
			# 	ok = False

			if ok:
				cur_len+=1
				end = pt
			else:
				if cur_len>=len_thr:
					lines.append((beam_idx, start, end))
				start = end
				end = pt
				ref = pt
				cur_len=0

	return lines

def process_lidar(pc, thr=0.05, n_beams=16, line_length=500):

	beams = pc[:,-1]

	res = np.zeros_like(beams)

	for n in range(n_beams):
		idx = beams==n

		p = pc[idx, :]
		dst = np.zeros_like(p[:,0])

		for i, x in enumerate(p[:-1]):
			x_ = p[i+1]
			dt = np.sqrt((x[0]-x_[0])**2+(x[1]-x_[1])**2+(x[2]-x_[2])**2)

			if np.abs(dt)>thr:

				dst[i]=1
				dst[i+1]=1

		if dst.shape[0]!=0:
			dst[0]=1
			dst[-1]=1

		res[idx]=dst

	edges = pc[res==1, :]
	# res = res[res==1]

	# print(edges.shape)

	out = np.zeros_like(beams)

	for n in range(n_beams):

		beam_idx = beams==n
		beam = pc[idx, :] 

		# edges_id = 
		beam_edges_idx = (beams==n) & (res==1)
		# print(len(beam), np.sum(res[beam_edges_idx]))
		# print(beam_edges.shape)

		indices = np.argwhere(beam_edges_idx)
		# print(indices)

		for i,x in enumerate(indices[:-1]):

			pt1 = pc[x]
			pt2 = pc[indices[i+1]]
			# print(x, indices[i+1], pt1,pt2)
			# idx2 = indices[i+1]

			start_idx = x[0]
			end_idx = indices[i+1][0]

			


			potential_line = pc[start_idx:end_idx,:]

			# print(x, indices[i+1])
			# print(potential_line.shape)

			if potential_line.shape[0]>line_length:

				# print(np.var(potential_line))

				out[start_idx]=1
				out[end_idx]=1

			continue


			s = 0

			if len(potential_line)<5:
				continue

			for pt in potential_line:
				ok = is_collinear(pc[start_idx,:], pt, pc[end_idx,:])
				if ok:
					s+=1

			perc = (s/len(potential_line))*100

			if perc>80:
				out[start_idx]=1
				out[end_idx]=1

			# print(f"line percent: {perc}")
			# print(f"line length: {len(potential_line)}")



		# TODO iterate through raw points, check if line exists between edges
		# for i,x in enumerate(res[beam_edges_idx]):
		# 	print(i,x)

	final = pc[out==1, :]

	return final
	# return edges

def worker(camera, shutter, framerate):
	global thread_stop, I

	local_IP = get_local_IP()
	remote_IP = '192.168.90.74'

	server_socket = socket.socket()
	server_socket.bind(('192.168.90.14', 1200))
	server_socket.listen(0)

	# start raspivid on RPi
	command = f"python camera_stream.py {camera} {local_IP} {shutter} {framerate}"
	subprocess.call(['ssh', '-t', '-f', "pi@"+remote_IP, "nohup", command], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	connection = server_socket.accept()[0].makefile('rb')

	while True:
		x = connection.read(struct.calcsize('<L'))
		image_len = struct.unpack('<L', x)[0]
		if not image_len:
			print("fail")
			# break
		
		image_stream = io.BytesIO()
		image_stream.write(connection.read(image_len))
		image_stream.seek(0)
		image = Image.open(image_stream)

		data = np.fromstring(image_stream.getvalue(), dtype=np.uint8)
		I = cv2.imdecode(data, 1)

		if thread_stop:
			break

	connection.close()
	server_socket.close()

def main():

	global thread_stop, I

	if len(sys.argv)>1:
		camera_side = sys.argv[1]
	else:
		camera_side = 'left'

	if camera_side=='left':
		camera = 0
		shutter = 6000000
		framerate = 5
		sensor = 'stereo_front_left'
	elif camera_side=='right':
		camera = 1
		# shutter = 5000
		# shutter = 2000 # seems to work best

		# for testing
		framerate = 10
		shutter = 6000000
		framerate = 5
		sensor = 'stereo_front_right'

	# multiprocess solution
	lidar_height = 60000
	shr_lidar, pc = create_shm_lidar(lidar_height)
	p1 = Process(target=lidar_reader, args=(shr_lidar.name,))
	p1.start()

	# image reader worker
	t = threading.Thread(target=worker, args=(camera, shutter, framerate))
	t.start()

	# load lidar calibration
	calib_fn = f'helper_calibrations/calibration_{sensor}_helper.yaml'

	(width, height, M, D) = load_camera_calibration(calib_fn)
	R_lidar, T_lidar, C_lidar = load_lidar_calibration(f"/home/jon/Desktop/davimar_calibration/helper_calibrations/calibration_lidar_{sensor}.yaml")

	# save timestamp and set up out dir
	start_ts = datetime.now()
	start_timestamp = int(round(start_ts.timestamp()))

	out_dir = f'calibration_data/lidar_calibration_data_{sensor}_{start_timestamp}/'

	# display parameters
	rnge = 10
	radius = 5
	radius = 1
	edge_constant = 35

	# detection and saving parameters	
	patternsize = (3,5)
	display_lidar = False
	display_lidar = True
	detect_target = True
	display_corners = True
	display_edges = True
	calculate_lidar_edges = False	
	undistort = False
	undistort = True

	# window and trackbars
	cv2.namedWindow(sensor, cv2.WINDOW_NORMAL)
	cv2.createTrackbar('lidar', sensor, 0, 1, lambda *args: None)	
	cv2.createTrackbar('corners', sensor, 0, 1, lambda *args: None)
	cv2.createTrackbar('edges', sensor, 0, 1, lambda *args: None)
	# cv2.createTrackbar('lidar edges', sensor, 0, 1, lambda *args: None)
	cv2.createTrackbar('undistort', sensor, 0, 1, lambda *args: None)

	cv2.setTrackbarPos('lidar', sensor, 1 if display_lidar else 0)
	cv2.setTrackbarPos('corners', sensor, 1 if display_corners else 0)
	cv2.setTrackbarPos('edges', sensor, 1 if display_edges else 0)
	cv2.setTrackbarPos('undistort', sensor, 1 if undistort else 0)

	# setup data storage
	positions = []
	position_threshold = 5

	# stability
	corners_thr = 1.0
	stability_count = 0
	stability_threshold = 10
	stability_threshold = 1
	stable = False
	corners_prev = None

	# # for calibration process
	# q = Queue()
	# calibration_running = False

	# try:
	while True:

		# check queue
		try:
			(R_lidar, T_lidar) = q.get_nowait()
			calibration_running = False
		except:
			pass

		# print(T_lidar)

		# query trackbars
		display_lidar = True if cv2.getTrackbarPos('lidar', sensor)==1 else False
		display_corners = True if cv2.getTrackbarPos('corners', sensor)==1 else False
		display_edges = True if cv2.getTrackbarPos('edges', sensor)==1 else False
		undistort = True if cv2.getTrackbarPos('undistort', sensor)==1 else False

		# cur = datetime.now()

		if I is None:
			continue

		ts = datetime.now()
		ts = int(round(ts.timestamp()))

		im = I.copy()
		im_raw = im.copy()

		print(im.shape)
		# print(T)

		if undistort:
			im = cv2.undistort(im, M, D)

		# timing = (datetime.now()-cur).total_seconds()*1000; cur = datetime.now()
		# print(f"image decoding took {timing} ms")

		if detect_target:

			corners = find_corners(im)
			edges = None

			if corners is None:
				corners = find_corners(255-im)

			if corners is not None:
				if display_corners:
					cv2.drawChessboardCorners(im, patternsize, corners, True)

				edges = get_edges(im, M, corners, c=edge_constant)
				edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2RGB)
				edges = edges.astype(np.float32)

				# check stability
				if corners_prev is not None:
					# print(stability_count)
					diff = np.mean(np.abs(corners-corners_prev))
					if diff>corners_thr:
						stability_count = 0
						stable = False
					else:
						stability_count+=1
						if stability_count>stability_threshold:
							stable=True
				corners_prev = corners

				# check if new position
				new_position = True
				for c in positions:
					diff = np.mean(np.abs(c-corners))
					if diff < position_threshold:
						new_position = False

				new_position = False # don't save anything


		# timing = (datetime.now()-cur).total_seconds()*1000; cur = datetime.now()
		# print(f"target detection took {timing} ms")

		# print("lidar drawing start")

		if pc is not None:
		
			if display_lidar:

				ldr = pc.copy()
				ldr[:,:3] = (C_lidar @ ldr[:,:3].T).T
				ldr = ldr[~np.all(ldr[:,0:3]==0, axis=1),:] # remove zero points
				ldr = ldr[(ldr[:,2]>0),:]

				# print(ldr.shape)
				# print(ldr[:10, :])

				# print(pc.shape)
				# continue
				
				if undistort:
				
					pts, colors, _ = project_lidar_points(ldr, im.shape, R_lidar, T_lidar, M, np.array([]), rn=rnge)
				else:
					pts, colors, _ = project_lidar_points(ldr, im.shape, R_lidar, T_lidar, M, D, rn=rnge)

				if pts is not None and pts.shape[1]!=0:
					for pt, color in zip(pts,colors):
						# print(pt)
						# c = tuple([int(x*255) for x in color][::-1])
						# c = tuple([int(x*255) for x in color])
						c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)][::-1])
						try:
							cv2.circle(im, (int(pt[0]),int(pt[1])), radius=radius, color=c, thickness=-1)
						except:
							pass

			if calculate_lidar_edges:

				ldr_raw = pc.copy()
				ldr_raw[:,:3] = (C_lidar @ ldr_raw[:,:3].T).T
				ldr = ldr[~np.all(ldr[:,0:3]==0, axis=1),:] # remove zero points

				dist_thr = 0.2
				lidar_edges = process_lidar(ldr, thr=dist_thr, line_length=500)

				pts, colors, _ = project_lidar_points(lidar_edges, im.shape, R_lidar, T_lidar, M, np.array([]), rn=rnge)

				if pts is not None and pts.shape[1]!=0:
					for pt, color in zip(pts,colors):
						# c = tuple([int(color[2]*255), int(color[1]*255), int(color[0]*255)][::-1])
						
						try:
							cv2.circle(im, (int(pt[0]),int(pt[1])), radius=radius, color=(0,0,255), thickness=-1)
						except:
							pass

		# timing = (datetime.now()-cur).total_seconds()*1000; cur = datetime.now()
		# print(f"lidar draw took {timing} ms")

		# print("lidar drawing end")


		if edges is not None:
			if display_edges:
				if not new_position:
					edges[...,0]=0
					edges[...,2]=0
				else:
					edges[...,0]=0
					edges[...,1]=0
				im = (im/255).astype(float)
				im = im*(1-edges)+edges

		# draw stability indicator
		draw_border(im, clr=(0,255,0) if stable else (0,0,255), thickness=5)

		cv2.imshow(sensor, im)

		key = cv2.waitKey(1) & 0xFF
		if key == ord('q'):

			break

		elif key == ord('c'):
			continue
			print(calibration_running)
			if not calibration_running:
				if corners is not None:
					print("running calibration")

					edges_list = [edges]
					lidar_edges_list = [lidar_edges]
					p2 = Process(target=calibrate, args=(q, edges_list, lidar_edges_list, M, R_lidar, T_lidar))
					p2.start()
					calibration_running = True

		elif key == ord('s'):
			# save image, corners and scan
			# iff target was detected, target position is new and target position is stable
			if corners is not None and stable:	
			
				if new_position:
					ldr_raw = pc.copy()
					positions.append(corners)

					print("saving data")

					try:
						os.mkdir(out_dir)
					except:
						pass

					np.save(f"{out_dir}{ts}.npy", {'pc': ldr_raw, 'corners': corners})
					cv2.imwrite(f"{out_dir}{ts}.png", im_raw)

				else:
					print("this position was aleady saved")

			else:
				print("image not stable or target not detected")

	thread_stop = True
	t.join()
	p1.terminate()
	p1.join()
	shr_lidar.close()
	shr_lidar.unlink()
	cv2.destroyAllWindows() #cleanup windows 

if __name__=='__main__':
	main()
